//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#ifndef InfoLocate_Bridging_Header_h
#define InfoLocate_Bridging_Header_h

#import "CustomObject.h"
//#import "Masonry.h"
//#import "iToast.h"
//#import "TTXTalkback.h"
//#import "TTXPlaybackSearch.h"
//#import "TTXPlaybackView.h"
//#import "TTXPlaybackVideoController.h"
//#import "AFNetworking.h"
//#import "PlaybackViewController.h"
//#import "VehicleInfoModel.h"
//#import "EquipmentInfoModel.h"
//#import "DeviceSearchView.h"
//#import "Singleton.h"
//#import "TTXAccountManager.h"

//#import "TTXSDKPrepare.h"
//#import "TTXRealVideoView.h"

#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>

#define MainScreenWidth [UIScreen mainScreen].bounds.size.width
#define MainScreenHeight [UIScreen mainScreen].bounds.size.height

#endif /* InfoLocate_Bridging_Header_h */
