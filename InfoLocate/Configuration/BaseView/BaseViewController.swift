
import UIKit
//import RSLoadingView
import SideMenu
//import Alamofire
import Foundation
import MessageUI

class BaseViewController: UIViewController, UITextFieldDelegate, MFMailComposeViewControllerDelegate {
    let defaults = UserDefaults.standard
    var status: String = ""
    var message: String = ""
//    var loadingView : RSLoadingView!
    
//    let uid = "\(Defaults.getValueForKey(key: UserDetailsServerMap.userId))"
//    let name =  "\(Defaults.getValueForKey(key: UserDetailsServerMap.firstName)) \(Defaults.getValueForKey(key: UserDetailsServerMap.middleName)) \(Defaults.getValueForKey(key: UserDetailsServerMap.lastName))"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func showLoading() {
//        loadingView = RSLoadingView(effectType: RSLoadingView.Effect.twins)
//        loadingView.showOnKeyWindow()
    }
    
    func hideLoading() {
//        RSLoadingView.hideFromKeyWindow()
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["rsgtl4@gmail.com"])
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)

            self.present(mail, animated: true)
        } else {
            // show failure alert
            print("Can't send mail")
        }
    }
    
    func call() {
        print("calling")
        let phoneNumber = "8147365099"
        if let phoneURL = URL(string: ("tel:" + phoneNumber)) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(phoneURL)
            }
        }
    }
    
    func shadowView_GrayColor(view: UIView, radius: CGFloat) {
        view.layer.shadowColor = Color.blackColor.cgColor
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 3.0
        view.layer.shadowOpacity = 0.4
        view.layer.cornerRadius = radius
        //        view.layer.masksToBounds = true
    }
    
    func showAlert(_ title: String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertForASec(_ title: String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)
        let delay = 0.5 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            alertController.dismiss(animated: true, completion: nil)
        })
    }
    
    
    
    
    /** This Method will help to Setup the menu width, Color and Presentation Appearance for that particular viewcontroller.*/
    func openSideMenu() {
        let menuModuleBuilder = MenuModuleBuilder()
        let menuViewController = menuModuleBuilder.build()
        let menu = SideMenuNavigationController(rootViewController: menuViewController)
        menu.settings = makeSettings()
        menu.leftSide = true
        menu.alwaysAnimate = true
        menu.usingSpringWithDamping = 1
        menu.setNavigationBarHidden(true, animated: true)
        present(menu, animated: true, completion: nil)
    }
    
    private func makeSettings() -> SideMenuSettings {
        let presentationStyle : SideMenuPresentationStyle = .menuSlideIn
        presentationStyle.menuStartAlpha = CGFloat(10)
        presentationStyle.onTopShadowOpacity = 1
        
        var settings = SideMenuSettings()
        settings.presentationStyle = presentationStyle
        settings.menuWidth = UIScreen.main.bounds.width - 100
        settings.blurEffectStyle = .regular
        settings.statusBarEndAlpha = 0
        return settings
    }
    
   
    override func touchesBegan(_ touches: Set<UITouch>, with event:UIEvent?) {
        view.endEditing(true)
    }
}

extension UISearchBar {
    var textField: UITextField? {
        if #available(iOS 13.0, *) {
            return self.searchTextField
        } else {
            // Fallback on earlier versions
            for view in (self.subviews[0]).subviews {
                if let textField = view as? UITextField {
                    return textField
                }
            }
        }
        return nil
    }
}


