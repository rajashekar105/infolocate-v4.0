////  NetService.swift
////  Mobi
////  Copyright © 2017 INWITZ. All rights reserved.
//
//import Foundation
//import Alamofire
//
//class NetService : BaseViewController  {
//
//    var jSON = NSDictionary()
//
//    func hitPostAPIWith(url : String, parameters: [String: Any], completion: @escaping (NSDictionary?) -> Void) {
//        print(parameters)
//        Alamofire.request(
//            URL(string: url)!,
//            method: .post,
//            parameters: parameters,
//            encoding: JSONEncoding.default,
//            headers:["Content-Type":"application/json"])
//            .validate()
//            .responseJSON { (response) -> Void in
//                debugPrint(response)
//                guard response.result.isSuccess else {
//                    print("Error while authenticating: \(String(describing: response.result.error))")
//                    completion(["result":"Fail"])
//                    return
//                }
//                if let result = response.result.value {
//                    self.jSON = (result as? NSDictionary)!
//                    completion(self.jSON)
//                } else {
//                    completion(["result":"Fail"])
//                }
//        }
//    }
//
//    func hitGetAPIWith( url : String, completion: @escaping (NSDictionary?) -> Void) {
//        print(url)
//        Alamofire.request(
//            URL(string: url)!,
//            method: .get,
//            encoding: JSONEncoding.default,
//            headers:["Content-Type":"application/json"])
//            .validate()
//            //            .validate(statusCode: 200..<300)
//            .responseJSON { (response) -> Void in
//                guard response.result.isSuccess else {
//                    print("Error while authenticating: \(String(describing: response.result.error))")
//                    completion(["result":"Fail"] )
//                    return
//                }
//                if let result = response.result.value {
//                    self.jSON = (result as? NSDictionary)!
//                    completion(self.jSON)
//                } else {
//                    completion(["result":"Fail"])
//                }
//        }
//    }
//
//    func hitPutAPIWith(url : String, parameters: [String: Any], completion: @escaping (NSDictionary?) -> Void) {
//        Alamofire.request(
//            URL(string: url)!,
//            method: .put,
//            parameters: parameters,
//            encoding: JSONEncoding.default,
//            headers:["Content-Type":"application/json"])
//            .validate()
//            .responseJSON { (response) -> Void in
//                guard response.result.isSuccess else {
//                    print("Error while authenticating: \(String(describing: response.result.error))")
//                    completion(["result":"Fail"])
//                    return
//                }
//                if let result = response.result.value {
//                    self.jSON = (result as? NSDictionary)!
//                    completion(self.jSON)
//                } else {
//                    completion(["result":"Fail"])
//                }
//        }
//    }
//
//    func hitDeleteAPIWith(url : String, parameters: [String: Any], completion: @escaping (NSDictionary?) -> Void) {
//        Alamofire.request(
//            URL(string: url)!,
//            method: .delete,
//            parameters: parameters,
//            encoding: JSONEncoding.default,
//            headers:["Content-Type":"application/json"])
//            .validate()
//            .responseJSON { (response) -> Void in
//                guard response.result.isSuccess else {
//                    completion(["result":"Fail"])
//                    return
//                }
//                if let result = response.result.value {
//                    self.jSON = (result as? NSDictionary)!
//                    completion(self.jSON)
//                } else {
//                    completion(["result":"Fail"])
//                }
//        }
//    }
//
//
//    func hitGetUserProfilePicAPIWith( url : String, completion: @escaping (String?) -> Void) {
//        print(url)
//        Alamofire.request(
//            URL(string: url)!,
//            method: .get,
//            encoding: JSONEncoding.default,
//            headers:["Content-Type":"application/json"])
//            .validate()
//            //            .validate(statusCode: 200..<300)
//            .responseJSON { (response) -> Void in
//                guard response.result.isSuccess else {
//                    print("Error while authenticating: \(String(describing: response.result.error))")
//                    completion("Fail")
//                    return
//                }
//                if let result = response.result.value {
//                    //                         print(result)
//                    //                        print(self.jSON["PhotoBlob"] as Any)
//                    self.jSON = (result as? NSDictionary)!
//                    let value = self.jSON["PhotoBlob"] as Any
//                    if value as? String == nil {
//                        completion("")
//                    } else  {
//                        let imageString = self.jSON["PhotoBlob"] as! String
//                        completion(imageString)
//                    }
//                } else {
//                    completion("Fail")
//                }
//        }
//    }
//
//    func uploadImage(url : String, imageData : Data, completion: @escaping (NSArray?) -> Void) {
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            multipartFormData.append(imageData, withName: "",fileName: "userPic.jpg", mimeType: "image/jpg")
//        },
//                         to:url)
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.uploadProgress(closure: { (progress) in
//                    print("Upload Progress: \(progress.fractionCompleted)")
//                })
//
//                upload.responseJSON { response in
//                    print(response.result.value!)
//                    completion(response.result.value as? NSArray)
//                }
//
//            case .failure(let encodingError):
//                print(encodingError)
//            }
//        }
//    }
//
//    func hitRequestForAccessTokenRequest(url : String, parameters: [String: Any], completion: @escaping (NSDictionary?) -> Void) {
//        let str = Constants.fitBitClientID + ":" + Constants.fitBitClientSecret
//        let base64EncodedString = str.toBase64()
//        print(base64EncodedString)
//        Alamofire.request(
//            URL(string: url)!,
//            method: .post,
//            parameters: parameters,
//            //            encoding: f.default,
//            headers:["Authorization":"Basic " + base64EncodedString, "Content-Type":"application/x-www-form-urlencoded"])
//            .validate()
//            .responseJSON { (response) -> Void in
//                debugPrint(response)
//                guard response.result.isSuccess else {
//                    print("Error while authenticating: \(String(describing: response.result.error))")
//                    completion(["result":"Fail"])
//                    return
//                }
//                if let result = response.result.value {
//                    self.jSON = (result as? NSDictionary)!
//                    completion(self.jSON)
//                } else {
//                    completion(["result":"Fail"])
//                }
//        }
//    }
//
//
//
//    func hitGetFitBitProfileRequest( url : String, access_token: String, completion: @escaping (NSDictionary?) -> Void) {
//        print(url)
//        Alamofire.request(
//            URL(string: url)!,
//            method: .get,
//            encoding: JSONEncoding.default,
//            headers:["Authorization":"Bearer \(access_token)"])
//            .validate()
//            //            .validate(statusCode: 200..<300)
//            .responseJSON { (response) -> Void in
//                guard response.result.isSuccess else {
//                    print("Error while authenticating: \(String(describing: response.result.error))")
//                    completion(["result":"Fail"] )
//                    return
//                }
//                if let result = response.result.value {
//                    self.jSON = (result as? NSDictionary)!
//                    completion(self.jSON)
//                } else {
//                    completion(["result":"Fail"])
//                }
//        }
//    }
//
//
//    func hitGetFitBitActivity( url : String, token: String, completion: @escaping (NSDictionary?) -> Void) {
//        print(url)
//        Alamofire.request(
//            URL(string: url)!,
//            method: .get,
//            //            encoding: JSONEncoding.default,
//            headers:["Authorization":"Bearer " + token, "Content-Type":"application/x-www-form-urlencoded"])
//            .validate()
//            //            .validate(statusCode: 200..<300)
//            .responseJSON { (response) -> Void in
//                guard response.result.isSuccess else {
//                    print("Error while authenticating: \(String(describing: response.result.error))")
//                    completion(["result":"Fail"] )
//                    return
//                }
//                if let result = response.result.value {
//                    self.jSON = (result as? NSDictionary)!
//                    completion(self.jSON)
//                } else {
//                    completion(["result":"Fail"])
//                }
//        }
//    }
//}
