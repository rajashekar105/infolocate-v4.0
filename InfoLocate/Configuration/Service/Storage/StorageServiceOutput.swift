import Foundation

protocol StorageServiceOutput: class {
  
    func loggedInSaved()
    func notLoggedInSaved()
}
