

import Foundation

class ServerConfiguration {
    
    static let serverType : SERVER_TYPE = .PRO
    ///API List
    static let DEVELOPMENT_BASE_URL = "http://infolocatefms.infotracktelematics.com/ItlService.svc/"
    //    static let QA_BASE_URL = "http://staging.infotracktelematics.com/InfoLocateClientAuthStaging/Itlservice.svc/"
    static let QA_BASE_URL = "http://staging.infotracktelematics.com/InfoLocateClientAuthWebApi/api/service/"  // Client Staging  qat/welcome
//    static let QA_BASE_URL = "http://earthsupportpoc.infotracktelematics.com/ilouserapitest/ApiServices/" // 03-05-2021 testing alert video
//    static let QA_BASE_URL = "http://staging.infotracktelematics.com/InfoLocateClientAuthWebApiRESTIOS/api/service/" // 22-3-2021
    static let UAT_BASE_URL = "http://staging.infotracktelematics.com/commonurl/ItlService.svc/"
//    static let PRO_BASE_URL = "http://itlfmsv12.infotracktelematics.com/casv1/ItlService.svc/"
    static let PRO_BASE_URL = "https://mappapi.infotracktelematics.com/InfoLocateClientAuth/api/service/" // 25-3-2021
   
    //http://staging.infotracktelematics.com/InfoLocateClientAuthStaging/Itlservice.svc/
    // Post API = http://staging.infotracktelematics.com/InfoLocateClientAuthWebApi/api/service/URLAuthorization
    // encenabled API = http://staging.infotracktelematics.com/InfoLocateClientAuthWebApiRESTIOS/api/service
    
    func getBaseURL() -> String {
        switch ServerConfiguration.serverType {
        case .DEV:
            return ServerConfiguration.DEVELOPMENT_BASE_URL
        case .QA:
            return ServerConfiguration.QA_BASE_URL
        case .UAT:
            return ServerConfiguration.UAT_BASE_URL
        case .PRO:
            return ServerConfiguration.PRO_BASE_URL
        }
    }
    
    // Post API = http://staging.infotracktelematics.com/InfoLocateClientAuthWebApi/api/service/URLAuthorization
    static var CLIENT_LOGIN_API = ServerConfiguration().getBaseURL() + "URLAuthorization"
    // Check for update API = http://staging.infotracktelematics.com/InfoLocateClientAuthWebApi/api/service/CheckAppUpdate
    static var CHECK_APP_UPDATE = ServerConfiguration().getBaseURL() + "CheckAppUpdate"
    
    static var APP_UPDATE_URL_LINK = "http://apps.apple.com/in/app/infolocate-v3-0/id1515221265"


//    func getUserBaseUrl() -> String {
//        var baseUrl = ""
//        if let url = UserDefaults.standard.value(forKey: "BaseURL") {
//            baseUrl = url as! String
//        }
//        print(baseUrl)
//        return baseUrl
//    }
    
//    static var USER_LOGIN_API = ServerConfiguration().getUserBaseUrl() + "Login"
//    static var USER_LOGOUT_API = ServerConfiguration().getBaseURL() + "LogOut"
//    static var UPDATE_DEVICE_TOKEN_V2_API = ServerConfiguration().getUserBaseUrl() + "UpdateDeviceTokenV2"
//    static var DASHBOARD_API = ServerConfiguration().getUserBaseUrl() + "DashBoard"
}
