

import Foundation

//MARK: User Defaults
/*! User Defaults */
class Defaults {
    /**
     This function is used to set a login_value for key in userdefaults.
     */
    static func setValueForLogin(value : Bool) {
        UserDefaults.standard.set(value, forKey: "LoggedIn")
    }
    /**
     This function is used to get a login_value from userdefaults.
     */
    static func getValueForLogin() -> Bool {
        let value = UserDefaults.standard.bool(forKey: "LoggedIn")
        return value
    }
    
    static func setValueForUserPic(value : Bool) {
        UserDefaults.standard.set(value, forKey: "UserPic")
    }
    static func getValueForUserPic() -> Bool {
           let value = UserDefaults.standard.bool(forKey: "UserPic")
           return value
       }
    /**
    This function is used to set value for key from userdefaults.
    */
    static func setValueforKey(value : Any, for key : String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    /**
    This function is used to get value for key from userdefaults.
    */
    static func getValueForKey(key : String) -> Any {
        let value = UserDefaults.standard.value(forKey: key)
        if value == nil {
            return ""
        } else {
            return value!
        }
    }
    
}

class UD_Keys {
    static let clientLoginStatus = "clientLogin"
    static let initialLogin = "initialLogin"
    static let clientId = "clientId"
    static let clientName = "clientName"
    static let clientPassword = "clientPassword"
//    static let baseUrl = "BaseURL"
    static let companyLogoUrl = "companyLogoUrl"
    
    static let userLoginStatus = "userLoginStatus"
    static let userId = "userId"
    static let userName = "userName"
    static let phoneNo = "phoneNo"
    static let desgination = "desgination"
    static let userResponse = "userResponse"
    
    static let vehicleList = "vehicleList"
    static let isToday = "isToday"
    static let isAccident = "isAccident"
}
