

import Foundation

enum SERVER_TYPE {
    case DEV
    case QA
    case UAT
    case PRO
}

enum COMING_TYPE {
    case RECENT_ALERTS
    case VEHICLE_LIST
    case DYNAMIC
}


