//
//  Validation.swift
//  InfoLocateFMS
//
//  Created by infotrack telematics on 29/06/16.
//  Copyright © 2016 InfotrackTelematics. All rights reserved.
//
import UIKit
import Foundation

class Validation: UIViewController{
    
    var isEntriesValid = true
    
    
    func checkEmpty(_ data: UITextField) -> Bool {
        
        isEntriesValid = true
        
        if data.text == ""
        {
            isEntriesValid = false
        }
        else
        {
            isEntriesValid = true
        }
        return isEntriesValid
    }
    
    
    
    func checkMinLength(_ textField: UITextField, iLength: Int) -> Bool {
        
        isEntriesValid = true
        
        let iInt = textField.text!.count
        
        if( iInt < iLength)
        {
            isEntriesValid = false
        }
        else
        {
            isEntriesValid = true
        }
        return isEntriesValid
    }

    
    
    func checkMaxLength(_ textField: UITextField, iLength: Int) -> Bool {
       
        isEntriesValid = true
        
        let iInt = textField.text!.count
        
        if( iInt > iLength)
        {
            isEntriesValid = false
        }
        else
        {
            isEntriesValid = true
        }
        return isEntriesValid
    }
    
//    "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,10}"

    func validpassword(mypassword : String) -> Bool {
        let passwordreg =  "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{10,15}"
        let passwordtesting = NSPredicate(format: "SELF MATCHES %@", passwordreg)
        return passwordtesting.evaluate(with: mypassword)
    }
    
    func isValidPassword() -> Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,10}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
  
    
}
