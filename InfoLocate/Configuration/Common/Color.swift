
import Foundation
import UIKit

class Color {
    static let blackColor = UIColor.black
    static let MAIN_BACKGROUND_COLOR = UIColor.init(red: 244/255, green: 247/255, blue: 252/255, alpha: 1.0) // hex = F4F7FC
    static let purpleColor = UIColor.init(red: 101/255, green: 49/255, blue: 196/255, alpha: 1.0) // hex = 6531C4
//    static let purpleColor = UIColor.init(red: 154/255, green: 95/255, blue: 248/255, alpha: 1.0) // hex = 9a5ff8
    static let greenColor = UIColor.init(red: 66/255, green: 165/255, blue: 65/255, alpha: 1.0) // hex = 42A541
    static let VIEW_SHADOW_CGCOLOR = UIColor(red: 192/255.0, green: 192/255.0, blue: 192/255.0, alpha: 1.0).cgColor
    static let TABBAR_SELECTION_COLOR = blackColor //UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
    static let DASHBOARD_MOVING_COLOR = UIColor(red: 40/255.0, green: 183/255.0, blue: 121/255.0, alpha: 1.0) // 28B779
    static let DASHBOARD_IDLE_COLOR = UIColor(red: 255/255.0, green: 184/255.0, blue: 72/255.0, alpha: 1.0)  // FFB848
    static let DASHBOARD_STOPPED_COLOR = UIColor(red: 231/255.0, green: 25/255.0, blue: 27/255.0, alpha: 1.0) // E7191B
    static let DASHBOARD_INACTIVE_COLOR = UIColor(red: 128/255.0, green: 125/255.0, blue: 125/255.0, alpha: 1.0) // 807D7D
    static let DASHBOARD_OUTOFSERVICE_COLOR = UIColor(red: 133/255.0, green: 43/255.0, blue: 152/255.0, alpha: 1.0) // 852B99
    static let DASHBOARD_NOTPOLLING_COLOR = UIColor(red: 39/255.0, green: 169/255.0, blue: 229/255.0, alpha: 1.0) // 27a9e5
    @available(iOS 13.0, *)
    static let DASHBOARD_Accdient_COLOR_DARK_MODE = UIColor(red: 201/255.0, green: 95/255.0, blue: 91/255.0, alpha: 1.0) // c95f5b
    @available(iOS 13.0, *)
    static let secondarySystemBackground = UIColor.secondarySystemBackground
}
