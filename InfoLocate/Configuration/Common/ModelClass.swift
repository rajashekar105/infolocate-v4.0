

import Foundation


class LiveVehicleListServerMap {
    static let vehicleno = "vehicleno"
    static let datetime = "datetime"
    static let location = "location"
    static let mdvrunitno = "mdvrunitno"
}

class LiveVehicleListModel : NSObject {
    var vehicleNo : String!
    var dateTime : String!
    var location : String!
    var mdvrunitno : String!
    
    func addLiveVehicleListDetailsWith(vehicleNo : String, dateTime : String, location : String, mdvrunitno : String) -> LiveVehicleListModel {
        self.vehicleNo = vehicleNo
        self.dateTime = dateTime
        self.location = location
        self.mdvrunitno = mdvrunitno
        return self
    }
}




