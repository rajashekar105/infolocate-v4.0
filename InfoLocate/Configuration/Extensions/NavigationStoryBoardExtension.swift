
import Foundation
import UIKit

class Navigation {
    
    /**
     This function is used to navigate to previous screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func back(viewController : UIViewController) {
        viewController.navigationController?.popViewController(animated: true)
    }
    
   
    /**
     This function is used to navigate to Login screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToLoginScreen(viewController : UIViewController) {
        let moduleBuilder = LoginModuleBuilder()
        let controller = moduleBuilder.build()
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to Login screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToClientLoginScreen(viewController : UIViewController) {
        let moduleBuilder = ClientLoginModuleBuilder()
        let controller = moduleBuilder.build()
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
   
    
    /**
     This function is used to navigate to Dashboard screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToDashboardScreen(viewController : UIViewController) {
//        let moduleBuilder = DashboardModuleBuilder()
//        let controller = moduleBuilder.build()
//        viewController.navigationController?.pushViewController(controller, animated: true)
        
        
        let dashboardViewController = DashboardModuleBuilder().build()
        let trackViewController = TrackModuleBuilder().build()
        let recentAlertsViewController = RecentAlertsModuleBuilder().build()
        let dynamicViewController = DynamicModuleBuilder().build()
        let logoutViewController = LogoutModuleBuilder().build()
        let storyboard = UIStoryboard(name: Constants.MAINMODULE_STORYBOARD, bundle: nil)
        let tabbarVC = storyboard.instantiateViewController(withIdentifier: Constants.HOME_TABBAR_IDENTIFIER) as! UITabBarController
        let views = [dashboardViewController,trackViewController,recentAlertsViewController,dynamicViewController,logoutViewController]
        tabbarVC.viewControllers = views
        tabbarVC.tabBar.tintColor = Color.TABBAR_SELECTION_COLOR
        tabbarVC.selectedIndex = 0
        guard let items = tabbarVC.tabBar.items else { return }
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            items[0].title = English.Dashboard
            items[1].title = English.Track
            items[2].title = English.Alerts
            items[3].title = English.Dynamic
            items[4].title = "Title1"
        } else if type == Constants.japanese {
            items[0].title = Japanese.Dashboard
            items[1].title = Japanese.Track
            items[2].title = Japanese.Alerts
            items[3].title = Japanese.Dynamic
            items[4].title = "Title1"
        }
        viewController.navigationController?.view.backgroundColor = .white
        viewController.navigationController?.pushViewController( tabbarVC, animated: true)
    }
    
    /**
     This function is used to navigate to VehicleList screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToVehicleListScreen(viewController : UIViewController) {
        let moduleBuilder = VehicleListModuleBuilder()
        let controller = moduleBuilder.build()
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    /**
     This function is used to navigate to Track screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToTrackScreen(viewController : UIViewController) {
        let moduleBuilder = TrackModuleBuilder()
        let controller = moduleBuilder.build()
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to Trips screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToTripsScreen(vehicleNumber: String, viewController : UIViewController) {
        let moduleBuilder = TripsModuleBuilder()
        let controller = moduleBuilder.build(vehicleNumber: vehicleNumber)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to Animation screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToAnimationScreen(animationDetails: AnimationModel, viewController : UIViewController) {
        let moduleBuilder = AnimationModuleBuilder()
        let controller = moduleBuilder.build(animationDetails: animationDetails)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to Animation screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToHistoryDetailsScreen(vehicleNo : String, currentlat : Double, currentlng : Double, viewController : UIViewController) {
        let moduleBuilder = HistoryDetailsModuleBuilder()
        let controller = moduleBuilder.build(vehicleNo: vehicleNo, currentlat : currentlat, currentlng : currentlng)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to LiveVehicleList screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToLiveVehicleListScreen(viewController : UIViewController) {
        let moduleBuilder = LiveVehicleListModuleBuilder()
        let controller = moduleBuilder.build()
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to LiveVideoList screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToLiveVideoListScreen(liveVehicleDetails: LiveVehicleListModel, viewController : UIViewController) {
        let moduleBuilder = LiveVideoListModuleBuilder()
        let controller = moduleBuilder.build(liveVehicleDetails: liveVehicleDetails)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to LiveVideoList screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToLiveVideoScreen(mdvrUnitNo: String, channelId: Int32, viewController : UIViewController) {
        let moduleBuilder = LiveVideoModuleBuilder()
        let controller = moduleBuilder.build(mdvrUnitNo: mdvrUnitNo, channelId: channelId)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
        This function is used to navigate to TMStatus screen.
        - Parameter viewController: **UIViewController Object**
        */
       static func navigateToTMStatusScreen(viewController : UIViewController) {
           let moduleBuilder = TMStatusModuleBuilder()
           let controller = moduleBuilder.build()
           viewController.navigationController?.pushViewController(controller, animated: true)
       }
    
    /**
     This function is used to navigate to RecentAlerts screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToRecentAlertsScreen(viewController : UIViewController) {
        let moduleBuilder = RecentAlertsModuleBuilder()
        let controller = moduleBuilder.build()
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to Alerts screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToAlertsScreen(alertTypeArray: [String], viewController : UIViewController) {
        let moduleBuilder = AlertsModuleBuilder()
        let controller = moduleBuilder.build(alertTypeArray: alertTypeArray)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to AlertVideoList screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToAlertVideoListScreen(mapDetails: MapModel, viewController : UIViewController) {
        let moduleBuilder = AlertVideoListModuleBuilder()
        let controller = moduleBuilder.build(mapDetails: mapDetails)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to AlertVideoPlayer screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToAlertVideoPlayerScreen(url: URL, vehicleno: String, viewController : UIViewController) {
        let moduleBuilder = AlertVideoPlayerModuleBuilder()
        let controller = moduleBuilder.build(url: url, vehicleno: vehicleno)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to Dynamic screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToDynamicScreen(viewController : UIViewController) {
        let moduleBuilder = DynamicModuleBuilder()
        let controller = moduleBuilder.build()
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to DynamicMapView screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToDynamicMapViewScreen(mapDetails: DynamicMapModel, viewController : UIViewController) {
        let moduleBuilder = DynamicMapModuleBuilder()
        let controller = moduleBuilder.build(mapDetails: mapDetails)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    /**
     This function is used to navigate to Map screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToMapViewScreen(mapDetails: MapModel, viewController : UIViewController) {
        let moduleBuilder = MapModuleBuilder()
        let controller = moduleBuilder.build(mapDetails: mapDetails)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     This function is used to navigate to Logout screen.
     - Parameter viewController: **UIViewController Object**
     */
    static func navigateToLogoutScreen(viewController : UIViewController) {
        let moduleBuilder = LogoutModuleBuilder()
        let controller = moduleBuilder.build()
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
       
}

class StoryBoard {
    
    /// This function is used to get main module storyboard.
    static func mainModuleStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.MAINMODULE_STORYBOARD, bundle: Bundle.main)
        return storyboard
    }
    
    /// This function is used to get startUp storyboard.
    static func startUpStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.STARTUP_STORYBOARD, bundle: Bundle.main)
        return storyboard
    }
}
