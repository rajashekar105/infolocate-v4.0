

import Foundation
import UIKit

extension String {
    
    static var one = 1
    static var two = 2
    
    /// Encode a String to Base64
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }

    /// Decode a String from Base64. Returns nil if unsuccessful.
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    func convertUTCToLocalDate(dateStringToConvert: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = formatter.date(from: dateStringToConvert) // create   date from string
        
        // change to a readable time format and change to local time zone
        formatter.dateFormat = "EEEE MMM d, yyyy"
        formatter.timeZone = NSTimeZone.local
        let localString = formatter.string(from: date!)
        return localString
    }
    
    func convertUTCToLocalTime(dateStringToConvert: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = formatter.date(from: dateStringToConvert) // create   date from string
        
        // change to a readable time format and change to local time zone
        formatter.dateFormat = "h:mm a"
        formatter.timeZone = NSTimeZone.local
        let localString = formatter.string(from: date!)
        return localString
    }
    
    func convertUTCToLocalData_Xaxis(dateStringToConvert: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = formatter.date(from: dateStringToConvert) // create   date from string
        
        // change to a readable time format and change to local time zone
        formatter.dateFormat = "dd/MM"
        formatter.timeZone = NSTimeZone.local
        let localString = formatter.string(from: date!)
        return localString
    }
    
    func encrypt() -> String {
//        print(UserDefaults.standard.value(forKey: "encenabled") as Any)
//        let value = UserDefaults.standard.value(forKey: "encenabled") as! Int
//        if value == 1 {
            do {
                let encryptedText:String? = try Encryption.encryptData(plainText: self, hexKey: Constants.key)
                return encryptedText! as String
            }
            catch { return "" }
//        } else {
//            return self
//        }
    }
    
    func decrypt() -> String {
//        print(UserDefaults.standard.value(forKey: "fcmenabled") as Any)
//        let value = UserDefaults.standard.value(forKey: "fcmenabled") as! Int
//        if value == 1 {
            do {
                let decryptedText:String? = try Encryption.decryptData(hexStr: self, hexKey: Constants.key)
                return decryptedText! as String
            }
            catch { return "" }
//        } else {
//            return self
//        }
    }
}
