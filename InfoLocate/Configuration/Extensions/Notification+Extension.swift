
import Foundation

extension Notification.Name {
    static let LocalNotification = Notification.Name(
        rawValue: "LocalNotification")
}
