import UIKit

class RootRouter {
    
    var window:UIWindow!
// MARK: - Public methods
    private static let _sharedInstance: RootRouter = RootRouter()
    
    private init() {
        
    }
    
    func showRootViewController(vc viewController:UIViewController, window:UIWindow) {
        let navigationController = navigationControllerFromWindow(window:window)
        navigationController.isNavigationBarHidden = true
        navigationController.viewControllers = [viewController];
    }

// MARK: - Private methods

    func navigationControllerFromWindow(window:UIWindow) -> UINavigationController {
        let navigationController = window.rootViewController
        return navigationController as! UINavigationController
    }
    

    
    public static  func getRouter() -> (RootRouter){
    return _sharedInstance
    }

}
