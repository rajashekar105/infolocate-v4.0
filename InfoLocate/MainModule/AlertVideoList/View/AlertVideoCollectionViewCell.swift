//
//  LiveVideoCollectionViewCell.swift
//  InfoLocate
//
//  Created by Infotrack on 13/04/20.
//  Copyright © 2020 Hemalatha T. All rights reserved.
//

import UIKit


protocol AlertVideoCollectionViewCellDelegate {
    func downloadButtonAction(index : Int)
//    func playPauseButtonAction(index: Int)
}

class AlertVideoCollectionViewCell: UICollectionViewCell {

    /// This property is videoView.
    @IBOutlet weak var videoView: UIView!
    /// This property knows download button.
    @IBOutlet var downloadButton : UIButton!
    /// This property knows playPause button.
//    @IBOutlet var playPauseButton : UIView!

    /// This property knows AlertVideoCollectionViewCellDelegate.
    var delegate : AlertVideoCollectionViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func downloadButtonAction(_ sender : UIButton) {
        self.delegate.downloadButtonAction(index: sender.tag)
    }
    
//    @IBAction func playPauseButtonAction(_ sender : UIButton) {
//        self.delegate.playPauseButtonAction(index: sender.tag)
//    }
    
}
