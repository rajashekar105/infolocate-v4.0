import Foundation

protocol AlertVideoListViewInput: class {
    func configureModule(mapDetails: MapModel)
}
