import Foundation

protocol AlertVideoListViewOutput {
    func back()
    func navigateToAlertVideoPlayerScreen(url: URL, vehicleno: String)
}
