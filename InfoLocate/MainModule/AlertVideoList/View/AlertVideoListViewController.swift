import UIKit
import AVKit
import AVFoundation
import Photos
import MLVideoPlayer
import Alamofire

class AlertVideoListViewController: BaseViewController, AlertVideoListViewInput, AlertVideoCollectionViewCellDelegate {
   
    /// This property is AlertVideoListViewOutput.
    var output: AlertVideoListViewOutput!
    
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is mainLabel.
    @IBOutlet var mainLabel: UILabel!
    /// This property is alertVideoCollectionView.
    @IBOutlet weak var alertVideoCollectionView : UICollectionView!
    /// This property is channelArray.
    let channelArray = ["CH1","CH2","CH3","CH4","CH5","CH6","CH7","CH8"]
    /// This property is urlArray.
    var urlArray: NSArray = NSArray()
    var activity1: UIActivityIndicatorView!
    var activity2: UIActivityIndicatorView!
    var activity3: UIActivityIndicatorView!
    var activity4: UIActivityIndicatorView!
    var activity5: UIActivityIndicatorView!
    var activity6: UIActivityIndicatorView!
    var activity7: UIActivityIndicatorView!
    var activity8: UIActivityIndicatorView!
    /// This property is video1.
    var video1 = MLVideoPlayer()
    /// This property is video2.
    var video2 = MLVideoPlayer()
    /// This property is video3.
    var video3 = MLVideoPlayer()
    /// This property is video4.
    var video4 = MLVideoPlayer()
    /// This property is video5.
    var video5 = MLVideoPlayer()
    /// This property is video6.
    var video6 = MLVideoPlayer()
    /// This property is video7.
    var video7 = MLVideoPlayer()
    /// This property is video8.
    var video8 = MLVideoPlayer()
    /// This property is vehicleno.
    var vehicleno: String = ""
    /// This property is trackTime.
    var trackTime: String = ""
    /// This property is alerttypeid.
    var alerttypeid: String = ""
    var alert = UIAlertController()
    
    func configureModule(mapDetails: MapModel){
        vehicleno = mapDetails.vehicleno
        trackTime = mapDetails.trackTime
        alerttypeid = mapDetails.alerttypeid
        urlArray = mapDetails.videoUrlListArray
//        urlArray = ["http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_06/72137/ch01_20210706_130357_130540_007.mp4","http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_06/72137/ch02_20210706_130357_130540_007.mp4","http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_06/72137/ch01_20210706_130357_130540_007.mp4","http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_06/72137/ch02_20210706_130357_130540_007.mp4"]
//        urlArray = ["http://124.153.111.204/UserRequestVideos/72101/2021-06-15/72101_0-210615-142452-142452-20020100.mp4","http://124.153.111.204/UserRequestVideos/72101/2021-06-15/72101_0-210615-142452-142452-20020100.mp4","http://124.153.111.204/UserRequestVideos/72101/2021-06-15/72101_0-210615-142452-142452-20020100.mp4","http://124.153.111.204/UserRequestVideos/72101/2021-06-15/72101_0-210615-142452-142452-20020100.mp4"]
//        urlArray = ["http://static.videokart.ir/clip/100/480.mp4",
//        "http://staging.infosmart.co.in/mapp/infoinsure/resources/images/accidentimages/22_c8d82c20-a662-4e3f-8d13-582993a9a02b/VID-200313174433.mp4",
//        "http://staging.infosmart.co.in/mapp/infoinsure/resources/images/accidentimages/22_c8d82c20-a662-4e3f-8d13-582993a9a02b/VID-200313174433.mp4",
//        "http://static.videokart.ir/clip/100/480.mp4"]
//        urlArray = ["file:///Users/infotrack/Desktop/Rnd/AudioVideoPlaver/AudioVideoPlaver/v5.mp4", "file:///Users/infotrack/Desktop/Rnd/AudioVideoPlaver/AudioVideoPlaver/v5.mp4", "file:///Users/infotrack/Desktop/Rnd/AudioVideoPlaver/AudioVideoPlaver/v5.mp4", "file:///Users/infotrack/Desktop/Rnd/AudioVideoPlaver/AudioVideoPlaver/v5.mp4"]
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCollectionViewCell()
        self.setCollectionViewFlowLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainLabel.text = "\(vehicleno)"
        activity.layer.cornerRadius = 10
//        resumeVideo()
    }
    
    func resumeVideo() {
        let count:Int = urlArray.count
        switch(count) {
        case 1 :
            self.video1.player.play()
            break
        case 2 :
            self.video1.player.play()
            self.video2.player.play()
            break
        case 3 :
            self.video1.player.play()
            self.video2.player.play()
            self.video3.player.play()
            break
        case 4 :
            self.video1.player.play()
            self.video2.player.play()
            self.video3.player.play()
            self.video4.player.play()
            break
        case 5 :
            self.video1.player.play()
            self.video2.player.play()
            self.video3.player.play()
            self.video4.player.play()
            self.video5.player.play()
            break
        case 6 :
            self.video1.player.play()
            self.video2.player.play()
            self.video3.player.play()
            self.video4.player.play()
            self.video5.player.play()
            self.video6.player.play()
            break
        case 7 :
            self.video1.player.play()
            self.video2.player.play()
            self.video3.player.play()
            self.video4.player.play()
            self.video5.player.play()
            self.video6.player.play()
            self.video7.player.play()
            break
        case 8 :
            self.video1.player.play()
            self.video2.player.play()
            self.video3.player.play()
            self.video4.player.play()
            self.video5.player.play()
            self.video6.player.play()
            self.video7.player.play()
            self.video8.player.play()
            break
        default:
            break
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //stopVideo()
    }
    
    @IBAction func back(_ sender: Any) {
        stopVideo()
        self.output.back()
    }
    
    /// This method is used to stop/pause video.
    func stopVideo() {
        let count:Int = urlArray.count
        switch(count) {
        case 1 :
            self.video1.player.pause()
            break
        case 2 :
            self.video1.player.pause()
            self.video2.player.pause()
            break
        case 3 :
            self.video1.player.pause()
            self.video2.player.pause()
            self.video3.player.pause()
            break
        case 4 :
            self.video1.player.pause()
            self.video2.player.pause()
            self.video3.player.pause()
            self.video4.player.pause()
            break
        case 5 :
            self.video1.player.pause()
            self.video2.player.pause()
            self.video3.player.pause()
            self.video4.player.pause()
            self.video5.player.pause()
            break
        case 6 :
            self.video1.player.pause()
            self.video2.player.pause()
            self.video3.player.pause()
            self.video4.player.pause()
            self.video5.player.pause()
            self.video6.player.pause()
            break
        case 7 :
            self.video1.player.pause()
            self.video2.player.pause()
            self.video3.player.pause()
            self.video4.player.pause()
            self.video5.player.pause()
            self.video6.player.pause()
            self.video7.player.pause()
            break
        case 8 :
            self.video1.player.pause()
            self.video2.player.pause()
            self.video3.player.pause()
            self.video4.player.pause()
            self.video5.player.pause()
            self.video6.player.pause()
            self.video7.player.pause()
            self.video8.player.pause()
            break
        default:
            break
        }
    }

    /// This method is used to registerCollectionViewCell.
    func registerCollectionViewCell() {
        alertVideoCollectionView.register(UINib(nibName: "AlertVideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AlertVideoCollectionViewCell")
    }
    
    /// This method is used to setCollectionViewFlowLayout.
    func setCollectionViewFlowLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: (width / 2)-4, height: (width / 1.7))
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 0
        alertVideoCollectionView.collectionViewLayout = layout
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    /// This method is used to playVideo.
//    func playVideo(url: URL) {
//        let player = AVPlayer(url: url)
//        let vc = AVPlayerViewController()
//        vc.player = player
//        self.present(vc, animated: true) {
//            vc.player?.play()
//        }
//    }
    
    
    /// This method is used as downloadButtonAction.
    func downloadButtonAction(index: Int) {
        if PHPhotoLibrary.authorizationStatus() == .denied {
            var title = NSLocalizedString("Please click OK to set access to the photos.", comment: "")
            var cancelTitle = NSLocalizedString("CANCEL", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.Please_click_OK_to_set_access_to_the_photos
                cancelTitle = English.CANCEL
            } else if type == Constants.japanese {
                title = Japanese.Please_click_OK_to_set_access_to_the_photos
                cancelTitle = Japanese.CANCEL
            }
            let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                if let bundleIdentifier = Bundle.main.bundleIdentifier, let appSettings = URL(string: UIApplication.openSettingsURLString + bundleIdentifier) {
                    if UIApplication.shared.canOpenURL(appSettings) {
                        UIApplication.shared.open(appSettings)
                    }
                }
            }
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default) {
                UIAlertAction in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else if PHPhotoLibrary.authorizationStatus() == .authorized {
            if Reachability.connectedToNetwork() == false
            {
                var title = NSLocalizedString("No Internet Connection", comment: "")
                var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    title = English.No_Internet_Connection
                    message = English.Make_sure_your_device_is_connected_to_the_internet
                } else if type == Constants.japanese {
                    title = Japanese.No_Internet_Connection
                    message = Japanese.Make_sure_your_device_is_connected_to_the_internet
                }
                let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                var title = NSLocalizedString("Are you sure want to download?", comment: "")
                var cancelTitle = NSLocalizedString("CANCEL", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    title = English.Are_you_sure_want_to_download
                    cancelTitle = English.CANCEL
                } else if type == Constants.japanese {
                    title = Japanese.Are_you_sure_want_to_download
                    cancelTitle = Japanese.CANCEL
                }
                let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
    //                 self.view.isUserInteractionEnabled = false
    //                 self.activity.startAnimating()
                    self.popUp(index: index)
                }
                alertController.addAction(okAction)
                let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default) {
                    UIAlertAction in
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func popUp(index: Int) {
        var title = NSLocalizedString("Video is Downloading , Please wait...", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            title = English.Video_is_Downloading_Please_wait
        } else if type == Constants.japanese {
            title = Japanese.Video_is_Downloading_Please_wait
        }
        alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        self.present(alert, animated: true, completion:{
 //            alert.view.superview?.isUserInteractionEnabled = true
 //                alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
            })
        self.download(urlString: self.urlArray[index] as! String, index: index)
    }
    
//    @objc func alertControllerBackgroundTapped() {
//        self.dismiss(animated: true, completion: nil)
//    }

    /// This method is used as download.
    func download(urlString: String, index: Int) {
        let videoImageUrl = urlString
        DispatchQueue.global(qos: .userInitiated).async {
            if let url = URL(string: videoImageUrl),
               let urlData = NSData(contentsOf: url) {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/tempFile\(index).mp4"
                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                    }) { completed, error in
                        if completed {
                            print("Video is saved!")
                            DispatchQueue.main.async(execute: {
                                self.alert.dismiss(animated: true, completion: nil)
                                self.statusPop()
                            })
                        } else {
                            print(error!)
                            print("Video is not saved!")
                        }
                    }
                }
            }
        }
    }
    
    func statusPop() {
        var title = NSLocalizedString("Video has been downloaded. It will be saved in the photo!", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            title = English.Video_has_been_downloaded_It_will_be_saved_in_the_photo
        } else if type == Constants.japanese {
            title = Japanese.Video_has_been_downloaded_It_will_be_saved_in_the_photo
        }
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
//            imageView.image = UIImage(named: const2)
        } else {
            print("Portrait")
//            imageView.image = UIImage(named: const)
        }
    }
}

extension AlertVideoListViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urlArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlertVideoCollectionViewCell", for: indexPath as IndexPath) as! AlertVideoCollectionViewCell
        cell.delegate = self
        cell.downloadButton.tag = indexPath.row
//        cell.playPauseButton.tag = indexPath.row
//        cell.playPauseButton.layer.cornerRadius = cell.playPauseButton.frame.size.width/2
        
        cell.downloadButton.setTitle("DOWNLOAD", for: .normal)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            cell.downloadButton.setTitle(English.DOWNLOAD, for: .normal)
        } else if type == Constants.japanese {
            cell.downloadButton.setTitle(Japanese.DOWNLOAD, for: .normal)
        }
        
        switch(indexPath.row) {
        case 0 :
            video1 = MLVideoPlayer(url: urlArray[indexPath.row] as! String, width: cell.videoView.bounds.width, height: cell.videoView.bounds.height)
            video1.player.play()
            video1.player.isMuted = true
            video1.loadingDisplayView.alignmentRect(forFrame: cell.videoView.frame)
            cell.videoView.addSubview(video1.viewController.view)
            break
        case 1 :
            video2 = MLVideoPlayer(url: urlArray[indexPath.row] as! String, width: cell.videoView.bounds.width, height: cell.videoView.bounds.height)
            video2.player.play()
            video2.player.isMuted = true
            video2.loadingDisplayView.alignmentRect(forFrame: cell.videoView.frame)
            cell.videoView.addSubview(video2.viewController.view)
            break
        case 2 :
            video3 = MLVideoPlayer(url: urlArray[indexPath.row] as! String, width: cell.videoView.bounds.width, height: cell.videoView.bounds.height)
            video3.player.play()
            video3.player.isMuted = true
            video3.loadingDisplayView.alignmentRect(forFrame: cell.videoView.frame)
            cell.videoView.addSubview(video3.viewController.view)
            break
        case 3 :
            video4 = MLVideoPlayer(url: urlArray[indexPath.row] as! String, width: cell.videoView.bounds.width, height: cell.videoView.bounds.height)
            video4.player.play()
            video4.player.isMuted = true
            video4.loadingDisplayView.alignmentRect(forFrame: cell.videoView.frame)
            cell.videoView.addSubview(video4.viewController.view)
            break
        case 4 :
            video5 = MLVideoPlayer(url: urlArray[indexPath.row] as! String, width: cell.videoView.bounds.width, height: cell.videoView.bounds.height)
            video5.player.play()
            video5.player.isMuted = true
            video5.loadingDisplayView.alignmentRect(forFrame: cell.videoView.frame)
            cell.videoView.addSubview(video5.viewController.view)
            break
        case 5 :
            video6 = MLVideoPlayer(url: urlArray[indexPath.row] as! String, width: cell.videoView.bounds.width, height: cell.videoView.bounds.height)
            video6.player.play()
            video6.player.isMuted = true
            video6.loadingDisplayView.alignmentRect(forFrame: cell.videoView.frame)
            cell.videoView.addSubview(video6.viewController.view)
            break
        case 6 :
            video7 = MLVideoPlayer(url: urlArray[indexPath.row] as! String, width: cell.videoView.bounds.width, height: cell.videoView.bounds.height)
            video7.player.play()
            video7.player.isMuted = true
            video7.loadingDisplayView.alignmentRect(forFrame: cell.videoView.frame)
            cell.videoView.addSubview(video7.viewController.view)
            break
        case 7 :
            video8 = MLVideoPlayer(url: urlArray[indexPath.row] as! String, width: cell.videoView.bounds.width, height: cell.videoView.bounds.height)
            video8.player.play()
            video8.player.isMuted = true
            video8.loadingDisplayView.alignmentRect(forFrame: cell.videoView.frame)
            cell.videoView.addSubview(video8.viewController.view)
            break
        default:
            break
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let url = URL(string: urlArray[indexPath.row] as! String)!
        print(urlArray[indexPath.row])
//        output.navigateToAlertVideoPlayerScreen(url: url, vehicleno: vehicleno)
//        stopVideo()
        
    }

}



//extension AlertVideoListViewController {
//
//    func showToast(message : String, seconds: Double){
//        let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
//        alert.view.backgroundColor = .white
//        alert.view.alpha = 0.5
//        alert.view.layer.cornerRadius = 15
//        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
//            UIAlertAction in
//            self.waitingView.isHidden = true
//        }
//        alert.addAction(okAction)
//        self.present(alert, animated: false)
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
//            alert.dismiss(animated: false)
//            self.waitingView.isHidden = true
//        }
//    }
//}
