import UIKit

@objc class AlertVideoListModuleBuilder: NSObject {
    
    func build(mapDetails: MapModel) -> UIViewController {
        let viewController = AlertVideoListControllerFromStoryboard()
        
        let router = AlertVideoListRouter()
        router.viewController = viewController
        
        let presenter = AlertVideoListPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = AlertVideoListInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(mapDetails: mapDetails)
        
        return viewController
    }
    
    func AlertVideoListControllerFromStoryboard() -> AlertVideoListViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.ALERTS_VIDEO_LIST_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! AlertVideoListViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.MAINMODULE_STORYBOARD, bundle: Bundle.main)
        return storyboard
    }
    
}
