import Foundation

protocol AlertVideoListModuleInput: class {
    
    func configureModule(mapDetails: MapModel)
}
