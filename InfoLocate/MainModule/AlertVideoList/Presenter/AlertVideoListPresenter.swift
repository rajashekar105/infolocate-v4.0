import Foundation

class AlertVideoListPresenter: AlertVideoListModuleInput, AlertVideoListViewOutput, AlertVideoListInteractorOutput {
    
    weak var view: AlertVideoListViewInput!
    var interactor: AlertVideoListInteractorInput!
    var router: AlertVideoListRouterInput!
    
    // MARK: - AlertVideoListViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToAlertVideoPlayerScreen(url: URL, vehicleno: String) {
        router.navigateToAlertVideoPlayerScreen(url: url, vehicleno: vehicleno)
    }
    
    // MARK: - AlertVideoListModuleInput
    
    func configureModule(mapDetails: MapModel) {
        view.configureModule(mapDetails: mapDetails)
    }
    
    // MARK: - AlertVideoListInteractorOutput
    
}
