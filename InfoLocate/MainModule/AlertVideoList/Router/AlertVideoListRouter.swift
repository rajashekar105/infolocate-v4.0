import UIKit

class AlertVideoListRouter:AlertVideoListRouterInput {
    
    weak var viewController: AlertVideoListViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToAlertVideoPlayerScreen(url: URL, vehicleno: String) {
        Navigation.navigateToAlertVideoPlayerScreen(url: url, vehicleno: vehicleno, viewController: viewController)
    }
}
