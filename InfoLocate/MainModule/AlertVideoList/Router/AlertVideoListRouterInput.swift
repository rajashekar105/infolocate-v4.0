import Foundation

protocol AlertVideoListRouterInput {
    func back()
    func navigateToAlertVideoPlayerScreen(url: URL, vehicleno: String)
}
