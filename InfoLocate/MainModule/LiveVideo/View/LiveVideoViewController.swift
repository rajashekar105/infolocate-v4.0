import UIKit
import MobileVLCKit

class LiveVideoViewController: BaseViewController, LiveVideoViewInput {

    /// This property is output for LiveVideoViewOutput.
    var output: LiveVideoViewOutput!
    /// This property is videoView.
    @IBOutlet var videoView: UIView!
    /// This property is titleLabel.
    @IBOutlet weak var titleLabel: UILabel!
    /// This property is downloadButton.
    @IBOutlet weak var downloadButton: UIButton!
    /// This property is pauseButton.
    @IBOutlet weak var pauseButton: UIButton!
    /// This property is soundButton.
    @IBOutlet weak var soundButton: UIButton!
    /// This property is captureButton.
    @IBOutlet weak var captureButton: UIButton!
    /// This property is videoDisplay.
    let videoDisplay = VLCMediaPlayer()
    var activity: UIActivityIndicatorView!
    /// This property is mdvrUnitNo.
    var mdvrUnitNo : String!
    /// This property is channelId.
    var channelId : Int32!
    var movieView: UIView!
    
    
    func configureModule(mdvrUnitNo: String, channelId: Int32) {
        self.mdvrUnitNo = mdvrUnitNo
        self.channelId = channelId
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = "\(NSLocalizedString("Live Video", comment: ""))"
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.titleLabel.text = English.Live_Video
        } else if type == Constants.japanese {
            self.titleLabel.text = Japanese.Live_Video
        }
        
        movieView = UIView()
        movieView.backgroundColor = UIColor.black
        movieView.frame = CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: UIScreen.main.bounds.size.height-120, height: UIScreen.main.bounds.size.width)
        self.movieView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
        movieView.center = view.center
//        Add tap gesture to movieView for play/pause
//        let gesture = UITapGestureRecognizer(target: self, action: #selector(fullScreenTapped(_:)))
//        movieView.addGestureRecognizer(gesture)
        //Add movieView to view controller
        self.view.addSubview(movieView)
        
        let mdvripaddress = self.defaults.value(forKey: "mdvripaddress")!
        let vsstoken = self.defaults.value(forKey: "vsstoken")!
        let url = "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_\(channelId as Int32)_1"
//        url = "http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_20/72130/ch02_20210720_162843_162921_007.mp4"
        
        activity = UIActivityIndicatorView.init(style: .whiteLarge)
        activity.frame = CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: 400.0)
        activity.center = view.center
        activity.startAnimating()
        activity.isHidden = false
        self.view.addSubview(activity)
        
        let media = VLCMedia(url: URL(string: url as String)! )
        videoDisplay.media = media
        videoDisplay.delegate = self
        videoDisplay.drawable = movieView
        videoDisplay.play()
//        videoDisplay.audio.setMute(true)
    }

    
    @objc func fullScreenTapped(_ sender: UITapGestureRecognizer) {
//        sender.view!.frame = UIScreen.main.bounds
        movieView.frame.size.width = UIScreen.main.bounds.size.height
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        sender.view!.addGestureRecognizer(tap)
    }
        
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(fullScreenTapped(_:)))
        self.movieView.addGestureRecognizer(gesture)
        movieView.frame.size.width = UIScreen.main.bounds.size.height-120
//        movieView.center = view.center
//        self.movieView.transform = CGAffineTransform(rotationAngle: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    /// This method is back button action.
    @IBAction func back(_ sender: Any) {
        self.videoDisplay.stop()
        self.output.back()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is used to configureTTXSDKPrepare.
    func configureURLPrepare() {
//        let prepareObj = TTXSDKPrepare()
//        prepareObj.initializationSDK()
//        //        prepareObj.setJsession("8137eaed-0863-4a2a-be7d-91e4ecf93c88")
//        //        prepareObj.setServer("mdvr.infosmart.co.in", lanIP: "mdvr.infosmart.co.in", port: 6605)
//        let mdvripaddress = self.defaults.value(forKey: "mdvripaddress")!
//        //        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//        //        if type == Constants.english {
//        //            prepareObj.setServer("mdvr.infosmart.co.in", lanIP: "mdvr.infosmart.co.in", port: 6605)
//        //        } else if type == Constants.japanese {
//        prepareObj.setServer(mdvripaddress as! String, lanIP: mdvripaddress as! String, port: 6605)
//        //        }
    }
    
    /// This method is used to downloadVideos from button action.
    @IBAction func downloadVideos(_ sender: Any) {
//        downloadStatus()
    }
    
    /// This method is used to downloadStatus.
//    func downloadStatus() {
//        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//        if (self.videoDisplay.isViewing()) {
//            if (!self.videoDisplay.isRecording()) {
//                self.videoDisplay.startRecord()
//                if type == Constants.english {
//                    self.alertForASec("", message: English.Start_recording)
//                } else if type == Constants.japanese {
//                    self.alertForASec("", message: Japanese.Start_recording)
//                }
////                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
//            } else {
//                self.videoDisplay.stopRecord()
//                if type == Constants.english {
//                    self.alertForASec("", message: English.Stop_recording)
//                } else if type == Constants.japanese {
//                    self.alertForASec("", message: Japanese.Stop_recording)
//                }
////                countLabel.isHidden = true
////                timer.invalidate()
////                counter = 90
//            }
//        }
//    }

    /// This method is used to pauseVideo from button action.
//    @IBAction func pauseVideo(_ sender: Any) {
//        if (!self.videoDisplay.isPlaying) {
//            self.videoDisplay.play()
//            pauseButton.setImage(UIImage(named: "pause_circle"), for: .normal)
//        } else {
//            self.videoDisplay.pause()
//            pauseButton.setImage(UIImage(named: "play_circle"), for: .normal)
//        }
//    }

    /// This method is used to soundVideo from button action.
//    @IBAction func soundVideo(_ sender: Any) {
//        if self.videoDisplay.audio.isMuted {
//            self.videoDisplay.audio.setMute(false)
//            soundButton.setImage(UIImage(named: "mute"), for: .normal)
//        } else {
//            self.videoDisplay.audio.setMute(true)
//            soundButton.setImage(UIImage(named: "unmute"), for: .normal)
//        }
//    }
//
//    /// This method is used to saveImagesFromVideos from button action.
//    @IBAction func saveImagesFromVideos(_ sender: Any) {
//
//        if (self.videoDisplay.isViewing()) {
//            if (self.videoDisplay.savePngFile()) {
//               let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//                if type == Constants.english {
//                    self.alertForASec("", message: English.Captured_preview_saved)
//                } else if type == Constants.japanese {
//                    self.alertForASec("", message: Japanese.Captured_preview_saved)
//                }
//            }
//        }
//    }

}

extension LiveVideoViewController: VLCMediaPlayerDelegate {

    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        switch(videoDisplay.state) {
        case .opening :
            activity.stopAnimating()
            activity.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            activity.stopAnimating()
            activity.isHidden = true
            break
        case .stopped :
            activity.stopAnimating()
            activity.isHidden = true
            break
        case .ended :
            activity.stopAnimating()
            activity.isHidden = true
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            activity.stopAnimating()
            activity.isHidden = true
            break
        default:
            activity.stopAnimating()
            activity.isHidden = true
            break
        }
    }
}

