import Foundation

protocol LiveVideoViewInput: class {
        func configureModule(mdvrUnitNo: String, channelId: Int32)
}
