import Foundation

protocol LiveVideoViewOutput {
    func back()
}
