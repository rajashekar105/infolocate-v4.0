import Foundation

protocol LiveVideoModuleInput: class {
    
    func configureModule(mdvrUnitNo: String, channelId: Int32)
}
