import UIKit

@objc class LiveVideoModuleBuilder: NSObject {
    
    func build(mdvrUnitNo: String, channelId: Int32) -> UIViewController {
        let viewController = LiveVideoControllerFromStoryboard()
        
        let router = LiveVideoRouter()
        router.viewController = viewController
        
        let presenter = LiveVideoPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = LiveVideoInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(mdvrUnitNo: mdvrUnitNo, channelId: channelId)
        
        return viewController
    }
    
    func LiveVideoControllerFromStoryboard() -> LiveVideoViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.LIVE_VIDEO_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! LiveVideoViewController
    }
    
    
}
