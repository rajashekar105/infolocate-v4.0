import Foundation

class LiveVideoPresenter: LiveVideoModuleInput, LiveVideoViewOutput, LiveVideoInteractorOutput {
    
    weak var view: LiveVideoViewInput!
    var interactor: LiveVideoInteractorInput!
    var router: LiveVideoRouterInput!
    
    // MARK: - LiveVideoViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - LiveVideoModuleInput
    
    func configureModule(mdvrUnitNo: String, channelId: Int32) {
        view.configureModule(mdvrUnitNo: mdvrUnitNo, channelId: channelId)
    }
    
    // MARK: - LiveVideoInteractorOutput
    
}
