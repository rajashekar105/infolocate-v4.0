import UIKit

class LiveVideoRouter:LiveVideoRouterInput {
    
    weak var viewController: LiveVideoViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
