import Foundation

protocol VehicleListRouterInput {
    func back()
    func navigateToMapViewController(mapDetails: MapModel)
}
