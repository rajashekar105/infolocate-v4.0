import UIKit

class VehicleListRouter:VehicleListRouterInput {
    
    weak var viewController: VehicleListViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToMapViewController(mapDetails: MapModel) {
        Navigation.navigateToMapViewScreen(mapDetails: mapDetails, viewController: viewController)
    }
}
