import Foundation

protocol VehicleListModuleInput: class {
    
    func configureModule()
}
