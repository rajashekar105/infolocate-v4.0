import UIKit

@objc class VehicleListModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = VehicleListControllerFromStoryboard()
        
        let router = VehicleListRouter()
        router.viewController = viewController
        
        let presenter = VehicleListPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = VehicleListInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func VehicleListControllerFromStoryboard() -> VehicleListViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.VEHICLE_LIST_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! VehicleListViewController
    }
    
}
