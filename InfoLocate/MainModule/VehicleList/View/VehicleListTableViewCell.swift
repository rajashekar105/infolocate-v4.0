//
//  GetVehicleListTableViewCell.swift
//  InfoLocateFMS
//
//  Created by infotrack telematics on 07/07/16.
//  Copyright © 2016 InfotrackTelematics. All rights reserved.
//

import UIKit
import Alamofire

class VehicleListTableViewCell: UITableViewCell {
   
    /// This property is slnoLabel.
    @IBOutlet var slnoLabel: UILabel!
    /// This property is vehicleNoLabel.
    @IBOutlet var vehicleNoLabel: UILabel!
    /// This property is drivername.
    @IBOutlet var drivername: UILabel!
    /// This property is mobileno.
    @IBOutlet var mobileno: UILabel!
    /// This property is temperature.
    @IBOutlet var temperature: UILabel!
    /// This property is acStatusLabel.
    @IBOutlet var acStatusLabel: UILabel!
    /// This property is fuelLevelLabel.
    @IBOutlet var fuelLevelLabel: UILabel!
    /// This property is durationLabel.
    @IBOutlet var durationLabel: UILabel!
    /// This property is dateTimeLabel.
    @IBOutlet var dateTimeLabel: UILabel!
    /// This property is locationLabel.
    @IBOutlet var locationLabel: UILabel!
    /// This property is ignitionLabel.
    @IBOutlet var ignitionLabel: UILabel!
    /// This property is vehicleStatusLabel.
    @IBOutlet var vehicleStatusLabel: UILabel!
    /// This property is speedLabel.
    @IBOutlet var speedLabel: UILabel!
    /// This property is ViewBackground.
    @IBOutlet var ViewBackground: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
