import UIKit
import Alamofire

class VehicleListViewController: BaseViewController, VehicleListViewInput, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate  {

    /// This property is output for VehicleListViewOutput.
    var output: VehicleListViewOutput!
    /// This property is LoginID.
    var LoginID: String = ""
    /// This property is StatusID.
    var StatusID: String = ""
    /// This property is StatusName.
    var StatusName: String = ""
    /// This property is currentlat.
    var currentlat: Double!
    /// This property is currentlng.
    var currentlng: Double!
    /// This property is vehicleno.
    var vehicleno: String = ""
    /// This property is vehiclename.
    var vehiclename: String = ""
    /// This property is drivername.
    var drivername: String = ""
    /// This property is mobileno.
    var mobileno: String = ""
    /// This property is temperature.
    var temperature: String = ""
    /// This property is acStatus.
    var acStatus: String = ""
    /// This property is fuel.
    var fuel: String = ""
    /// This property is duration.
    var duration: String = ""
    /// This property is trackTime.
    var trackTime: String = ""
    /// This property is speed.
    var speed: String = ""
    /// This property is ignition.
    var ignition: String = ""
    /// This property is location.
    var location: String = ""
    /// This property is is_searching.
    var is_searching:Bool!
    /// This property is searchingDataArray.
    var searchingDataArray = NSMutableArray()
    /// This property is mainDataArray.
    var mainDataArray = NSArray()
    /// This property is workingArray.
    var workingArray = NSArray()
    /// This property is finalArray.
    var finalArray = NSArray()
    /// This property is searchBar.
    @IBOutlet var searchBar: UISearchBar!
    /// This property is statusLabel.
    @IBOutlet var statusLabel: UILabel!
    /// This property is uiTableView.
    @IBOutlet var uiTableView: UITableView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.placeholder = NSLocalizedString("Search Vehicle No/Driver Name", comment: "")
        searchBar.textField?.font = UIFont(name: "", size: 9.0)
        self.statusLabel.text = NSLocalizedString("Vehicle List", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            searchBar.placeholder = English.Search_Vehicle_No_Driver_Name
            self.statusLabel.text = "\(English.Vehicle_List) ( \(self.finalArray.count) )"
        } else if type == Constants.japanese {
            searchBar.placeholder = Japanese.Search_Vehicle_No_Driver_Name
            self.statusLabel.text = "\(Japanese.Vehicle_List) ( \(self.finalArray.count) )"
        }
        activity.layer.cornerRadius = 10
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            if let ID = defaults.object(forKey: "StatusID") {
                print(ID)
                StatusID = ID as! String
            }
            if let ID = defaults.object(forKey: "StatusName") {
                print(ID)
                StatusName = ID as! String
            }
            if let ID = defaults.value(forKey: "LoginID") {
                LoginID = ID as! String
            }
            GetVehicleStatusList()
            activity.startAnimating()
        }
        searchingDataArray = []
        is_searching = false
        searchBar.delegate = self
        let tblView =  UIView(frame: CGRect.zero)
        uiTableView.tableFooterView = tblView
        uiTableView.backgroundColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /// This method is back button action.
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //                is_searching = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //                is_searching = false
    }
    
    /// This method is called when searchBarCancelButtonClicked.
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false
        uiTableView.isHidden = false
        finalArray = workingArray
        uiTableView.reloadData()
        searchBar.text = ""
        dismissKeyboard()
        if searchBar.text!.isEmpty {
           // do nothing.
        } else {
            dismissKeyboard()
        }
    }
    
    /// This method is called when searchBarSearchButtonClicked.
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        is_searching = false
        dismissKeyboard()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /// This method is called when searchBar textDidChange.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text!.isEmpty{
            is_searching = false
            uiTableView.isHidden = false
            finalArray = workingArray
            self.uiTableView.reloadData()
        }
        else
        {
            is_searching = true
            searchingDataArray.removeAllObjects()
            for index in 0 ..< workingArray.count
            {
                var drivername = ""
                let vehicleNo = ((workingArray[index] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
                if (workingArray[index] as AnyObject).object(forKey: "drivername") == nil ||
                    ((workingArray[index] as AnyObject).object(forKey: "drivername") as? String)!.decrypt() == "" ||
                    ((workingArray[index] as AnyObject).object(forKey: "drivername") as? String)!.decrypt() == "-" ||
                    ((workingArray[index] as AnyObject).object(forKey: "drivername") as? String)!.decrypt() == "NA" {
                    drivername = NSLocalizedString("NA", comment: "")
                    let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                    if type == Constants.english {
                        drivername = English.NA
                    } else if type == Constants.japanese {
                        drivername = Japanese.NA
                    }
                }
                else
                {
                    drivername = ((workingArray[index] as AnyObject).object(forKey: "drivername") as? String)!.decrypt()
                }
                
                if vehicleNo.lowercased().range(of: searchText.lowercased())  != nil ||
                drivername.lowercased().range(of: searchText.lowercased())  != nil ||
                    (vehicleNo.lowercased().range(of: searchText.lowercased())  != nil && drivername.lowercased().range(of: searchText.lowercased())  != nil)
                {
                    searchingDataArray.add(workingArray[index])
                }
            }
            if(searchingDataArray.count == 0)
            {
                uiTableView.isHidden = true
                var message = NSLocalizedString("No Data Found", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.No_Data_Found
                } else if type == Constants.japanese {
                    message = Japanese.No_Data_Found
                }
                alertForASec("", message: message)
            }
            else if(searchingDataArray.count > 0)
            {
                finalArray = searchingDataArray
                uiTableView.isHidden = false
            }
            self.uiTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return finalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") as Any)
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") as? String)!.decrypt() == "NA" {
            drivername = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                drivername = English.NA
            } else if type == Constants.japanese {
                drivername = Japanese.NA
            }
        }
        else
        {
            drivername = (((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") as? String)!).decrypt()
        }
        
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "NA" {
            mobileno = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                mobileno = English.NA
            } else if type == Constants.japanese {
                mobileno = Japanese.NA
            }
        }
        else
        {
            mobileno = (((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!).decrypt()
        }
        
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "NA" {
            temperature = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                temperature = English.NA
            } else if type == Constants.japanese {
                temperature = Japanese.NA
            }
        }
        else
        {
            temperature = (((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!).decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus")) == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "NA" {
            acStatus = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acStatus = English.NA
            } else if type == Constants.japanese {
                acStatus = Japanese.NA
            }
        } else {
            acStatus = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel")) == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "-" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "NA" {
            fuel = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                fuel = English.NA
            } else if type == Constants.japanese {
                fuel = Japanese.NA
            }
        } else {
            fuel = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration")) == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "-" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "NA" {
            duration = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                duration = English.NA
            } else if type == Constants.japanese {
                duration = Japanese.NA
            }
        } else {
            duration = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! VehicleListTableViewCell
        
        cell.ViewBackground.layer.shadowColor = UIColor.darkGray.cgColor
        cell.ViewBackground.layer.shadowOffset = CGSize.zero
        cell.ViewBackground.layer.shadowRadius = 1.0
        cell.ViewBackground.layer.shadowOpacity = 0.4
        cell.slnoLabel.layer.cornerRadius = 5
        cell.slnoLabel.clipsToBounds = true
        cell.slnoLabel.text = "\(indexPath.row + 1)"
        cell.vehicleNoLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
        cell.dateTimeLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "datetime") as? String)!.decrypt()
        cell.speedLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "speed") as? String)!.decrypt() + "Km/hr"
        cell.locationLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as? String)!.decrypt()
        // cell.ignitionLabel.text = (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "ignition") as? String
        cell.vehicleStatusLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclestatus") as? String)!.decrypt()
        cell.drivername.text = drivername
        cell.mobileno.text = mobileno
        cell.temperature.text = temperature
        cell.acStatusLabel.text = acStatus
        cell.fuelLevelLabel.text = fuel
        cell.durationLabel.text = duration
        var strStatus = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclestatus") as? String)!.decrypt()
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            // do nothing.
        } else if type == Constants.japanese {
            if strStatus.contains(English.Stopped) {
                strStatus = strStatus.replacingOccurrences(of: English.Stopped, with: Japanese.Stopped)
            } else if strStatus.contains(English.Moving)  {
                strStatus = strStatus.replacingOccurrences(of: English.Moving, with: Japanese.Moving)
            } else if strStatus.contains(English.Idle)  {
                strStatus = strStatus.replacingOccurrences(of: English.Idle, with: Japanese.Idle)
            } else if strStatus.contains(English.Inactive){
                strStatus = strStatus.replacingOccurrences(of: English.Inactive, with: Japanese.Inactive)
            } else if strStatus.contains(English.OutOfService){
                strStatus = strStatus.replacingOccurrences(of: English.OutOfService, with: Japanese.OutOfService)
            }
            for i in 0...English.DirectionArray.count-1 {
                if strStatus.contains(English.DirectionArray[i]) {
                    strStatus = strStatus.replacingOccurrences(of: English.DirectionArray[i], with: Japanese.DirectionArray[i])
                }
            }
            cell.vehicleStatusLabel.text = strStatus
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentlat = Double(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "lat") as? String)!.decrypt())
        currentlng = Double(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "lon") as? String)!.decrypt())
        vehicleno = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
        trackTime = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "datetime") as? String)!.decrypt()
        speed = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "speed") as? String)!.decrypt() + "Km/hr"
        ignition = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "ignition") as? String)!.decrypt()
        location = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as? String)!.decrypt()
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "ignition") as? String)!.decrypt() == "ON" {
            ignition = NSLocalizedString("ON", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                ignition = English.ON
            } else if type == Constants.japanese {
                ignition = Japanese.ON
            }
        }
        else
        {
            ignition = NSLocalizedString("OFF", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                ignition = English.OFF
            } else if type == Constants.japanese {
                ignition = Japanese.OFF
            }
        }
        
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "NA" {
            vehiclename = ""
        }
        else
        {
            vehiclename = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt()
        }
        
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") as? String)?.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") as? String)?.decrypt() == "-" {
            drivername = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                drivername = English.NA
            } else if type == Constants.japanese {
                drivername = Japanese.NA
            }
        }
        else
        {
            drivername = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "drivername") as? String)!.decrypt()
        }
        
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)?.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)?.decrypt() == "NA" {
            mobileno = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                mobileno = English.NA
            } else if type == Constants.japanese {
                mobileno = Japanese.NA
            }
        }
        else
        {
            mobileno = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt()
        }
        
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)?.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)?.decrypt() == "NA" {
            temperature = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                temperature = English.NA
            } else if type == Constants.japanese {
                temperature = Japanese.NA
            }
        }
        else
        {
            temperature = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus")) == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "NA" {
            acStatus = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acStatus = English.NA
            } else if type == Constants.japanese {
                acStatus = Japanese.NA
            }
        } else {
            acStatus = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel")) == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "-" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "NA" {
            fuel = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                fuel = English.NA
            } else if type == Constants.japanese {
                fuel = Japanese.NA
            }
        } else {
            fuel = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration")) == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "-" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "NA" {
            duration = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                duration = English.NA
            } else if type == Constants.japanese {
                duration = Japanese.NA
            }
        } else {
            duration = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt()
        }
        
        uiTableView.deselectRow(at: indexPath, animated: true)
        let mapDetails = MapModel().addMapDetailsWith(
            currentlat: currentlat,
            currentlng: currentlng,
            vehicleno: vehicleno,
            vehiclename: vehiclename,
            drivername: drivername,
            mobileno: mobileno,
            temperature: temperature,
            acstatus: acStatus,
            fuel: fuel,
            duration: duration,
            trackTime: trackTime,
            speed: speed,
            location: location,
            ignition: ignition,
            unitno: "",
            alerttypeid: "",
            videoPresent: "",
            videoUrlListArray: [],
            comingFrom: COMING_TYPE.VEHICLE_LIST)
        output.navigateToMapViewController(mapDetails: mapDetails)
    }
        
    /// This method is used to GetVehicleStatusList details with api.
    func GetVehicleStatusList() {
        
        var baseUrl:String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        let parameters = [
            "userid" : LoginID.encrypt(),
            "listtype" : StatusID.encrypt()
            ]
        print("\(baseUrl)"+"GetVehicleStatusList")
        print(parameters)
        AF.request("\(baseUrl)"+"GetVehicleStatusList", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let sts = JSON.object(forKey: "status") as? NSString {
                        self.status = (sts as String).decrypt()
                    }
                    if self.status == "success" {
                        self.mainDataArray = JSON.object(forKey: "data") as! NSArray
                        self.workingArray = self.mainDataArray
                        self.finalArray = self.mainDataArray
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                            self.uiTableView.reloadData()
                            self.statusLabel.text = "\(self.StatusName) \(NSLocalizedString("Vehicle List", comment: "")) ( \(self.finalArray.count) )"
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            var displayStatus = ""
                            switch self.StatusName {
                            case "moving" :
                                if type == Constants.english {
                                    displayStatus = English.Moving
                                } else if type == Constants.japanese {
                                    displayStatus = Japanese.Moving
                                }
                                break
                            case "idle" :
                                if type == Constants.english {
                                    displayStatus = English.Idle
                                } else if type == Constants.japanese {
                                    displayStatus = Japanese.Idle
                                }
                                break
                            case "stopped" :
                                if type == Constants.english {
                                    displayStatus = English.Stopped
                                } else if type == Constants.japanese {
                                    displayStatus = Japanese.Stopped
                                }
                                break
                            case "inactive" :
                                if type == Constants.english {
                                    displayStatus = English.Inactive
                                } else if type == Constants.japanese {
                                    displayStatus = Japanese.Inactive
                                }
                                break
                            case "outofservice" :
                                if type == Constants.english {
                                    displayStatus = English.Out_Of_Service
                                } else if type == Constants.japanese {
                                    displayStatus = Japanese.Out_Of_Service
                                }
                                break
                            case "notpolling" :
                                if type == Constants.english {
                                    displayStatus = English.Not_Polling
                                } else if type == Constants.japanese {
                                    displayStatus = Japanese.Not_Polling
                                }
                                break
                            default:
                                break
                            }
                            if type == Constants.english {
                                self.statusLabel.text = "\(displayStatus) \(English.Vehicle_List) ( \(self.finalArray.count) )"
                            } else if type == Constants.japanese {
                                self.statusLabel.text = "\(displayStatus) \(Japanese.Vehicle_List) ( \(self.finalArray.count) )"
                            }
                        })
                    } else if self.status == "failure" {
                        if let msg = JSON.object(forKey: "message") as? NSString {
                            self.message = (msg as String).decrypt()
                        }
                        print(self.message)
                        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                        if self.message == English.No_Records_found {
                            if type == Constants.english {
                                self.message = English.No_Records_found
                            } else if type == Constants.japanese {
                                self.message = Japanese.No_Records_found
                            }
                        }
                        self.showAlert("", message: self.message)
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                        })
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}
