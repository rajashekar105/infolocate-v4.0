import Foundation

protocol VehicleListViewOutput {
    func back()
    func navigateToMapViewController(mapDetails: MapModel)
}
