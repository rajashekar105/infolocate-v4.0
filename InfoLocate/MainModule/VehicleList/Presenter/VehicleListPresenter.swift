import Foundation

class VehicleListPresenter: VehicleListModuleInput, VehicleListViewOutput, VehicleListInteractorOutput {
    
    weak var view: VehicleListViewInput!
    var interactor: VehicleListInteractorInput!
    var router: VehicleListRouterInput!
    
    // MARK: - VehicleListViewOutput
    
    func back() {
        router.back()
    }
    func navigateToMapViewController(mapDetails: MapModel) {
        router.navigateToMapViewController(mapDetails: mapDetails)
    }
    
    // MARK: - VehicleListModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - VehicleListInteractorOutput
    
}
