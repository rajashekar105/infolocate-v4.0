import UIKit

class DashboardRouter:DashboardRouterInput {
    
    weak var viewController: DashboardViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToVehicleListScreen() {
        Navigation.navigateToVehicleListScreen(viewController: viewController)
    }
    
    func navigateToAlertScreen() {
        Navigation.navigateToRecentAlertsScreen(viewController: viewController)
    }
    
    func navigateToLiveVehicleListScreen() {
        Navigation.navigateToLiveVehicleListScreen(viewController: viewController)
    }
    
    func navigateToLoginScreen() {
        Navigation.navigateToLoginScreen(viewController: viewController)
    }
}
