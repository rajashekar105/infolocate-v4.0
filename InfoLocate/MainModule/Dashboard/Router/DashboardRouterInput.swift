import Foundation

protocol DashboardRouterInput {
    func back()
    func navigateToVehicleListScreen()
    func navigateToLiveVehicleListScreen()
    func navigateToAlertScreen()
    func navigateToLoginScreen()
}
