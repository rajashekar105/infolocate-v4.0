import UIKit
import Alamofire
import UserNotifications
import Foundation

class DashboardViewController: BaseViewController, DashboardViewInput, UITableViewDelegate, UITableViewDataSource {
    
    /// This property is output for DashboardViewOutput.
    var output: DashboardViewOutput!
    /// This property is LoginID.
    var LoginID: String = ""
    /// This property is loginStatus.
    var loginStatus: String = ""
    /// This property is loginMessage.
    var loginMessage: String = ""
    /// This property is keyArray.
    var keyArray: [String] = []
    /// This property is valueArray.
    var valueArray: [String] = []
    /// This property is totalCount.
    var totalCount: String = ""
    /// This property is statusnameArrayDisplay.
    var statusnameArrayDisplay: [String] = []
    /// This property is statusnameArray.
    var statusnameArray: [String] = []
    /// This property is statusnameArray.
    var statusCountArray: [String] = []
    /// This property is statusnameArray.
    var percentageArray: [String] = []
    /// This property is statusIdArray.
    var statusIdArray: [String] = []
    /// This property is statusColorArray.
    var statusColorArray: [UIColor] = []
    /// This property is statusImageViewArray.
    var statusImageViewArray: [String] = []
    /// This property is colors.
//    var colors: [UIColor] = []
    /// This property is listType.
    var listType: String = ""
    /// This property is userName.
    @IBOutlet weak var userName: UILabel!
    /// This property is totalCountLabel.
    @IBOutlet var totalCountLabel: UILabel!
    /// This property is uiTableView.
    @IBOutlet var uiTableView: UITableView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is scrollView.
    @IBOutlet var scrollView: UIScrollView!
    /// This property is view1.
    @IBOutlet var detailsView: UIView!
    /// This property is daySummaryLabel.
    @IBOutlet var daySummaryLabel: UILabel!
    /// This property is alertView.
    @IBOutlet var alertView: UIView!
    /// This property is alertCountLabel.
    @IBOutlet var alertCountLabel: UILabel!
    /// This property is tripsView.
    @IBOutlet var tripsView: UIView!
    /// This property is tripsLabel.
    @IBOutlet var tripsLabel: UILabel!
    /// This property is tripsCountLabel.
    @IBOutlet var tripsCountLabel: UILabel!
    /// This property is distanceView.
    @IBOutlet var distanceView: UIView!
    /// This property is distanceLabel.
    @IBOutlet var distanceLabel: UILabel!
    /// This property is distanceCountLabel.
    @IBOutlet var distanceCountLabel: UILabel!
    /// This property is distanceCountLabel.
    @IBOutlet var liveVideoButton: UIButton!
    /// This property is view.
    @IBOutlet var titleView: UIView!
    /// This property is view1.
    @IBOutlet var view1: UIView!
    /// This property is view2.
    @IBOutlet var view2: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    var timer : Timer?
    var currentPage : Int = 0
    var adsImagesArray : NSArray!
    var imageDataArray = NSMutableArray()
    let previousimageview = UIImageView()
    let nextimageview = UIImageView()
    /// This property is alertNavigation.
    var alertNavigation = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getImages()
        setScrollView(showView: detailsView)
//        setGradientBackground(setView: titleView)
//        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
//        print(appVersion)
//        Defaults.setValueforKey(value: Constants.english, for: Constants.language_Key)
        print("Welcome \(defaults.object(forKey: "username") as? String ?? "")")
        userName.text = "\(defaults.object(forKey: "username") as? String ?? "")".capitalized
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = NSLocalizedString("Cancel", comment: "")
        totalCountLabel.text = NSLocalizedString("Fleet Total: 0", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            totalCountLabel.text = English.Fleet_Total + ": 0"
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = English.Cancel
        } else if type == Constants.japanese {
            totalCountLabel.text = Japanese.Fleet_Total + ": 0"
            totalCountLabel.font = UIFont.boldSystemFont(ofSize: 15.0) //.systemFont(ofSize: 11.0)
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = Japanese.Cancel
        }
        let instanceOfCustomObject = CustomObject()
        instanceOfCustomObject.someProperty = "Hello World"
        print(instanceOfCustomObject.someProperty!)
        print(instanceOfCustomObject.someMethod()!)
    }
    
    override func viewWillAppear(_ animated: Bool) {

        if let alert = defaults.object(forKey: "AlertNavigation") {
            alertNavigation = alert as! String
        }
        print("AlertNavigation = \(alertNavigation)")
        if alertNavigation == "" {
            defaults.setValue("false", forKey: "isFiltered")
            keyArray = []
            valueArray = []
            totalCount  = ""
            statusnameArrayDisplay = []
            statusnameArray = []
            statusIdArray = []
            statusColorArray = []
//            colors = []
            listType = ""
            activity.layer.cornerRadius = 10
            if Reachability.connectedToNetwork() == false
            {
                var title = NSLocalizedString("No Internet Connection", comment: "")
                var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    title = English.No_Internet_Connection
                    message = English.Make_sure_your_device_is_connected_to_the_internet
                } else if type == Constants.japanese {
                    title = Japanese.No_Internet_Connection
                    message = Japanese.Make_sure_your_device_is_connected_to_the_internet
                }
                let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                
                let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                if let ID = defaults.value(forKey: "LoginID") {
                    LoginID = ID as! String
                }
                GetVehicleStats()
                activity.startAnimating()
//                uiTableView.isHidden = true
//                chartView.isHidden = true
                view.isUserInteractionEnabled = false
            }
            uiTableView.delegate = self
            uiTableView.dataSource = self
            let tblView =  UIView(frame: CGRect.zero)
            uiTableView.tableFooterView = tblView
//            uiTableView.backgroundColor = UIColor.white
            shadowView_GrayColor(view: alertView, radius: 5)
            shadowView_GrayColor(view: tripsView, radius: 5)
            shadowView_GrayColor(view: distanceView, radius: 5)
            shadowView_GrayColor(view: view2, radius: 5)
            statusnameArrayDisplay = [NSLocalizedString("Moving", comment: ""),
                                      NSLocalizedString("Idle", comment: ""),
                                      NSLocalizedString("Stopped", comment: ""),
                                      NSLocalizedString("Inactive", comment: ""),
                                      NSLocalizedString("Out Of Service", comment: ""),
                                      NSLocalizedString("Not Polling", comment: "")]
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                statusnameArrayDisplay = [English.Moving,
                                          English.Idle,
                                          English.Stopped,
                                          English.Inactive,
                                          English.Out_Of_Service,
                                          English.Not_Polling]
            } else if type == Constants.japanese {
                statusnameArrayDisplay = [Japanese.Moving,
                                          Japanese.Idle,
                                          Japanese.Stopped,
                                          Japanese.Inactive,
                                          Japanese.Out_Of_Service,
                                          Japanese.Not_Polling]
            }
            statusImageViewArray = ["moving","idle","stopped","inactive","outofservice","notpolling"]
            statusnameArray = ["moving", "idle", "stopped", "inactive", "outofservice","notpolling"]
            //statusCountArray = ["0","0","0","0","0","0"]
            statusIdArray = ["M", "I", "S", "IA", "IO","NP"]
            statusColorArray = [Color.DASHBOARD_MOVING_COLOR, Color.DASHBOARD_IDLE_COLOR, Color.DASHBOARD_STOPPED_COLOR, Color.DASHBOARD_INACTIVE_COLOR, Color.DASHBOARD_OUTOFSERVICE_COLOR, Color.DASHBOARD_NOTPOLLING_COLOR]
            let fcmenabled = self.defaults.object(forKey: "fcmenabled") as Any as! Int
            if fcmenabled == 0 {
                // do nothing
            } else if fcmenabled == 1 {
                self.updateMobileAppVersionToServer()
            }
        } else {
            self.output.navigateToAlertScreen()
        }
    }
    
    func setGradientBackground(setView: UIView) {
        let colorTop = UIColor.darkGray.cgColor//UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
        let colorMiddle =  Color.DASHBOARD_INACTIVE_COLOR.cgColor//UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor.darkGray.cgColor//UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop,colorMiddle,colorBottom]
        gradientLayer.locations = [0.0, 0.5, 1.0]
        gradientLayer.frame = CGRect(x: setView.bounds.origin.x, y: setView.bounds.origin.y, width: self.view.bounds.size.width, height: setView.bounds.height)
                
        setView.layer.insertSublayer(gradientLayer, at:0)
    }
    
    /// This method is used for updateMobileAppVersionToServer with api.
    func updateMobileAppVersionToServer() {
        
        var baseUrl:String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let parameters = [
            "userid" : LoginID.encrypt(),
            "appVersion" : "3.0".encrypt(),
            "appSubVersion" : appVersion.encrypt()
        ]
        print("\(baseUrl)"+"UpdateMobileAppVersion")
        //            print("http://5.195.195.181/FCMAPI/ItlService.svc/UpdateMobileAppVersion")
        print(parameters)
        AF.request("\(baseUrl)"+"UpdateMobileAppVersion", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                return
            }
            print(response.request ?? "")
            print(response)
            
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let status = JSON.object(forKey: "status") as? NSString {
                        self.loginStatus = (status as String).decrypt()
                    }
                    if self.loginStatus == "success" {
                     
                    } else if self.loginStatus == "failure" {
                        // do nothing.
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is menu button action.
    @IBAction func menuButtonAction(_ sender: UIButton) {
        openSideMenu()
    }
    
    /// This method is refresh button action.
    @IBAction func refreshButton(_ sender: AnyObject) {
        if self.activity.isAnimating == true {
            // do nothing.
        } else {
            viewWillAppear(true)
        }
    }
    
    @IBAction func callButtonAction(_ sender: AnyObject) {
        self.call()
    }
    
    @IBAction func emailButtonAction(_ sender: AnyObject) {
        self.sendEmail()
    }
    
    @IBAction func liveVideoButtonAction(_ sender: AnyObject) {
        output.navigateToLiveVehicleListScreen()
    }
    
    @IBAction func logoutButtonAction(_ sender: AnyObject) {
        defaults.setValue("false", forKey: "isFiltered")
        var title = NSLocalizedString("Are you sure want to logout?", comment: "")
        var cancelTitle = NSLocalizedString("CANCEL", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            title = English.Are_you_sure_want_to_logout
            cancelTitle = English.CANCEL
        } else if type == Constants.japanese {
            title = Japanese.Are_you_sure_want_to_logout
            cancelTitle = Japanese.CANCEL
        }
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
            UIAlertAction in
            let fcmenabled = self.defaults.object(forKey: "fcmenabled") as Any as! Int
            if fcmenabled == 0 {
                self.defaults.set("false", forKey: "userLogin")
                self.defaults.setValue("", forKey: "LoginID")
                self.output.navigateToLoginScreen()
            } else if fcmenabled == 1 {
                self.activity.startAnimating()
                self.appLogoutToServer()
            }
        }
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default) {
            UIAlertAction in
//            self.output.back()
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
   
    /// This method is used for appLogoutToServer with api.
    func appLogoutToServer() {
        var LoginID:String = ""
        var baseUrl:String = ""
        //            let user: String = userName.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        if let ID = defaults.value(forKey: "LoginID") {
            LoginID = ID as! String
        }
        let parameters = [
            "userid" : LoginID.encrypt()
        ]
        print("\(baseUrl)"+"Logout")
        //            print("http://5.195.195.181/FCMAPI/ItlService.svc/UpdateMobileAppVersion")
        print(parameters)
        AF.request("\(baseUrl)"+"Logout", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            print(response.request ?? "")
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let status = JSON.object(forKey: "status") as? NSString {
                        self.loginStatus = (status as String).decrypt()
                    }
                    if self.loginStatus == "success" {
                        self.defaults.set("false", forKey: "userLogin")
                        self.defaults.setValue("", forKey: "LoginID")
                        self.output.navigateToLoginScreen()
                        self.activity.stopAnimating()
                    } else
                        if self.loginStatus == "failure" {
                            if let message = JSON.object(forKey: "message") as? NSString {
                                self.loginMessage = (message as String).decrypt()
                            }
                            self.showAlert("" , message: self.loginMessage)
                            self.activity.stopAnimating()
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func setScrollView(showView: UIView) {
       // scrollViewYPositionContent = 0
        showView.frame = CGRect(x: 0, y: 0, width: scrollView.frame.size.width, height: showView.bounds.size.height
        )
//        if showView == display_DeliveryDetailsView {
//             scrollView.contentSize.height = 1890.0
//        } else {
            scrollView.contentSize.height = showView.bounds.size.height
//        }
        scrollView.addSubview(showView)
    }
    
//    /// This method is used to get chartValueSelected and navigate to VehicleListScreen.
//    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
//        print(entry.description)
//        print(highlight.x)
////        defaults.set(chartStatusIdArray[Int(highlight.x)], forKey: "StatusID")
////        defaults.set(chartStatusnameArray[Int(highlight.x)], forKey: "StatusName")
//        output.navigateToVehicleListScreen()
//    }
    
    func getImages() {
//    func hitApiResponse(dic: NSDictionary) {
//        //        print(dic)
//
//        let apiresponse = dic
//        let dataArr : NSArray = (apiresponse["DataBanner"] as? NSArray)!
//        let data1 = dataArr[0] as! NSDictionary
//        let data2 = dataArr[1] as! NSDictionary
//        let data3 = dataArr[2] as! NSDictionary
//        let data4 = dataArr[3] as! NSDictionary
//        let data5 = dataArr[4] as! NSDictionary
//        let path1 = data1["ImagePath"] as! String
//        let path2 = data2["ImagePath"] as! String
//        let path3 = data3["ImagePath"] as! String
//        let path4 = data4["ImagePath"] as! String
//        let path5 = data5["ImagePath"] as! String
//        self.imageDataArray = [path1, path2, path3, path4, path5]
//        let imageUrlArray: NSMutableArray = [path1, path2, path3, path4, path5]
//
//        for n in 0...4 {
//            let path = imageUrlArray.object(at: n) as! String
//            print(path)
//            print(path1)
//            DispatchQueue.global(qos: .userInitiated).async {
//                guard let imUrl = URL(string: path) else {
//                    return
//                }
//                print(imUrl)
//                let imageData:NSData = NSData(contentsOf: imUrl)!
//                //            let imageView = UIImageView(frame: CGRect(x:0, y:0, width:200, height:200))
//                //            imageView.center = self.view.center
//
//                // When from background thread, UI needs to be updated on main_queue
//                DispatchQueue.main.async {
//                    let image = UIImage(data: imageData as Data)
//                    self.imageView.image = image
//                    self.imageDataArray.insert(image!, at: n)
//                    print("done")
//                }
//            }
//        }
        for n in 0...4 {
            imageDataArray.insert(UIImage(named: "loginscreen")!, at: n)
        }
        createpageimage()
    }
    
    func createpageimage() {
        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector:#selector(DashboardViewController.moveToNextPage), userInfo: nil, repeats: true)
        self.pageControl.currentPage = 0
        pageControl.numberOfPages = 5
        imageView.transform = CGAffineTransform(scaleX: 0.8, y: 1);
        imageView.isUserInteractionEnabled = true
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(DashboardViewController.swiped(_:)))
        leftSwipe.direction = .left
        imageView.addGestureRecognizer(leftSwipe)
        let rightSwipe = UISwipeGestureRecognizer(target: self, action:#selector(DashboardViewController.swiped(_:)))
        rightSwipe.direction = .right
        imageView.addGestureRecognizer(rightSwipe)
        
        previousimageview.frame = CGRect(x: -10-self.imageView.bounds.size.width, y: 10, width: self.imageView.bounds.size.width, height: self.imageView.bounds.size.height-20)
        previousimageview.layer.masksToBounds = true
        previousimageview.isOpaque = true
        previousimageview.alpha = 0.5
        self.imageView.addSubview(previousimageview)
        
        nextimageview.frame = CGRect(x: self.imageView.bounds.size.width+10, y: 10, width: self.imageView.bounds.size.width, height: self.imageView.bounds.size.height-20)
        nextimageview.layer.masksToBounds = true
        nextimageview.isOpaque = true
        nextimageview.alpha = 0.5
        self.imageView.addSubview(nextimageview)
        
    }
    
    @objc func swiped(_ gesture : UISwipeGestureRecognizer){
        
        if gesture.direction == .left{
            
            currentPage += 1
            if currentPage > imageDataArray.count-1{
                currentPage = 0
            }
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.fillMode = CAMediaTimingFillMode.removed
            imageView.layer.add(transition, forKey:nil)
        }else if gesture.direction == .right{
            currentPage -= 1
            if currentPage < 0 {
                currentPage = imageDataArray.count-1
            }
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            transition.fillMode = CAMediaTimingFillMode.removed
            imageView.layer.add(transition, forKey:nil)
        }
//        moveToNextPage()
        if currentPage > imageDataArray.count-1 || currentPage < 0{
//            currentPage = 0
            previousimageview.image = self.imageDataArray.object(at: imageDataArray.count-1) as? UIImage
            self.imageView.image = self.imageDataArray.object(at: currentPage) as? UIImage
            nextimageview.image = self.imageDataArray.object(at: currentPage+1) as? UIImage
        } else {
//            currentPage += 1
            if currentPage == imageDataArray.count-1 {
                previousimageview.image = self.imageDataArray.object(at: currentPage-1) as? UIImage
                self.imageView.image = self.imageDataArray.object(at: currentPage) as? UIImage
                nextimageview.image = self.imageDataArray.object(at: currentPage) as? UIImage
            } else if currentPage == 0 {
                previousimageview.image = self.imageDataArray.object(at: imageDataArray.count-1) as? UIImage
                self.imageView.image = self.imageDataArray.object(at: currentPage) as? UIImage
                nextimageview.image = self.imageDataArray.object(at: currentPage+1) as? UIImage
            } else {
                previousimageview.image = self.imageDataArray.object(at: currentPage-1) as? UIImage
                self.imageView.image = self.imageDataArray.object(at: currentPage) as? UIImage
                nextimageview.image = self.imageDataArray.object(at: currentPage+1) as? UIImage
            }
            
        }
        self.pageControl.currentPage = self.currentPage
        
    }
    
    @objc func moveToNextPage (){
        if currentPage >= imageDataArray.count-1 || currentPage < 0{
            currentPage = 0
            previousimageview.image = self.imageDataArray.object(at: imageDataArray.count-1) as? UIImage
            self.imageView.image = self.imageDataArray.object(at: currentPage) as? UIImage
            nextimageview.image = self.imageDataArray.object(at: currentPage+1) as? UIImage
        }else{
            currentPage += 1
            if currentPage == imageDataArray.count-1 {
                previousimageview.image = self.imageDataArray.object(at: currentPage-1) as? UIImage
                self.imageView.image = self.imageDataArray.object(at: currentPage) as? UIImage
                nextimageview.image = self.imageDataArray.object(at: currentPage) as? UIImage
            } else {
                previousimageview.image = self.imageDataArray.object(at: currentPage-1) as? UIImage
                self.imageView.image = self.imageDataArray.object(at: currentPage) as? UIImage
                nextimageview.image = self.imageDataArray.object(at: currentPage+1) as? UIImage
            }
            
        }
        self.pageControl.currentPage = self.currentPage
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.fillMode = CAMediaTimingFillMode.removed
        imageView.layer.add(transition, forKey:nil)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusCountArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DashBoardTableViewCell
        tableView.separatorStyle = .none
//        cell.colorView.layer.cornerRadius = 13
        shadowView_GrayColor(view: cell.backView, radius: 0)
        cell.colorView.backgroundColor = statusColorArray[indexPath.row]
        cell.statusImageView.image = UIImage(named: statusImageViewArray[indexPath.row])
        cell.nameLabel.text = statusnameArrayDisplay[indexPath.row]
        cell.countLabel.text = statusCountArray[indexPath.row]
        cell.percentLabel.text = percentageArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listType = statusIdArray[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        defaults.set(statusIdArray[indexPath.row], forKey: "StatusID")
        defaults.set(statusnameArray[indexPath.row], forKey: "StatusName")
        output.navigateToVehicleListScreen()
    }
    
    /// This method is used to GetVehicleStats with api.
    func GetVehicleStats() {
        var baseUrl:String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        print(LoginID)
        let parameters = [
            "userid" : LoginID.encrypt(),
        ]
        print("\(baseUrl)"+"GetVehicleStats")
        print(parameters)
        AF.request("\(baseUrl)"+"GetVehicleStats", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                print(parameters)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                return
            }
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    
                    if let status = JSON.object(forKey: "status") as? NSString {
                        self.loginStatus = (status as String).decrypt()
                    }
                    if self.loginStatus == "success" {
                        DispatchQueue.main.async(execute: {
                            if let count = JSON.object(forKey: "total") as? String
                            {
                                self.totalCount = count.decrypt()
                            }
                            for (key, value) in JSON
                            {
                                self.keyArray.append(String(describing: key))
                                self.valueArray.append(String(describing: value))
                            }
                            self.statusCountArray.removeAll()
                            self.percentageArray.removeAll()
                            for i in 0 ..< self.statusnameArray.count
                            {
                                if self.keyArray.contains(self.statusnameArray[i])
                                {
                                    let indexOfA = self.keyArray.firstIndex(of: self.statusnameArray[i])
                                    self.statusCountArray.append(self.valueArray[indexOfA!].decrypt())
                                    let per = Int(((Float(self.valueArray[indexOfA!].decrypt())! * 100 ) / Float(self.totalCount)!).rounded(.toNearestOrEven))
                                    self.percentageArray.append("\(per)%")
                                }
                            }
                            if self.statusCountArray.count == 6 {
                                self.detailsView.frame.size.height = 494 + 55
                                self.scrollView.contentSize.height = 494 + 55
                            }
                            
                            self.totalCountLabel.text = "\(NSLocalizedString("Fleet Total", comment: "") ) : \(self.totalCount)"
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.totalCountLabel.text = "\(English.Fleet_Total) : \(self.totalCount)"
                            } else if type == Constants.japanese {
                                self.totalCountLabel.text = "\(Japanese.Fleet_Total) : \(self.totalCount)"
                            }
                            
//                            self.uiTableView.isHidden = false
//                            self.chartView.isHidden = false
                            self.uiTableView.reloadData()
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                        })
                    }
                    else
                        if self.loginStatus == "failure" {
                            if let message = JSON.object(forKey: "message") as? NSString {
                                self.loginMessage = (message as String).decrypt()
                            }
                            self.showAlert("" , message: self.loginMessage)
                            DispatchQueue.main.async(execute: {
                                self.activity.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                            })
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}

