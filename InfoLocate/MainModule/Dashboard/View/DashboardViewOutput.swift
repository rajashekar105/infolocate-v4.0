import Foundation

protocol DashboardViewOutput {
    func back()
    func navigateToVehicleListScreen()
    func navigateToLiveVehicleListScreen()
    func navigateToAlertScreen()
    func navigateToLoginScreen()
}
