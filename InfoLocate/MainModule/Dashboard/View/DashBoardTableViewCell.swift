//
//  DashBoardTableViewCell.swift
//  InfoLocateFMS
//
//  Created by infotrack telematics on 04/07/16.
//  Copyright © 2016 InfotrackTelematics. All rights reserved.
//

import UIKit

class DashBoardTableViewCell: UITableViewCell {
    /// This property is colorView.
    @IBOutlet var backView: UIView!
    /// This property is colorView.
    @IBOutlet var colorView: UIView!
    /// This property is percentLabel..
    @IBOutlet var percentLabel: UILabel!
    /// This property is statusImageView.
    @IBOutlet var statusImageView: UIImageView!
    /// This property is nameLabel.
    @IBOutlet var nameLabel: UILabel!
    /// This property is countLabel.
    @IBOutlet var countLabel: UILabel!
    
    override func awakeFromNib() {
       
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
