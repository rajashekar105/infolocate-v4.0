import Foundation

protocol DashboardModuleInput: class {
    
    func configureModule()
}
