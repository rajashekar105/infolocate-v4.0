import UIKit

@objc class DashboardModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = DashboardControllerFromStoryboard()
        
        let router = DashboardRouter()
        router.viewController = viewController
        
        let presenter = DashboardPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = DashboardInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func DashboardControllerFromStoryboard() -> DashboardViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.DASHBOARD_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! DashboardViewController
    }
    
}
