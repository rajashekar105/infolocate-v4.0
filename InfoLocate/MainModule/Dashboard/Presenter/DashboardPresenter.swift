import Foundation

class DashboardPresenter: DashboardModuleInput, DashboardViewOutput, DashboardInteractorOutput {
    
    weak var view: DashboardViewInput!
    var interactor: DashboardInteractorInput!
    var router: DashboardRouterInput!
    
    // MARK: - DashboardViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToVehicleListScreen() {
        router.navigateToVehicleListScreen()
    }
    
    func navigateToLiveVehicleListScreen() {
        router.navigateToLiveVehicleListScreen()
    }
    
    func navigateToAlertScreen() {
        router.navigateToAlertScreen()
    }
    
    func navigateToLoginScreen() {
        router.navigateToLoginScreen()
    }
    
    // MARK: - DashboardModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - DashboardInteractorOutput
    
}
