import Foundation

class LiveVehicleListPresenter: LiveVehicleListModuleInput, LiveVehicleListViewOutput, LiveVehicleListInteractorOutput {
    
    weak var view: LiveVehicleListViewInput!
    var interactor: LiveVehicleListInteractorInput!
    var router: LiveVehicleListRouterInput!
    
    // MARK: - LiveVehicleListViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToLiveVideoListViewController(liveVehicleDetails: LiveVehicleListModel) {
        router.navigateToLiveVideoListViewController(liveVehicleDetails: liveVehicleDetails)
    }
    
    // MARK: - LiveVehicleListModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - LiveVehicleListInteractorOutput
    
}
