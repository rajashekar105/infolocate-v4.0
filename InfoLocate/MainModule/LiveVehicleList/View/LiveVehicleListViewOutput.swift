import Foundation

protocol LiveVehicleListViewOutput {
    func back()
    func navigateToLiveVideoListViewController(liveVehicleDetails: LiveVehicleListModel)
}
