import UIKit
import Alamofire

class LiveVehicleListViewController: BaseViewController, LiveVehicleListViewInput, UITableViewDelegate, UITableViewDataSource {

    var output: LiveVehicleListViewOutput!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is noRecordsLabel.
    @IBOutlet var noRecordsLabel: UILabel!
    /// This property is liveVehicleListTableView.
    @IBOutlet var liveVehicleListTableView: UITableView!
    /// This property is view2.
    @IBOutlet var view2: UIView!
    /// This property is titleLabel.
    @IBOutlet var titleLabel: UILabel!
    /// This property is LoginID.
    var LoginID: String = ""
    /// This property is finalArray.
    var finalArray = NSMutableArray()
    /// This property is vehicleNo.
    var vehicleNo: String = ""
    /// This property is dateTime.
    var dateTime: String = ""
    /// This property is location.
    var location: String = ""
    /// This property is mdvrunitno.
    var mdvrunitno: String = ""
    /// This property is timer.
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noRecordsLabel.text = NSLocalizedString("No records found.", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.noRecordsLabel.text = English.No_records_found
        } else if type == Constants.japanese {
            self.noRecordsLabel.text = Japanese.No_records_found
        }
        activity.layer.cornerRadius = 10
//        timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.autoRefreshList), userInfo: nil, repeats: true)
        //getLiveVehicleListDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.autoRefreshList), userInfo: nil, repeats: true)
        finalArray.removeAllObjects()
        getLiveVehicleListDetails()
        self.noRecordsLabel.isHidden = true
        self.titleLabel.text = NSLocalizedString("Live Vehicle List", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.titleLabel.text = English.Live_Vehicle_List
        } else if type == Constants.japanese {
            self.titleLabel.text = Japanese.Live_Vehicle_List
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer.invalidate()
    }
    
    /// This method is menu button action.
    @IBAction func menuButtonAction(_ sender: UIButton) {
        openSideMenu()
    }
    
    /// This method is back button action.
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
    @objc func autoRefreshList() {
//        viewWillAppear(true)
        finalArray.removeAllObjects()
        getLiveVehicleListDetails()
        self.noRecordsLabel.isHidden = true
    }
    
    /// This method is refreshButton action.
    @IBAction func refreshButton(_ sender: AnyObject) {
//        viewWillAppear(true)
        finalArray.removeAllObjects()
        getLiveVehicleListDetails()
        self.noRecordsLabel.isHidden = true
//        vehicleNo = ""
//        dateTime = ""
//        location = ""
//        mdvrunitno = "72080"
//        let liveVehicleDetails = LiveVehicleListModel().addLiveVehicleListDetailsWith(
//            vehicleNo: vehicleNo,
//            dateTime: dateTime,
//            location: location,
//            mdvrunitno: mdvrunitno)
//        output.navigateToLiveVideoListViewController(liveVehicleDetails: liveVehicleDetails)
    }
    
    /// This method is used to get LiveVehicleList Details.
    func getLiveVehicleListDetails() {
        
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            if let ID = defaults.value(forKey: "LoginID")
            {
                LoginID = ID as! String
            }
            getLiveVehicleList()
            activity.startAnimating()
            liveVehicleListTableView.isHidden = true
            view.isUserInteractionEnabled = false
        }
//        let tblView =  UIView(frame: CGRect.zero)
//        liveVehicleListTableView.tableFooterView = tblView
        liveVehicleListTableView.backgroundColor = UIColor.white
        liveVehicleListTableView.delegate = self
        liveVehicleListTableView.dataSource = self
    }
    
    /// This method is used to get LiveVehicleList with api.
    func getLiveVehicleList() {
        
        finalArray = []
        
        var baseUrl:String = ""
        
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        
        let parameters = [
            "userid" : LoginID.encrypt(),
            "listtype" : "M".encrypt()
        ]
        print("\(baseUrl)"+"GetVehicleStatusList")
        print(parameters)
        
        AF.request("\(baseUrl)"+"GetVehicleStatusList", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                //                    print(response.error!)
                self.view.isUserInteractionEnabled = true
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let sts = JSON.object(forKey: "status") as? NSString {
                        self.status = (sts as String).decrypt()
                    }
                    
                    if self.status == "success" {
                        var tempArray = NSArray()
                        tempArray = JSON.object(forKey: "data") as! NSArray
                        for i in tempArray {
                            let value = i as! NSObject
                            let unitno = ((i as! NSDictionary).object(forKey:"mdvrunitno") as? String ?? "").decrypt()
                            if unitno == "" || unitno == "null" {
                            } else {
                                self.finalArray.add(value)
                            }
                        }
                        //                            print(self.finalArray.count)
                        //                            self.finalArray = JSON.object(forKey: "data") as! NSArray
                        
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            
                            if self.finalArray.count == 0 {
                                self.noRecordsLabel.isHidden = false
                                self.liveVehicleListTableView.isHidden = true
                            } else {
                                self.noRecordsLabel.isHidden = true
                                self.liveVehicleListTableView.isHidden = false
                                self.liveVehicleListTableView.separatorStyle = .none
                            }
                            
                            self.liveVehicleListTableView.reloadData()
                        })
                        
                    } else if self.status == "failure" {
                        if let msg = JSON.object(forKey: "message") as? NSString {
                            self.message = (msg as String).decrypt()
                            print(self.message)
                        }
                        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                        if self.message == English.No_Records_found {
                            if type == Constants.english {
                                self.message = English.No_Records_found
                            } else if type == Constants.japanese {
                                self.message = Japanese.No_Records_found
                            }
                        }
                        self.showAlert("" , message: self.message)
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                        })
                    }
                }
                break
                
            case .failure(let error):
                print(error)
                self.view.isUserInteractionEnabled = true
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return finalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LiveVehicleListTableViewCell
        
        //        cell.colorView.layer.cornerRadius = 13
        //        cell.colorView.backgroundColor = colors[(indexPath as NSIndexPath).row]
        //        cell.nameLabel.text = pieChartNameArray[(indexPath as NSIndexPath).row]
        //        cell.countLabel.text = String(Int(pieChartcountArray[(indexPath as NSIndexPath).row]))
        cell.vehicleNoLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehicleno") as! String).decrypt()
        cell.dataTimeLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "datetime") as! String).decrypt()
        cell.locationLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as! String).decrypt()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        vehicleNo = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehicleno") as! String).decrypt()
        dateTime = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "datetime") as! String).decrypt()
        location = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as! String).decrypt()
        mdvrunitno = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mdvrunitno") as! String).decrypt()
        let liveVehicleDetails = LiveVehicleListModel().addLiveVehicleListDetailsWith(
            vehicleNo: vehicleNo,
            dateTime: dateTime,
            location: location,
            mdvrunitno: mdvrunitno)
        output.navigateToLiveVideoListViewController(liveVehicleDetails: liveVehicleDetails)
    }
    
}
