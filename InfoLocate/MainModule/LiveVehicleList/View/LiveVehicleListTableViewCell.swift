//
//  LiveVehicleListTableViewCell.swift
//  InfoLocate
//
//  Created by Infotrack on 09/04/20.
//  Copyright © 2020 Hemalatha T. All rights reserved.
//

import UIKit

class LiveVehicleListTableViewCell: UITableViewCell {

    /// This property is slnoLabel.
    @IBOutlet var slnoLabel: UILabel!
    /// This property is vehicleNoLabel.
    @IBOutlet var vehicleNoLabel: UILabel!
    /// This property is dataTimeLabel.
    @IBOutlet var dataTimeLabel: UILabel!
    /// This property is locationLabel.
    @IBOutlet var locationLabel: UILabel!
    /// This property is ViewBackground.
    @IBOutlet var ViewBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
