import Foundation

protocol LiveVehicleListModuleInput: class {
    
    func configureModule()
}
