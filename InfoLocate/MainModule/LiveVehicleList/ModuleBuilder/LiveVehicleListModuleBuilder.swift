import UIKit

@objc class LiveVehicleListModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = LiveVehicleListControllerFromStoryboard()
        
        let router = LiveVehicleListRouter()
        router.viewController = viewController
        
        let presenter = LiveVehicleListPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = LiveVehicleListInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func LiveVehicleListControllerFromStoryboard() -> LiveVehicleListViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.LIVE_VEHICLE_LIST_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! LiveVehicleListViewController
    }
    
}
