import Foundation

protocol LiveVehicleListRouterInput {
    func back()
    func navigateToLiveVideoListViewController(liveVehicleDetails: LiveVehicleListModel)
}
