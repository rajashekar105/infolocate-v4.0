import UIKit

class LiveVehicleListRouter:LiveVehicleListRouterInput {
    
    weak var viewController: LiveVehicleListViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToLiveVideoListViewController(liveVehicleDetails: LiveVehicleListModel) {
        Navigation.navigateToLiveVideoListScreen(liveVehicleDetails: liveVehicleDetails, viewController: viewController)
    }
}
