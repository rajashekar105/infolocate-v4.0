import Foundation

protocol LiveVideoListModuleInput: class {
    
    func configureModule(liveVehicleDetails: LiveVehicleListModel)
}
