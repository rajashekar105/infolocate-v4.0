import UIKit

@objc class LiveVideoListModuleBuilder: NSObject {
    
    func build(liveVehicleDetails: LiveVehicleListModel) -> UIViewController {
        let viewController = LiveVideoListControllerFromStoryboard()
        
        let router = LiveVideoListRouter()
        router.viewController = viewController
        
        let presenter = LiveVideoListPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = LiveVideoListInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(liveVehicleDetails: liveVehicleDetails)
        
        return viewController
    }
    
    func LiveVideoListControllerFromStoryboard() -> LiveVideoListViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.LIVE_VIDEO_LIST_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! LiveVideoListViewController
    }
    
    
    
}
