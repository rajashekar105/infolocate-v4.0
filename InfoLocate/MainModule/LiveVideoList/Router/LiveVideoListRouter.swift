import UIKit

class LiveVideoListRouter:LiveVideoListRouterInput {
    
    weak var viewController: LiveVideoListViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToLiveVideoScreen(mdvrUnitNo: String, channelId: Int32) {
        Navigation.navigateToLiveVideoScreen(mdvrUnitNo: mdvrUnitNo, channelId: channelId, viewController: viewController)
    }
    
}
