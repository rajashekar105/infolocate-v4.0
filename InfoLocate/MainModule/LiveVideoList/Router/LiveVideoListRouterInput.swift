import Foundation

protocol LiveVideoListRouterInput {
    func back()
    func navigateToLiveVideoScreen(mdvrUnitNo: String, channelId: Int32)
}
