import Foundation

protocol LiveVideoListViewInput: class {
    func configureModule(liveVehicleDetails: LiveVehicleListModel)
}
