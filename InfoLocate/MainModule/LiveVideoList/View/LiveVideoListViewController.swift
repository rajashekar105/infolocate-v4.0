import UIKit
import AVKit
import MobileVLCKit
import CryptoKit
import Alamofire
import CommonCrypto

// Defines types of hash string outputs available
public enum HashOutputType {
    // standard hex string output
    case hex
    // base 64 encoded string output
    case base64
}

// Defines types of hash algorithms available
public enum HashType {
    case md5
    case sha1
    case sha224
    case sha256
    case sha384
    case sha512

    var length: Int32 {
        switch self {
        case .md5: return CC_MD5_DIGEST_LENGTH
        case .sha1: return CC_SHA1_DIGEST_LENGTH
        case .sha224: return CC_SHA224_DIGEST_LENGTH
        case .sha256: return CC_SHA256_DIGEST_LENGTH
        case .sha384: return CC_SHA384_DIGEST_LENGTH
        case .sha512: return CC_SHA512_DIGEST_LENGTH
        }
    }
}

public extension String {

    /// Hashing algorithm for hashing a string instance.
    ///
    /// - Parameters:
    ///   - type: The type of hash to use.
    ///   - output: The type of output desired, defaults to .hex.
    /// - Returns: The requested hash output or nil if failure.
    func hashed(_ type: HashType, output: HashOutputType = .hex) -> String? {

        // convert string to utf8 encoded data
        guard let message = data(using: .utf8) else { return nil }
        return message.hashed(type, output: output)
    }
}

class LiveVideoListViewController: BaseViewController, LiveVideoListViewInput {

    /// This property is output for LiveVideoListViewOutput.
    var output: LiveVideoListViewOutput!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is view2.
    @IBOutlet var view2: UIView!
    /// This property is titleLabel.
    @IBOutlet weak var titleLabel: UILabel!
    /// This property is liveVideoCollectionView.
    @IBOutlet weak var liveVideoCollectionView : UICollectionView!
    /// This property is countBackgroundView.
    @IBOutlet weak var countBackgroundView: UIView!
    /// This property is countLabel.
    @IBOutlet weak var countLabel: UILabel!
    /// This property is downloadButton.
    @IBOutlet weak var downloadButton: UIButton!
    /// This property is pauseButton.
    @IBOutlet weak var pauseButton: UIButton!
    /// This property is captureButton.
    @IBOutlet weak var captureButton: UIButton!
    /// This property is liveVehicleDetails.
    var liveVehicleDetails: LiveVehicleListModel!
//    /// This property is player.
//    var player : AVPlayer!
//    /// This property is isPlaying.
//    var isPlaying = true
    /// This property is urlArray.
    var urlArray: NSArray = NSArray()
    /// This property is mdvrUnitNo.
    var mdvrUnitNo : String!
    /// This property is video1.
    var video1 = VLCMediaPlayer()
    var activity1: UIActivityIndicatorView!
    /// This property is video2.
    var video2 = VLCMediaPlayer()
    var activity2: UIActivityIndicatorView!
    /// This property is video3.
    var video3 = VLCMediaPlayer()
    var activity3: UIActivityIndicatorView!
    /// This property is video4.
    var video4 = VLCMediaPlayer()
    var activity4: UIActivityIndicatorView!
    /// This property is video5.
    var video5 = VLCMediaPlayer()
    var activity5: UIActivityIndicatorView!
    /// This property is video6.
    var video6 = VLCMediaPlayer()
    var activity6: UIActivityIndicatorView!
    /// This property is video7.
    var video7 = VLCMediaPlayer()
    var activity7: UIActivityIndicatorView!
    /// This property is video8.
    var video8 = VLCMediaPlayer()
    var activity8: UIActivityIndicatorView!
    var movieView1: UIView!
    var movieView2: UIView!
    var movieView3: UIView!
    var movieView4: UIView!
    var movieView5: UIView!
    var movieView6: UIView!
    var movieView7: UIView!
    var movieView8: UIView!
    /// This property is counter.
    var counter = 90
    /// This property is timer.
    var timer = Timer()
    /// This property viewAdded.
    var viewAdded = [false,false,false,false,false,false,false,false]
    var channalCount = 0
    
    
    func configureModule(liveVehicleDetails: LiveVehicleListModel) {
        self.liveVehicleDetails = liveVehicleDetails
        mdvrUnitNo = self.liveVehicleDetails.mdvrunitno
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countBackgroundView.layer.cornerRadius = 10
        
        self.titleLabel.text = "\(NSLocalizedString("Live Video", comment: "")) (" + liveVehicleDetails.vehicleNo + ")"
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.titleLabel.text = English.Live_Video + " (" + liveVehicleDetails.vehicleNo + ")"
        } else if type == Constants.japanese {
            self.titleLabel.text = Japanese.Live_Video + " (" + liveVehicleDetails.vehicleNo + ")"
        }
        self.registerCollectionViewCell()
        self.setCollectionViewFlowLayout()
        getTokenIdAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       
    }
    
    /// This method is back button action.
    @IBAction func back(_ sender: Any) {
        stopVideo()
        self.output.back()
    }
    
    func startVideo() {
        let count:Int = channalCount
        switch(count) {
        case 1 :
            activity1.startAnimating()
            self.video1.play()
            break
        case 2 :
            activity1.startAnimating()
            activity2.startAnimating()
            self.video1.play()
            self.video2.play()
            break
        case 3 :
            activity1.startAnimating()
            activity2.startAnimating()
            activity3.startAnimating()
            self.video1.play()
            self.video2.play()
            self.video3.play()
            break
        case 4 :
            activity1.startAnimating()
            activity2.startAnimating()
            activity3.startAnimating()
            activity4.startAnimating()
            self.video1.play()
            self.video2.play()
            self.video3.play()
            self.video4.play()
            break
        case 5 :
            activity1.startAnimating()
            activity2.startAnimating()
            activity3.startAnimating()
            activity4.startAnimating()
            activity5.startAnimating()
            self.video1.play()
            self.video2.play()
            self.video3.play()
            self.video4.play()
            self.video5.play()
            break
        case 6 :
            activity1.startAnimating()
            activity2.startAnimating()
            activity3.startAnimating()
            activity4.startAnimating()
            activity5.startAnimating()
            activity6.startAnimating()
            self.video1.play()
            self.video2.play()
            self.video3.play()
            self.video4.play()
            self.video5.play()
            self.video6.play()
            break
        case 7 :
            activity1.startAnimating()
            activity2.startAnimating()
            activity3.startAnimating()
            activity4.startAnimating()
            activity5.startAnimating()
            activity6.startAnimating()
            activity7.startAnimating()
            self.video1.play()
            self.video2.play()
            self.video3.play()
            self.video4.play()
            self.video5.play()
            self.video6.play()
            self.video7.play()
            break
        case 8 :
            activity1.startAnimating()
            activity2.startAnimating()
            activity3.startAnimating()
            activity4.startAnimating()
            activity5.startAnimating()
            activity6.startAnimating()
            activity7.startAnimating()
            activity8.startAnimating()
            self.video1.play()
            self.video2.play()
            self.video3.play()
            self.video4.play()
            self.video5.play()
            self.video6.play()
            self.video7.play()
            self.video8.play()
            break
        default:
            break
        }
    }
    

    /// This method is used to stop/pause video.
    func stopVideo() {
        let count:Int = channalCount
        switch(count) {
        case 1 :
            self.video1.stop()
            break
        case 2 :
            self.video1.stop()
            self.video2.stop()
            break
        case 3 :
            self.video1.stop()
            self.video2.stop()
            self.video3.stop()
            break
        case 4 :
            self.video1.stop()
            self.video2.stop()
            self.video3.stop()
            self.video4.stop()
            break
        case 5 :
            self.video1.stop()
            self.video2.stop()
            self.video3.stop()
            self.video4.stop()
            self.video5.stop()
            break
        case 6 :
            self.video1.stop()
            self.video2.stop()
            self.video3.stop()
            self.video4.stop()
            self.video5.stop()
            self.video6.stop()
            break
        case 7 :
            self.video1.stop()
            self.video2.stop()
            self.video3.stop()
            self.video4.stop()
            self.video5.stop()
            self.video6.stop()
            self.video7.stop()
            break
        case 8 :
            self.video1.stop()
            self.video2.stop()
            self.video3.stop()
            self.video4.stop()
            self.video5.stop()
            self.video6.stop()
            self.video7.stop()
            self.video8.stop()
            break
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is used to get LiveVehicleList Details.
    func getTokenIdAPI() {
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            activity.startAnimating()
            let mdvripaddress = self.defaults.value(forKey: "mdvripaddress")!
            let url:String = "http://\(mdvripaddress):9966/vss/user/apiLogin.action"
            let value = "admin"
            print(value.hashed(.md5)!)
            let parameters = [
                "username" : "admin",
                "password" : value.hashed(.md5)
            ]
            print(parameters)
            
            AF.request(url, method: .post, parameters: parameters).responseJSON { response in
                guard response.error == nil else {
                    // got an error in getting the data, need to handle it
                    //                    print(response.error!)
                    self.view.isUserInteractionEnabled = true
                    var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                    let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                    if type == Constants.english {
                        message = English.Something_went_wrong_Please_try_again_later
                    } else if type == Constants.japanese {
                        message = Japanese.Something_went_wrong_Please_try_again_later
                    }
                    self.showAlert("", message: message)
                    self.activity.stopAnimating()
                    return
                }
                print(response)
                switch response.result {
                case .success:
                    if let result = response.value {
                        let JSON = result as! NSDictionary
                        if let sts = JSON.object(forKey: "status") as? Int64 {
                            self.status = String(sts)
                        }
                        
                        if self.status == "10000" {
                            let data = JSON.object(forKey: "data") as! NSDictionary
                            let vsstoken = data.object(forKey:"token") as? String ?? ""
                            print(vsstoken)
                            self.defaults.setValue(vsstoken, forKey: "vsstoken")
                            
                            DispatchQueue.main.async(execute: {
                                self.activity.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                                self.configureURLPrepare()
                                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                                if type == Constants.english {
                                    self.channalCount = 8
                                } else if type == Constants.japanese {
                                    self.channalCount = 4
                                }
                                self.liveVideoCollectionView.reloadData()
                            })
                            
                        } else if self.status == "failure" {
                            if let msg = JSON.object(forKey: "message") as? NSString {
                                self.message = (msg as String).decrypt()
                                print(self.message)
                            }
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if self.message == English.No_Records_found {
                                if type == Constants.english {
                                    self.message = English.No_Records_found
                                } else if type == Constants.japanese {
                                    self.message = Japanese.No_Records_found
                                }
                            }
                            self.showAlert("" , message: self.message)
                            DispatchQueue.main.async(execute: {
                                self.activity.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                            })
                        }
                    }
                    break
                    
                case .failure(let error):
                    print(error)
                    self.view.isUserInteractionEnabled = true
                    break
                }
            }
        }
        
    }
    
    /// This method is used to configureURLPrepare.
    func configureURLPrepare() {
        let mdvripaddress = self.defaults.value(forKey: "mdvripaddress")!
        let vsstoken = self.defaults.value(forKey: "vsstoken")!
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            urlArray = ["http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_1_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_2_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_3_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_4_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_5_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_6_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_7_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_8_1"]
        } else if type == Constants.japanese {
            urlArray = ["http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_1_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_2_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_3_1",
                        "http://\(mdvripaddress):33122/live?\(vsstoken)_\(mdvrUnitNo as String)_4_1"]
            
//            urlArray = ["http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_20/72130/ch02_20210720_162843_162921_007.mp4",
//                        "http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_20/72130/ch02_20210720_162843_162921_007.mp4",
//                        "http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_20/72130/ch02_20210720_162843_162921_007.mp4",
//                        "http://124.153.111.204:9988/vssFiles/alarmRecord/2021_07_20/72130/ch02_20210720_162843_162921_007.mp4"]
        }
    }
    
    /// This method is used to registerCollectionViewCell.
    func registerCollectionViewCell() {
           liveVideoCollectionView.register(UINib(nibName: "LiveVideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LiveVideoCollectionViewCell")
    }
    
    /// This method is used to setCollectionViewFlowLayout.
    func setCollectionViewFlowLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        // let height = liveVideoCollectionView.bounds.height
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            layout.itemSize = CGSize(width: (width / 2) - 3, height: 160)
        } else if type == Constants.japanese {
            layout.itemSize = CGSize(width: (width / 2) - 3, height: UIScreen.main.bounds.height/2.30)
        }
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 2
        liveVideoCollectionView.collectionViewLayout = layout
    }

//    /// This method is called every time interval from the timer
//    @objc func timerAction() {
//        counter -= 1
//        if counter == 0 {
//            countBackgroundView.isHidden = true
//            countLabel.isHidden = true
//            timer.invalidate()
//            counter = 90
//            downloadStatus()
//        } else {
//            countBackgroundView.isHidden = false
//            countLabel.isHidden = false
//            countLabel.text = "\(counter)"
//        }
//        
//    }
    
//    /// This method is used to downloadVideos.
//    @IBAction func downloadVideos(_ sender: Any) {
//        downloadStatus()
//    }
//
//    /// This method is used to stopDownloading.
//    func stopDownloading() {
//        self.video1.stopRecord()
//        self.video2.stopRecord()
//        self.video3.stopRecord()
//        self.video4.stopRecord()
//        self.video5.stopRecord()
//        self.video6.stopRecord()
//        self.video7.stopRecord()
//        self.video8.stopRecord()
//        countBackgroundView.isHidden = true
//        countLabel.isHidden = true
//        timer.invalidate()
//        counter = 90
//    }
    
//    /// This method is used for downloadStatus.
//    func downloadStatus() {
//        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//        if (self.video1.isViewing()) {
//            if (!self.video1.isRecording()) {
//                self.video1.startRecord()
//                if type == Constants.english {
//                    self.alertForASec("", message: English.Start_recording)
//                } else if type == Constants.japanese {
//                    self.alertForASec("", message: Japanese.Start_recording)
//                }
//                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
//            } else {
//                self.video1.stopRecord()
//                if type == Constants.english {
//                    self.alertForASec("", message: English.Stop_recording)
//                } else if type == Constants.japanese {
//                    self.alertForASec("", message: Japanese.Stop_recording)
//                }
//                countBackgroundView.isHidden = true
//                countLabel.isHidden = true
//                timer.invalidate()
//                counter = 90
//            }
//        }
//        if (self.video2.isViewing()) {
//            if (!self.video2.isRecording()) {
//                self.video2.startRecord()
//            } else {
//                self.video2.stopRecord()
//            }
//        }
//        if (self.video3.isViewing()) {
//            if (!self.video3.isRecording()) {
//                self.video3.startRecord()
//            } else {
//                self.video3.stopRecord()
//            }
//        }
//        if (self.video4.isViewing()) {
//            if (!self.video4.isRecording()) {
//                self.video4.startRecord()
//            } else {
//                self.video4.stopRecord()
//            }
//        }
//        if (self.video5.isViewing()) {
//            if (!self.video5.isRecording()) {
//                self.video5.startRecord()
//            } else {
//                self.video5.stopRecord()
//            }
//        }
//        if (self.video6.isViewing()) {
//            if (!self.video6.isRecording()) {
//                self.video6.startRecord()
//            } else {
//                self.video6.stopRecord()
//            }
//        }
//        if (self.video7.isViewing()) {
//            if (!self.video7.isRecording()) {
//                self.video7.startRecord()
//            } else {
//                self.video7.stopRecord()
//            }
//        }
//        if (self.video8.isViewing()) {
//            if (!self.video8.isRecording()) {
//                self.video8.startRecord()
//            } else {
//                self.video8.stopRecord()
//            }
//        }
//
//    }
    
//    /// This method is used for pauseVideo action.
//    @IBAction func pauseVideo(_ sender: Any) {
//
//        if (!self.video1.isPlaying) {
//            self.video1.play()
////            activity1.startAnimating()
//            pauseButton.setImage(UIImage(named: "pause_circle"), for: .normal)
//        } else {
//            self.video1.pause()
////            activity1.stopAnimating()
//            pauseButton.setImage(UIImage(named: "play_circle"), for: .normal)
//        }
//        if (!self.video2.isPlaying) { self.video2.play() } else { self.video2.pause() }
//        if (!self.video3.isPlaying) { self.video3.play() } else { self.video3.pause() }
//        if (!self.video4.isPlaying) { self.video4.play() } else { self.video4.pause() }
//        if (!self.video5.isPlaying) { self.video5.play() } else { self.video5.pause() }
//        if (!self.video6.isPlaying) { self.video6.play() } else { self.video6.pause() }
//        if (!self.video7.isPlaying) { self.video7.play() } else { self.video7.pause() }
//        if (!self.video8.isPlaying) { self.video8.play() } else { self.video8.pause() }
//    }
    
//    /// This method is used for saveImagesFromVideos button action.
//    @IBAction func saveImagesFromVideos(_ sender: Any) {
//
//        if (self.video1.isViewing()) {
//            if (self.video1.savePngFile()) {
//                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//                if type == Constants.english {
//                    self.alertForASec("", message: English.Captured_preview_saved)
//                } else if type == Constants.japanese {
//                    self.alertForASec("", message: Japanese.Captured_preview_saved)
//                }
//            }
//        }
//        if (self.video2.isViewing()) { self.video2.savePngFile() }
//        if (self.video3.isViewing()) { self.video3.savePngFile() }
//        if (self.video4.isViewing()) { self.video4.savePngFile() }
//        if (self.video5.isViewing()) { self.video5.savePngFile() }
//        if (self.video6.isViewing()) { self.video6.savePngFile() }
//        if (self.video7.isViewing()) { self.video7.savePngFile() }
//        if (self.video8.isViewing()) { self.video8.savePngFile() }
//    }
}


extension LiveVideoListViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return channalCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveVideoCollectionViewCell", for: indexPath as IndexPath) as! LiveVideoCollectionViewCell
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        
        if channalCount == 4 {
            cell.videoView.frame.size.height = UIScreen.main.bounds.height/2.30
            cell.videoView.frame.size.width = UIScreen.main.bounds.width/2.02
            switch(indexPath.row) {
            case 0 :
                activity1 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity1.center = cell.videoView.center
                activity1.startAnimating()
                cell.videoView.addSubview(activity1)
                
                movieView1 = UIView()
                movieView1.backgroundColor = .clear
                movieView1.frame = CGRect(x: cell.videoView.bounds.origin.x, y: cell.videoView.bounds.origin.y, width: cell.videoView.frame.height, height: cell.videoView.frame.width )
                //        Add tap gesture to movieView for play/pause
                //            let gesture = UITapGestureRecognizer(target: self, action: #selector(fullScreenTapped(_:)))
                //            movieView1.addGestureRecognizer(gesture)
                //Add movieView to view controller
                cell.videoView.addSubview(movieView1)
                movieView1.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
                movieView1.center = cell.videoView.center
                
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video1.media = media
                video1.delegate = self
                video1.drawable = movieView1
                video1.play()
                video1.audio.isMuted = true
                break
            case 1 :
                activity2 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity2.center = cell.videoView.center
                activity2.startAnimating()
                cell.videoView.addSubview(activity2)
                
                movieView2 = UIView()
                movieView2.backgroundColor = .clear
                movieView2.frame = CGRect(x: cell.videoView.bounds.origin.x, y: cell.videoView.bounds.origin.y, width: cell.videoView.bounds.height, height: cell.videoView.bounds.width )
                //        Add tap gesture to movieView for play/pause
                //            let gesture = UITapGestureRecognizer(target: self, action: #selector(fullScreenTapped(_:)))
                //            movieView1.addGestureRecognizer(gesture)
                //Add movieView to view controller
                cell.videoView.addSubview(movieView2)
                movieView2.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
                movieView2.center = cell.videoView.center
                
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video2.media = media
                video2.delegate = self
                video2.drawable = movieView2
                video2.play()
                video2.audio.isMuted = true
                break
            case 2 :
                activity3 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity3.center = cell.videoView.center
                activity3.startAnimating()
                cell.videoView.addSubview(activity3)
                
                movieView3 = UIView()
                movieView3.backgroundColor = .clear
                movieView3.frame = CGRect(x: cell.videoView.bounds.origin.x, y: cell.videoView.bounds.origin.y, width: cell.videoView.bounds.height, height: cell.videoView.bounds.width )
                //        Add tap gesture to movieView for play/pause
                //            let gesture = UITapGestureRecognizer(target: self, action: #selector(fullScreenTapped(_:)))
                //            movieView1.addGestureRecognizer(gesture)
                //Add movieView to view controller
                cell.videoView.addSubview(movieView3)
                movieView3.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
                movieView3.center = cell.videoView.center
                
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video3.media = media
                video3.delegate = self
                video3.drawable = movieView3
                video3.play()
                video3.audio.isMuted = true
                break
            case 3 :
                activity4 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity4.center = cell.videoView.center
                activity4.startAnimating()
                cell.videoView.addSubview(activity4)
                
                movieView4 = UIView()
                movieView4.backgroundColor = .clear
                movieView4.frame = CGRect(x: cell.videoView.bounds.origin.x, y: cell.videoView.bounds.origin.y, width: cell.videoView.bounds.height, height: cell.videoView.bounds.width )
                //        Add tap gesture to movieView for play/pause
                //            let gesture = UITapGestureRecognizer(target: self, action: #selector(fullScreenTapped(_:)))
                //            movieView1.addGestureRecognizer(gesture)
                //Add movieView to view controller
                cell.videoView.addSubview(movieView4)
                movieView4.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
                movieView4.center = cell.videoView.center
                
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video4.media = media
                video4.delegate = self
                video4.drawable = movieView4
                video4.play()
                video4.audio.isMuted = true
                break
            default:
                break
            }
        } else {
            switch(indexPath.row) {
            case 0 :
                activity1 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity1.center = cell.videoView.center
                activity1.startAnimating()
                cell.videoView.addSubview(activity1)
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video1.media = media
                video1.delegate = self
                video1.drawable = cell.videoView
                video1.play()
                video1.audio.isMuted = true
                break
            case 1 :
                activity2 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity2.center = cell.videoView.center
                activity2.startAnimating()
                cell.videoView.addSubview(activity2)
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video2.media = media
                video2.delegate = self
                video2.drawable = cell.videoView
                video2.play()
                video2.audio.isMuted = true
                break
            case 2 :
                activity3 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity3.center = cell.videoView.center
                activity3.startAnimating()
                cell.videoView.addSubview(activity3)
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video3.media = media
                video3.delegate = self
                video3.drawable = cell.videoView
                video3.play()
                video3.audio.isMuted = true
                break
            case 3 :
                activity4 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity4.center = cell.videoView.center
                activity4.startAnimating()
                cell.videoView.addSubview(activity4)
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video4.media = media
                video4.delegate = self
                video4.drawable = cell.videoView
                video4.play()
                video4.audio.isMuted = true
                break
            case 4 :
                activity5 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity5.center = cell.videoView.center
                activity5.startAnimating()
                cell.videoView.addSubview(activity5)
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video5.media = media
                video5.delegate = self
                video5.drawable = cell.videoView
                video5.play()
                video5.audio.isMuted = true
                break
            case 5 :
                activity6 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity6.center = cell.videoView.center
                activity6.startAnimating()
                cell.videoView.addSubview(activity6)
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video6.media = media
                video6.delegate = self
                video6.drawable = cell.videoView
                video6.play()
                video6.audio.isMuted = true
                break
            case 6 :
                activity7 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity7.center = cell.videoView.center
                activity7.startAnimating()
                cell.videoView.addSubview(activity7)
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video7.media = media
                video7.delegate = self
                video7.drawable = cell.videoView
                video7.play()
                video7.audio.isMuted = true
                break
            case 7 :
                activity8 = UIActivityIndicatorView.init(style: .whiteLarge)
                activity8.center = cell.videoView.center
                activity8.startAnimating()
                cell.videoView.addSubview(activity8)
                let media = VLCMedia(url: URL(string: urlArray[indexPath.row] as! String)! )
                video8.media = media
                video8.delegate = self
                video8.drawable = cell.videoView
                video8.play()
                video8.audio.isMuted = true
                break
            default:
                break
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath) as! LiveVideoCollectionViewCell
////        self.view.addSubview(cell.contentView)
////        cell.contentView.frame = collectionView.frame
////        movieView1.frame = UIScreen.main.bounds
//
////        self.view.addSubview(cell.contentView)
////        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreen))
////        movieView.addGestureRecognizer(tap)
////        })
//
//        if channalCount == 4 {
//            let movieView = UIView()
//            movieView.backgroundColor = .yellow
//            movieView.frame = CGRect(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.height, height: self.view.bounds.size.width)
//            movieView.center = self.view.center
//            movieView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
//            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreen))
//            movieView.addGestureRecognizer(tap)
//            self.view.addSubview(movieView)
//            switch(indexPath.row) {
//            case 0 :
//                video1.drawable = movieView
//                break
//            case 1 :
//                video2.drawable = movieView
//                break
//            case 2 :
//                video3.drawable = movieView
//                break
//            case 3 :
//                video4.drawable = movieView
//                break
//            default:
//                break
//            }
//        } else {
            stopVideo()
            output.navigateToLiveVideoScreen(mdvrUnitNo: mdvrUnitNo, channelId: Int32(indexPath.row+1))
//        }
    }
    
    @objc func dismissFullscreen(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
        video1.drawable = movieView1

    }
}

extension LiveVideoListViewController: VLCMediaPlayerDelegate {

    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        switch(video1.state) {
        case .opening :
            activity1.stopAnimating()
            activity1.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            break
        case .stopped :
            break
        case .ended :
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            break
        default:
            break
        }
        switch(video2.state) {
        case .opening :
            activity2.stopAnimating()
            activity2.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            break
        case .stopped :
            break
        case .ended :
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            break
        default:
            break
        }
        switch(video3.state) {
        case .opening :
            activity3.stopAnimating()
            activity3.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            break
        case .stopped :
            break
        case .ended :
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            break
        default:
            break
        }
        switch(video4.state) {
        case .opening :
            activity4.stopAnimating()
            activity4.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            break
        case .stopped :
            break
        case .ended :
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            break
        @unknown default:
            break
        }
        switch(video5.state) {
        case .opening :
            activity5.stopAnimating()
            activity5.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            break
        case .stopped :
            break
        case .ended :
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            break
        default:
            break
        }
        switch(video6.state) {
        case .opening :
            activity6.stopAnimating()
            activity6.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            break
        case .stopped :
            break
        case .ended :
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            break
        default:
            break
        }
        switch(video7.state) {
        case .opening :
            activity7.stopAnimating()
            activity7.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            break
        case .stopped :
            break
        case .ended :
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            break
        default:
            break
        }
        switch(video8.state) {
        case .opening :
            activity8.stopAnimating()
            activity8.isHidden = true
            break
        case .buffering :
            break
        case .playing :
            break
        case .stopped :
            break
        case .ended :
            break
        case .error:
            break
        case .paused:
            break
        case .esAdded:
            break
        default:
            break
        }
    }
}


extension Data {

    /// Hashing algorithm that prepends an RSA2048ASN1Header to the beginning of the data being hashed.
    ///
    /// - Parameters:
    ///   - type: The type of hash algorithm to use for the hashing operation.
    ///   - output: The type of output string desired.
    /// - Returns: A hash string using the specified hashing algorithm, or nil.
    public func hashWithRSA2048Asn1Header(_ type: HashType, output: HashOutputType = .hex) -> String? {

        let rsa2048Asn1Header:[UInt8] = [
            0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86,
            0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03, 0x82, 0x01, 0x0f, 0x00
        ]

        var headerData = Data(rsa2048Asn1Header)
        headerData.append(self)

        return hashed(type, output: output)
    }

    /// Hashing algorithm for hashing a Data instance.
    ///
    /// - Parameters:
    ///   - type: The type of hash to use.
    ///   - output: The type of hash output desired, defaults to .hex.
    ///   - Returns: The requested hash output or nil if failure.
    public func hashed(_ type: HashType, output: HashOutputType = .hex) -> String? {

        // setup data variable to hold hashed value
        var digest = Data(count: Int(type.length))

        _ = digest.withUnsafeMutableBytes{ digestBytes -> UInt8 in
            self.withUnsafeBytes { messageBytes -> UInt8 in
                if let mb = messageBytes.baseAddress, let db = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let length = CC_LONG(self.count)
                    switch type {
                    case .md5: CC_MD5(mb, length, db)
                    case .sha1: CC_SHA1(mb, length, db)
                    case .sha224: CC_SHA224(mb, length, db)
                    case .sha256: CC_SHA256(mb, length, db)
                    case .sha384: CC_SHA384(mb, length, db)
                    case .sha512: CC_SHA512(mb, length, db)
                    }
                }
                return 0
            }
        }

        // return the value based on the specified output type.
        switch output {
        case .hex: return digest.map { String(format: "%02hhx", $0) }.joined()
        case .base64: return digest.base64EncodedString()
        }
    }
}
