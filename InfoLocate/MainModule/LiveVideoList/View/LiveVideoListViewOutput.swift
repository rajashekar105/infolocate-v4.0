import Foundation

protocol LiveVideoListViewOutput {
    func back()
    func navigateToLiveVideoScreen(mdvrUnitNo: String, channelId: Int32)
}
