//
//  LiveVideoCollectionViewCell.swift
//  InfoLocate
//
//  Created by Infotrack on 13/04/20.
//  Copyright © 2020 Hemalatha T. All rights reserved.
//

import UIKit

class LiveVideoCollectionViewCell: UICollectionViewCell {

    /// This property is videoView.
    @IBOutlet weak var videoView: UIView!
    /// This property is topLabel.
    @IBOutlet weak var topLabel: UILabel!
    /// This property is bottomLabel.
    @IBOutlet weak var bottomLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
