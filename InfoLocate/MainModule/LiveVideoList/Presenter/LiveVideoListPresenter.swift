import Foundation

class LiveVideoListPresenter: LiveVideoListModuleInput, LiveVideoListViewOutput, LiveVideoListInteractorOutput {
    
    weak var view: LiveVideoListViewInput!
    var interactor: LiveVideoListInteractorInput!
    var router: LiveVideoListRouterInput!
    
    // MARK: - LiveVideoListViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToLiveVideoScreen(mdvrUnitNo: String, channelId: Int32) {
        router.navigateToLiveVideoScreen(mdvrUnitNo: mdvrUnitNo, channelId: channelId)
    }
    
    // MARK: - LiveVideoListModuleInput
    
    func configureModule(liveVehicleDetails: LiveVehicleListModel) {
        view.configureModule(liveVehicleDetails: liveVehicleDetails)
    }
    
    // MARK: - LiveVideoListInteractorOutput
    
}
