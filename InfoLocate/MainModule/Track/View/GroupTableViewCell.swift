//
//  GroupTableViewCell.swift
//  InfoLocateFMS
//
//  Created by Hemalatha T on 27/11/18.
//  Copyright © 2018 certInfotrack. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

    /// This property is groupLabel.
    @IBOutlet weak var groupLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
