import Foundation

protocol TrackViewOutput {
    func back()
    func navigateToTripsScreen(vehicleNumber: String)
}
