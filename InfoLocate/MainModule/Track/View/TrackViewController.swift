import UIKit
import GoogleMaps
import Alamofire
import SwiftHEXColors

class TrackViewController: BaseViewController, TrackViewInput, UITableViewDelegate, UITableViewDataSource, GMSMapViewDelegate {
    
    /// This property is output for TrackViewOutput.
    var output: TrackViewOutput!
    /// This property is mapTypeValue.
    var mapTypeValue: Int = 0
    /// This property is trafficValue.
    var trafficValue: Int = 0
    /// This property is LoginID.
    var LoginID: String = ""
    /// This property is zoomlevel.
    var zoomlevel: Float = 0.0
    /// This property is dataArray.
    var dataArray = NSArray()
    /// This property is StatusID.
    var StatusID: String = "A"
    /// This property is StatusName.
    var StatusName: String = ""
    /// This property is infowindowstatus.
    var infowindowstatus: Bool = false
    /// This property is marker.
//    var marker = GMSMarker()
    var markarArray: [GMSMarker] = []
    var previousMarkerStatus = false
    var currentMarkerStatus = false
    var previousMarker: Int = 0
    var currentMarker: Int = 0
    /// This property is image.
    var image = UIImage()
    /// This property is vehicleNumber.
    var vehicleNumber: String = ""
    /// This property is ignitionStatus.
    var ignitionStatus:String = ""
    /// This property is speed.
    var speed: Double = 0.0
    /// This property is dateTime.
    var dateTime: String = ""
    /// This property is location.
    var location: String = ""
    /// This property is mobileno.
    var mobileno: String = ""
    /// This property is vehiclename.
    var vehiclename: String = ""
    /// This property is drivername.
    var drivername: String = ""
    /// This property is temperature.
    var temperature: String = ""
    /// This property is acstatus.
    var acstatus: String = ""
    /// This property is fuel.
    var fuel: String = ""
    /// This property is durartion.
    var duration: String = ""
    /// This property is statusnameArrayDisplay.
    var statusnameArrayDisplay: [String] = [NSLocalizedString("All", comment: ""),
                                            NSLocalizedString("Moving", comment: ""),
                                            NSLocalizedString("Idle", comment: ""),
                                            NSLocalizedString("Stopped", comment: ""),
                                            NSLocalizedString("Inactive", comment: ""),
                                            NSLocalizedString("Out Of Service", comment: "")]
    /// This property is statusnameArray.
    var statusnameArray = ["all","moving", "idle", "stopped", "inactive", "outofservice","notpolling"]
    /// This property is statusIdArray.
    var statusIdArray: [String] = ["A","M", "I", "S", "IA", "IO"]
    /// This property is statusColorArray.
    var statusColorArray: [String] = ["#28B779", "#FFB848", "#E7191B", "#807D7D", "#852B99"]
    /// This property is datetimeArray.
    var datetimeArray: [String] = []
    /// This property is ignitionArray.
    var ignitionArray: [String] = []
    /// This property is latArray.
    var latArray: [Double] = []
    /// This property is lonArray.
    var lonArray: [Double] = []
    /// This property is locationArray.
    var locationArray: [String] = []
    /// This property is speedArray.
    var speedArray: [Double] = []
    /// This property is vehiclenoArray.
    var vehiclenoArray: [String] = []
    /// This property is vehiclestatusArray.
    var vehiclestatusArray: [String] = []
    /// This property is mobilenoArray.
    var mobilenoArray: [String] = []
    /// This property is vehiclenameArray.
    var vehiclenameArray: [String] = []
    /// This property is drivernameArray.
    var drivernameArray: [String] = []
    /// This property is temperatureArray.
    var temperatureArray: [String] = []
    /// This property is acstatusArray.
    var acstatusArray: [String] = []
    /// This property is fuelArray.
    var fuelArray: [String] = []
    /// This property is duration.
    var durationArray: [String] = []
    /// This property is mapview.
    @IBOutlet var mapview: GMSMapView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is uitableview.
    @IBOutlet var uitableview: UITableView!
    /// This property is infowindow.
    @IBOutlet var infowindow: UIView!
    /// This property is vehicleNoTableView.
    @IBOutlet var vehicleNoTableView: UITableView!
    /// This property is hideinfowindow.
    @IBOutlet var hideinfowindow: UIButton!
    /// This property is vehicleNumberLabel.
    @IBOutlet var vehicleNumberLabel: UILabel!
    /// This property is driverNameLabel.
    @IBOutlet var driverNameLabel: UILabel!
    /// This property is mobileNoLabel.
    @IBOutlet var mobileNoLabel: UILabel!
    /// This property is tempertureLabel.
    @IBOutlet var tempertureLabel: UILabel!
    /// This property is fuelLabel.
    @IBOutlet var acstatusLabel: UILabel!
    /// This property is fuelLabel.
    @IBOutlet var fuelLabel: UILabel!
    /// This property is durationLabel.
    @IBOutlet var durationLabel: UILabel!
    /// This property is ignitionLabel.
    @IBOutlet var ignitionLabel: UILabel!
    /// This property is speedLabel.
    @IBOutlet var speedLabel: UILabel!
    /// This property is dateTimeLabel.
    @IBOutlet var dateTimeLabel: UILabel!
    /// This property is locationLabel.
    @IBOutlet var locationLabel: UILabel!
    /// This property is trips.
    @IBOutlet var trips: UIButton!
    /// This property is maptype.
    @IBOutlet var maptype: UIButton!
    /// This property is traffic.
    @IBOutlet var traffic: UIButton!
//    /// This property is statusLabel.
//    @IBOutlet var statusLabel: UILabel!
    /// This property is vehicleNumberSelection.
    @IBOutlet var vehicleNumberSelection: UIButton!
//    /// This property is groupNumberSelection.
//    @IBOutlet weak var groupNumberSelection: UIButton!
//    /// This property is groupTableView.
//    @IBOutlet weak var groupTableView: UITableView!
//    /// This property is LogroupLayoutConstraintginID.
//    @IBOutlet weak var groupLayoutConstraint: NSLayoutConstraint!
//    /// This property is groupTableViewHeightConstraint.
//    @IBOutlet weak var groupTableViewHeightConstraint: NSLayoutConstraint!
//    /// This property is vehicleListTableViewHeightConstraint.
//    @IBOutlet weak var vehicleListTableViewHeightConstraint: NSLayoutConstraint!
    /// This property is groupDefaults.
    let groupDefaults = UserDefaults.standard
    /// This property is loginGroupId.
    var loginGroupId: Int = 0
    /// This property is vehicleGroupId.
    var vehicleGroupId : Int = 0
    /// This property is vehicleGroupArray.
    var vehicleGroupArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.vehicleNumberSelection.setTitle("\(NSLocalizedString("Select vehicle no", comment: ""))", for: .normal)
        self.trips.setTitle("\(NSLocalizedString("Trips", comment: ""))", for: .normal)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.vehicleNumberSelection.setTitle("\(English.Select_vehicle_no_driver_name)", for: .normal)
            self.trips.setTitle("\(English.Trips)", for: .normal)
            statusnameArrayDisplay = [English.All,
                                      English.Moving,
                                      English.Idle,
                                      English.Stopped,
                                      English.Inactive,
                                      English.Out_Of_Service]
        } else if type == Constants.japanese {
            self.vehicleNumberSelection.setTitle("\(Japanese.Select_vehicle_no_driver_name)", for: .normal)
            self.trips.setTitle("\(Japanese.Trips)", for: .normal)
            statusnameArrayDisplay = [Japanese.All,
                                      Japanese.Moving,
                                      Japanese.Idle,
                                      Japanese.Stopped,
                                      Japanese.Inactive,
                                      Japanese.Out_Of_Service]
        }
        vehicleNoTableView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        currentMarkerStatus = false
        previousMarkerStatus = false
        currentMarker = 0
        previousMarker = 0
        defaults.setValue("false", forKey: "isFiltered")
        mapview.clear()
        mapview.mapType = GMSMapViewType.normal
        mapview.isTrafficEnabled = false
        mapview.delegate = self
        StatusID = "A"
        //        StatusName = "moving"
        //        image = UIImage(named: "moving")!
        uitableview.delegate = self
        uitableview.dataSource = self
        vehicleNoTableView.delegate = self
        vehicleNoTableView.dataSource = self
        uitableview.isHidden = true
        vehicleNoTableView.isHidden = true
        activity.layer.cornerRadius = 10
        var newcolor1 = UIColor()
        newcolor1 = UIColor(hexString: "#1ACAA6")!
        trips.layer.backgroundColor = newcolor1.cgColor
        infowindowstatus = false
        hideinfowindow.isHidden = true
        infowindow.isHidden = true
        infowindow.layer.shadowColor = UIColor.black.cgColor
        infowindow.layer.shadowOffset = CGSize.zero
        infowindow.layer.shadowRadius = 3.0
        infowindow.layer.shadowOpacity = 0.7
        apiCalling()
//        self.statusLabel.text = "\(NSLocalizedString("Track", comment: ""))  (\(self.vehiclenoArray.count))"
//        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//        if type == Constants.english {
//            self.statusLabel.text = "\(English.Track)  (\(self.vehiclenoArray.count))"
//        } else if type == Constants.japanese {
//            self.statusLabel.text = "\(Japanese.Track))  (\(self.vehiclenoArray.count))"
//        }
    }
    
    /// This method is hideinfowindow action.
    @IBAction func hideinfowindow(_ sender: Any) {
        animateRideNowView()
    }
    
    /// This method is menu button action.
    @IBAction func menuButtonAction(_ sender: UIButton) {
        uitableview.isHidden = true
        vehicleNoTableView.isHidden = true
        openSideMenu()
    }
    
    /// This method is back button action.
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
    /// This method is refresh button action.
    @IBAction func refresh(_ sender: Any) {
        if self.activity.isAnimating == true {
            
        } else {
            self.vehicleNumberSelection.setTitle("\(NSLocalizedString("Select vehicle no", comment: ""))", for: .normal)
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                self.vehicleNumberSelection.setTitle("\(English.Select_vehicle_no_driver_name)", for: .normal)
            } else if type == Constants.japanese {
                self.vehicleNumberSelection.setTitle("\(Japanese.Select_vehicle_no_driver_name)", for: .normal)
            }
            viewWillAppear(true)
        }
    }
    
    /// This method is vehicleType button action.
    @IBAction func vehicleType(_ sender: Any) {
        if self.uitableview.isHidden == false {
            self.uitableview.isHidden = true
        } else {
            uitableview.isHidden = false
            vehicleNoTableView.isHidden = true
            self.hideinfowindow.isHidden = true
            self.infowindow.isHidden = true
            self.infowindowstatus = false
            self.vehicleNumberSelection.setTitle("\(NSLocalizedString("Select vehicle no", comment: ""))", for: .normal)
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                self.vehicleNumberSelection.setTitle("\(English.Select_vehicle_no_driver_name)", for: .normal)
            } else if type == Constants.japanese {
                self.vehicleNumberSelection.setTitle("\(Japanese.Select_vehicle_no_driver_name)", for: .normal)
            }
            UIView.transition(with: uitableview, duration: 0.5, options: .transitionCurlDown, animations: { () -> Void in }, completion: nil)
        }
    }
    
    /// This method is maptype button action.
    @IBAction func maptype(_ sender: Any) {
        var image1 = UIImage()
        if mapTypeValue < 2
        {
            mapTypeValue += 1
        }
        else
        {
            mapTypeValue = 0
        }
        switch mapTypeValue {
        case 0 :
            image1 = UIImage(named: "maptypeyellow")!
            mapview.mapType = GMSMapViewType.normal
            break
        case 1 :
            image1 = UIImage(named: "maptypegrey")!
            mapview.mapType = GMSMapViewType.hybrid
            break
        case 2 :
            image1 = UIImage(named: "maptypewhite")!
            mapview.mapType = GMSMapViewType.terrain
            break
        default:
            image1 = UIImage(named: "maptypeyellow")!
            mapview.mapType = GMSMapViewType.normal
            break
        }
        maptype.setBackgroundImage(nil, for: UIControl.State.normal)
        maptype.setBackgroundImage(image1, for: UIControl.State.normal)
    }
    
    /// This method is traffic button action.
    @IBAction func traffic(_ sender: Any) {
        var image2 = UIImage()
        if trafficValue == 0 {
            mapview.isTrafficEnabled = true
            image2 = UIImage(named: "trafficcolor")!
            traffic.setBackgroundImage(nil, for: UIControl.State.normal)
            traffic.setBackgroundImage(image2, for: UIControl.State.normal)
            trafficValue = 1
        } else if trafficValue == 1 {
            mapview.isTrafficEnabled = false
            image2 = UIImage(named: "trafficgrey")!
            traffic.setBackgroundImage(nil, for: UIControl.State.normal)
            traffic.setBackgroundImage(image2, for: UIControl.State.normal)
            trafficValue = 0
        }
    }
    
    /// This method is zoomIn button action.
    @IBAction func zoomIn(_ sender: Any) {
        zoomlevel = mapview.camera.zoom
        zoomlevel = zoomlevel + 2.0
        ZoomMap(zoomTo: Float(zoomlevel))
    }
    
    /// This method is zoomOut button action.
    @IBAction func zoomOut(_ sender: Any) {
        zoomlevel = mapview.camera.zoom
        zoomlevel = zoomlevel - 2.0
        ZoomMap(zoomTo: Float(zoomlevel))
    }
    
    /// This method is trips button action.
    @IBAction func trips(_ sender: Any) {
        animateRideNowView()
        self.vehicleNumberSelection.setTitle("\(NSLocalizedString("Select vehicle no", comment: ""))", for: .normal)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.vehicleNumberSelection.setTitle("\(English.Select_vehicle_no_driver_name)", for: .normal)
        } else if type == Constants.japanese {
            self.vehicleNumberSelection.setTitle("\(Japanese.Select_vehicle_no_driver_name)", for: .normal)
        }
        output.navigateToTripsScreen(vehicleNumber: vehicleNumber)
    }
    
    /// This method is called when user taps on ZoomMap.
    func ZoomMap(zoomTo: Float){
        mapview.animate(toZoom: zoomTo)
    }
    
    /// This method is used for vehicleNumberSelection action.
    @IBAction func vehicleNumberSelection(_ sender: Any) {
        if self.vehicleNoTableView.isHidden == false {
            self.vehicleNoTableView.isHidden = true
        } else {
            DispatchQueue.main.async(execute: {
                self.uitableview.isHidden = true
                self.hideinfowindow.isHidden = true
                self.infowindow.isHidden = true
                self.infowindowstatus = false
                self.vehicleNoTableView.reloadData()
                if (self.vehiclenoArray.count > 0) {
                    self.vehicleNoTableView.isHidden = false
                } else {
                    self.vehicleNoTableView.isHidden = true
                    var message = NSLocalizedString("No Records Found", comment: "")
                     let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                    if type == Constants.english {
                        message = English.No_records_found
                    } else if type == Constants.japanese {
                        message = Japanese.No_records_found
                    }
                    let alertController = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
                    let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
    }

    
    /// This method is called when user didTapAt  coordinate in mapView.
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        uitableview.isHidden = true
        vehicleNoTableView.isHidden = true
    }
    
    /// This method is called when user didTap on marker in mapView.
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        showSelectedVehicleMarkerOnMapview(marker: marker)
        return false
    }
    
    func showSelectedVehicleMarkerOnMapview(marker: GMSMarker) {
        if infowindowstatus == false
        {
            hideinfowindow.isHidden = false
            infowindow.isHidden = false
            infowindowstatus = true
        }
        let a: Int = vehiclenoArray.firstIndex(of: marker.title!)!
        if self.previousMarkerStatus == true {
            self.previousMarker = self.currentMarker
        }
        self.currentMarkerStatus = true
        self.currentMarker = a
        updateMarker(selectedMarker: marker, index: a)
        
        loadData(i: a)
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: self.latArray[a], longitude: self.lonArray[a], zoom: 19.0)
        self.mapview.camera = camera
        self.zoomlevel = self.mapview.camera.zoom
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.5)
        self.mapview.animate(toZoom: 19)
        CATransaction.commit()
    }
    
    
    
    /// This method is used  to loadData.
    func loadData(i: Int) {
        vehicleNumber = vehiclenoArray [i]
        ignitionStatus = ignitionArray[i]
        speed = speedArray[i]
        dateTime = datetimeArray[i]
        location = locationArray[i]
        if(drivernameArray.count>0){
            drivername = drivernameArray[i]
        } else {
            drivername = ""
        }
        if(mobilenoArray.count>0){
            mobileno = mobilenoArray[i]
        } else {
            mobileno = ""
        }
        if(temperatureArray.count>0){
            temperature = temperatureArray[i]
        } else {
            temperature = ""
        }
        if(acstatusArray.count>0){
            acstatus = acstatusArray[i]
        } else {
            acstatus = ""
        }
        if(fuelArray.count>0){
            fuel = fuelArray[i]
        } else {
            fuel = ""
        }
        if(durationArray.count>0){
            duration = durationArray[i]
        } else {
            duration = ""
        }
        if(vehiclenameArray.count>0){
            vehiclename = vehiclenameArray[i]
        } else {
            vehiclename = ""
        }
        if vehiclename == "" || vehiclename == "-" || vehiclename == "NA" {
            vehicleNumberLabel.text = vehicleNumber
        } else {
            vehicleNumberLabel.text = vehicleNumber + "::" + vehiclename
        }
        if drivername == "" || drivername == "-" || drivername == "NA" {
            drivername = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                drivername = English.NA
            } else if type == Constants.japanese {
                drivername = Japanese.NA
            }
        }
        if mobileno == "" || mobileno == "-" || mobileno == "NA" {
            mobileno = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                mobileno = English.NA
            } else if type == Constants.japanese {
                mobileno = Japanese.NA
            }
            mobileNoLabel.textColor = .init(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
            mobileNoLabel.isUserInteractionEnabled = false
        } else {
            mobileNoLabel.textColor = .init(red: 0/255, green: 0/255, blue: 238/255, alpha: 1)
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
            mobileNoLabel.isUserInteractionEnabled = true
            mobileNoLabel.addGestureRecognizer(tap)
        }
        if temperature == "" || temperature == "-" || temperature == "NA" {
            temperature = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                temperature = English.NA
            } else if type == Constants.japanese {
                temperature = Japanese.NA
            }
        }
        if acstatus == "" || acstatus == "-" || acstatus == "NA" {
            acstatus = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acstatus = English.NA
            } else if type == Constants.japanese {
                acstatus = Japanese.NA
            }
        }
        if fuel == "" || fuel == "-" || fuel == "NA" {
            fuel = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                fuel = English.NA
            } else if type == Constants.japanese {
                fuel = Japanese.NA
            }
        }
        if duration == "" || duration == "-" || duration == "NA" {
            temperature = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                duration = English.NA
            } else if type == Constants.japanese {
                duration = Japanese.NA
            }
        }
        tempertureLabel.text = temperature
        acstatusLabel.text = acstatus
        if acstatus == "ON" {
            acstatusLabel.text = NSLocalizedString("ON", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acstatusLabel.text = English.ON
            } else if type == Constants.japanese {
                acstatusLabel.text = Japanese.ON
            }
        } else if acstatus == "OFF" {
            acstatusLabel.text = NSLocalizedString("OFF", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acstatusLabel.text = English.OFF
            } else if type == Constants.japanese {
                acstatusLabel.text = Japanese.OFF
            }
        }
        fuelLabel.text = fuel
        durationLabel.text = duration
        driverNameLabel.text = drivername
        mobileNoLabel.text = mobileno
        ignitionLabel.text = ignitionStatus
        if ignitionStatus == "ON" {
            ignitionLabel.text = NSLocalizedString("ON", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                ignitionLabel.text = English.ON
            } else if type == Constants.japanese {
                ignitionLabel.text = Japanese.ON
            }
        } else {
            ignitionLabel.text = NSLocalizedString("OFF", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                ignitionLabel.text = English.OFF
            } else if type == Constants.japanese {
                ignitionLabel.text = Japanese.OFF
            }
        }
        speedLabel.text = "\(speed) km/hr"
        locationLabel.text = location
        dateTimeLabel.text = dateTime
    }
    
    /// This method is called when user tap on phoneNumber.
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        print("tap working")
        print("calling")
        let phoneNumber = mobileno
        if let phoneURL = URL(string: ("tel:" + phoneNumber)) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(phoneURL)
            }
        }
    }
    
    /// This method is used for checking infowindowstatus.
    func animateRideNowView() {
        if infowindowstatus == false
        {
            self.hideinfowindow.isHidden = false
            self.infowindow.isHidden = false
            infowindowstatus = true
        }
        else
        {
            self.hideinfowindow.isHidden = true
            self.infowindow.isHidden = true
            infowindowstatus = false
        }
    }
    
    /// This method is used for apiCalling.
    func apiCalling() {
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            if let ID = defaults.value(forKey: "LoginID") {
                LoginID = ID as! String
            }
            if let groupId = groupDefaults.value(forKey: "groupEnable") {
                loginGroupId = groupId as! Int
            }
            GetVehicleStatusList()
            activity.startAnimating()
        }
    }

    
    // MARK:- Other than QAT persons can see this results
    /// This method is used to GetVehicleStatusList with api.
    func GetVehicleStatusList() {
        mapview.clear()
        datetimeArray = []
        ignitionArray = []
        latArray = []
        lonArray = []
        locationArray = []
        speedArray = []
        vehiclenoArray = []
        drivernameArray = []
        mobilenoArray = []
        vehiclenameArray = []
        temperatureArray = []
        acstatusArray = []
        fuelArray = []
        durationArray = []
        markarArray = []
        var baseUrl:String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        let parameters = [
            "userid" : LoginID.encrypt(),
            "listtype" : StatusID.encrypt()
        ]
        print(baseUrl+"GetVehicleStatusList")
        print(parameters)
        AF.request("\(baseUrl)"+"GetVehicleStatusList", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let sts = JSON.object(forKey: "status") as? NSString {
                        self.status = (sts as String).decrypt()
                    }
                    self.currentMarkerStatus = false
                    self.previousMarkerStatus = false
                    self.currentMarker = 0
                    self.previousMarker = 0
                    if self.status == "success" {
                        self.dataArray = JSON.object(forKey: "data")as! NSArray
                        for i in 0 ..< self.dataArray.count {
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "datetime") as? String {
                                self.datetimeArray.append(value.decrypt())
                            }
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "ignition") as? String {
                                self.ignitionArray.append(value.decrypt())
                            }
                            if let value = (self.dataArray[i] as AnyObject).object(forKey: "lat") as? String {
                                self.latArray.append((value.decrypt() as NSString).doubleValue)
                            }
                            if let value = (self.dataArray[i] as AnyObject).object(forKey: "lon") as? String {
                                self.lonArray.append((value.decrypt() as NSString).doubleValue)
                            }
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "location") as? String {
                                self.locationArray.append(value.decrypt())
                            }
                            if let value = (self.dataArray[i] as AnyObject).object(forKey: "speed") as? String {
                                self.speedArray.append((value.decrypt() as NSString).doubleValue)
                            }
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "vehicleno") as? String {
                                self.vehiclenoArray.append(value.decrypt())
                            }
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "vehiclestatus") as? String {
                                self.vehiclestatusArray.append(value.decrypt())
                            }
                            if((self.dataArray[i] as AnyObject).object(forKey: "drivername") != nil){
                                if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "drivername") as? String {
                                    self.drivernameArray.append(value.decrypt())
                                }
                            } else {
                                self.drivernameArray.append("")
                            }
                            if((self.dataArray[i] as AnyObject).object(forKey: "mobileno") != nil){
                                if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "mobileno") as? String {
                                    self.mobilenoArray.append(value.decrypt())
                                }
                            } else {
                                self.mobilenoArray.append("")
                            }
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "vehiclename") as? String {
                                self.vehiclenameArray.append(value.decrypt())
                            }
                            if((self.dataArray[i] as AnyObject).object(forKey: "temperature") != nil){
                                if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "temperature") as? String {
                                    self.temperatureArray.append(value.decrypt())
                                }
                            } else {
                                self.temperatureArray.append("")
                            }
                            
                            if((self.dataArray[i] as AnyObject).object(forKey: "acstatus") != nil){
                                if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "acstatus") as? String {
                                    self.acstatusArray.append(value.decrypt())
                                }
                            } else {
                                self.fuelArray.append("")
                            }
                            
                            if((self.dataArray[i] as AnyObject).object(forKey: "fuellevel") != nil){
                                if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "fuellevel") as? String {
                                    self.fuelArray.append(value.decrypt())
                                }
                            } else {
                                self.fuelArray.append("")
                            }
                            
                            if((self.dataArray[i] as AnyObject).object(forKey: "duration") != nil){
                                if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "duration") as? String {
                                    self.durationArray.append(value.decrypt())
                                }
                            } else {
                                self.durationArray.append("")
                            }
                        }
                        
                        DispatchQueue.main.async(execute: {
                            let path = GMSMutablePath()
                            path.removeAllCoordinates()
                            for i in 0 ..< self.latArray.count {
                                self.addmarker(lat: self.latArray[i], lon: self.lonArray[i], index: i)
                                path.add(CLLocationCoordinate2D(latitude: self.latArray[i], longitude: self.lonArray[i]))
                            }
                            let bounds = GMSCoordinateBounds(path: path)
                            self.mapview.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 40))
                            self.zoomlevel = self.mapview.camera.zoom
                            let a: Int = self.vehiclenoArray.count - 1
                            self.loadData(i: a)
                            self.vehicleNoTableView.reloadData()
                            self.activity.stopAnimating()
                        })
                        
                    } else if self.status == "failure" {
                        if let msg = JSON.object(forKey: "message") as? NSString {
                            self.message = (msg as String).decrypt()
                        }
                        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                        if self.message == English.No_Records_found {
                            if type == Constants.english {
                                self.message = English.No_Records_found
                            } else if type == Constants.japanese {
                                self.message = Japanese.No_Records_found
                            }
                        } else if self.message == English.No_Records_found {
                            if type == Constants.english {
                                self.message = English.No_Records_found
                            } else if type == Constants.japanese {
                                self.message = Japanese.No_Records_found
                            }
                        }
                        self.showAlert("", message: self.message)
                        DispatchQueue.main.async(execute: {
                            if self.infowindowstatus == true
                            {
                                self.hideinfowindow.isHidden = true
                                self.infowindow.isHidden = true
                                self.infowindowstatus = false
                            }
                            self.activity.stopAnimating()
                        })
                    }
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    // MARK:- Groups API Call
    /// This method is used to GetGroupsList details..
    func GetGroupsList() {
        
        vehicleGroupArray = []
        let groupParameters = ["userid" : self.LoginID]
        var baseUrl:String=""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        
        AF.request("\(baseUrl)"+"GetGroupDetails?", parameters: groupParameters).responseJSON { response in
            
            //Alamofire.request("https://5.195.195.181/QATAPIBeta/ItlService.svc/GetGroupDetails?", parameters: groupParameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            
            switch response.result {
            case .success:
                if let result = response.value {
                    
                    let JSON = result as! NSDictionary
                    
                    if let gsts = JSON.object(forKey: "status") as? NSString {
                        self.status = gsts as String
                    }
                    if self.status == "success" {
                        
                        self.vehicleGroupArray = JSON.object(forKey: "data") as! NSArray
                        
                        // This array have to load on Groups TableView
                        print(self.vehicleGroupArray)
                        
                        // MARK:- Other than Group Related Button & TableView remaining have to hide
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                            self.vehicleNoTableView.isHidden = true
                            self.uitableview.isHidden = true
                            self.activity.stopAnimating()
                        })
                        
                    } else if self.status == "failure" {
                        
                        if let msg = JSON.object(forKey: "message") as? NSString {
                            self.message = msg as String
                        }
                        self.showAlert("", message: self.message)
                        DispatchQueue.main.async(execute: {
                            if self.infowindowstatus == true
                            {
                                self.hideinfowindow.isHidden = true
                                self.infowindow.isHidden = true
                                self.infowindowstatus = false
                            }
                            self.activity.stopAnimating()
                        })
                    }
                }
                break;
            case .failure(let error):
                print(error)
                break;
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count : Int?
        if tableView == uitableview {
            count = statusnameArrayDisplay.count
        }
        if tableView == vehicleNoTableView {
            count =  vehiclenoArray.count
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell1: UITableViewCell!
        if tableView == uitableview
        {
            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
            print(indexPath.row)
            tableViewCell.textLabel?.text = statusnameArrayDisplay[indexPath.row]
            tableViewCell.textLabel?.textAlignment = .center
            cell1 = tableViewCell
        }
        
        if tableView == vehicleNoTableView
        {
            let vehicle = self.vehiclenoArray[indexPath.row]
            let driver = self.drivernameArray[indexPath.row]
            let cellIdentifier = "GroupTableViewCell"
            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GroupTableViewCell
            tableViewCell.groupLabel!.text =  "\(driver) (\(vehicle))"
            print("\n" + tableViewCell.groupLabel!.text!)
            tableViewCell.groupLabel?.textAlignment = .center
            cell1 = tableViewCell
        }
        return cell1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView == uitableview {
            uitableview.isHidden = true
            StatusID = statusIdArray[indexPath.row]
            StatusName = statusnameArray[indexPath.row]
            mapview.clear()
            UIView.transition(with: uitableview, duration: 0.5, options: .transitionCurlUp, animations: { () -> Void in }, completion: nil)
            apiCalling()
        }
        
        if tableView == vehicleNoTableView {
            DispatchQueue.main.async(execute: {
                let a: Int = self.vehiclenoArray.firstIndex(of: self.vehiclenoArray[indexPath.row])!
                self.vehicleNumberSelection.setTitle(self.drivernameArray[indexPath.row] + " (" + self.vehiclenoArray[indexPath.row] + ")", for: UIControl.State.normal)
//                let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: self.latArray[a], longitude: self.lonArray[a], zoom: 12.0)
//                self.mapview.camera = camera
               
//                //                self.mapview.
//                //                self.marker.position = position
//                //                self.marker.title = self.vehiclenoArray[indexPath.row]
//                //                self.mapview.selectedMarker = self.marker
//
//                self.zoomlevel = self.mapview.camera.zoom
//                CATransaction.begin()
//                CATransaction.setAnimationDuration(1.0)
//                self.mapview.animate(toZoom: 15)
//                CATransaction.commit()
                let position = CLLocationCoordinate2D(latitude: self.latArray[a], longitude: self.lonArray[a])
                let marker = self.markarArray[a]
                if (marker.position.latitude == position.latitude) && (marker.position.longitude == position.longitude) {
                    self.mapview.selectedMarker = marker
                    self.showSelectedVehicleMarkerOnMapview(marker: marker)
                }
                self.vehicleNoTableView.isHidden = true
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is used to addmarker on the map.
    func addmarker(lat: Double, lon: Double, index: Int) {
        //        StatusName = "moving"
        //        image = UIImage(named: "moving")!
        if StatusID == "A" {
            if vehiclestatusArray[index].contains("Moving") {
                StatusName = "moving"
            } else if vehiclestatusArray[index].contains("Stopped") {
                StatusName = "stopped"
            } else if vehiclestatusArray[index].contains("Idle") {
                StatusName = "idle"
            } else if vehiclestatusArray[index].contains("Inactive") {
                StatusName = "inactive"
            } else if vehiclestatusArray[index].contains("service") {
                StatusName = "outofservice"
            }
            else {
                StatusName = "notpolling"
            }
        }
        switch StatusName {
        case "moving":
            image = UIImage(named: "moving")!
            break
        case "idle":
            image = UIImage(named: "idle")!
            break
        case "stopped":
            image = UIImage(named: "stopped")!
            break
        case "inactive":
            image = UIImage(named: "inactive")!
            break
        case "outofservice":
            image = UIImage(named: "outofservice")!
            break
        case "notpolling":
            image = UIImage(named: "notpolling")!
            break
        default:
            image = UIImage(named: "stopped")!
            break
        }
        let position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let marker = GMSMarker(position: position)
        marker.title = vehiclenoArray[index]
        marker.icon = image
        marker.map = mapview
        markarArray.insert(marker, at: index)
    }
    
    func updateMarker(selectedMarker: GMSMarker, index: Int) {
        
        if previousMarkerStatus == false {
            self.previousMarkerStatus = true
            previousMarker = index
        } else {
            let previousMark = markarArray[self.previousMarker]
            if previousMark != selectedMarker {
                if StatusID == "A" {
                    if vehiclestatusArray[self.previousMarker].contains("Moving") {
                        StatusName = "moving"
                    } else if vehiclestatusArray[self.previousMarker].contains("Stopped") {
                        StatusName = "stopped"
                    } else if vehiclestatusArray[self.previousMarker].contains("Idle") {
                        StatusName = "idle"
                    } else if vehiclestatusArray[self.previousMarker].contains("Inactive") {
                        StatusName = "inactive"
                    } else if vehiclestatusArray[self.previousMarker].contains("service") {
                        StatusName = "outofservice"
                    }
                    else {
                        StatusName = "notpolling"
                    }
                }
                switch StatusName {
                case "moving":
                    image = UIImage(named: "moving")!
                    break
                case "idle":
                    image = UIImage(named: "idle")!
                    break
                case "stopped":
                    image = UIImage(named: "stopped")!
                    break
                case "inactive":
                    image = UIImage(named: "inactive")!
                    break
                case "outofservice":
                    image = UIImage(named: "outofservice")!
                    break
                case "notpolling":
                    image = UIImage(named: "notpolling")!
                    break
                default:
                    image = UIImage(named: "stopped")!
                    break
                }
                previousMark.icon = image
                previousMark.map = mapview
            }
        }
        let currentMark = markarArray[self.currentMarker]
        if currentMark == selectedMarker {
            if StatusID == "A" {
                if vehiclestatusArray[self.currentMarker].contains("Moving") {
                    StatusName = "moving"
                } else if vehiclestatusArray[self.currentMarker].contains("Stopped") {
                    StatusName = "stopped"
                } else if vehiclestatusArray[self.currentMarker].contains("Idle") {
                    StatusName = "idle"
                } else if vehiclestatusArray[self.currentMarker].contains("Inactive") {
                    StatusName = "inactive"
                } else if vehiclestatusArray[self.currentMarker].contains("service") {
                    StatusName = "outofservice"
                }
                else {
                    StatusName = "notpolling"
                }
            }
            switch StatusName {
            case "moving":
                image = UIImage(named: "moving-selected")!
                break
            case "idle":
                image = UIImage(named: "idle-selected")!
                break
            case "stopped":
                image = UIImage(named: "stopped-selected")!
                break
            case "inactive":
                image = UIImage(named: "inactive-selected")!
                break
            case "outofservice":
                image = UIImage(named: "outofservice-selected")!
                break
            case "notpolling":
                image = UIImage(named: "notpolling-selected")!
                break
            default:
                image = UIImage(named: "stopped-selected")!
                break
            }
            currentMark.icon = image
            currentMark.map = mapview
        }
        
        
        
    }
}

