import Foundation

class TrackPresenter: TrackModuleInput, TrackViewOutput, TrackInteractorOutput {
    
    weak var view: TrackViewInput!
    var interactor: TrackInteractorInput!
    var router: TrackRouterInput!
    
    // MARK: - TrackViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToTripsScreen(vehicleNumber: String) {
        router.navigateToTripsScreen(vehicleNumber: vehicleNumber)
    }
    
    // MARK: - TrackModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - TrackInteractorOutput
    
}
