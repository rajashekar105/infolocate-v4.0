import UIKit

@objc class TrackModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = TrackControllerFromStoryboard()
        
        let router = TrackRouter()
        router.viewController = viewController
        
        let presenter = TrackPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = TrackInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func TrackControllerFromStoryboard() -> TrackViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.TRACK_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! TrackViewController
    }
    
}
