import Foundation

protocol TrackModuleInput: class {
    
    func configureModule()
}
