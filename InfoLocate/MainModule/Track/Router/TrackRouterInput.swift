import Foundation

protocol TrackRouterInput {
    func back()
    func navigateToTripsScreen(vehicleNumber: String)
}
