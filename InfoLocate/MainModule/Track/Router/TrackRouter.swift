import UIKit

class TrackRouter:TrackRouterInput {
    
    weak var viewController: TrackViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToTripsScreen(vehicleNumber: String) {
        Navigation.navigateToTripsScreen(vehicleNumber: vehicleNumber, viewController: viewController)
    }
    
}
