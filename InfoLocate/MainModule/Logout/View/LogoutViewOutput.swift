import Foundation

protocol LogoutViewOutput {
    func back()
    func navigateToLoginScreen()
}
