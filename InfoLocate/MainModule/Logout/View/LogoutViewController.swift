import UIKit
import Alamofire

class LogoutViewController: BaseViewController, LogoutViewInput {

    /// This property is output for LogoutViewOutput.
    var output: LogoutViewOutput!
    /// This property is loginStatus.
    var loginStatus: String = ""
    /// This property is loginMessage.
    var loginMessage: String = ""
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaults.setValue("false", forKey: "isFiltered")
        var title = NSLocalizedString("Are you sure want to logout?", comment: "")
        var cancelTitle = NSLocalizedString("CANCEL", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            title = English.Are_you_sure_want_to_logout
            cancelTitle = English.CANCEL
        } else if type == Constants.japanese {
            title = Japanese.Are_you_sure_want_to_logout
            cancelTitle = Japanese.CANCEL
        }
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
            UIAlertAction in
            let fcmenabled = self.defaults.object(forKey: "fcmenabled") as Any as! Int
            if fcmenabled == 0 {
                self.defaults.set("false", forKey: "userLogin")
                self.defaults.setValue("", forKey: "LoginID")
                self.output.navigateToLoginScreen()
            } else if fcmenabled == 1 {
                self.activity.startAnimating()
                self.appLogoutToServer()
            }
        }
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default) {
            UIAlertAction in
//            self.output.back()
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is used for appLogoutToServer with api.
    func appLogoutToServer() {
        var LoginID:String = ""
        var baseUrl:String = ""
        //            let user: String = userName.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        if let ID = defaults.value(forKey: "LoginID") {
            LoginID = ID as! String
        }
        let parameters = [
            "userid" : LoginID.encrypt()
        ]
        print("\(baseUrl)"+"Logout")
        //            print("http://5.195.195.181/FCMAPI/ItlService.svc/UpdateMobileAppVersion")
        print(parameters)
        AF.request("\(baseUrl)"+"Logout", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            print(response.request ?? "")
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let status = JSON.object(forKey: "status") as? NSString {
                        self.loginStatus = (status as String).decrypt()
                    }
                    if self.loginStatus == "success" {
                        self.defaults.set("false", forKey: "userLogin")
                        self.defaults.setValue("", forKey: "LoginID")
                        self.output.navigateToLoginScreen()
                        self.activity.stopAnimating()
                    } else
                        if self.loginStatus == "failure" {
                            if let message = JSON.object(forKey: "message") as? NSString {
                                self.loginMessage = (message as String).decrypt()
                            }
                            self.showAlert("" , message: self.loginMessage)
                            self.activity.stopAnimating()
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}
