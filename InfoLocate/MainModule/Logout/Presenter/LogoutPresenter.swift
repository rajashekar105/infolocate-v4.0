import Foundation

class LogoutPresenter: LogoutModuleInput, LogoutViewOutput, LogoutInteractorOutput {
    
    weak var view: LogoutViewInput!
    var interactor: LogoutInteractorInput!
    var router: LogoutRouterInput!
    
    // MARK: - LogoutViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToLoginScreen() {
        router.navigateToLoginScreen()
    }
    
    // MARK: - LogoutModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - LogoutInteractorOutput
    
}
