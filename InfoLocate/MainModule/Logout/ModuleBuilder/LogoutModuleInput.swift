import Foundation

protocol LogoutModuleInput: class {
    
    func configureModule()
}
