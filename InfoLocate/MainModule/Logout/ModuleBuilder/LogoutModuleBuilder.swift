import UIKit

@objc class LogoutModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = LogoutControllerFromStoryboard()
        
        let router = LogoutRouter()
        router.viewController = viewController
        
        let presenter = LogoutPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = LogoutInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func LogoutControllerFromStoryboard() -> LogoutViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.LOGOUT_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! LogoutViewController
    }
    
}
