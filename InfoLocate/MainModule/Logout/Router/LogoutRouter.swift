import UIKit

class LogoutRouter:LogoutRouterInput {
    
    weak var viewController: LogoutViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToLoginScreen() {
        Navigation.navigateToLoginScreen(viewController: viewController)
    }
    
}
