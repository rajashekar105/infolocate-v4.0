import Foundation

protocol LogoutRouterInput {
    func back()
    func navigateToLoginScreen()
}
