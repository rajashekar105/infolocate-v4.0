import UIKit

class MenuRouter:MenuRouterInput {
    
    weak var viewController: MenuViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToDashboardViewController() {
        Navigation.navigateToDashboardScreen(viewController: viewController)
    }
    
    func navigateToTrackViewController() {
        Navigation.navigateToTrackScreen(viewController: viewController)
    }
    
    func navigateToLiveVehicleListViewController() {
        Navigation.navigateToLiveVehicleListScreen(viewController: viewController)
    }
    
    func navigateToTMStatusViewController() {
        Navigation.navigateToTMStatusScreen(viewController: viewController)
    }
    
    func navigateToAlertViewController() {
        Navigation.navigateToRecentAlertsScreen(viewController: viewController)
    }
    
    func navigateToDynamicViewController() {
        Navigation.navigateToDynamicScreen(viewController: viewController)
    }
    
    func navigateToLogoutViewController() {
        Navigation.navigateToLogoutScreen(viewController: viewController)
    }
}
