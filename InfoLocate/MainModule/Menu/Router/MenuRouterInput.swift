import Foundation

protocol MenuRouterInput {
    func back()
    func navigateToDashboardViewController()
    func navigateToTrackViewController()
    func navigateToLiveVehicleListViewController()
    func navigateToTMStatusViewController()
    func navigateToAlertViewController()
    func navigateToDynamicViewController()
    func navigateToLogoutViewController()
}
