import Foundation

class MenuPresenter: MenuModuleInput, MenuViewOutput, MenuInteractorOutput {
    
    weak var view: MenuViewInput!
    var interactor: MenuInteractorInput!
    var router: MenuRouterInput!
    
    // MARK: - MenuViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToDashboardViewController() {
        router.navigateToDashboardViewController()
    }
    
    func navigateToTrackViewController() {
        router.navigateToTrackViewController()
    }
    
    func navigateToLiveVehicleListViewController() {
        router.navigateToLiveVehicleListViewController()        
    }
    
    func navigateToTMStatusViewController() {
        router.navigateToTMStatusViewController()
    }
    
    func navigateToAlertViewController() {
        router.navigateToAlertViewController()
    }
    
    func navigateToDynamicViewController() {
        router.navigateToDynamicViewController()
    }
    
    func navigateToLogoutViewController() {
        router.navigateToLogoutViewController()
    }
    
    // MARK: - MenuModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - MenuInteractorOutput
    
}
