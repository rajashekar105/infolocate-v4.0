import UIKit

class MenuViewController: BaseViewController, MenuViewInput {

    var output: MenuViewOutput!
    /// This property is versionLabel.
    @IBOutlet weak var versionLabel: UILabel!
    /// This property is userName.
    @IBOutlet weak var userName: UILabel!
    /// This property is menuTableView.
    @IBOutlet weak var menuTableView: UITableView!
    /// This property is itemArray.
    var itemArray = [NSLocalizedString("Dashboard", comment: ""),
                     NSLocalizedString("Track", comment: ""),
                     NSLocalizedString("Live Video", comment: ""),
                     NSLocalizedString("Alerts", comment: ""),
                     NSLocalizedString("Dynamic", comment: ""),
                     NSLocalizedString("Logout", comment: "")]
    /// This property is itemImagesArray.
    var itemImagesArray =
        ["dashboard icon","track icon","video","alert icon","dynamic icon","logout"]
    var ctype = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        registerTableView()
//        self.defaults.setValue("raj", forKey: "username")
        print("Welcome \(defaults.object(forKey: "username") as? String ?? "")")
        userName.text = "Welcome \(defaults.object(forKey: "username") as? String ?? "")"
        
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        ctype = Defaults.getValueForKey(key: Constants.ctype) as! String
        let mdvrenabled = self.defaults.object(forKey: "mdvrenabled") as Any as! Int
        if mdvrenabled == 0 {
            if ctype == Constants.XMIX {
                itemImagesArray = ["dashboard icon","track icon","ctype","alert icon","dynamic icon","logout"]
                itemArray = [English.Dashboard,
                             English.Track,
                             "TM Status",
                             English.Alerts,
                             English.Dynamic,
                             English.Logout]
            } else {
                itemImagesArray = ["dashboard icon","track icon","alert icon","dynamic icon","logout"]
                if type == Constants.english {
                    itemArray = [English.Dashboard,
                                 English.Track,
                                 English.Alerts,
                                 English.Dynamic,
                                 English.Logout]
                    userName.text = "\(English.Welcome) \(defaults.object(forKey: "username") as! String)"
                } else if type == Constants.japanese {
                    itemArray = [Japanese.Dashboard,
                                 Japanese.Track,
                                 Japanese.Alerts,
                                 Japanese.Dynamic,
                                 Japanese.Logout]
                    userName.text = "\(Japanese.Welcome) \(defaults.object(forKey: "username") as! String)"
                }
            }
             
        } else if mdvrenabled == 1 {
            if ctype == Constants.XMIX {
                itemImagesArray = ["dashboard icon","track icon","video","ctype","alert icon","dynamic icon","logout"]
                itemArray = [English.Dashboard,
                             English.Track,
                             English.Live_Video,
                             "TM Status",
                             English.Alerts,
                             English.Dynamic,
                             English.Logout]
            } else {
                itemImagesArray = ["dashboard icon","track icon","video","alert icon","dynamic icon","logout"]
                if type == Constants.english {
                    itemArray = [English.Dashboard,
                                 English.Track,
                                 English.Live_Video,
                                 English.Alerts,
                                 English.Dynamic,
                                 English.Logout]
                    userName.text = "\(English.Welcome) \(defaults.object(forKey: "username") as! String)"
                } else if type == Constants.japanese {
                    itemArray = [Japanese.Dashboard,
                                 Japanese.Track,
                                 Japanese.Live_Video,
                                 Japanese.Alerts,
                                 Japanese.Dynamic,
                                 Japanese.Logout]
                    userName.text = "\(Japanese.Welcome) \(defaults.object(forKey: "username") as! String)"
                }
            }
            
        }
        
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        if type == Constants.english {
            versionLabel.text = "\(English.Version): \(appVersion)"
        } else if type == Constants.japanese {
            versionLabel.text = "\(Japanese.Version): \(appVersion)"
        }
        
        getUserBasicInfo()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func goAndHideLoading() {
        //        hideLoading()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This function is used to register table view.
    func registerTableView() {
        menuTableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        menuTableView.separatorStyle = .none
        
    }
    
    func getUserBasicInfo() {
        //        self.userName.text = "\(Defaults.getValueForKey(key: UserDetailsServerMap.name))"
    }
    
}

extension MenuViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count //Constants.MenuItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MenuTableViewCell
        //        cell.delegate = self
        //cell.menuItemLabel.text = "Item \(indexPath.row)" //Constants.MenuItems[indexPath.row]
        cell.imageView?.image = UIImage(named: itemImagesArray[indexPath.row])
        cell.textLabel?.text = itemArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        //        dismiss(animated: true, completion: nil)
        let mdvrenabled = self.defaults.object(forKey: "mdvrenabled") as Any as! Int
        if mdvrenabled == 0 {
            if ctype == Constants.XMIX {
                switch (indexPath.row) {
                case 0:
                    self.output.navigateToDashboardViewController()
                    break
                case 1:
                    self.output.navigateToTrackViewController()
                    break
                case 2:
                    self.output.navigateToTMStatusViewController()
                    break
                case 3:
                    self.output.navigateToAlertViewController()
                    break
                case 4:
                    self.output.navigateToDynamicViewController()
                    break
                case 5:
                    self.output.navigateToLogoutViewController()
                    break
                case 6:
                    break
                default:
                    break
                }
            } else {
                switch (indexPath.row) {
                case 0:
                    self.output.navigateToDashboardViewController()
                    break
                case 1:
                    self.output.navigateToTrackViewController()
                    break
                case 2:
                    self.output.navigateToAlertViewController()
                    break
                case 3:
                    self.output.navigateToDynamicViewController()
                    break
                case 4:
                    self.output.navigateToLogoutViewController()
                    break
                case 5:
                    break
                case 6:
                    break
                default:
                    break
                }
            }
            
        } else if mdvrenabled == 1 {
            if ctype == Constants.XMIX {
                switch (indexPath.row) {
                case 0:
                    self.output.navigateToDashboardViewController()
                    break
                case 1:
                    self.output.navigateToTrackViewController()
                    break
                case 2:
                    self.output.navigateToLiveVehicleListViewController()
                    break
                case 3:
                    self.output.navigateToTMStatusViewController()
                    break
                case 4:
                    self.output.navigateToAlertViewController()
                    break
                case 5:
                    self.output.navigateToDynamicViewController()
                    break
                case 6:
                    self.output.navigateToLogoutViewController()
                    break
                default:
                    break
                }
            } else {
                switch (indexPath.row) {
                case 0:
                    self.output.navigateToDashboardViewController()
                    break
                case 1:
                    self.output.navigateToTrackViewController()
                    break
                case 2:
                    self.output.navigateToLiveVehicleListViewController()
                    break
                case 3:
                    self.output.navigateToAlertViewController()
                    break
                case 4:
                    self.output.navigateToDynamicViewController()
                    break
                case 5:
                    self.output.navigateToLogoutViewController()
                    break
                case 6:
                    break
                default:
                    break
                }
            }
        }
        
    }
}
