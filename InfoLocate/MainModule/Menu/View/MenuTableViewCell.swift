//
//  MenuTableViewCell.swift
//  InfoLocateFMS
//
//  Created by Infotrack on 18/03/20.
//  Copyright © 2020 certInfotrack. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    /// This property is menuItemLabel.
    @IBOutlet weak var menuItemLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
