import Foundation

protocol MenuModuleInput: class {
    
    func configureModule()
}
