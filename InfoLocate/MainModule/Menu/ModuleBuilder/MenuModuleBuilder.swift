import UIKit

@objc class MenuModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = MenuControllerFromStoryboard()
        
        let router = MenuRouter()
        router.viewController = viewController
        
        let presenter = MenuPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = MenuInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func MenuControllerFromStoryboard() -> MenuViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.MENU_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! MenuViewController
    }
    
}
