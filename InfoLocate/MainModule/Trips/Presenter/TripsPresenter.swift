import Foundation

class TripsPresenter: TripsModuleInput, TripsViewOutput, TripsInteractorOutput {
    
    weak var view: TripsViewInput!
    var interactor: TripsInteractorInput!
    var router: TripsRouterInput!
    
    // MARK: - TripsViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToAnimationScreen(animationDetails: AnimationModel) {
        router.navigateToAnimationScreen(animationDetails: animationDetails)
    }
    
    // MARK: - TripsModuleInput
    
    func configureModule(vehicleNumber: String) {
        view.configureModule(vehicleNumber: vehicleNumber)
    }
    
    // MARK: - TripsInteractorOutput
    
}
