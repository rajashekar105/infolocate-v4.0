import Foundation

protocol TripsModuleInput: class {
    
    func configureModule(vehicleNumber: String)
}
