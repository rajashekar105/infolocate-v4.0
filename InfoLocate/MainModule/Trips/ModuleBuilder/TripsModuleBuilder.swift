import UIKit

@objc class TripsModuleBuilder: NSObject {
    
    func build(vehicleNumber: String) -> UIViewController {
        let viewController = TripsControllerFromStoryboard()
        
        let router = TripsRouter()
        router.viewController = viewController
        
        let presenter = TripsPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = TripsInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(vehicleNumber: vehicleNumber)
        
        return viewController
    }
    
    func TripsControllerFromStoryboard() -> TripsViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.TRIPS_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! TripsViewController
    }
    
}
