import UIKit
import Alamofire

class TripsViewController: BaseViewController, TripsViewInput, UITableViewDelegate, UITableViewDataSource  {
    
    /// This property is outpu for TripsViewOutput.
    var output: TripsViewOutput!
    /// This property is formatter.
    let formatter = DateFormatter()
    /// This property is dataArray.
    var dataArray = NSArray()
    /// This property is vehicleNo.
    var vehicleNo: String = ""
    /// This property is vehicleName.
    var vehicleName: String = ""
    /// This property is fromplaceArray.
    var fromplaceArray: [String] = []
    /// This property is toplaceArray.
    var toplaceArray: [String] = []
    /// This property is ignitionstartdatetimeArray.
    var ignitionstartdatetimeArray: [String] = []
    /// This property is ignitionoffdatetimeArray.
    var ignitionoffdatetimeArray: [String] = []
    /// This property is travelleddistArray.
    var travelleddistArray: [Int] = []
    /// This property is tripDurationArray.
    var tripDurationArray: [String] = []
    /// This property is ignitionStartTime
    var ignitionStartTime: String = ""
    /// This property is ignitionStopTime.
    var ignitionStopTime: String = ""
    /// This property is titleLabel.
    @IBOutlet var titleLabel: UILabel!
    /// This property is uitableview.
    @IBOutlet var uitableview: UITableView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    
    func configureModule(vehicleNumber: String) {
        vehicleNo = vehicleNumber
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = NSLocalizedString("Trips", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.titleLabel.text = English.Trips
        } else if type == Constants.japanese {
            self.titleLabel.text = Japanese.Trips
        }
        activity.layer.cornerRadius = 10
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            GetIgnitionDetails()
            activity.startAnimating()
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /// This method is back button action
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let fromAddress:String = fromplaceArray[indexPath.row]
        let fromAddressheight = fromAddress.height(withConstrainedWidth: tableView.frame.size.width - 72, font: UIFont(name: "Avenir-Light", size: 13.0)!)
        let actualFromAddressHeight = max(30,fromAddressheight)
        let toAddress:String = toplaceArray[indexPath.row]
        let toAddressheight = toAddress.height(withConstrainedWidth: tableView.frame.size.width - 72, font: UIFont(name: "Avenir-Light", size: 13.0)!)
        let actualToAddressHeight = max(40,toAddressheight)
        return actualFromAddressHeight + actualToAddressHeight + 140
        //        return UITableView.automaticDimension
        //return 190
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fromplaceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TripsTableViewCell
        
        cell.ViewBackground.layer.shadowColor = UIColor.darkGray.cgColor
        cell.ViewBackground.layer.shadowOffset = CGSize.zero
        cell.ViewBackground.layer.shadowRadius = 1.0
        cell.ViewBackground.layer.shadowOpacity = 0.4

        cell.ignitionStartTime.text = ignitionstartdatetimeArray[indexPath.row]
        cell.ignitionStopTime.text = ignitionoffdatetimeArray[indexPath.row]
        cell.fromAddress.text = fromplaceArray[indexPath.row]
        cell.toAddress.text = toplaceArray[indexPath.row]
        cell.travelledDistance.text = "\(travelleddistArray[indexPath.row]) Kms"
        cell.tripDuration.text = tripDurationArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ignitionStartTime = convertDateFormat(dateString: ignitionstartdatetimeArray[indexPath.row])
        ignitionStopTime = convertDateFormat(dateString: ignitionoffdatetimeArray[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
        let animationDetails = AnimationModel().addAnimationDetailsWith(
            vehicleno: vehicleNo,
            ignitionStartTime: ignitionStartTime,
            ignitionStopTime: ignitionStopTime)
        output.navigateToAnimationScreen(animationDetails: animationDetails)
    }
    
    /// This method is used to convertDateFormat.
    func convertDateFormat(dateString: String) -> String {
        print(dateString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        print("Dateobj: \(dateFormatter.string(from: dateObj!))")
        return (dateFormatter.string(from: dateObj!))
    }
    
    /// This method is used to GetIgnitionDetails with api.
    func GetIgnitionDetails() {
        fromplaceArray = []
        toplaceArray = []
        ignitionstartdatetimeArray = []
        ignitionoffdatetimeArray = []
        travelleddistArray = []
        var baseUrl:String = ""
        let date = Date()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let currentDate = formatter.string(from: date)
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        let parameters = [
            "VehicleNo" : vehicleNo.encrypt(),
            "IgnitionDate" : currentDate.encrypt()
        ]
        print("\(baseUrl)GetIgnitionDetails")
        print(parameters)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        AF.request("\(baseUrl)GetIgnitionDetails", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let sts = JSON.object(forKey: "status") as? NSString {
                        self.status = (sts as String).decrypt()
                    }
                    if self.status == "success" {
                        self.dataArray = JSON.object(forKey: "lastTenDataFromCurrentTime")as! NSArray
                        for i in 0 ..< self.dataArray.count {
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "fromplace") as? String {
                                self.fromplaceArray.append(value.decrypt())
                            }
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "toplace") as? String {
                                self.toplaceArray.append(value.decrypt())
                            }
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "ignitionstartdatetime") as? String {
                                self.ignitionstartdatetimeArray.append(value.decrypt())
                            }
                            if let value: String = (self.dataArray[i] as AnyObject).object(forKey: "ignitionoffdatetime") as? String {
                                self.ignitionoffdatetimeArray.append(value.decrypt())
                            }
                            if let value = (self.dataArray[i] as AnyObject).object(forKey: "travelleddist") as? String {
                                self.travelleddistArray.append(Int(value.decrypt())!)
                            }
                            //tripDuration
                            if let value = (self.dataArray[i] as AnyObject).object(forKey: "tripduration") as? String {
                                print(value.decrypt())
                                self.tripDurationArray.append(value.decrypt())
                            } else {
                                var value = ""
                                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                                if type == Constants.english {
                                    value = English.NA
                                } else if type == Constants.japanese {
                                    value = Japanese.NA
                                }
                                self.tripDurationArray.append(value)
                            }
                        }
                        DispatchQueue.main.async(execute: {
                            self.uitableview.reloadData()
                            self.activity.stopAnimating()
                        })
                    } else if self.status == "failure" {
                        if let msg = JSON.object(forKey: "message") as? NSString {
                            self.message = (msg as String).decrypt()
                        }
                        if self.message ==  English.No_records_found_dynamic {
                            if type == Constants.english {
                                self.message = English.No_records_found_dynamic
                            } else if type == Constants.japanese {
                                self.message = Japanese.No_records_found_dynamic
                            }
                        }
                        self.showAlert("", message: self.message)
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                        })
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}
