//
//  TripsTableViewCell.swift
//  InfoLocateFMS
//
//  Created by infotrack telematics on 09/05/17.
//  Copyright © 2017 certInfotrack. All rights reserved.
//

import UIKit

class TripsTableViewCell: UITableViewCell {

    /// This property is ignitionStartTime.
    @IBOutlet var ignitionStartTime: UILabel!
    /// This property is ignitionStopTime.
    @IBOutlet var ignitionStopTime: UILabel!
    /// This property is fromAddress.
    @IBOutlet var fromAddress: UILabel!
    /// This property is toAddress.
    @IBOutlet var toAddress: UILabel!
    /// This property is travelledDistance.
    @IBOutlet var travelledDistance: UILabel!
    /// This property is tripDuration.
    @IBOutlet var tripDuration: UILabel!
    /// This property is ViewBackground.
    @IBOutlet var ViewBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
