import Foundation

protocol TripsViewInput: class {
    func configureModule(vehicleNumber: String)
}
