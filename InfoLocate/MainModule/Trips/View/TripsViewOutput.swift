import Foundation

protocol TripsViewOutput {
    func back()
    func navigateToAnimationScreen(animationDetails: AnimationModel)
}
