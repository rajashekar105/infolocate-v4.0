import UIKit

class TripsRouter:TripsRouterInput {
    
    weak var viewController: TripsViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToAnimationScreen(animationDetails: AnimationModel){
        Navigation.navigateToAnimationScreen(animationDetails: animationDetails, viewController: viewController)
    }
    
}
