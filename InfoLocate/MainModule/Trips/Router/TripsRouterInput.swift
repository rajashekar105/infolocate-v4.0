import Foundation

protocol TripsRouterInput {
    func back()
    func navigateToAnimationScreen(animationDetails: AnimationModel)
}
