import UIKit

class TMStatusRouter:TMStatusRouterInput {
    
    weak var viewController: TMStatusViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
