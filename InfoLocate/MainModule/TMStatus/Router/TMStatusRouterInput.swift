import Foundation

protocol TMStatusRouterInput {
    func back()
}
