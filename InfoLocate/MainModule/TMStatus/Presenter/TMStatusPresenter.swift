import Foundation

class TMStatusPresenter: TMStatusModuleInput, TMStatusViewOutput, TMStatusInteractorOutput {
    
    weak var view: TMStatusViewInput!
    var interactor: TMStatusInteractorInput!
    var router: TMStatusRouterInput!
    
    // MARK: - TMStatusViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - TMStatusModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - TMStatusInteractorOutput
    
}
