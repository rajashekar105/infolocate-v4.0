import Foundation

protocol TMStatusModuleInput: class {
    
    func configureModule()
}
