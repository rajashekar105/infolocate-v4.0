import UIKit

@objc class TMStatusModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = TMStatusControllerFromStoryboard()
        
        let router = TMStatusRouter()
        router.viewController = viewController
        
        let presenter = TMStatusPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = TMStatusInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func TMStatusControllerFromStoryboard() -> TMStatusViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.TMSTATUS_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! TMStatusViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.MAINMODULE_STORYBOARD, bundle: Bundle.main)
        return storyboard
    }
    
}
