import Foundation

protocol TMStatusViewOutput {
    func back()
}
