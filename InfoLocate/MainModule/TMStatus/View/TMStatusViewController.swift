import UIKit
import Charts
import SwiftHEXColors
import Alamofire
import UserNotifications
import Foundation

class TMStatusViewController: BaseViewController, TMStatusViewInput, ChartViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    /// This property is output for TMStatusViewOutput.
    var output: TMStatusViewOutput!
    /// This property is LoginID.
    var LoginID: String = ""
    /// This property is loginStatus.
    var loginStatus: String = ""
    /// This property is loginMessage.
    var loginMessage: String = ""
    /// This property is keyArray.
    var keyArray: [String] = []
    /// This property is valueArray.
    var valueArray: [String] = []
    /// This property is totalCount.
    var totalCount: String = ""
    /// This property is statusnameArrayDisplay.
    var statusnameArrayDisplay: [String] = []
    /// This property is statusnameArray.
    var statusnameArray: [String] = []
    /// This property is statusIdArray.
    var statusIdArray: [String] = []
    /// This property is chartStatusIdArray.
    var chartStatusIdArray: [String] = []
    /// This property is chartStatusnameArray.
    var chartStatusnameArray: [String] = []
    /// This property is statusColorArray.
    var statusColorArray: [String] = []
    /// This property is pieChartNameArray.
    var pieChartNameArray: [String] = []
    /// This property is pieChartcountArray.
    var pieChartcountArray: [Double] = []
    /// This property is pieChartIDArray.
    var pieChartIDArray: [String] = []
    /// This property is pieChartColorArray.
    var pieChartColorArray: [String] = []
    /// This property is colors.
    var colors: [UIColor] = []
    /// This property is chartColors.
    var chartColors: [UIColor] = []
    /// This property is listType.
    var listType: String = ""
    /// This property is totalCountLabel.
    @IBOutlet var totalCountLabel: UILabel!
    /// This property is uiTableView.
    @IBOutlet var uiTableView: UITableView!
    /// This property is chartView.
    @IBOutlet var chartView: PieChartView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is view1.
    @IBOutlet var view1: UIView!
    /// This property is view2.
    @IBOutlet var view2: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        totalCountLabel.text = "TM Status"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        defaults.setValue("false", forKey: "isFiltered")
        keyArray = []
        valueArray = []
        totalCount  = ""
        statusnameArrayDisplay = []
        statusColorArray = []
        pieChartNameArray = []
        pieChartcountArray = []
        pieChartIDArray = []
        pieChartColorArray = []
        colors = []
        listType = ""
        chartColors = []
        chartStatusIdArray = []
        chartStatusnameArray = []
        activity.layer.cornerRadius = 10
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            if let ID = defaults.value(forKey: "LoginID") {
                LoginID = ID as! String
            }
            GetVehicleStats()
            activity.startAnimating()
            //                uiTableView.isHidden = true
            //                chartView.isHidden = true
            view.isUserInteractionEnabled = false
        }
        chartView.delegate = self
        chartView.noDataText = ""
        uiTableView.delegate = self
        uiTableView.dataSource = self
        let tblView =  UIView(frame: CGRect.zero)
        uiTableView.tableFooterView = tblView
        uiTableView.backgroundColor = UIColor.white
        view1.layer.shadowColor = UIColor.black.cgColor
        view1.layer.shadowOffset = CGSize.zero
        view1.layer.shadowRadius = 3.0
        view1.layer.shadowOpacity = 0.4
        view2.layer.shadowColor = UIColor.black.cgColor
        view2.layer.shadowOffset = CGSize.zero
        view2.layer.shadowRadius = 3.0
        view2.layer.shadowOpacity = 0.4
        statusnameArrayDisplay = ["Queing",
                                  "Plant",
                                  "Loading",
                                  "Check Point",
                                  "On Route",
                                  "Paused",
                                  "On Site",
                                  "Unloading",
                                  "Left Site",
                                  "Idle",
                                  "Workshop",
                                  "Washing",
                                  "TM Inactive",
                                  "State NA"]
        statusnameArray = ["queing",
                         "plant",
                         "loading",
                         "checkpoint",
                         "onroute",
                         "paused",
                         "onsite",
                         "unloading",
                         "leftsite",
                         "idle",
                         "workshop",
                         "washing",
                         "tminactive",
                         "statena"]
        statusColorArray = ["#0cc003", "#ffb848", "#e7191b", "#807D7D", "#852b99", "#f0ff00", "#27a9e3",
        "#ff814b", "#00ffb0", "#186b2e", "#005b96", "#ffc300", "#a52a2a", "#79D6F9"]
    }
    
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is menu button action.
    @IBAction func menuButtonAction(_ sender: UIButton) {
        openSideMenu()
    }
    
    /// This method is refresh button action.
    @IBAction func refreshButton(_ sender: AnyObject) {
        if self.activity.isAnimating == true {
            // do nothing.
        } else {
            viewWillAppear(true)
        }
    }
    
    /// This method is used to setChart  data.
    func setChart(_ dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            if self.pieChartcountArray[i] != 0 {
                let dataEntry = ChartDataEntry(x: values[i], y: pieChartcountArray[i], data: pieChartcountArray[i] as AnyObject)
                dataEntries.append(dataEntry)
                chartColors.append(colors[i])
                chartStatusnameArray.append(statusnameArray[i])
            }
        }
        // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        pieChartDataSet.colors = chartColors
        pieChartDataSet.selectionShift = 0.0
        // 3. Set ChartData
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        // 4. Assign it to the chart’s data
        chartView.data = pieChartData
        chartView.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption:ChartEasingOption.easeOutBack)
        chartView.drawHoleEnabled = false
        chartView.drawEntryLabelsEnabled = false
        chartView.rotationEnabled = false
        chartView.legend.enabled = false
        chartView.isUserInteractionEnabled = true
        chartView.data?.setValueTextColor(UIColor.white)
        if chartColors.count == 0 {
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                chartView.centerText = English.No_Records_found
            } else if type == Constants.japanese {
                chartView.centerText = Japanese.No_Records_found
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pieChartNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DashBoardTableViewCell
        
        cell.colorView.layer.cornerRadius = 13
        cell.colorView.backgroundColor = colors[(indexPath as NSIndexPath).row]
        cell.nameLabel.text = pieChartNameArray[(indexPath as NSIndexPath).row]
        cell.countLabel.text = String(Int(pieChartcountArray[(indexPath as NSIndexPath).row]))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // do nothing
    }
    
    /// This method is used to GetVehicleStats with api.
    func GetVehicleStats() {
        var baseUrl:String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        print(LoginID)
        let parameters = [
            "userid" : LoginID.encrypt(),
        ]
        print("\(baseUrl)"+"GetTransitMixerStatus")
        print(parameters)
        AF.request("\(baseUrl)"+"GetTransitMixerStatus", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                print(parameters)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                return
            }
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    
                    if let status = JSON.object(forKey: "status") as? NSString {
                        self.loginStatus = (status as String).decrypt()
                    }
                    if self.loginStatus == "success" {
                        DispatchQueue.main.async(execute: {
                            if let count = JSON.object(forKey: "total") as? String
                            {
                                self.totalCount = count.decrypt()
                            }
                            for (key, value) in JSON
                            {
                                self.keyArray.append(String(describing: key))
                                self.valueArray.append(String(describing: value))
                            }
                            for i in 0 ..< self.statusnameArray.count
                            {
                                if self.keyArray.contains(self.statusnameArray[i])
                                {
                                    self.pieChartNameArray.append(self.statusnameArrayDisplay[i])
                                    self.pieChartColorArray.append(self.statusColorArray[i])
                                    let indexOfA = self.keyArray.firstIndex(of: self.statusnameArray[i])
                                    self.pieChartcountArray.append(NSString(string: self.valueArray[indexOfA!].decrypt()).doubleValue)
                                }
                            }
                            for a in self.pieChartColorArray {
                                let b: UIColor = UIColor(hexString: a)!
                                self.colors.append(b)
                            }
                            self.setChart(self.pieChartNameArray, values: self.pieChartcountArray)
                            self.totalCountLabel.text = "TM Status (Fleet Total - \(self.totalCount))"
                            self.uiTableView.reloadData()
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                        })
                    }
                    else
                        if self.loginStatus == "failure" {
                            if let message = JSON.object(forKey: "message") as? NSString {
                                self.loginMessage = (message as String).decrypt()
                            }
                            self.showAlert("" , message: self.loginMessage)
                            DispatchQueue.main.async(execute: {
                                self.activity.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                            })
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}
