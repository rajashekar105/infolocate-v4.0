import Foundation

protocol AnimationRouterInput {
    func back()
}
