import UIKit

class AnimationRouter:AnimationRouterInput {
    
    weak var viewController: AnimationViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
