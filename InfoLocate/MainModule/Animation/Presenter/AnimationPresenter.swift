import Foundation

class AnimationPresenter: AnimationModuleInput, AnimationViewOutput, AnimationInteractorOutput {
    
    weak var view: AnimationViewInput!
    var interactor: AnimationInteractorInput!
    var router: AnimationRouterInput!
    
    // MARK: - AnimationViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - AnimationModuleInput
    
    func configureModule(animationDetails: AnimationModel) {
        view.configureModule(animationDetails: animationDetails)
    }
    
    // MARK: - AnimationInteractorOutput
    
}
