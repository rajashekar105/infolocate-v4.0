import UIKit

@objc class AnimationModuleBuilder: NSObject {
    
    func build(animationDetails: AnimationModel) -> UIViewController {
        let viewController = AnimationControllerFromStoryboard()
        
        let router = AnimationRouter()
        router.viewController = viewController
        
        let presenter = AnimationPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = AnimationInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(animationDetails: animationDetails)
        
        return viewController
    }
    
    func AnimationControllerFromStoryboard() -> AnimationViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.ANIMATION_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! AnimationViewController
    }
    
}
