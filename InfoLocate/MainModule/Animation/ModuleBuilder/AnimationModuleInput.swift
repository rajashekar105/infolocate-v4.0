import Foundation

protocol AnimationModuleInput: class {
    
    func configureModule(animationDetails: AnimationModel)
}
