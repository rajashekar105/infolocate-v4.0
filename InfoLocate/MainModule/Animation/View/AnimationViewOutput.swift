import Foundation

protocol AnimationViewOutput {
    func back()
}
