import Foundation

protocol AnimationViewInput: class {
         func configureModule(animationDetails: AnimationModel)
}
