//
//  Animation.swift
//  InfoLocate
//
//  Created by Infotrack on 23/03/20.
//  Copyright © 2020 Hemalatha T. All rights reserved.
//

import Foundation

class AnimationModel: NSObject {
    var vehicleno : String!
    var ignitionStartTime : String!
    var ignitionStopTime : String!

    func addAnimationDetailsWith(vehicleno : String, ignitionStartTime : String, ignitionStopTime : String) -> AnimationModel {
        self.vehicleno = vehicleno
        self.ignitionStartTime = ignitionStartTime
        self.ignitionStopTime = ignitionStopTime
        return self
    }
    
}
