import Foundation

protocol DynamicRouterInput {
    func back()
    func navigateToMapViewController(mapDetails: DynamicMapModel)
}
