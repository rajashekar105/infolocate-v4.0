import UIKit

class DynamicRouter:DynamicRouterInput {
    
    weak var viewController: DynamicViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToMapViewController(mapDetails: DynamicMapModel) {
        Navigation.navigateToDynamicMapViewScreen(mapDetails: mapDetails, viewController: viewController)
    }
}
