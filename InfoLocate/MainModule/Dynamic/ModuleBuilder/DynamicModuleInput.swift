import Foundation

protocol DynamicModuleInput: class {
    
    func configureModule()
}
