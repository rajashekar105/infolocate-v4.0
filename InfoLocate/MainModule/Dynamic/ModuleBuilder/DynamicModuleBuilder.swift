import UIKit

@objc class DynamicModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = DynamicControllerFromStoryboard()
        
        let router = DynamicRouter()
        router.viewController = viewController
        
        let presenter = DynamicPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = DynamicInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func DynamicControllerFromStoryboard() -> DynamicViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.DYNAMIC_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! DynamicViewController
    }
    
}
