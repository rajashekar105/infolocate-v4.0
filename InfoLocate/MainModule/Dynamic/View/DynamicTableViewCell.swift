//
//  DynamicStatusTableViewCell.swift
//  InfoLocateFMS
//
//  Created by infotrack telematics on 04/07/16.
//  Copyright © 2016 InfotrackTelematics. All rights reserved.
//

import UIKit

class DynamicTableViewCell: UITableViewCell {

    /// This property is statusLabel.
    @IBOutlet var statusLabel: UILabel!
    /// This property is vehicleNoLabel.
    @IBOutlet var vehicleNoLabel: UILabel!
    /// This property is trackTimeLabel.
    @IBOutlet var trackTimeLabel: UILabel!
    /// This property is acStatusLabel.
    @IBOutlet var acStatusLabel: UILabel!
    /// This property is fuelLevelLabel.
    @IBOutlet var fuelLevelLabel: UILabel!
    /// This property is durationLabel.
    @IBOutlet var durationLabel: UILabel!
    /// This property is mobileno.
    @IBOutlet var mobileno: UILabel!
    /// This property is tempertureLabel.
    @IBOutlet var tempertureLabel: UILabel!
    /// This property is speedLabel.
    @IBOutlet var speedLabel: UILabel!
    /// This property is gpsLabel.
    @IBOutlet var gpsLabel: UILabel!
    /// This property is usernameLabel.
    @IBOutlet var usernameLabel: UILabel!
    /// This property is locationLabel.
    @IBOutlet var locationLabel: UILabel!
    /// This property is ignitionLabel.
    @IBOutlet var ignitionLabel: UILabel!
    /// This property is ViewBackground.
    @IBOutlet var ViewBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
