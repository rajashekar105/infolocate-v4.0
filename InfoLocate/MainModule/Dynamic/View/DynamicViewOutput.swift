import Foundation

protocol DynamicViewOutput {
    func back()
    func navigateToMapViewController(mapDetails: DynamicMapModel)
}
