import UIKit
import Alamofire

class DynamicViewController: BaseViewController, DynamicViewInput, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    /// This property is output for DynamicViewOutput.
    var output: DynamicViewOutput!
    /// This property is statusLabel.
    @IBOutlet var statusLabel: UILabel!
    /// This property is searchBar.
    @IBOutlet var searchBar: UISearchBar!
    /// This property is uiTableView.
    @IBOutlet var uiTableView: UITableView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is searchLayoutConstraint.
    @IBOutlet var searchLayoutConstraint: NSLayoutConstraint!
    /// This property is groupDefaults.
    let groupDefaults = UserDefaults.standard
    /// This property is dataArray.
    var dataArray = NSArray()
    /// This property is vehicleGroupArray.
    var vehicleGroupArray = NSArray()
    /// This property is groupId.
    var groupId : Int = 0
    /// This property is vehiclestatus.
    var vehiclestatus: String = ""
    /// This property is loginGroupId.
    var loginGroupId: Int = 0
    /// This property is groupName.
    var groupName : String = ""
    /// This property is vehicleSearchString.
    var vehicleSearchString : String = ""
    /// This property is groupSelection.
    var groupSelection : Bool!
    /// This property is LoginID.
    var LoginID: String = ""
    /// This property is currentlat.
    var currentlat: Double!
    /// This property is currentlng.
    var currentlng: Double!
    /// This property is vehicleno.
    var vehicleno: String = ""
    /// This property is vehiclename.
    var vehiclename: String = ""
    /// This property is drivername.
    var drivername: String = ""
    /// This property is mobileno.
    var mobileno: String = ""
    /// This property is temperature.
    var temperature: String = ""
    /// This property is acStatus.
    var acStatus: String = ""
    /// This property is fuel.
    var fuel: String = ""
    /// This property is duration.
    var duration: String = ""
    /// This property is previousdistance.
    var previousdistance: String = ""
    /// This property is currentdistance.
    var currentdistance: String = ""
    /// This property is trackTime.
    var trackTime: String = ""
    /// This property is speed.
    var speed: String = ""
    /// This property is ignition.
    var ignition: String = ""
    /// This property is location.
    var location: String = ""
    /// This property is is_searching.
    var is_searching: Bool!
    /// This property is searchingDataArray.
    var searchingDataArray = NSMutableArray()
    /// This property is mainDataArray.
    var mainDataArray = NSArray()
    /// This property is workingArray.
    var workingArray = NSArray()
    /// This property is finalArray.
    var finalArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ExtraFieldsHidden()
        searchBar.placeholder = NSLocalizedString("Search Vehicle No/Driver Name", comment: "")
        searchBar.textField?.font = UIFont(name: "", size: 9.0)
        self.statusLabel.text = "\(NSLocalizedString("Dynamic Status", comment: "")) ( \(self.finalArray.count) )"
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            searchBar.placeholder = English.Search_Vehicle_No_Driver_Name
            self.statusLabel.text = "\(English.Dynamic_Status) ( \(self.finalArray.count) )"
        } else if type == Constants.japanese {
            searchBar.placeholder = Japanese.Search_Vehicle_No_Driver_Name
            self.statusLabel.text = "\(Japanese.Dynamic_Status) ( \(self.finalArray.count) )"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        defaults.setValue("false", forKey: "isFiltered")
        activity.layer.cornerRadius = 10
        apiCalling()
//        self.statusLabel.text = "\(NSLocalizedString("Dynamic Status", comment: "")) ( \(self.finalArray.count) )"
//        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//        if type == Constants.english {
//            self.statusLabel.text = "\(English.Dynamic_Status) ( \(self.finalArray.count) )"
//        } else if type == Constants.japanese {
//            self.statusLabel.text = "\(Japanese.Dynamic_Status) ( \(self.finalArray.count) )"
//        }
        is_searching = false
        searchBar.delegate = self
        let tblView =  UIView(frame: CGRect.zero)
        uiTableView.tableFooterView = tblView
        uiTableView.backgroundColor = UIColor.white
        uiTableView.delegate = self
        uiTableView.dataSource = self
//        GetSearchViewFromMap()
    }
    
    /// This method is menu button action.
    @IBAction func menuButtonAction(_ sender: UIButton) {
        openSideMenu()
    }
    
    /// This method is back button action.
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
    /// This method is refresh button action.
    @IBAction func refreshButton(_ sender: AnyObject) {
        if self.activity.isAnimating == true {
            // do nothing.
        } else {
            if isScrollingStart == false {
                searchingDataArray = []
                searchBar.text = ""
                dismissKeyboard()
                viewWillAppear(true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchDisplayControllerDidBeginSearch(_ controller: UISearchController) {
        self.searchBar.showsCancelButton = true
    }
    
    /// This method is called when searchBarCancelButtonClicked.
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false
        uiTableView.isHidden = false
        
        finalArray = workingArray
        uiTableView.reloadData()
        searchBar.text = ""
        dismissKeyboard()
        self.statusLabel.text = "\(NSLocalizedString("Dynamic Status", comment: "")) ( \(self.finalArray.count) )"
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.statusLabel.text = "\(English.Dynamic_Status) ( \(self.finalArray.count) )"
        } else if type == Constants.japanese {
            self.statusLabel.text = "\(Japanese.Dynamic_Status) ( \(self.finalArray.count) )"
        }
    }
    
    /// This method is called when searchBarSearchButtonClicked.
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }
    
    /// This method is used  to dismissKeyboard.
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /// This method is called when searchBar textDidChange.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        print(searchBar.text as Any)
        if searchBar.text!.isEmpty{
            is_searching = false
            uiTableView.isHidden = false
            finalArray = workingArray
            self.uiTableView.reloadData()
            self.statusLabel.text = "\(NSLocalizedString("Dynamic Status", comment: "")) ( \(self.finalArray.count) )"
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                self.statusLabel.text = "\(English.Dynamic_Status) ( \(self.finalArray.count) )"
            } else if type == Constants.japanese {
                self.statusLabel.text = "\(Japanese.Dynamic_Status) ( \(self.finalArray.count) )"
            }
        } else {
            is_searching = true
            searchingDataArray.removeAllObjects()
            for index in 0 ..< workingArray.count
            {
                var drivername = ""
                let vehicleNo = ((workingArray[index] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
                
                if (workingArray[index] as AnyObject).object(forKey: "username") == nil ||
                    ((workingArray[index] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "" ||
                    ((workingArray[index] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "-" ||
                    ((workingArray[index] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "NA" {
                    drivername = NSLocalizedString("NA", comment: "")
                    let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                    if type == Constants.english {
                        drivername = English.NA
                    } else if type == Constants.japanese {
                        drivername = Japanese.NA
                    }
                } else {
                    drivername = ((workingArray[index] as AnyObject).object(forKey: "username") as? String)!.decrypt()
                }
                
                if vehicleNo.lowercased().range(of: searchText.lowercased())  != nil ||
                    drivername.lowercased().range(of: searchText.lowercased())  != nil ||
                    (vehicleNo.lowercased().range(of: searchText.lowercased())  != nil && drivername.lowercased().range(of: searchText.lowercased())  != nil) {
                    searchingDataArray.add(workingArray[index])
                }
                vehicleSearchString = self.searchBar.text!
            }
            if(searchingDataArray.count == 0)
            {
                uiTableView.isHidden = true
                var message = NSLocalizedString("No Data Found.", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.No_Data_Found
                } else if type == Constants.japanese {
                    message = Japanese.No_Data_Found
                }
                alertForASec("", message: message)
            } else if(searchingDataArray.count > 0) {
                finalArray = searchingDataArray
                uiTableView.isHidden = false
                self.statusLabel.text = "\(NSLocalizedString("Dynamic Status", comment: "")) ( \(self.finalArray.count) )"
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    self.statusLabel.text = "\(English.Dynamic_Status) ( \(self.finalArray.count) )"
                } else if type == Constants.japanese {
                    self.statusLabel.text = "\(Japanese.Dynamic_Status) ( \(self.finalArray.count) )"
                }
            }
            self.uiTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let userlabel:String = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt()
        let height = userlabel.height(withConstrainedWidth: tableView.frame.size.width - 80, font: UIFont(name: "Avenir-Light", size: 13.0)!)
        let actualHeight = max(30,height)
        let location:String = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as? String)!.decrypt()
        let locationheight = location.height(withConstrainedWidth: tableView.frame.size.width - 80, font: UIFont(name: "Avenir-Light", size: 12.0)!)
        let actualLocationHeight = max(25,locationheight)
        return actualHeight + actualLocationHeight + 155        //        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == uiTableView) {
            return finalArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == uiTableView) {
            let cellIdentifier = "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DynamicTableViewCell
            cell.ViewBackground.layer.shadowColor = UIColor.darkGray.cgColor
            cell.ViewBackground.layer.shadowOffset = CGSize.zero
            cell.ViewBackground.layer.shadowRadius = 1.0
            cell.ViewBackground.layer.shadowOpacity = 0.4
            cell.statusLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclestatus") as? String)!.decrypt()
            var strStatus = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclestatus") as? String)!.decrypt()
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                // do nothing.
            } else if type == Constants.japanese {
                if strStatus.contains(English.Stopped) {
                    strStatus = strStatus.replacingOccurrences(of: English.Stopped, with: Japanese.Stopped)
                } else if strStatus.contains(English.Moving)  {
                    strStatus = strStatus.replacingOccurrences(of: English.Moving, with: Japanese.Moving)
                } else if strStatus.contains(English.Idle)  {
                    strStatus = strStatus.replacingOccurrences(of: English.Idle, with: Japanese.Idle)
                } else if strStatus.contains(English.Inactive){
                    strStatus = strStatus.replacingOccurrences(of: English.Inactive, with: Japanese.Inactive)
                }
                for i in 0...English.DirectionArray.count-1 {
                    if strStatus.contains(English.DirectionArray[i]) {
                        strStatus = strStatus.replacingOccurrences(of: English.DirectionArray[i], with: Japanese.DirectionArray[i])
                    }
                }
                cell.statusLabel.text = strStatus
            }
            cell.vehicleNoLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
            cell.trackTimeLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "tracktime") as? String)!.decrypt()
            cell.speedLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "speed") as? String)!.decrypt()
            
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "NA" {
                cell.tempertureLabel.text = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.tempertureLabel.text = English.NA
                } else if type == Constants.japanese {
                    cell.tempertureLabel.text = Japanese.NA
                }
            } else {
                cell.tempertureLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt()
            }
            
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus")) == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "NA" {
                cell.acStatusLabel.text = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.acStatusLabel.text = English.NA
                } else if type == Constants.japanese {
                    cell.acStatusLabel.text = Japanese.NA
                }
            } else {
                cell.acStatusLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt()
            }
            
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel")) == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "NA" {
                cell.fuelLevelLabel.text = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.fuelLevelLabel.text = English.NA
                } else if type == Constants.japanese {
                    cell.fuelLevelLabel.text = Japanese.NA
                }
            } else {
                cell.fuelLevelLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt()
            }
            
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration")) == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "NA" {
                cell.durationLabel.text = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.durationLabel.text = English.NA
                } else if type == Constants.japanese {
                    cell.durationLabel.text = Japanese.NA
                }
            } else {
                cell.durationLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt()
            }
            
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "gpsstatus") as? String)!.decrypt() == "GpsValid.gif" {
                cell.gpsLabel.text = NSLocalizedString("ON", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.gpsLabel.text = English.ON
                } else if type == Constants.japanese {
                    cell.gpsLabel.text = Japanese.ON
                }
            } else {
                cell.gpsLabel.text = NSLocalizedString("OFF", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.gpsLabel.text = English.OFF
                } else if type == Constants.japanese {
                    cell.gpsLabel.text = Japanese.OFF
                }
            }
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "NA" {
                cell.usernameLabel.text = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.usernameLabel.text = English.NA
                } else if type == Constants.japanese {
                    cell.usernameLabel.text = Japanese.NA
                }
            } else {
                cell.usernameLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt()
            }
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "NA" {
                cell.mobileno.text = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.mobileno.text = English.NA
                } else if type == Constants.japanese {
                    cell.mobileno.text = Japanese.NA
                }
            } else {
                cell.mobileno.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt()
            }
            cell.locationLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as? String)!.decrypt()
            cell.ignitionLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "ignition") as? String)!.decrypt()
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "ignition") as? String)!.decrypt() == "ON" {
                cell.ignitionLabel.text = NSLocalizedString("ON", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.ignitionLabel.text = English.ON
                } else if type == Constants.japanese {
                    cell.ignitionLabel.text = Japanese.ON
                }
            } else {
                cell.ignitionLabel.text = NSLocalizedString("OFF", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    cell.ignitionLabel.text = English.OFF
                } else if type == Constants.japanese {
                    cell.ignitionLabel.text = Japanese.OFF
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == uiTableView) {
            dismissKeyboard()
            currentlat = NSString(string: ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "lat") as? String)!.decrypt()).doubleValue
            currentlng = NSString(string: ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "lon") as? String)!.decrypt()).doubleValue
            vehicleno = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
            trackTime = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "tracktime") as? String)!.decrypt()
            speed = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "speed") as? String)!.decrypt()
            ignition = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "ignition") as? String)!.decrypt()
            location = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as? String)!.decrypt()
            vehiclestatus = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclestatus") as? String)!.decrypt()
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "ignition") as? String)!.decrypt() == "ON" {
                ignition = NSLocalizedString("ON", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    ignition = English.ON
                } else if type == Constants.japanese {
                    ignition = Japanese.ON
                }
            } else {
                ignition = NSLocalizedString("OFF", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    ignition = English.OFF
                } else if type == Constants.japanese {
                    ignition = Japanese.OFF
                }
            }
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "NA" {
                vehiclename = ""
            } else {
                vehiclename = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt()
            }
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "NA" {
                drivername = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    drivername = English.NA
                } else if type == Constants.japanese {
                    drivername = Japanese.NA
                }
            } else {
                drivername = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt()
            }
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "NA" {
                mobileno = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    mobileno = English.NA
                } else if type == Constants.japanese {
                    mobileno = Japanese.NA
                }
            } else {
                mobileno = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt()
            }
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "NA" {
                temperature = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    temperature = English.NA
                } else if type == Constants.japanese {
                    temperature = Japanese.NA
                }
            } else {
                temperature = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt()
            }
            
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "previousDistance") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "previousDistance") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "previousDistance") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "previousDistance") as? String)!.decrypt() == "NA" {
                previousdistance = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    previousdistance = English.NA
                } else if type == Constants.japanese {
                    previousdistance = Japanese.NA
                }
            } else {
                previousdistance = "\(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "previousDistance") as? String)!)"
            }
            if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "currentDistance") == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "currentDistance") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "currentDistance") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "currentDistance") as? String)!.decrypt() == "NA" {
                currentdistance = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    currentdistance = English.NA
                } else if type == Constants.japanese {
                    currentdistance = Japanese.NA
                }
            } else {
                currentdistance = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "currentDistance") as? String)!.decrypt()
            }
            
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus")) == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "NA" {
                acStatus = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    acStatus = English.NA
                } else if type == Constants.japanese {
                    acStatus = Japanese.NA
                }
            } else {
                acStatus = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt()
            }
            
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel")) == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "NA" {
                fuel = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    fuel = English.NA
                } else if type == Constants.japanese {
                    fuel = Japanese.NA
                }
            } else {
                fuel = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt()
            }
            
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration")) == nil ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "-" ||
                ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "NA" {
                duration = NSLocalizedString("NA", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    duration = English.NA
                } else if type == Constants.japanese {
                    duration = Japanese.NA
                }
            } else {
                duration = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt()
            }
            
            defaults.set("", forKey: "StatusName")
            if vehiclestatus.contains(NSLocalizedString("Stopped", comment: "")) {
                defaults.set("stopped", forKey: "StatusName")
            } else if vehiclestatus.contains(NSLocalizedString("Moving", comment: ""))  {
                defaults.set("moving", forKey: "StatusName")
            } else if vehiclestatus.contains(NSLocalizedString("Idle", comment: ""))  {
                defaults.set("idle", forKey: "StatusName")
            } else if vehiclestatus.contains(NSLocalizedString("Inactive", comment: "")){
                defaults.set("inactive", forKey: "StatusName")
            }
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                if vehiclestatus.contains(English.Stopped) {
                    defaults.set("stopped", forKey: "StatusName")
                } else if vehiclestatus.contains(English.Moving)  {
                    defaults.set("moving", forKey: "StatusName")
                } else if vehiclestatus.contains(English.Idle)  {
                    defaults.set("idle", forKey: "StatusName")
                } else if vehiclestatus.contains(English.Inactive){
                    defaults.set("inactive", forKey: "StatusName")
                }
            } else if type == Constants.japanese {
                //                if vehiclestatus.contains(Japanese.Stopped) {
                //                    defaults.set("stopped", forKey: "StatusName")
                //                } else if vehiclestatus.contains(Japanese.Moving)  {
                //                    defaults.set("moving", forKey: "StatusName")
                //                } else if vehiclestatus.contains(Japanese.Idle)  {
                //                    defaults.set("idle", forKey: "StatusName")
                //                } else if vehiclestatus.contains(Japanese.Inactive){
                //                    defaults.set("inactive", forKey: "StatusName")
                //                }
                if vehiclestatus.contains(English.Stopped) {
                    defaults.set("stopped", forKey: "StatusName")
                } else if vehiclestatus.contains(English.Moving)  {
                    defaults.set("moving", forKey: "StatusName")
                } else if vehiclestatus.contains(English.Idle)  {
                    defaults.set("idle", forKey: "StatusName")
                } else if vehiclestatus.contains(English.Inactive){
                    defaults.set("inactive", forKey: "StatusName")
                }
            }
            uiTableView.deselectRow(at: indexPath, animated: true)
            let mapDetails = DynamicMapModel().addMapDetailsWith(
                currentlat: currentlat,
                currentlng: currentlng,
                vehicleno: vehicleno,
                vehiclename: vehiclename,
                drivername: drivername,
                mobileno: mobileno,
                temperature: temperature,
                fuel: fuel,
                duration: duration,
                previousdistance: previousdistance,
                currentdistance: currentdistance,
                trackTime: trackTime,
                speed: speed,
                location: location,
                ignition: ignition,
                acstatus: acStatus,
                comingFrom: COMING_TYPE.DYNAMIC)
            output.navigateToMapViewController(mapDetails: mapDetails)
        }
    }
    
    var isScrollingStart: Bool = false
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if isScrollingStart {
                isScrollingStart = false
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if isScrollingStart {
            isScrollingStart = false
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        isScrollingStart = true
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isScrollingStart = true
    }
    
    /// This method is used to GetVehicleStatus with api.
    func GetVehicleStatus() {
        mainDataArray = []
        workingArray = []
        finalArray = []
        var baseUrl:String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        let parameters = [
            "userid" : LoginID.encrypt()
        ]
        print("\(baseUrl)"+"GetVehicleStatus")
        self.activity.startAnimating()
        self.view.isUserInteractionEnabled = false
        AF.request("\(baseUrl)"+"GetVehicleStatus", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                return
            }
//            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
//                    print(JSON)
                    if let sts = JSON.object(forKey: "status") as? NSString {
                        self.status = (sts as String).decrypt()
                    }
                    if self.status == "success" {
                        self.mainDataArray = JSON.object(forKey: "data") as! NSArray
                        self.workingArray = self.mainDataArray
                        self.finalArray = self.mainDataArray
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            self.uiTableView.isHidden = false
                            // MARK:- All Vehicle Status without grouping here it will reload data
                            // Here it reloading ALL Data into tableview
                            if (self.searchingDataArray.count > 0) {
                                self.finalArray = self.searchingDataArray
                                self.searchBar.text! = self.vehicleSearchString
                                self.uiTableView.reloadData()
                            } else {
                                self.uiTableView.reloadData()
                            }
                            self.statusLabel.text = "\(NSLocalizedString("Dynamic Status", comment: "")) ( \(self.finalArray.count) )"
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.statusLabel.text = "\(English.Dynamic_Status) ( \(self.finalArray.count) )"
                            } else if type == Constants.japanese {
                                self.statusLabel.text = "\(Japanese.Dynamic_Status) ( \(self.finalArray.count) )"
                            }
                        })
                    } else
                        if self.status == "failure" {
                            if let msg = JSON.object(forKey: "message") as? NSString {
                                self.message = (msg as String).decrypt()
                            }
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if self.message == English.No_Records_found || self.message == English.No_records_found_dynamic {
                                if type == Constants.english {
                                    self.message = English.No_Records_found
                                } else if type == Constants.japanese {
                                    self.message = Japanese.No_Records_found
                                }
                            }
                            self.showAlert("" , message: self.message)
                            DispatchQueue.main.async(execute: {
                                self.activity.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                            })
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    /// This method is used to GetSearchViewFromMap.
//    func GetSearchViewFromMap() {
//        dismissKeyboard()
//        if (searchingDataArray.count > 0) {
//            if (groupSelection == true) {
//                if (is_searching != false) {
//                    self.searchBar.text = "\(vehicleSearchString)"
//                    finalArray = searchingDataArray
//                    uiTableView.isHidden = false
//                    uiTableView.reloadData()
//                } else {
//                    searchingDataArray = []
//                    finalArray = searchingDataArray
//                    uiTableView.isHidden = false
//                    uiTableView.reloadData()
//                }
//            } else {
//                if (is_searching != false) {
//
//                } else {
//                    self.searchBar.text = "\(vehicleSearchString)"
//                    finalArray = searchingDataArray
//                    uiTableView.isHidden = false
//                    uiTableView.reloadData()
//                }
//            }
//        } else {
//            if (groupSelection == true) {
//                searchingDataArray = []
//                finalArray = searchingDataArray
//                uiTableView.isHidden = false
//                uiTableView.reloadData()
//            } else {
//                searchingDataArray = []
//                searchBar.isHidden = false
//                finalArray = searchingDataArray
//                uiTableView.isHidden = false
//                uiTableView.reloadData()
//            }
//        }
//    }
    
    /// This method is used to call api.
    func apiCalling() {
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle:UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            if let ID = defaults.value(forKey: "LoginID") {
                LoginID = ID as! String
            }
            GetVehicleStatus()
            ExtraFieldsHidden()
        }
    }
    
    /// This method is used to ExtraFieldsHidden.
    func ExtraFieldsHidden () {
        searchBar.isHidden = false
        searchLayoutConstraint.priority = UILayoutPriority(1000.0)
        searchLayoutConstraint.constant = 0
        
    }
    
}

