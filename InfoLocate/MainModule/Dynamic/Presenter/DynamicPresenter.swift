import Foundation

class DynamicPresenter: DynamicModuleInput, DynamicViewOutput, DynamicInteractorOutput {
    
    weak var view: DynamicViewInput!
    var interactor: DynamicInteractorInput!
    var router: DynamicRouterInput!
    
    // MARK: - DynamicViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToMapViewController(mapDetails: DynamicMapModel) {
        router.navigateToMapViewController(mapDetails: mapDetails)
    }
    
    // MARK: - DynamicModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - DynamicInteractorOutput
    
}
