import Foundation

class MapPresenter: MapModuleInput, MapViewOutput, MapInteractorOutput {
    
    weak var view: MapViewInput!
    var interactor: MapInteractorInput!
    var router: MapRouterInput!
    
    // MARK: - MapViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToAlertVideoListScreen(mapDetails: MapModel) {
        router.navigateToAlertVideoListScreen(mapDetails: mapDetails)
    }
    
    // MARK: - MapModuleInput
    
    func configureModule(mapDetails: MapModel) {
        view.configureModule(mapDetails: mapDetails)
    }
    
    // MARK: - MapInteractorOutput
    
}
