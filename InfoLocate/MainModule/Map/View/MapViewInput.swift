import Foundation

protocol MapViewInput: class {
    func configureModule(mapDetails: MapModel)
}
