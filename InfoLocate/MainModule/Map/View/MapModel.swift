//
//  MapModel.swift
//  InfoLocate
//
//  Created by Infotrack on 20/03/20.
//  Copyright © 2020 Hemalatha T. All rights reserved.
//

import Foundation

class MapModel: NSObject {
    var currentlat : Double!
    var currentlng : Double!
    var vehicleno : String!
    var vehiclename: String!
    var drivername : String!
    var mobileno : String!
    var temperature : String!
    var acstatus: String!
    var fuel : String!
    var duration : String!
    var trackTime : String!
    var speed : String!
    var location : String!
    var ignition : String!
    var unitno : String!
    var alerttypeid : String!
    var videoPresent: String!
    var videoUrlListArray : NSArray!
    var comingFrom : COMING_TYPE!

    func addMapDetailsWith(currentlat : Double, currentlng : Double, vehicleno : String, vehiclename : String, drivername : String, mobileno : String, temperature : String, acstatus: String, fuel : String, duration : String, trackTime : String, speed : String, location : String, ignition : String, unitno : String, alerttypeid: String, videoPresent: String, videoUrlListArray : NSArray, comingFrom: COMING_TYPE) -> MapModel {
        self.currentlat = currentlat
        self.currentlng = currentlng
        self.vehicleno = vehicleno
        self.vehiclename = vehiclename
        self.drivername = drivername
        self.mobileno = mobileno
        self.temperature = temperature
        self.acstatus = acstatus
        self.fuel = fuel
        self.duration = duration
        self.trackTime = trackTime
        self.speed = speed
        self.location = location
        self.ignition = ignition
        self.unitno = unitno
        self.alerttypeid = alerttypeid
        self.videoPresent = videoPresent
        self.videoUrlListArray = videoUrlListArray
        self.comingFrom = comingFrom
        return self
    }
    
}

class DynamicMapModel: NSObject {
    var currentlat : Double!
    var currentlng : Double!
    var vehicleno : String!
    var vehiclename: String!
    var drivername : String!
    var mobileno : String!
    var temperature : String!
    var fuel : String!
    var duration : String!
    var previousdistance : String!
    var currentdistance : String!
    var trackTime : String!
    var speed : String!
    var location : String!
    var ignition : String!
    var acstatus: String!
    var comingFrom : COMING_TYPE!

    func addMapDetailsWith(currentlat : Double, currentlng : Double, vehicleno : String, vehiclename : String, drivername : String, mobileno : String, temperature : String, fuel : String, duration : String, previousdistance : String, currentdistance : String, trackTime : String, speed : String, location : String, ignition : String, acstatus : String, comingFrom: COMING_TYPE) -> DynamicMapModel {
        self.currentlat = currentlat
        self.currentlng = currentlng
        self.vehicleno = vehicleno
        self.vehiclename = vehiclename
        self.drivername = drivername
        self.mobileno = mobileno
        self.temperature = temperature
        self.fuel = fuel
        self.duration = duration
        self.previousdistance = previousdistance
        self.currentdistance = currentdistance
        self.trackTime = trackTime
        self.speed = speed
        self.location = location
        self.ignition = ignition
        self.acstatus = acstatus
        self.comingFrom = comingFrom
        return self
    }
    
}
