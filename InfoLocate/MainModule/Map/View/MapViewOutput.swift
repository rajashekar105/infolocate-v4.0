import Foundation

protocol MapViewOutput {
    func back()
    func navigateToAlertVideoListScreen(mapDetails: MapModel)
}
