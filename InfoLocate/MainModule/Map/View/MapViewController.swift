import UIKit
import GoogleMaps
import Alamofire

class MapViewController: UIViewController, MapViewInput, GMSMapViewDelegate {
    
    /// This property is output for MapViewOutput.
    var output: MapViewOutput!
    /// This property is defaults.
    let defaults = UserDefaults.standard
    /// This property is mapDetails.
    var mapInfo: MapModel!
    /// This property is statusName.
    var statusName: String = ""
    /// This property is locationMarker.
    var locationMarker: GMSMarker!
    /// This property is currentlat.
    var currentlat: Double!
    /// This property is currentlng.
    var currentlng: Double!
    /// This property is vehicleno.
    var vehicleno: String = ""
    /// This property is vehiclename.
    var vehiclename: String = ""
    /// This property is drivername.
    var drivername: String = ""
    /// This property is mobileno.
    var mobileno: String = ""
    /// This property is temperature.
    var temperature: String = ""
    /// This property is acStatus.
    var acStatus: String = ""
    /// This property is fuel.
    var fuel: String = ""
    /// This property is duration.
    var duration: String = ""
    /// This property is trackTime.
    var trackTime: String = ""
    /// This property is speed.
    var speed: String = ""
    /// This property is ignition.
    var ignition: String = ""
    /// This property is location.
    var location: String = ""
    /// This property is unitno.
    var unitno: String = ""
    /// This property is alerttypeid.
    var alerttypeid: String = ""
    /// This property is videopresent.
    var videopresent: String = ""
    /// This property is videoUrlListArray.
    var videoUrlListArray: NSArray = NSArray()
    /// This property is comingFrom.
    var comingFrom: COMING_TYPE!
    /// This property is segueID.
    var segueID: String = ""
    /// This property is mapTypeValue.
    var mapTypeValue: Int = 0
    /// This property is trafficValue.
    var trafficValue: Int = 0
    /// This property is zoomlevel.
    var zoomlevel: Float = 0.0
    /// This property is statusColorArray.
    var statusColorArray: [String] = ["#28B779", "#FFB848", "#E7191B", "#807D7D", "#852B99"]
    /// This property is vehicleStatus.
    var vehicleStatus: String = ""
    /// This property is vehicleStatusLabel.
    var vehicleStatusLabel: String = ""
    /// This property is image.
    var image = UIImage()
    /// This property is infowindowstatus.
    var infowindowstatus: Bool = false
    /// This property is mapView.
    @IBOutlet var mapView: GMSMapView!
    /// This property is activity
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is mainLabel.
    @IBOutlet var mainLabel: UILabel!
    /// This property is maptype.
    @IBOutlet var maptype: UIButton!
    /// This property is traffic.
    @IBOutlet var traffic: UIButton!
    /// This property is infowindow.
    @IBOutlet var infowindow: UIView!
    /// This property is hideinfowindow.
    @IBOutlet var hideinfowindow: UIButton!
    /// This property is vehicleNumberLabel.
    @IBOutlet var vehicleNumberLabel: UILabel!
    /// This property is driverNameLabel.
    @IBOutlet var driverNameLabel: UILabel!
    /// This property is mobileNoLabel.
    @IBOutlet var mobileNoLabel: UILabel!
    /// This property is tempertureLabel.
    @IBOutlet var tempertureLabel: UILabel!
    /// This property is ignitionLabel.
    @IBOutlet var ignitionLabel: UILabel!
    /// This property is acStatusLabel.
    @IBOutlet var acStatusLabel: UILabel!
    /// This property is fuelLevelLabel.
    @IBOutlet var fuelLevelLabel: UILabel!
    /// This property is durationLabel.
    @IBOutlet var durationLabel: UILabel!
    /// This property is speedLabel.
    @IBOutlet var speedLabel: UILabel!
    /// This property is dateTimeLabel.
    @IBOutlet var dateTimeLabel: UILabel!
    /// This property is locationLabel.
    @IBOutlet var locationLabel: UILabel!
    /// This property is downloadVideoButton.
    @IBOutlet var downloadVideoButton: UIButton!
    /// This property is ignitionImageView.
    @IBOutlet var ignitionImageView: UIImageView!
    /// This property is infoWindowHeight.
    @IBOutlet var infoWindowHeight: NSLayoutConstraint!
    
    func configureModule(mapDetails: MapModel){
        currentlat = mapDetails.currentlat
        currentlng = mapDetails.currentlng
        vehicleno = mapDetails.vehicleno
        vehiclename = mapDetails.vehiclename
        drivername = mapDetails.drivername
        mobileno = mapDetails.mobileno
        temperature = mapDetails.temperature
        acStatus = mapDetails.acstatus
        fuel = mapDetails.fuel
        duration = mapDetails.duration
        trackTime = mapDetails.trackTime
        speed = mapDetails.speed
        ignition = mapDetails.ignition
        location = mapDetails.location
        unitno = mapDetails.unitno
        alerttypeid = mapDetails.alerttypeid
        videopresent = mapDetails.videoPresent
//        videopresent = "1"
//        unitno = "89890"
        videoUrlListArray = mapDetails.videoUrlListArray
        comingFrom = mapDetails.comingFrom
        mapInfo = mapDetails
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        print(comingFrom as Any)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if comingFrom == COMING_TYPE.RECENT_ALERTS {
            ignitionImageView.image = UIImage(named: "alert")
            downloadVideoButton.isHidden = true
            if unitno == "" || unitno == "NA" {
                downloadVideoButton.isHidden = true
            } else {
                downloadVideoButton.isHidden = false
                if videopresent == "1" {
                    if type == Constants.english {
                        self.downloadVideoButton.setTitle(English.VIEW_VIDEO, for: .normal)
                    } else if type == Constants.japanese {
                        self.downloadVideoButton.setTitle(Japanese.VIEW_VIDEO, for: .normal)
                    }
                } else if videopresent == "2" {
                    if type == Constants.english {
                        self.downloadVideoButton.setTitle(English.IN_PROGRESS, for: .normal)
                    } else if type == Constants.japanese {
                        self.downloadVideoButton.setTitle(Japanese.IN_PROGRESS, for: .normal)
                    }
                } else if videopresent == "0" {
                    if type == Constants.english {
                        self.downloadVideoButton.setTitle(English.DOWNLOAD_VIDEO, for: .normal)
                    } else if type == Constants.japanese {
                        self.downloadVideoButton.setTitle(Japanese.DOWNLOAD_VIDEO, for: .normal)
                    }
                }
            }
        } else {
            downloadVideoButton.isHidden = true
        }
        if let range = vehicleStatus.range(of: "-") {
            //    vehicleStatusLabel = vehicleStatus.substring(to: range.lowerBound)
            vehicleStatusLabel = String(vehicleStatus[..<range.lowerBound])
        }
        mapView.mapType = GMSMapViewType.normal
        mapView.isTrafficEnabled = false
        if let ID = defaults.object(forKey: "StatusName") {
            statusName = ID as! String
            print(statusName)
        }
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            mainLabel.text = "\(vehicleno)"
            self.showMarkerOnMap()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        infowindowstatus = true
        vehicleNumberLabel.text = vehicleno
        driverNameLabel.text = drivername
        mobileNoLabel.text = mobileno
        tempertureLabel.text = temperature
        acStatusLabel.text = acStatus
        if acStatus == "ON" {
            acStatusLabel.text = NSLocalizedString("ON", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acStatusLabel.text = English.ON
            } else if type == Constants.japanese {
                acStatusLabel.text = Japanese.ON
            }
        } else if acStatus == "OFF" {
            acStatusLabel.text = NSLocalizedString("OFF", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acStatusLabel.text = English.OFF
            } else if type == Constants.japanese {
                acStatusLabel.text = Japanese.OFF
            }
        }
        fuelLevelLabel.text = fuel
        durationLabel.text = duration
        ignitionLabel.text = ignition
        speedLabel.text = speed
        dateTimeLabel.text = trackTime
        locationLabel.text = location
        if vehiclename == "" {
            vehicleNumberLabel.text = vehicleno
        } else {
            vehicleNumberLabel.text = vehicleno + "::" + vehiclename
        }
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            if mobileno == English.NA {
                
            } else {
                mobileNoLabel.textColor = .init(red: 0/255, green: 0/255, blue: 238/255, alpha: 1)
                let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
                mobileNoLabel.isUserInteractionEnabled = true
                mobileNoLabel.addGestureRecognizer(tap)
            }
        } else if type == Constants.japanese {
            if mobileno == Japanese.NA {
                // do nothing. 
            } else {
                mobileNoLabel.textColor = .init(red: 0/255, green: 0/255, blue: 238/255, alpha: 1)
                let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
                mobileNoLabel.isUserInteractionEnabled = true
                mobileNoLabel.addGestureRecognizer(tap)
            }
        }

//        print(infoWindowHeight.constant)
//        let height = drivername.height(withConstrainedWidth: infowindow.frame.size.width, font: UIFont(name: "Avenir-Light", size: 13.0)!)
//        let actualHeight = max(30,height)
//        let value1 = detectLabelHeight(label: locationLabel)
//        infoWindowHeight.constant = infoWindowHeight.constant - 50 + value1 + actualHeight
    }
    
    /// This method is used for phone call feature.
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        print("tap working")
        print("calling")
        let phoneNumber = mobileno
        if let phoneURL = URL(string: ("tel:" + phoneNumber)) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(phoneURL)
            }
        }
    }
    
    
    /// This method is downloadVideoButton action.
    @IBAction func downloadVideoButton(_ sender: Any) {
        // Api request for downloading alert video.
        if self.downloadVideoButton.titleLabel?.text == English.DOWNLOAD_VIDEO || self.downloadVideoButton.titleLabel?.text == Japanese.DOWNLOAD_VIDEO {
            var title = NSLocalizedString("Are you sure want to request to download video onto server?", comment: "")
            var cancelTitle = NSLocalizedString("CANCEL", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.Are_you_sure_you_want_to_download_video_onto_server
                cancelTitle = English.CANCEL
            } else if type == Constants.japanese {
                title = Japanese.Are_you_sure_you_want_to_download_video_onto_server
                cancelTitle = Japanese.CANCEL
            }
            let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.view.isUserInteractionEnabled = false
                self.activity.startAnimating()
                self.requestAlertVideoDownloadAPI()
            }
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default) {
                UIAlertAction in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else if self.downloadVideoButton.titleLabel?.text == English.IN_PROGRESS ||  self.downloadVideoButton.titleLabel?.text == Japanese.IN_PROGRESS {
            self.showInProgressAlert()
        } else {
            self.output.navigateToAlertVideoListScreen(mapDetails: self.mapInfo)
        }
    }

    func showInProgressAlert() {
        var title = NSLocalizedString("Request submitted. Please come back after 20 seconds to check the download status again.", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            title = English.Request_submitted_Please_come_back_after_20_seconds_to_check_the_download_status_again
        } else if type == Constants.japanese {
            title = Japanese.Request_submitted_Please_come_back_after_20_seconds_to_check_the_download_status_again
        }
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }

    /// This method is used to request AlertVideoDownloadAPI.
    func requestAlertVideoDownloadAPI() {
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            var baseUrl:String = ""
            var LoginID: String = ""
            var status = ""
            var message = ""
            if let ID = defaults.value(forKey: "LoginID")
            {
                LoginID = ID as! String
            }
            print(LoginID)
            if let url = defaults.object(forKey: "BaseURL") {
                baseUrl = url as! String
            }
            let parameters = [
                "unitNo" : unitno.encrypt(),
                "alertDateTime" : trackTime.encrypt(),
                "alertTypeMasterId" : alerttypeid.encrypt(),
            ]
            print("\(baseUrl)"+"RequestAlertVideo")
            AF.request("\(baseUrl)"+"RequestAlertVideo", method: .post, parameters: parameters).responseJSON { response in
                guard response.error == nil else {
                    // got an error in getting the data, need to handle it
                    print(response.error!)
                    var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                    let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                    if type == Constants.english {
                        message = English.Something_went_wrong_Please_try_again_later
                    } else if type == Constants.japanese {
                        message = Japanese.Something_went_wrong_Please_try_again_later
                    }
                    self.showAlert("", message: message)
                    self.view.isUserInteractionEnabled = true
                    self.activity.stopAnimating()
                    return
                }
                print(response)
                switch response.result {
                case .success:
                    if let result = response.value {
                        let JSON = result as! NSDictionary
                        if let sts = JSON.object(forKey: "status") as? NSString {
                            status = (sts as String).decrypt()
                        }
                        if status == "success" {
                            if let msg = JSON.object(forKey: "message") as? NSString {
                                message = (msg as String).decrypt()
                            }
                            print(message)
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.downloadVideoButton.setTitle(English.IN_PROGRESS, for: .normal)
                            } else if type == Constants.japanese {
                                self.downloadVideoButton.setTitle(Japanese.IN_PROGRESS, for: .normal)
                            }
                            self.showInProgressAlert()
                        } else if status == "failure" {
                            if let msg = JSON.object(forKey: "message") as? NSString {
                                message = (msg as String).decrypt()
                            }
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if message == English.No_Records_found {
                                if type == Constants.english {
                                    message = English.No_Records_found
                                } else if type == Constants.japanese {
                                    message = Japanese.No_Records_found
                                }
                            }
                            self.showAlert("" , message: message)
                            DispatchQueue.main.async(execute: {
                                self.activity.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                            })
                        }
                    }
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
        }
    }
    
    /// This method is back button action.
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
    /// This method is used to hideinfowindow.
    @IBAction func hideinfowindow(_ sender: Any) {
        animateRideNowView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is used to animateRideNowView.
    func animateRideNowView() {
        if infowindowstatus == false
        {
            self.hideinfowindow.isHidden = false
            self.infowindow.isHidden = false
            infowindowstatus = true
        } else {
            self.hideinfowindow.isHidden = true
            self.infowindow.isHidden = true
            infowindowstatus = false
        }
    }
    
    /// This method is called  when didTap on marker on map.
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if infowindowstatus == false
        {
            hideinfowindow.isHidden = false
            infowindow.isHidden = false
            infowindowstatus = true
        }
        return false
    }
    
    /// This method is maptype  button action.
    @IBAction func maptype(_ sender: Any) {
        var image1 = UIImage()
        if mapTypeValue < 2 {
            mapTypeValue += 1
        } else {
            mapTypeValue = 0
        }
        switch mapTypeValue {
        case 0 :
            image1 = UIImage(named: "maptypeyellow")!
            mapView.mapType = GMSMapViewType.normal
            break
        case 1 :
            image1 = UIImage(named: "maptypegrey")!
            mapView.mapType = GMSMapViewType.hybrid
            break
        case 2 :
            image1 = UIImage(named: "maptypewhite")!
            mapView.mapType = GMSMapViewType.terrain
            break
        default:
            image1 = UIImage(named: "maptypeyellow")!
            mapView.mapType = GMSMapViewType.normal
            break
        }
        maptype.setBackgroundImage(nil, for: UIControl.State.normal)
        maptype.setBackgroundImage(image1, for: UIControl.State.normal)
    }
    
    /// This method is traffic  button action.
    @IBAction func traffic(_ sender: Any) {
        var image2 = UIImage()
        if trafficValue == 0 {
            mapView.isTrafficEnabled = true
            image2 = UIImage(named: "trafficcolor")!
            traffic.setBackgroundImage(nil, for: UIControl.State.normal)
            traffic.setBackgroundImage(image2, for: UIControl.State.normal)
            trafficValue = 1
        } else if trafficValue == 1 {
            mapView.isTrafficEnabled = false
            image2 = UIImage(named: "trafficgrey")!
            traffic.setBackgroundImage(nil, for: UIControl.State.normal)
            traffic.setBackgroundImage(image2, for: UIControl.State.normal)
            trafficValue = 0
        }
    }
    
    /// This method is zoomin  button action.
    @IBAction func zoomin(_ sender: Any) {
        zoomlevel = mapView.camera.zoom
        zoomlevel = zoomlevel + 2.0
        ZoomMap(zoomTo: Float(zoomlevel))
    }
    
    /// This method is zoomout  button action.
    @IBAction func zoomout(_ sender: Any) {
        zoomlevel = mapView.camera.zoom
        zoomlevel = zoomlevel - 2.0
        ZoomMap(zoomTo: Float(zoomlevel))
    }
    
    /// This method is used zoomMap  on map.
    func ZoomMap(zoomTo: Float){
        mapView.animate(toZoom: zoomTo)
    }
    
    
    /// This method is used to showMarkerOnMap.
    func showMarkerOnMap() {
        if (self.currentlat == nil || self.currentlng == nil) {
            currentlng = 0
            currentlat = 0
        }
        if locationMarker != nil {
            locationMarker.map = nil
        }
        if let vehicleStatus = defaults.object(forKey: "StatusName") {
            vehicleStatusLabel = vehicleStatus as! String
        }
        
        switch vehicleStatusLabel {
        case "moving" :
            image = UIImage(named: "moving")!
            break
        case "idle" :
            image = UIImage(named: "idle")!
            break
        case "stopped" :
            image = UIImage(named: "stopped")!
            break
        case "inactive" :
            image = UIImage(named: "inactive")!
            break
        case "outofservice" :
            image = UIImage(named: "outofservice")!
            break
        case "notpolling" :
            image = UIImage(named: "notpolling")!
            break
        default:
            image = UIImage(named: "stopped")!
            break
        }
        let coordinate = CLLocationCoordinate2D(latitude: currentlat, longitude: currentlng)
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = mapView
        locationMarker.icon = image
        locationMarker.title = vehicleno
        mapView.selectedMarker = locationMarker
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: currentlat, longitude: currentlng, zoom: 15.0)
        self.mapView.camera = camera
    }

    /// This method is called when mapViewSnapshotReady.
    func mapViewSnapshotReady(_ mapView: GMSMapView) {
        DispatchQueue.main.async(execute: {
            self.showMarkerOnMap()
            // Center camera to marker position
            self.mapView.camera = GMSCameraPosition.camera(withTarget: self.locationMarker.position, zoom: 15)
        })
    }
    
    /// This method is used to detectLabelHeight.
    func detectLabelHeight(label: UILabel) -> (CGFloat) {
        return (CGFloat(label.frame.height))
    }
    
    /// This method is used to showAlert.
    func showAlert(_ title: String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}


