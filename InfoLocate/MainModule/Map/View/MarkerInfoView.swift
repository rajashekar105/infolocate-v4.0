//
//  MarkerInfoView.swift
//  InfoLocateFMS
//
//  Created by infotrack telematics on 11/07/16.
//  Copyright © 2016 InfotrackTelematics. All rights reserved.
//

import UIKit

class MarkerInfoView: UIView {

    /// This property is vehicleNoLabel.
    @IBOutlet var vehicleNoLabel: UILabel!
    /// This property is vehicleNo.
    @IBOutlet var vehicleNo: UILabel!
    /// This property is driverNameLabel.
    @IBOutlet var driverNameLabel: UILabel!
    /// This property is driverName.
    @IBOutlet var driverName: UILabel!
    /// This property is mobileNoLabel.
    @IBOutlet var mobileNoLabel: UILabel!
    /// This property is mobileNo.
    @IBOutlet var mobileNo: UILabel!
    /// This property is ignitionLabel.
    @IBOutlet var ignitionLabel: UILabel!
    /// This property is trackTimeLabel.
    @IBOutlet var ignition: UILabel!
    /// This property is trackTimeLabel.
    @IBOutlet var trackTimeLabel: UILabel!
    /// This property is trackTime.
    @IBOutlet var trackTime: UILabel!
    /// This property is speedLabel.
    @IBOutlet var speedLabel: UILabel!
    /// This property is speed.
    @IBOutlet var speed: UILabel!
    /// This property is locationLabel.
    @IBOutlet var locationLabel: UILabel!
    /// This property is location.
    @IBOutlet var location: UILabel!
    
}
