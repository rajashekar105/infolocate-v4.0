import UIKit

@objc class MapModuleBuilder: NSObject {
    
    func build(mapDetails: MapModel) -> UIViewController {
        let viewController = MapControllerFromStoryboard()
        
        let router = MapRouter()
        router.viewController = viewController
        
        let presenter = MapPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = MapInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(mapDetails: mapDetails)
        
        return viewController
    }
    
    func MapControllerFromStoryboard() -> MapViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.MAP_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! MapViewController
    }
    
}
