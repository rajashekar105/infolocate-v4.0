import Foundation

protocol MapModuleInput: class {
    
    func configureModule(mapDetails: MapModel)
}
