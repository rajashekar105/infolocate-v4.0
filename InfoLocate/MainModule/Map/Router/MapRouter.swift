import UIKit

class MapRouter:MapRouterInput {
    
    weak var viewController: MapViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToAlertVideoListScreen(mapDetails: MapModel) {
        Navigation.navigateToAlertVideoListScreen(mapDetails: mapDetails,viewController: viewController)
    }
    
}
