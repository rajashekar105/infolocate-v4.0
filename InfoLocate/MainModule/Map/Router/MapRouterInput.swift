import Foundation

protocol MapRouterInput {
    func back()
    func navigateToAlertVideoListScreen(mapDetails: MapModel)
}
