import Foundation

protocol AlertsViewOutput {
    func back()
}
