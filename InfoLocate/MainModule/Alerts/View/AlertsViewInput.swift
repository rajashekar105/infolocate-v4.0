import Foundation

protocol AlertsViewInput: class {
    func configureModule(alertTypeArray: [String])
}
