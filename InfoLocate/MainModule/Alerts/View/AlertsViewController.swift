import UIKit

class AlertsViewController: UIViewController, AlertsViewInput, UITableViewDelegate, UITableViewDataSource {
    
    /// This property is output for AlertsViewOutput.
    var output: AlertsViewOutput!
    /// This property is defaults.
    let defaults = UserDefaults.standard
    /// This property is is_filtered.
    var is_filtered: String = ""
    /// This property is dropDownList.
    var dropDownList: [String] = []
    /// This property is dataArray.
    var dataArray = NSArray()
    /// This property is filteredDataArray.
    var filteredDataArray = NSMutableArray()
    /// This property is alertTypeArray.
    var alertTypeArray:[String] = []
    /// This property is alertTypeFinalArray.
    var alertTypeFinalArray:[String] = []
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is uiTableView.
    @IBOutlet var uiTableView: UITableView!
    /// This property is cancelButton.
    @IBOutlet var cancelButton: UIButton!
    /// This property is doneButton.
    @IBOutlet var doneButton: UIButton!
    /// This property is titleLabel.
    @IBOutlet var titleLabel: UILabel!
    
    
    func configureModule(alertTypeArray: [String]) {
        dropDownList = alertTypeArray
        print(alertTypeArray)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = NSLocalizedString("Alerts", comment: "")
        self.cancelButton.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        self.doneButton.setTitle(NSLocalizedString("Done", comment: ""), for: .normal)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.titleLabel.text = English.Alerts
            self.cancelButton.setTitle(English.Cancel, for: .normal)
            self.doneButton.setTitle(English.Done, for: .normal)
        } else if type == Constants.japanese {
            self.titleLabel.text = Japanese.Alerts
            self.cancelButton.setTitle(Japanese.Cancel, for: .normal)
            self.doneButton.setTitle(Japanese.Done, for: .normal)
        }
        if let ID = defaults.value(forKey: "isFiltered")
        {
            is_filtered = ID as! String
        }
        if is_filtered == "false"
        {
            alertTypeArray = dropDownList
        } else  if is_filtered == "true" {
            if let alert = defaults.object(forKey: "alertTypeArray") {
                alertTypeArray = alert as! [String]
                alertTypeFinalArray = alertTypeArray
            }
        }
        if let data = defaults.object(forKey: "MainArray") {
            dataArray = data as! NSArray
        }
        let tblView =  UIView(frame: CGRect.zero)
        uiTableView.tableFooterView = tblView
        uiTableView.backgroundColor = UIColor.white
        uiTableView.delegate = self
        uiTableView.dataSource = self
        uiTableView.allowsMultipleSelection = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /// This method is back button action.
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
    /// This method is done button action.
    @IBAction func doneButton(_ sender: Any) {
        alertTypeFinalArray = alertTypeArray
        print(alertTypeArray)
        defaults.setValue(alertTypeFinalArray, forKey: "alertTypeArray")
        filterData(data: alertTypeFinalArray)
        self.output.back()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = dropDownList[indexPath.row]
        if alertTypeArray.contains(dropDownList[indexPath.row]) {
            cell.accessoryType = .checkmark
            uiTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        alertTypeArray.append(dropDownList[indexPath.row])
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            cell.accessoryType = .checkmark
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        alertTypeArray = alertTypeArray.filter(){$0 != "\(dropDownList[indexPath.row])"}
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            cell.accessoryType = .none
        }
    }
    
    /// This method is used to get filterData.
    func filterData (data: [String]) {
        filteredDataArray.removeAllObjects()
        for index in 0 ..< alertTypeArray.count {
            search(data: alertTypeArray[index])
        }
        defaults.setValue(filteredDataArray, forKey: "FilteredArray")
        defaults.setValue("true", forKey: "isFiltered")
        dismiss(animated: true, completion: nil)
    }
    
    /// This method is used to get search data.
    func search(data: String) {
        for index in 0 ..< dataArray.count
        {
            let vehicleNo = (dataArray[index] as AnyObject).object(forKey: "vehicleno") as? String
            let dataTime = (dataArray[index] as AnyObject).object(forKey: "alertdatetime") as? String
            let location = (dataArray[index] as AnyObject).object(forKey: "location") as? String
            let alertType = (dataArray[index] as AnyObject).object(forKey: "alerttype") as? String
            let speed = (dataArray[index] as AnyObject).object(forKey: "speed") as? String
            let lat: Double = (dataArray[index] as AnyObject).object(forKey: "lat")as! Double
            let lon: Double = (dataArray[index] as AnyObject).object(forKey: "lon") as! Double
            let dictFilter:Dictionary<String,AnyObject> = ["vehicleno": vehicleNo! as String as AnyObject,
                                                           "alertdatetime": dataTime! as String as AnyObject,
                                                           "location": location! as String as AnyObject,
                                                           "alerttype": alertType! as String as AnyObject,
                                                           "speed": speed! as String as AnyObject,
                                                           "lat": lat as AnyObject,
                                                           "lon": lon as AnyObject ]
            if alertType!.lowercased().range(of: data.lowercased())  != nil
            {
                filteredDataArray.add(dictFilter)
            }
        }
    }
}
