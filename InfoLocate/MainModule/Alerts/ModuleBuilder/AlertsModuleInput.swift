import Foundation

protocol AlertsModuleInput: class {
    
    func configureModule(alertTypeArray: [String])
}
