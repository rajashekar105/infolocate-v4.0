import UIKit

@objc class AlertsModuleBuilder: NSObject {
    
    func build(alertTypeArray: [String]) -> UIViewController {
        let viewController = AlertsControllerFromStoryboard()
        
        let router = AlertsRouter()
        router.viewController = viewController
        
        let presenter = AlertsPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = AlertsInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(alertTypeArray: alertTypeArray)
        
        return viewController
    }
    
    func AlertsControllerFromStoryboard() -> AlertsViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.ALERTS_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! AlertsViewController
    }
    
}
