import Foundation

protocol AlertsRouterInput {
    func back()
}
