import UIKit

class AlertsRouter:AlertsRouterInput {
    
    weak var viewController: AlertsViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
