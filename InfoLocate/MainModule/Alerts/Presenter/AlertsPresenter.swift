import Foundation

class AlertsPresenter: AlertsModuleInput, AlertsViewOutput, AlertsInteractorOutput {
    func configureModule() {
    }
    
    
    weak var view: AlertsViewInput!
    var interactor: AlertsInteractorInput!
    var router: AlertsRouterInput!
    
    // MARK: - AlertsViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - AlertsModuleInput
    
    func configureModule(alertTypeArray: [String]) {
        view.configureModule(alertTypeArray: alertTypeArray)
    }
    
    // MARK: - AlertsInteractorOutput
    
}
