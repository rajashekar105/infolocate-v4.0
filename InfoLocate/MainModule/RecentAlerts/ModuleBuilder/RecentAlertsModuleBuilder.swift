import UIKit

@objc class RecentAlertsModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = RecentAlertsControllerFromStoryboard()
        
        let router = RecentAlertsRouter()
        router.viewController = viewController
        
        let presenter = RecentAlertsPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = RecentAlertsInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func RecentAlertsControllerFromStoryboard() -> RecentAlertsViewController {
        let storyboard = StoryBoard.mainModuleStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.RECENT_ALERTS_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! RecentAlertsViewController
    }
 
    
}
