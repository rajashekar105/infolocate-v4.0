import Foundation

protocol RecentAlertsModuleInput: class {
    
    func configureModule()
}
