import Foundation

protocol RecentAlertsRouterInput {
    func back()
    func navigateToAlertViewController(alertTypeArray: [String])
    func navigateToMapViewController(mapDetails: MapModel)
}
