import UIKit

class RecentAlertsRouter:RecentAlertsRouterInput {
    
    weak var viewController: RecentAlertsViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToAlertViewController(alertTypeArray: [String]) {
        Navigation.navigateToAlertsScreen(alertTypeArray: alertTypeArray, viewController: viewController)
    }
    
    func navigateToMapViewController(mapDetails: MapModel) {
        Navigation.navigateToMapViewScreen(mapDetails: mapDetails, viewController: viewController)
    }
    
}
