
//  RecentAlertsTableViewCell.swift
//  InfoLocateFMS
//
//  Created by infotrack telematics on 05/07/16.
//  Copyright © 2016 InfotrackTelematics. All rights reserved.
//

import UIKit

class RecentAlertsTableViewCell: UITableViewCell {

    /// This property is slnoLabel.
    @IBOutlet var slnoLabel: UILabel!
    /// This property is vehicleNoLabel.
    @IBOutlet var vehicleNoLabel: UILabel!
    /// This property is drivername.
    @IBOutlet var drivername: UILabel!
    /// This property is mobileno.
    @IBOutlet var mobileno: UILabel!
    /// This property is temperature.
    @IBOutlet var temperature: UILabel!
    /// This property is dataTimeLabel.
    @IBOutlet var dataTimeLabel: UILabel!
    /// This property is locationLabel.
    @IBOutlet var locationLabel: UILabel!
    /// This property is alertTypeLabel.
    @IBOutlet var alertTypeLabel: UILabel!
    /// This property is speedLabel.
    @IBOutlet var speedLabel: UILabel!
    /// This property is ViewBackground.
    @IBOutlet var ViewBackground: UIView!
    /// This property is acStatusLabel.
    @IBOutlet var acStatusLabel: UILabel!
    /// This property is fuelLevelLabel.
    @IBOutlet var fuelLevelLabel: UILabel!
    /// This property is durationLabel.
    @IBOutlet var durationLabel: UILabel!
    
    /// This property is videoIconStatus.
    @IBOutlet var videoIconStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
