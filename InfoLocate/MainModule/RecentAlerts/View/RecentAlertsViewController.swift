import UIKit
import Alamofire

class RecentAlertsViewController: BaseViewController, RecentAlertsViewInput, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    /// This property is output for RecentAlertsViewOutput.
    var output: RecentAlertsViewOutput!
    /// This property is statusLabel.
    @IBOutlet var statusLabel: UILabel!
    /// This property is searchBar.
    @IBOutlet var searchBar: UISearchBar!
    /// This property is uiTableView.
    @IBOutlet var uiTableView: UITableView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is LoginID.
    var LoginID: String = ""
    /// This property is is_searching.
    var is_searching:Bool!
    /// This property is is_filtered.
    var is_filtered: String = "false"
    /// This property is currentlat.
    var currentlat: Double!
    /// This property is currentlng.
    var currentlng: Double!
    /// This property is vehicleno.
    var vehicleno: String = ""
    /// This property is vehiclename.
    var vehiclename: String = ""
    /// This property is drivername.
    var drivername: String = ""
    /// This property is mobileno.
    var mobileno: String = ""
    /// This property is temperature.
    var temperature: String = ""
    /// This property is acStatus.
    var acStatus: String = ""
    /// This property is fuel.
    var fuel: String = ""
    /// This property is duration.
    var duration: String = ""
    /// This property is trackTime.
    var trackTime: String = ""
    /// This property is speed.
    var speed: String = ""
    /// This property is ignition.
    var ignition: String = ""
    /// This property is location.
    var location: String = ""
    /// This property is buttonTitle.
    var buttonTitle: String = ""
    /// This property is is_launchTime.
    var is_launchTime: Bool = true
    /// This property is alertTypeArray.
    var alertTypeArray: [String] = []
    /// This property is mainDataArray.
    var mainDataArray = NSArray()
    /// This property is filteredDataArray.
    var filteredDataArray = NSArray()
    /// This property is workingArray.
    var workingArray = NSArray()
    /// This property is isScrollingStart.
    var isScrollingStart: Bool = false
    /// This property is searchingDataArray.
    var searchingDataArray = NSMutableArray()
    /// This property is finalArray.
    var finalArray = NSArray()
    /// This property is alertNavigation.
    var alertNavigation = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.placeholder = NSLocalizedString("Search Vehicle No/Driver Name", comment: "")
        searchBar.textField?.font = UIFont(name: "", size: 11.0)
        self.statusLabel.text = "\(NSLocalizedString("Recent Alerts", comment: "")) ( \(self.finalArray.count) )"
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            searchBar.placeholder = English.Search_Vehicle_No_Driver_Name
            self.statusLabel.text = "\(English.Recent_Alerts) ( \(self.finalArray.count) )"
        } else if type == Constants.japanese {
            searchBar.placeholder = Japanese.Search_Vehicle_No_Driver_Name
            self.statusLabel.text = "\(Japanese.Recent_Alerts) ( \(self.finalArray.count) )"
        }
        defaults.setValue("false", forKey: "isFiltered")
        finalArray = []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let ID = defaults.value(forKey: "LoginID")
        {
            LoginID = ID as! String
        }
        if let alert = defaults.object(forKey: "AlertNavigation") {
            alertNavigation = alert as! String
        }
        if alertNavigation == "" {
            activity.layer.cornerRadius = 10
//            workingArray = []            
            if let ID = defaults.value(forKey: "isFiltered")
            {
                is_filtered = ID as! String
            }
            if is_filtered == "true" {
                finalArray = mainDataArray
                let tempAlertTypeArray = defaults.value(forKey: "alertTypeArray") as! [String]
                let tempFinalArray = NSMutableArray()
                for index in 0 ..< finalArray.count {
                    let alertType_finalArray = (((finalArray[index]) as AnyObject).object(forKey: "alerttype") as? String)?.decrypt()
                    for type in tempAlertTypeArray {
                        let alertType = type as String
                        if alertType == "" && alertType_finalArray == nil {
                            tempFinalArray.add(finalArray[index] as AnyObject)
                        }
                        if alertType == alertType_finalArray {
                            tempFinalArray.add(finalArray[index] as AnyObject)
                        }
                    }
                }
                finalArray = tempFinalArray
                if(finalArray.count == 0)
                {
                    uiTableView.isHidden = true
                    var message = NSLocalizedString("No Data Found.", comment: "")
                    let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                    if type == Constants.english {
                        message = English.No_Data_Found
                    } else if type == Constants.japanese {
                        message = Japanese.No_Data_Found
                    }
                    showAlert("", message: message)
                } else if(finalArray.count > 0) {
                    workingArray = finalArray
                    uiTableView.isHidden = false
                    uiTableView.reloadData()
                }
            }
            else if is_filtered == "false"
            {
                if Reachability.connectedToNetwork() == false
                {
                    var title = NSLocalizedString("No Internet Connection", comment: "")
                    var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
                    let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                    if type == Constants.english {
                        title = English.No_Internet_Connection
                        message = English.Make_sure_your_device_is_connected_to_the_internet
                    } else if type == Constants.japanese {
                        title = Japanese.No_Internet_Connection
                        message = Japanese.Make_sure_your_device_is_connected_to_the_internet
                    }
                    let alertController = UIAlertController(title: title, message: message, preferredStyle:UIAlertController.Style.alert)
                    let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    GetAlertStatus()
                    activity.startAnimating()
                    uiTableView.isHidden = true
                    view.isUserInteractionEnabled = false
                }
                let tblView =  UIView(frame: CGRect.zero)
                uiTableView.tableFooterView = tblView
                uiTableView.backgroundColor = UIColor.white
                searchingDataArray = []
                is_searching = false
                searchBar.delegate = self
                uiTableView.delegate = self
                uiTableView.dataSource = self
            }
            self.statusLabel.text = "\(NSLocalizedString("Recent Alerts", comment: "")) ( \(self.finalArray.count) )"
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                self.statusLabel.text = "\(English.Recent_Alerts) ( \(self.finalArray.count) )"
            } else if type == Constants.japanese {
                self.statusLabel.text = "\(Japanese.Recent_Alerts) ( \(self.finalArray.count) )"
            }
            searchingDataArray = []
            is_searching = false
            searchBar.text = ""
        } else {
            activity.startAnimating()
            getAlertDetails()
        }
    }
    
    /// This method is menuButtonAction.
    @IBAction func menuButtonAction(_ sender: UIButton) {
        UserDefaults.standard.set("", forKey: "AlertNavigation")
        openSideMenu()
    }
    
    /// This method is backButtonAction.
    @IBAction func back(_ sender: Any) {
        UserDefaults.standard.set("", forKey: "AlertNavigation")
        self.output.back()
    }
    
    /// This method is AlertType button action.
    @IBAction func AlertType(_ sender: Any) {
        if alertTypeArray.count > 0 {
            self.output.navigateToAlertViewController(alertTypeArray: alertTypeArray)
        } else {
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                self.message = English.No_Records_found
            } else if type == Constants.japanese {
                self.message = Japanese.No_Records_found
            }
            self.showAlert("" , message: self.message)
        }
    }
    
    /// This method is refresh button action.
    @IBAction func refresh(_ sender: Any) {
        defaults.setValue("false", forKey: "isFiltered")
        if isScrollingStart == false {
            uiTableView.isHidden = true
            viewWillAppear(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    /// This method is called when searchBarCancelButtonClicked.
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false
        uiTableView.isHidden = false
        if is_filtered == "true" {
            if let ID = defaults.value(forKey: "FilteredArray")
            {
                filteredDataArray = ID as! NSArray
                workingArray = filteredDataArray
                finalArray = filteredDataArray
            }
        } else if is_filtered == "false" {
            workingArray = mainDataArray
            finalArray = mainDataArray
        }
        uiTableView.reloadData()
        searchBar.text = ""
        dismissKeyboard()
    }
    
    /// This method is called when searchBarSearchButtonClicked.
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }
    
    /// This method is used to dismissKeyboard.
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /// This method is called when textDidChange in searchbar.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchBar.text!.isEmpty {
            is_searching = false
            uiTableView.isHidden = false
            finalArray = workingArray
            self.statusLabel.text = "\(NSLocalizedString("Recent Alerts", comment: "")) ( \(self.finalArray.count) )"
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                self.statusLabel.text = "\(English.Recent_Alerts) ( \(self.finalArray.count) )"
            } else if type == Constants.japanese {
                self.statusLabel.text = "\(Japanese.Recent_Alerts) ( \(self.finalArray.count) )"
            }
            self.uiTableView.reloadData()
        } else {
            is_searching = true
            searchingDataArray.removeAllObjects()
            for index in 0 ..< workingArray.count {
                var drivername = ""
                let vehicleNo = ((workingArray[index] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
                if (workingArray[index] as AnyObject).object(forKey: "username") == nil ||
                    ((workingArray[index] as AnyObject).object(forKey: "username") as? String)?.decrypt() == "" ||
                    ((workingArray[index] as AnyObject).object(forKey: "username") as? String)?.decrypt() == "-" ||
                    ((workingArray[index] as AnyObject).object(forKey: "username") as? String)?.decrypt() == "NA" {
                    drivername = NSLocalizedString("NA", comment: "")
                    let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                    if type == Constants.english {
                        drivername = English.NA
                    } else if type == Constants.japanese {
                        drivername = Japanese.NA
                    }
                } else {
                    drivername = ((workingArray[index] as AnyObject).object(forKey: "username") as? String)!
                }
              
                if vehicleNo.lowercased().range(of: searchText.lowercased())  != nil ||
                drivername.lowercased().range(of: searchText.lowercased())  != nil ||
                    (vehicleNo.lowercased().range(of: searchText.lowercased())  != nil && drivername.lowercased().range(of: searchText.lowercased())  != nil)
                {
                    searchingDataArray.add(workingArray[index])
                }
            }
            if(searchingDataArray.count == 0)
            {
                uiTableView.isHidden = true
                var message = NSLocalizedString("No Data Found.", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.No_Data_Found
                } else if type == Constants.japanese {
                    message = Japanese.No_Data_Found
                }
                showAlert("", message: message)
            } else if(searchingDataArray.count > 0) {
                finalArray = searchingDataArray
                uiTableView.isHidden = false
                self.statusLabel.text = "\(NSLocalizedString("Recent Alerts", comment: "")) ( \(self.finalArray.count) )"
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    self.statusLabel.text = "\(English.Recent_Alerts) ( \(self.finalArray.count) )"
                } else if type == Constants.japanese {
                    self.statusLabel.text = "\(Japanese.Recent_Alerts) ( \(self.finalArray.count) )"
                }
            }
            self.uiTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return finalArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let location:String = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as? String)!.decrypt()
        let locationheight = location.height(withConstrainedWidth: tableView.frame.size.width - 80, font: UIFont(name: "Avenir-Light", size: 18.0)!)
        let actualLocationHeight = max(30,locationheight)
        return actualLocationHeight + 120        //        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "NA" {
            drivername = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                drivername = English.NA
            } else if type == Constants.japanese {
                drivername = Japanese.NA
            }
        } else {
            drivername = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt()
        }
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "NA" {
            mobileno = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                mobileno = English.NA
            } else if type == Constants.japanese {
                mobileno = Japanese.NA
            }
        } else {
            mobileno = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt()
        }
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "NA" {
            temperature = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                temperature = English.NA
            } else if type == Constants.japanese {
                temperature = Japanese.NA
            }
        } else {
            temperature = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as! String).decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus")) == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "NA" {
            acStatus = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acStatus = English.NA
            } else if type == Constants.japanese {
                acStatus = Japanese.NA
            }
        } else {
            acStatus = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel")) == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "-" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "NA" {
            fuel = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                fuel = English.NA
            } else if type == Constants.japanese {
                fuel = Japanese.NA
            }
        } else {
            fuel = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration")) == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "-" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "NA" {
            duration = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                duration = English.NA
            } else if type == Constants.japanese {
                duration = Japanese.NA
            }
        } else {
            duration = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecentAlertsTableViewCell
        cell.ViewBackground.layer.shadowColor = UIColor.darkGray.cgColor
        cell.ViewBackground.layer.shadowOffset = CGSize.zero
        cell.ViewBackground.layer.shadowRadius = 1.0
        cell.ViewBackground.layer.shadowOpacity = 0.4
//        cell.slnoLabel.layer.cornerRadius = 5
//        cell.slnoLabel.clipsToBounds = true
//        cell.slnoLabel.text = "\(indexPath.row + 1)"
        cell.vehicleNoLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
        cell.dataTimeLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alertdatetime") as? String)!.decrypt()
        cell.locationLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as? String)!.decrypt()
        cell.alertTypeLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alerttype") as? String)!.decrypt()
        cell.speedLabel.text = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "speed") as? String)!.decrypt()
        //        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alertid") as? String)!.decrypt())
        //        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alerttypeid") as? String)!.decrypt())
        //        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videobaseurl") as? String)!.decrypt())
        //        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videofiles") as? String)!.decrypt())
        var unitno = ""
        cell.videoIconStatus.isHidden = true
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "unitno")) == nil {
        } else if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "unitno") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "unitno") as? String)!.decrypt() == "NA" {
        } else {
            let unitNumber = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "unitno") as? String)!.decrypt()
            unitno = unitNumber
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videopresent")) == nil {
            } else {
                cell.videoIconStatus.isHidden = false
                if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videopresent") as? String)!.decrypt() == "1" {
                    cell.videoIconStatus.image = UIImage(named: "alertvideoicon")
                } else {
                    cell.videoIconStatus.image = UIImage(named: "alertnovideoicon")
                }
            }
        }
        print(unitno)
        cell.drivername.text = drivername
        cell.mobileno.text = mobileno
        cell.temperature.text = temperature
        cell.acStatusLabel.text = acStatus
        cell.fuelLevelLabel.text = fuel
        cell.durationLabel.text = duration
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentlat = NSString(string: ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "lat") as? String)!.decrypt()).doubleValue
        currentlng = NSString(string: ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "lon") as? String)!.decrypt()).doubleValue
        vehicleno = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
        trackTime = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alertdatetime") as? String)!.decrypt()
        speed = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "speed") as? String)!.decrypt()
        location = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "location") as? String)!.decrypt()
        ignition = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alerttype") as? String)!.decrypt()
        defaults.set("", forKey: "StatusName")
        uiTableView.deselectRow(at: indexPath, animated: true)
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "NA" {
            vehiclename = ""
        } else {
            vehiclename = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "vehiclename") as? String)!
        }
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "NA" {
            drivername = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                drivername = English.NA
            } else if type == Constants.japanese {
                drivername = Japanese.NA
            }
        } else {
            drivername = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "username") as? String)!.decrypt()
        }
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "NA" {
            mobileno = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                mobileno = English.NA
            } else if type == Constants.japanese {
                mobileno = Japanese.NA
            }
        } else {
            mobileno = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt()
        }
        if (finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "NA" {
            temperature = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                temperature = English.NA
            } else if type == Constants.japanese {
                temperature = Japanese.NA
            }
        } else {
            temperature = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "temperature") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus")) == nil ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "-" ||
            ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "NA" {
            acStatus = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                acStatus = English.NA
            } else if type == Constants.japanese {
                acStatus = Japanese.NA
            }
        } else {
            acStatus = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel")) == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "-" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "NA" {
            fuel = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                fuel = English.NA
            } else if type == Constants.japanese {
                fuel = Japanese.NA
            }
        } else {
            fuel = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt()
        }
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration")) == nil ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "-" ||
        ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "NA" {
            duration = NSLocalizedString("NA", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                duration = English.NA
            } else if type == Constants.japanese {
                duration = Japanese.NA
            }
        } else {
            duration = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "duration") as? String)!.decrypt()
        }
//        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "unitno") as? String)!.decrypt())
//        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alertid") as? String)!.decrypt())
//        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alerttypeid") as? String)!.decrypt())
//        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videobaseurl") as? String)!.decrypt())
//        print(((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videofiles") as? String)!.decrypt())
        
        var unitno = ""
        var alerttypeid = ""
        var videopresent = ""
        var videoUrlListArray = [String]()
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "unitno")) == nil {
        } else {
            let unitNumber = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "unitno") as? String)!.decrypt()
            unitno = unitNumber
        }
        print(unitno)
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alerttypeid")) == nil {
        } else {
            let typeid = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "alerttypeid") as? String)!.decrypt()
            alerttypeid = typeid
        }
        print(alerttypeid)
        
        if ((self.finalArray[0] as AnyObject).object(forKey: "videopresent")) == nil {
        } else {
            let videopresentvalue = ((self.finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videopresent") as? String)!.decrypt()
            videopresent = videopresentvalue
        }
        print(videopresent)
        
        if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videobaseurl")) == nil {
        } else {
            let videobaseurl = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videobaseurl") as? String)!.decrypt()
            if ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videofiles")) == nil {
            } else {
                let videofiles = ((finalArray[(indexPath as NSIndexPath).row] as AnyObject).object(forKey: "videofiles") as? String)!.decrypt()
                let listArray = videofiles.split(separator: "|")
                print(listArray.count)
                for value in listArray {
                    let url = videobaseurl + value
                    videoUrlListArray.append(url)
                }
            }
        }
        print(videoUrlListArray)
       
        
        let mapDetails = MapModel().addMapDetailsWith(
            currentlat: currentlat,
            currentlng: currentlng,
            vehicleno: vehicleno,
            vehiclename: vehiclename,
            drivername: drivername,
            mobileno: mobileno,
            temperature: temperature,
            acstatus: acStatus,
            fuel: fuel,
            duration: duration,
            trackTime: trackTime,
            speed: speed,
            location: location,
            ignition: ignition,
            unitno: unitno,
            alerttypeid: alerttypeid,
            videoPresent: videopresent,
            videoUrlListArray: videoUrlListArray as NSArray,
            comingFrom: COMING_TYPE.RECENT_ALERTS)
        self.output.navigateToMapViewController(mapDetails: mapDetails)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if isScrollingStart {
                isScrollingStart = false
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if isScrollingStart {
            isScrollingStart = false
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        isScrollingStart = true
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isScrollingStart = true
    }

    /// This method is used to GetAlertStatus with api.
    func GetAlertStatus() {
        mainDataArray = []
        workingArray = []
        finalArray = []
        alertTypeArray = []
        var baseUrl:String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        let parameters = [
            "userid" : LoginID.encrypt()
        ]
//        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//        if type == Constants.english {
//            parameters = [
//                "userid" : LoginID.encrypt()
//            ]
//        } else if type == Constants.japanese {
//            parameters = [
//                "userid" : "2".encrypt()
//            ]
//        }
        print("\(baseUrl)"+"GetAlertStatus")
        AF.request("\(baseUrl)"+"GetAlertStatus", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.view.isUserInteractionEnabled = true
                self.activity.stopAnimating()
                return
            }
          //  print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let sts = JSON.object(forKey: "status") as? NSString {
                        self.status = (sts as String).decrypt()
                    }
                    if self.status == "success" {
                        self.mainDataArray = JSON.object(forKey: "data") as! NSArray
                        self.workingArray = self.mainDataArray
                        self.finalArray = self.mainDataArray
                        for item in self.mainDataArray {
                            let task = item as! NSDictionary
                            let alert = (task.object(forKey: "alerttype") as? String ?? "").decrypt()
                            if self.alertTypeArray.contains(alert) {
                                /// don't append
                            }
                            else
                            {
                                if alert == "" {
                                    self.alertTypeArray.append(alert)
                                } else {
                                    self.alertTypeArray.append(alert)
                                }
                            }
                        }
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            self.uiTableView.isHidden = false
                            self.uiTableView.reloadData()
                            self.statusLabel.text = "\(NSLocalizedString("Recent Alerts", comment: "")) ( \(self.finalArray.count) )"
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.statusLabel.text = "\(English.Recent_Alerts) ( \(self.finalArray.count) )"
                            } else if type == Constants.japanese {
                                self.statusLabel.text = "\(Japanese.Recent_Alerts) ( \(self.finalArray.count) )"
                            }
                        })
                    } else if self.status == "failure" {
                        if let msg = JSON.object(forKey: "message") as? NSString {
                            self.message = (msg as String).decrypt()
                        }
                        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                        if self.message == English.No_Records_found {
                            if type == Constants.english {
                                self.message = English.No_Records_found
                            } else if type == Constants.japanese {
                                self.message = Japanese.No_Records_found
                            }
                        }
                        self.showAlert("" , message: self.message)
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                        })
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    /// This method is used to getAlertDetails with api.
    func getAlertDetails() {
        mainDataArray = []
        finalArray = []
        alertTypeArray = []
        var baseUrl:String = ""
        var AlertId: String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        if let id = defaults.object(forKey: "AlertID") {
            AlertId = id as! String
        }
        print("\n\nAlertID = \(AlertId.encrypt())")
        let parameters = [
            "userid" : LoginID.encrypt(),
            "alertId" : AlertId.encrypt()
        ]
        print("\(baseUrl)"+"GetAlertDetail")
        //        print("http://5.195.195.181/FCMAPI/ItlService.svc/GetAlertDetail")
        print(parameters)
        //        http://5.195.195.181/FCMAPI/ItlService.svc/GetAlertDetail
        AF.request("\(baseUrl)"+"GetAlertDetail", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            // print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let sts = JSON.object(forKey: "status") as? NSString {
                        self.status = (sts as String).decrypt()
                    }
                    if self.status == "success" {
                        self.mainDataArray = JSON.object(forKey: "data") as! NSArray
                        self.finalArray = self.mainDataArray
                        // print(self.finalArray)
                        self.currentlat = NSString(string: ((self.finalArray[0] as AnyObject).object(forKey: "lat") as? String)!.decrypt() ).doubleValue
                        self.currentlng = NSString(string: ((self.finalArray[0] as AnyObject).object(forKey: "lon") as? String)!.decrypt() ).doubleValue
                        self.vehicleno = ((self.finalArray[0] as AnyObject).object(forKey: "vehicleno") as? String)!.decrypt()
                        self.trackTime = ((self.finalArray[0] as AnyObject).object(forKey: "alertdatetime") as? String)!.decrypt()
                        self.speed = ((self.finalArray[0] as AnyObject).object(forKey: "speed") as? String)!.decrypt()
                        self.location = ((self.finalArray[0] as AnyObject).object(forKey: "location") as? String)!.decrypt()
                        self.ignition = ((self.finalArray[0] as AnyObject).object(forKey: "alerttype") as? String)!.decrypt()
                        self.defaults.set("", forKey: "StatusName")
                        if (self.finalArray[0] as AnyObject).object(forKey: "vehiclename") == nil ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "-" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt() == "NA" {
                            self.vehiclename = ""
                        } else {
                            self.vehiclename = ((self.finalArray[0] as AnyObject).object(forKey: "vehiclename") as? String)!.decrypt()
                        }
                        if (self.finalArray[0] as AnyObject).object(forKey: "username") == nil ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "username") as? String)!.decrypt() == "-" {
                            self.drivername = NSLocalizedString("NA", comment: "")
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.drivername = English.NA
                            } else if type == Constants.japanese {
                                self.drivername = Japanese.NA
                            }
                        } else {
                            self.drivername = ((self.finalArray[0] as AnyObject).object(forKey: "username") as? String)!.decrypt()
                        }
                        if (self.finalArray[0] as AnyObject).object(forKey: "mobileno") == nil ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt() == "NA" {
                            self.mobileno = NSLocalizedString("NA", comment: "")
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.mobileno = English.NA
                            } else if type == Constants.japanese {
                                self.mobileno = Japanese.NA
                            }
                        } else {
                            self.mobileno = ((self.finalArray[0] as AnyObject).object(forKey: "mobileno") as? String)!.decrypt()
                        }
                        if (self.finalArray[0] as AnyObject).object(forKey: "temperature") == nil ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "-" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "temperature") as? String)!.decrypt() == "NA" {
                            self.temperature = NSLocalizedString("NA", comment: "")
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.temperature = English.NA
                            } else if type == Constants.japanese {
                                self.temperature = Japanese.NA
                            }
                        } else {
                            self.temperature = ((self.finalArray[0] as AnyObject).object(forKey: "temperature") as? String)!.decrypt()
                        }
                        
                        if ((self.finalArray[0] as AnyObject).object(forKey: "acstatus")) == nil ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "-" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt() == "NA" {
                            self.acStatus = NSLocalizedString("NA", comment: "")
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.acStatus = English.NA
                            } else if type == Constants.japanese {
                                self.acStatus = Japanese.NA
                            }
                        } else {
                            self.acStatus = ((self.finalArray[0] as AnyObject).object(forKey: "acstatus") as? String)!.decrypt()
                        }
                        
                        if ((self.finalArray[0] as AnyObject).object(forKey: "fuellevel")) == nil ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "-" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt() == "NA" {
                            self.fuel = NSLocalizedString("NA", comment: "")
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.fuel = English.NA
                            } else if type == Constants.japanese {
                                self.fuel = Japanese.NA
                            }
                        } else {
                            self.fuel = ((self.finalArray[0] as AnyObject).object(forKey: "fuellevel") as? String)!.decrypt()
                        }
                        
                        if ((self.finalArray[0] as AnyObject).object(forKey: "duration")) == nil ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "-" ||
                            ((self.finalArray[0] as AnyObject).object(forKey: "duration") as? String)!.decrypt() == "NA" {
                            self.duration = NSLocalizedString("NA", comment: "")
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.duration = English.NA
                            } else if type == Constants.japanese {
                                self.duration = Japanese.NA
                            }
                        } else {
                            self.duration = ((self.finalArray[0] as AnyObject).object(forKey: "duration") as? String)!.decrypt()
                        }
                        
                        var unitno = ""
                        var alerttypeid = ""
                        var videopresent = ""
                        var videoUrlListArray = [String]()
                        
                        if ((self.finalArray[0] as AnyObject).object(forKey: "unitno")) == nil {
                        } else {
                            let unitNumber = ((self.finalArray[0] as AnyObject).object(forKey: "unitno") as? String)!.decrypt()
                            unitno = unitNumber
                        }
                        print(unitno)
                        
                        if ((self.finalArray[0] as AnyObject).object(forKey: "alerttypeid")) == nil {
                        } else {
                            let typeid = ((self.finalArray[0] as AnyObject).object(forKey: "alerttypeid") as? String)!.decrypt()
                            alerttypeid = typeid
                        }
                        print(alerttypeid)
                        
                        if ((self.finalArray[0] as AnyObject).object(forKey: "videopresent")) == nil {
                        } else {
                            let videopresentvalue = ((self.finalArray[0] as AnyObject).object(forKey: "videopresent") as? String)!.decrypt()
                            videopresent = videopresentvalue
                        }
                        print(videopresent)
                        
                        if ((self.finalArray[0] as AnyObject).object(forKey: "videobaseurl")) == nil {
                        } else {
                            let videobaseurl = ((self.finalArray[0] as AnyObject).object(forKey: "videobaseurl") as? String)!.decrypt()
                            if ((self.finalArray[0] as AnyObject).object(forKey: "videofiles")) == nil {
                            } else {
                                let videofiles = ((self.finalArray[0] as AnyObject).object(forKey: "videofiles") as? String)!.decrypt()
                                let listArray = videofiles.split(separator: "|")
                                print(listArray.count)
                                for value in listArray {
                                    let url = videobaseurl + value
                                    videoUrlListArray.append(url)
                                }
                            }
                        }
                        print(videoUrlListArray)
                        
                        let mapDetails = MapModel().addMapDetailsWith(
                            currentlat: self.currentlat,
                            currentlng: self.currentlng,
                            vehicleno: self.vehicleno,
                            vehiclename: self.vehiclename,
                            drivername: self.drivername,
                            mobileno: self.mobileno,
                            temperature: self.temperature,
                            acstatus: self.acStatus,
                            fuel: self.fuel,
                            duration: self.duration,
                            trackTime: self.trackTime,
                            speed: self.speed,
                            location: self.location,
                            ignition: self.ignition,
                            unitno: unitno,
                            alerttypeid: alerttypeid,
                            videoPresent: videopresent,
                            videoUrlListArray: videoUrlListArray as NSArray,
                            comingFrom: COMING_TYPE.RECENT_ALERTS)
                        self.output.navigateToMapViewController(mapDetails: mapDetails)
                        UserDefaults.standard.set("", forKey: "AlertNavigation")
                        self.activity.stopAnimating()
                    } else if self.status == "failure" {
                        if let msg = JSON.object(forKey: "message") as? NSString {
                            self.message = (msg as String).decrypt()
                        }
                        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                        if self.message == English.No_Records_found {
                            if type == Constants.english {
                                self.message = English.No_Records_found
                            } else if type == Constants.japanese {
                                self.message = Japanese.No_Records_found
                            }
                        }
                        self.showAlert("" , message: self.message)
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            UserDefaults.standard.set("", forKey: "AlertNavigation")
                        })
                    }
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}
