import Foundation

protocol RecentAlertsViewOutput {
    func back()
    func navigateToAlertViewController(alertTypeArray: [String])
    func navigateToMapViewController(mapDetails: MapModel)
}
