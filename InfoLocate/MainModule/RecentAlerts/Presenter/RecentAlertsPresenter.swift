import Foundation

class RecentAlertsPresenter: RecentAlertsModuleInput, RecentAlertsViewOutput, RecentAlertsInteractorOutput {
    
    weak var view: RecentAlertsViewInput!
    var interactor: RecentAlertsInteractorInput!
    var router: RecentAlertsRouterInput!
    
    // MARK: - RecentAlertsViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToAlertViewController(alertTypeArray: [String]) {
        router.navigateToAlertViewController(alertTypeArray: alertTypeArray)
    }
    
    func navigateToMapViewController(mapDetails: MapModel) {
        router.navigateToMapViewController(mapDetails: mapDetails)
    }
    
    // MARK: - RecentAlertsModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - RecentAlertsInteractorOutput
    
}
