import Foundation

protocol DynamicMapViewInput: class {
    func configureModule(mapDetails: DynamicMapModel)
}
