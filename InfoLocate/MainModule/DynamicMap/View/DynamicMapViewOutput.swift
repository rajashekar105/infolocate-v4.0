import Foundation

protocol DynamicMapViewOutput {
    func back()
    func navigateToHistoryDetailsScreen(vehicleNo : String, currentlat : Double, currentlng : Double)
}
