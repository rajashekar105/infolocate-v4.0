import UIKit
import GoogleMaps


class DynamicMapViewController: BaseViewController, DynamicMapViewInput, GMSMapViewDelegate {

    /// This property is output for DynamicMapViewOutput.
    var output: DynamicMapViewOutput!
    /// This property is statusName.
    var statusName: String = ""
    /// This property is locationMarker.
    var locationMarker: GMSMarker!
    /// This property is currentlat.
    var currentlat: Double!
    /// This property is currentlng.
    var currentlng: Double!
    /// This property is vehicleno.
    var vehicleno: String = ""
    /// This property is vehiclename.
    var vehiclename: String = ""
    /// This property is drivername.
    var drivername: String = ""
    /// This property is mobileno.
    var mobileno: String = ""
    /// This property is temperature.
    var temperature: String = ""
    /// This property is acStatus.
    var acStatus: String = ""
    /// This property is fuel.
    var fuel: String = ""
    /// This property is duration.
    var duration: String = ""
    /// This property is previousdistance.
    var previousdistance: String = ""
    /// This property is currentdistance.
    var currentdistance: String = ""
    /// This property is trackTime.
    var trackTime: String = ""
    /// This property is speed.
    var speed: String = ""
    /// This property is ignition.
    var ignition: String = ""
    /// This property is location.
    var location: String = ""
    /// This property is comingFrom.
    var comingFrom: COMING_TYPE!
    /// This property is segueID.
    var segueID: String = ""
    /// This property is mapTypeValue.
    var mapTypeValue: Int = 0
    /// This property is trafficValue.
    var trafficValue: Int = 0
    /// This property is zoomlevel.
    var zoomlevel: Float = 0.0
    /// This property is statusColorArray.
    var statusColorArray: [String] = ["#28B779", "#FFB848", "#E7191B", "#807D7D", "#852B99"]
    /// This property is vehicleStatus.
    var vehicleStatus: String = ""
    /// This property is vehicleStatusLabel.
    var vehicleStatusLabel: String = ""
    /// This property is image.
    var image = UIImage()
    /// This property is infowindowstatus.
    var infowindowstatus: Bool = false
    /// This property is locationManager.
    var locationManager = CLLocationManager()
    /// This property is userLocation.
    var userLocation = CLLocation()
    /// This property is mapView.
    @IBOutlet var mapView: GMSMapView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is mainLabel.
    @IBOutlet var mainLabel: UILabel!
    /// This property is maptype.
    @IBOutlet var maptype: UIButton!
    /// This property is traffic.
    @IBOutlet var traffic: UIButton!
    /// This property is directionView.
    @IBOutlet var directionView: UIView!
    /// This property is historyView.
    @IBOutlet var historyView: UIView!
    /// This property is shareView.
    @IBOutlet var shareView: UIView!
    /// This property is vehicleInfo.
    @IBOutlet var vehicleInfo: UIView!
    /// This property is infowindow.
    @IBOutlet var infowindow: UIView!
    /// This property is hideinfowindow.
    @IBOutlet var hideinfowindow: UIButton!
    /// This property is vehicleNumberLabel.
    @IBOutlet var vehicleNumberLabel: UILabel!
    /// This property is driverNameLabel.
    @IBOutlet var driverNameLabel: UILabel!
    /// This property is mobileNoLabel
    @IBOutlet var mobileNoLabel: UILabel!
    /// This property is tempertureLabel.
    @IBOutlet var tempertureLabel: UILabel!
    /// This property is acStatusLabel.
    @IBOutlet var acStatusLabel: UILabel!
    /// This property is fuelLevelLabel.
    @IBOutlet var fuelLevelLabel: UILabel!
    /// This property is durationLabel.
    @IBOutlet var durationLabel: UILabel!
    /// This property is ignitionLabel.
    @IBOutlet var ignitionLabel: UILabel!
    /// This property is speedLabel.
    @IBOutlet var speedLabel: UILabel!
    /// This property is previousDistanceLabel.
    @IBOutlet var previousDistanceLabel: UILabel!
    /// This property is currentDistanceLabel.
    @IBOutlet var currentDistanceLabel: UILabel!
    /// This property is dateTimeLabel.
    @IBOutlet var dateTimeLabel: UILabel!
    /// This property is locationLabel.
    @IBOutlet var locationLabel: UILabel!
    /// This property is ignitionImageView.
    @IBOutlet var ignitionImageView: UIImageView!
    /// This property is infoWindowHeight.
    @IBOutlet var infoWindowHeight: NSLayoutConstraint!
    /// This property is historyButton.
    @IBOutlet var historyButton: UIButton!
    /// This property is routeButton.
    @IBOutlet var routeButton: UIButton!
    /// This property is shareButton.
    @IBOutlet var shareButton: UIButton!
    
    func configureModule(mapDetails: DynamicMapModel){
        currentlat = mapDetails.currentlat
        currentlng = mapDetails.currentlng
        vehicleno = mapDetails.vehicleno
        vehiclename = mapDetails.vehiclename
        drivername = mapDetails.drivername
        mobileno = mapDetails.mobileno
        temperature = mapDetails.temperature
        fuel = mapDetails.fuel
        duration = mapDetails.duration
        previousdistance = mapDetails.previousdistance
        currentdistance = mapDetails.currentdistance
        trackTime = mapDetails.trackTime
        speed = mapDetails.speed
        ignition = mapDetails.ignition
        location = mapDetails.location
        acStatus = mapDetails.acstatus
        comingFrom = mapDetails.comingFrom
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
//            historyButton.setTitle(English.HISTORY, for: .normal)
//            routeButton.setTitle(English.DIRECTIONS, for: .normal)
//            shareButton.setTitle(English.SHARE, for: .normal)
        } else if type == Constants.japanese {
//            historyButton.setTitle(Japanese.HISTORY, for: .normal)
//            routeButton.setTitle(Japanese.DIRECTIONS, for: .normal)
//            shareButton.setTitle(Japanese.SHARE, for: .normal)
        }
        print(comingFrom as Any)
        if comingFrom == COMING_TYPE.RECENT_ALERTS {
            ignitionImageView.image = UIImage(named: "AlertIcon")
        }
        if let range = vehicleStatus.range(of: "-") {
            //    vehicleStatusLabel = vehicleStatus.substring(to: range.lowerBound)
            vehicleStatusLabel = String(vehicleStatus[..<range.lowerBound])
        }
        mapView.mapType = GMSMapViewType.normal
        mapView.isTrafficEnabled = false
        if let ID = defaults.object(forKey: "StatusName") {
            statusName = ID as! String
            print(statusName)
        }
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            shadowView_GrayColor(view: vehicleInfo, radius: 5)
            shadowView_GrayColor(view: infowindow, radius: 0)
            shadowView_GrayColor(view: historyView, radius: 3)
            shadowView_GrayColor(view: shareView, radius: 3)
            shadowView_GrayColor(view: directionView, radius: 3)
            showMarkerOnMap()
            mainLabel.text = "\(vehicleno)"
            mapView.delegate = self
            vehicleNumberLabel.text = vehicleno
            driverNameLabel.text = drivername
            mobileNoLabel.text = mobileno
            tempertureLabel.text = temperature
            acStatusLabel.text = acStatus
            if acStatus == "ON" {
                acStatusLabel.text = NSLocalizedString("ON", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    acStatusLabel.text = English.ON
                } else if type == Constants.japanese {
                    acStatusLabel.text = Japanese.ON
                }
            } else if acStatus == "OFF" {
                acStatusLabel.text = NSLocalizedString("OFF", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    acStatusLabel.text = English.OFF
                } else if type == Constants.japanese {
                    acStatusLabel.text = Japanese.OFF
                }
            }
            fuelLevelLabel.text = fuel
            durationLabel.text = duration
            ignitionLabel.text = ignition
            speedLabel.text = speed
            dateTimeLabel.text = trackTime
            locationLabel.text = location
            previousDistanceLabel.text = previousdistance
            currentDistanceLabel.text = currentdistance
            if vehiclename == "" {
                vehicleNumberLabel.text = vehicleno
                if type == Constants.english {
                    vehiclename = English.NA
                } else if type == Constants.japanese {
                    vehiclename = Japanese.NA 
                }
            } else {
                vehicleNumberLabel.text = vehicleno //+ "::" + vehiclename
            }
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                if mobileno == English.NA {
                    // do nothing.
                } else {
                    mobileNoLabel.textColor = .init(red: 0/255, green: 0/255, blue: 238/255, alpha: 1)
                    let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
                    mobileNoLabel.isUserInteractionEnabled = true
                    mobileNoLabel.addGestureRecognizer(tap)
                }
            } else if type == Constants.japanese {
                if mobileno == Japanese.NA {
                    // do nothing.
                } else {
                    mobileNoLabel.textColor = .init(red: 0/255, green: 0/255, blue: 238/255, alpha: 1)
                    let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
                    mobileNoLabel.isUserInteractionEnabled = true
                    mobileNoLabel.addGestureRecognizer(tap)
                }
            }
//            print(infoWindowHeight.constant)
            let height = drivername.height(withConstrainedWidth: infowindow.frame.size.width, font: UIFont(name: "Avenir-Light", size: 13.0)!)
            let actualHeight = max(30,height)
            let locationHeight = location.height(withConstrainedWidth: infowindow.frame.size.width, font: UIFont(name: "Avenir-Light", size: 13.0)!)
            let actualLocationHeight = max(30,locationHeight)
            if actualHeight > 30 {
//                infoWindowHeight.constant = infoWindowHeight.constant - 15 + actualHeight
            }
            if actualLocationHeight > 30 {
//                infoWindowHeight.constant = infoWindowHeight.constant - 16 + actualLocationHeight
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        infowindowstatus = true
    }
    
    /// This method is used for phone call feature.
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        print("tap working")
        print("calling")
        let phoneNumber = mobileno
        if let phoneURL = URL(string: ("tel:" + phoneNumber)) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(phoneURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(phoneURL)
            }
        }
    }
    
    /// This method is used for history button action.
    @IBAction func historyButton(_ sender: Any) {
        output.navigateToHistoryDetailsScreen(vehicleNo: vehicleno, currentlat : currentlat, currentlng : currentlng)
    }
    
    /// This method is used for route button action.
    @IBAction func routeButton(_ sender: Any) {
        let directionsURL = "http://maps.apple.com/?saddr=\(currentlat ?? 0),\(currentlng ?? 0)"
        guard let url = URL(string: directionsURL) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    /// This method is used for share button action.
    @IBAction func shareButton(_ sender: Any) {
        var shareText = ""
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            shareText = "\(English.Vehicle_Number): " + vehicleno +
                "\n\(English.Vehicle_Name): " + vehiclename +
                "\n\(English.Driver_Name): " + drivername +
                "\n\(English.Mobile_Number): " + mobileno +
                "\n\(English.Temperature): " + temperature +
                "\n\(English.Previous_distance): " + previousdistance +
                "\n\(English.Current_distance): " + currentdistance +
                "\n\(English.AC_status): " + acStatus +
                "\n\(English.Fuel): " + fuel +
                "\n\(English.Duration): " + duration +
                "\n\(English.Ignition_Status): " + ignition +
                "\n\(English.Speed): " + speed +
                "\n\(English.DateTime): " + trackTime +
                "\n\(English.Location): " + location +
                "\n\(English.Map): https://www.google.com/maps/search/?api=1&query=\(currentlat ?? 0),\(currentlng ?? 0)"
        } else if type == Constants.japanese {
            shareText = "\(Japanese.Vehicle_Number): " + vehicleno +
                "\n\(Japanese.Vehicle_Name): " + vehiclename +
                "\n\(Japanese.Driver_Name): " + drivername +
                "\n\(Japanese.Mobile_Number): " + mobileno +
                "\n\(Japanese.Temperature): " + temperature +
                "\n\(Japanese.Previous_distance): " + previousdistance +
                "\n\(Japanese.Current_distance): " + currentdistance +
                "\n\(Japanese.AC_status): " + acStatus +
                "\n\(Japanese.Fuel): " + fuel +
                "\n\(Japanese.Duration): " + duration +
                "\n\(Japanese.Ignition_Status): " + ignition +
                "\n\(Japanese.Speed): " + speed +
                "\n\(Japanese.Date_Time): " + trackTime +
                "\n\(Japanese.Location): " + location +
                "\n\(Japanese.Map): https://www.google.com/maps/search/?api=1&query=\(currentlat ?? 0),\(currentlng ?? 0)"
        }
        let linkToShare = [shareText]
        let activityController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }
    
    /// This method is used for back button action.
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
    /// This method is used to hideinfowindow.
    @IBAction func hideinfowindow(_ sender: Any) {
        animateRideNowView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is used to animateRideNowView.
    func animateRideNowView() {
        if infowindowstatus == false
        {
            self.hideinfowindow.isHidden = false
            self.infowindow.isHidden = false
            infowindowstatus = true
        } else {
            self.hideinfowindow.isHidden = true
            self.infowindow.isHidden = true
            infowindowstatus = false
        }
    }
    
    /// This method is called when didTap on marker on mapView.
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if infowindowstatus == false
        {
            hideinfowindow.isHidden = false
            infowindow.isHidden = false
            infowindowstatus = true
        }
        return false
    }
    
    /// This method is maptype button action.
    @IBAction func maptype(_ sender: Any) {
        var image1 = UIImage()
        if mapTypeValue < 2 {
            mapTypeValue += 1
        } else {
            mapTypeValue = 0
        }
        switch mapTypeValue {
        case 0 :
            image1 = UIImage(named: "maptypeyellow")!
            mapView.mapType = GMSMapViewType.normal
            break
        case 1 :
            image1 = UIImage(named: "maptypegrey")!
            mapView.mapType = GMSMapViewType.hybrid
            break
        case 2 :
            image1 = UIImage(named: "maptypewhite")!
            mapView.mapType = GMSMapViewType.terrain
            break
        default:
            image1 = UIImage(named: "maptypeyellow")!
            mapView.mapType = GMSMapViewType.normal
            break
        }
        maptype.setBackgroundImage(nil, for: UIControl.State.normal)
        maptype.setBackgroundImage(image1, for: UIControl.State.normal)
    }
    
    /// This method is traffic button action.
    @IBAction func traffic(_ sender: Any) {
        var image2 = UIImage()
        if trafficValue == 0 {
            mapView.isTrafficEnabled = true
            image2 = UIImage(named: "trafficcolor")!
            traffic.setBackgroundImage(nil, for: UIControl.State.normal)
            traffic.setBackgroundImage(image2, for: UIControl.State.normal)
            trafficValue = 1
        } else if trafficValue == 1 {
            mapView.isTrafficEnabled = false
            image2 = UIImage(named: "trafficgrey")!
            traffic.setBackgroundImage(nil, for: UIControl.State.normal)
            traffic.setBackgroundImage(image2, for: UIControl.State.normal)
            trafficValue = 0
        }
    }
    
    /// This method is zoomin button action.
    @IBAction func zoomin(_ sender: Any) {
        zoomlevel = mapView.camera.zoom
        zoomlevel = zoomlevel + 2.0
        ZoomMap(zoomTo: Float(zoomlevel))
    }
    
    /// This method is zoomout button action.
    @IBAction func zoomout(_ sender: Any) {
        zoomlevel = mapView.camera.zoom
        zoomlevel = zoomlevel - 2.0
        ZoomMap(zoomTo: Float(zoomlevel))
    }
    
    /// This method is used to zoom on map.
    func ZoomMap(zoomTo: Float){
        mapView.animate(toZoom: zoomTo)
    }
    
    /// This method is used to showMarkerOnMap.
    func showMarkerOnMap() {
        if (self.currentlat == nil || self.currentlng == nil) {
            currentlng = 0
            currentlat = 0
        }
        if locationMarker != nil {
            locationMarker.map = nil
        }
        if let vehicleStatus = defaults.object(forKey: "StatusName") {
            vehicleStatusLabel = vehicleStatus as! String
        }
        switch vehicleStatusLabel {
        case "moving" :
            image = UIImage(named: "moving")!
            break
        case "idle" :
            image = UIImage(named: "idle")!
            break
        case "stopped" :
            image = UIImage(named: "stopped")!
            break
        case "inactive" :
            image = UIImage(named: "inactive")!
            break
        case "outofservice" :
            image = UIImage(named: "outofservice")!
            break
        default:
            image = UIImage(named: "stopped")!
            break
        }
        let coordinate = CLLocationCoordinate2D(latitude: currentlat, longitude: currentlng)
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = mapView
        locationMarker.icon = image
        locationMarker.title = vehicleno
        mapView.selectedMarker = locationMarker
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: currentlat, longitude: currentlng, zoom: 15.0)
        self.mapView.camera = camera
    }
    
    /// This method is called when mapViewSnapshotReady.
    func mapViewSnapshotReady(_ mapView: GMSMapView) {
        DispatchQueue.main.async(execute: {
            self.showMarkerOnMap()
            // Center camera to marker position
            self.mapView.camera = GMSCameraPosition.camera(withTarget: self.locationMarker.position, zoom: 15)
        })
    }
    
//    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
//        self.mapView = mapView
//        infoWindow = Bundle.main.loadNibNamed("MarkerInfoView", owner: self.view, options: nil)!.first! as? MarkerInfoView
//        infoWindow.vehicleNoLabel.text = NSLocalizedString("Vehicle #", comment: "")
//        infoWindow.trackTimeLabel.text = NSLocalizedString("Date Time", comment: "")
//        infoWindow.speedLabel.text = NSLocalizedString("Speed", comment: "")
//        infoWindow.locationLabel.text = NSLocalizedString("Location", comment: "")
//        infoWindow.ignitionLabel.text = NSLocalizedString("Ignition", comment: "")
//        if comingFrom == COMING_TYPE.RECENT_ALERTS {
//            infoWindow.ignitionLabel.text = NSLocalizedString("Alert Type", comment: "")
//        }
//
//        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//        if type == Constants.english {
//            infoWindow.vehicleNoLabel.text = English.Vehicle
//            infoWindow.driverNameLabel.text = English.Driver_Name
//            infoWindow.mobileNoLabel.text = English.Mobile_No
//            infoWindow.trackTimeLabel.text = English.DateTime
//            infoWindow.speedLabel.text = English.Speed
//            infoWindow.locationLabel.text = English.Location
//            infoWindow.ignitionLabel.text = English.Ignition
//            if comingFrom == COMING_TYPE.RECENT_ALERTS {
//                infoWindow.ignitionLabel.text = English.Alert_Type
//            }
//        } else if type == Constants.japanese {
//            infoWindow.vehicleNoLabel.text = Japanese.Vehicle
//            infoWindow.driverNameLabel.text = Japanese.Driver_Name
//            infoWindow.mobileNoLabel.text = Japanese.Mobile_No
//            infoWindow.trackTimeLabel.text = Japanese.Date_Time
//            infoWindow.speedLabel.text = Japanese.Speed
//            infoWindow.locationLabel.text = Japanese.Location
//            infoWindow.ignitionLabel.text = Japanese.Ignition
//            if comingFrom == COMING_TYPE.RECENT_ALERTS {
//                infoWindow.ignitionLabel.text = Japanese.Alert_Type
//            }
//        }
//
//        marker.appearAnimation = GMSMarkerAnimation.pop
//        marker.map = mapView
//
//        infoWindow.vehicleNo.text = ": \(vehicleno)"
//        infoWindow.driverName.text = ": \(drivername)"
//        infoWindow.mobileNo.text = ": \(mobileno)"
//        infoWindow.ignition.text = ": \(ignition)"
//        infoWindow.trackTime.text = ": \(trackTime)"
//        infoWindow.speed.text = ": \(speed)"
//        //infoWindow.status.text = ": \(vehicleStatusLabel)"
//
//        if location != "" {
//            infoWindow.location.text = ": \(location)"
//        } else {
//            infoWindow.location.text = ": -"
//        }
//
////        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//        if type == Constants.english {
//            if mobileno == English.NA {
//
//            } else {
//                infoWindow.mobileNo.textColor = .init(red: 0/255, green: 0/255, blue: 238/255, alpha: 1)
//            }
//        } else if type == Constants.japanese {
//            if mobileno == Japanese.NA {
//
//            } else {
//                infoWindow.mobileNo.textColor = .init(red: 0/255, green: 0/255, blue: 238/255, alpha: 1)
//            }
//        }
//
//
//        infoWindow.location.sizeToFit()
//        infoWindow.layer.cornerRadius = 5
//
//        let height = drivername.height(withConstrainedWidth: infoWindow.frame.size.width, font: UIFont(name: "Avenir-Light", size: 13.0)!)
//        let actualHeight = max(20,height)
//
//        //            let value2 = detectLabelHeight(label: infoWindow.driverName)
//        let value1 = detectLabelHeight(label: infoWindow.location)
//        var newframe = infoWindow.frame
//        newframe.size.height = newframe.size.height - 20 + value1 + actualHeight
//        infoWindow.frame = newframe
//        return infoWindow
//    }
    
//    @objc func tapFunction(sender: UITapGestureRecognizer) {
//        print("tap working")
//        print("calling")
//        let phoneNumber = mobileno
//        if let phoneURL = NSURL(string: ("tel://" + phoneNumber)) {
//            let alert = UIAlertController(title: (phoneNumber + " ?"), message: nil, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
//                UIApplication.shared.canOpenURL(phoneURL as URL)
//            }))
//
//            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//            self.present(alert, animated: true, completion: nil)
//        }
//    }
    
    /// This method is used to detectLabelHeight.
    func detectLabelHeight(label: UILabel) -> (CGFloat) {
        return (CGFloat(label.frame.height))
    }
    
//    /// This method is used to show alert.
//    func showAlert(_ title: String, message:String) {
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
//            UIAlertAction in
//        }
//        alertController.addAction(okAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
}
