import Foundation

class DynamicMapPresenter: DynamicMapModuleInput, DynamicMapViewOutput, DynamicMapInteractorOutput {
    
    weak var view: DynamicMapViewInput!
    var interactor: DynamicMapInteractorInput!
    var router: DynamicMapRouterInput!
    
    // MARK: - DynamicMapViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToHistoryDetailsScreen(vehicleNo : String, currentlat : Double, currentlng : Double) {
        router.navigateToHistoryDetailsScreen(vehicleNo : vehicleNo, currentlat : currentlat, currentlng : currentlng)
    }
    
    // MARK: - DynamicMapModuleInput
    
    func configureModule(mapDetails: DynamicMapModel) {
        view.configureModule(mapDetails: mapDetails)
    }
    
    // MARK: - DynamicMapInteractorOutput
    
}
