import UIKit

class DynamicMapRouter:DynamicMapRouterInput {
    
    weak var viewController: DynamicMapViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToHistoryDetailsScreen(vehicleNo : String, currentlat : Double, currentlng : Double) {
        Navigation.navigateToHistoryDetailsScreen(vehicleNo: vehicleNo, currentlat : currentlat, currentlng : currentlng, viewController: viewController)
    }
    
}
