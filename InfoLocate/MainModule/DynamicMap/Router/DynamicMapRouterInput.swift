import Foundation

protocol DynamicMapRouterInput {
    func back()
    func navigateToHistoryDetailsScreen(vehicleNo : String, currentlat : Double, currentlng : Double)
}
