import UIKit

@objc class DynamicMapModuleBuilder: NSObject {
    
    func build(mapDetails: DynamicMapModel) -> UIViewController {
        let viewController = DynamicMapControllerFromStoryboard()
        
        let router = DynamicMapRouter()
        router.viewController = viewController
        
        let presenter = DynamicMapPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = DynamicMapInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(mapDetails: mapDetails)
        
        return viewController
    }
    
    func DynamicMapControllerFromStoryboard() -> DynamicMapViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.DYNAMIC_MAP_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! DynamicMapViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.MAINMODULE_STORYBOARD, bundle: Bundle.main)
        return storyboard
    }
    
}
