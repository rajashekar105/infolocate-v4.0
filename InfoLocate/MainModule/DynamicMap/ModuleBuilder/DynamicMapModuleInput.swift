import Foundation

protocol DynamicMapModuleInput: class {
    
    func configureModule(mapDetails: DynamicMapModel)
}
