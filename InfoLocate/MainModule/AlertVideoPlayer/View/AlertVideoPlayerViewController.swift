import UIKit
//import MobileVLCKit
import Photos

class AlertVideoPlayerViewController: UIViewController, AlertVideoPlayerViewInput {

    
    var output: AlertVideoPlayerViewOutput!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is titleLabel.
    @IBOutlet weak var titleLabel: UILabel!
    /// This property is videoView.
    @IBOutlet weak var videoView: UIView!
    /// This property knows download button.
    @IBOutlet var downloadButton : UIButton!
    /// This property is playPauseButton.
    @IBOutlet weak var playPauseButton: UIButton!
    /// This property is mediaPlayer.
//    var mediaPlayer = VLCMediaPlayer()
    /// This property is mediaURL.
    var mediaURL = URL(string: "")
    /// This property is vehicleno.
    var vehicleno = ""
    // This property is loading.
    var loading: UIActivityIndicatorView!
    
    func configureModule(url: URL, vehicleno: String) {
        self.mediaURL = url
        self.vehicleno = vehicleno
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMediaPLayer()
        titleLabel.text = vehicleno
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
//            mediaPlayer.play()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        mediaPlayer.pause()
    }
    
    @IBAction func back(_ sender: Any) {
//        mediaPlayer.pause()
        self.output.back()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupMediaPLayer() {
//        mediaPlayer.delegate = self
//        mediaPlayer.drawable = videoView
//        mediaPlayer.media = VLCMedia(url: mediaURL!)
        
        loading = UIActivityIndicatorView.init(style: .whiteLarge)
        loading.frame = videoView.bounds
        loading.startAnimating()
        videoView.addSubview(loading)
    }

    @IBAction func playPauseButtonAction(_ sender: UIButton) {
//        if mediaPlayer.isPlaying
//        {
//            mediaPlayer.pause()
//            let remaining = mediaPlayer.remainingTime
//            let time = mediaPlayer.time
//            print("Paused at \(time?.stringValue ?? "nil") with \(remaining?.stringValue ?? "nil") time remaining")
//            playPauseButton.setImage(UIImage(named: "play_circle"), for: .normal)
//        } else {
//            let remaining = mediaPlayer.remainingTime
//            print(remaining?.stringValue! as Any)
//            if mediaPlayer.state == .stopped {
//                let media = VLCMedia(url: mediaURL!)
//                mediaPlayer.media = media
//                mediaPlayer.play()
//                print("Playing")
//                playPauseButton.setImage(UIImage(named: "pause_circle"), for: .normal)
//            } else {
//                mediaPlayer.play()
//                print("Playing")
//                playPauseButton.setImage(UIImage(named: "pause_circle"), for: .normal)
//            }
//        }
    }
    
    /// This method is used as downloadButtonAction.
    @IBAction func downloadButtonAction(_ sender : UIButton) {
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            var title = NSLocalizedString("Are you sure want to download?", comment: "")
            var cancelTitle = NSLocalizedString("CANCEL", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.Are_you_sure_want_to_download
                cancelTitle = English.CANCEL
            } else if type == Constants.japanese {
                title = Japanese.Are_you_sure_want_to_download
                cancelTitle = Japanese.CANCEL
            }
            let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.view.isUserInteractionEnabled = false
                self.activity.startAnimating()
                self.popUp()
                self.download()
            }
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default) {
                UIAlertAction in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func popUp() {
        var title = NSLocalizedString("The video is being downloaded. After downloading video will be saved in photos!", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            title = English.After_downloading_video_will_be_saved_in_photos
        } else if type == Constants.japanese {
            title = Japanese.After_downloading_video_will_be_saved_in_photos
        }
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /// This method is used as download.
    func download() {
//        mediaPlayer.pause()
        DispatchQueue.global(qos: .background).async {
            if let url = self.mediaURL,
                let urlData = NSData(contentsOf: url) {
                print(url.lastPathComponent)
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/\(url.lastPathComponent)"
                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                    }) { completed, error in
                        if completed {
                            self.hide()
                            print("Video is saved!")
                        }
                    }
                }
            }
        }
    }
    
    func hide() {
        self.view.isUserInteractionEnabled = true
        self.activity.stopAnimating()
    }
}

//extension AlertVideoPlayerViewController: VLCMediaPlayerDelegate {
//
//    func mediaPlayerStateChanged(_ aNotification: Notification!) {
//
//        switch(mediaPlayer.state) {
//        case .opening :
//            loading.stopAnimating()
//            playPauseButton.setImage(UIImage(named: "pause_circle"), for: .normal)
//            break
//        case .buffering :
//            break
//        case .playing :
//            playPauseButton.setImage(UIImage(named: "pause_circle"), for: .normal)
//            break
//        case .stopped :
//            playPauseButton.setImage(UIImage(named: "play_circle"), for: .normal)
//            break
//        case .ended :
//            break
//        case .error:
//            break
//        case .paused:
//            playPauseButton.setImage(UIImage(named: "play_circle"), for: .normal)
//            break
//        case .esAdded:
//            break
//        default:
//            break
//        }
//    }
//}
