import Foundation

protocol AlertVideoPlayerViewOutput {
    func back()
}
