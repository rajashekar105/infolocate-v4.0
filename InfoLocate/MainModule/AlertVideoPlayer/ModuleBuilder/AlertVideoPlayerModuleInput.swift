import Foundation

protocol AlertVideoPlayerModuleInput: class {
    
    func configureModule(url: URL, vehicleno: String)
}
