import UIKit

@objc class AlertVideoPlayerModuleBuilder: NSObject {
    
    func build(url: URL, vehicleno: String) -> UIViewController {
        let viewController = AlertVideoPlayerControllerFromStoryboard()
        
        let router = AlertVideoPlayerRouter()
        router.viewController = viewController
        
        let presenter = AlertVideoPlayerPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = AlertVideoPlayerInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(url: url, vehicleno: vehicleno)
        
        return viewController
    }
    
    func AlertVideoPlayerControllerFromStoryboard() -> AlertVideoPlayerViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.ALERTS_VIDEO_PLAYER_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! AlertVideoPlayerViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.MAINMODULE_STORYBOARD, bundle: Bundle.main)
        return storyboard
    }
    
}
