import Foundation

class AlertVideoPlayerPresenter: AlertVideoPlayerModuleInput, AlertVideoPlayerViewOutput, AlertVideoPlayerInteractorOutput {
    
    weak var view: AlertVideoPlayerViewInput!
    var interactor: AlertVideoPlayerInteractorInput!
    var router: AlertVideoPlayerRouterInput!
    
    // MARK: - AlertVideoPlayerViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - AlertVideoPlayerModuleInput
    
    func configureModule(url: URL, vehicleno: String) {
        view.configureModule(url: url, vehicleno: vehicleno)
    }
    
    // MARK: - AlertVideoPlayerInteractorOutput
    
}
