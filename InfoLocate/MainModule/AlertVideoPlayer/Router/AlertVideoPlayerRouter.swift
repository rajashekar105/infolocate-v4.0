import UIKit

class AlertVideoPlayerRouter:AlertVideoPlayerRouterInput {
    
    weak var viewController: AlertVideoPlayerViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
