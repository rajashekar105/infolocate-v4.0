import Foundation

protocol HistoryDetailsModuleInput: class {
    
    func configureModule(vehicleNo : String, currentlat : Double, currentlng : Double)
}
