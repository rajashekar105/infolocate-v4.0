import UIKit

@objc class HistoryDetailsModuleBuilder: NSObject {
    
    func build(vehicleNo : String, currentlat : Double, currentlng : Double) -> UIViewController {
        let viewController = HistoryDetailsControllerFromStoryboard()
        
        let router = HistoryDetailsRouter()
        router.viewController = viewController
        
        let presenter = HistoryDetailsPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = HistoryDetailsInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule(vehicleNo : vehicleNo, currentlat : currentlat, currentlng : currentlng)
        
        return viewController
    }
    
    func HistoryDetailsControllerFromStoryboard() -> HistoryDetailsViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.HISTORY_DETAILS_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! HistoryDetailsViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.MAINMODULE_STORYBOARD, bundle: Bundle.main)
        return storyboard
    }
    
}
