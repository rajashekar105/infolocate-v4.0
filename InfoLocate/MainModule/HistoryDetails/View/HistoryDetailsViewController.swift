import UIKit
import GoogleMaps
import Alamofire


class HistoryDetailsViewController: BaseViewController, HistoryDetailsViewInput, GMSMapViewDelegate {
    
    /// This property is output for HistoryDetailsViewOutput.
    var output: HistoryDetailsViewOutput!
    /// This property is marker.
    var marker = GMSMarker()
    /// This property is sourcemarker.
    var sourcemarker = GMSMarker()
    /// This property is destinationmarker.
    var destinationmarker = GMSMarker()
    /// This property is polyline.
    var polyline = GMSPolyline()
    /// This property is path.
    var path = GMSMutablePath()
    /// This property is dataArray.
    var dataArray = NSArray()
    /// This property is i.
    var i: Int = 0
    /// This property is timer1.
    var timer1: Timer!
    /// This property is timer2.
    var timer2: Timer!
    /// This property is timer3.
    var timer3: Timer!
    /// This property is duration.
    var duration: Float = 1.0
    /// This property is directionArray.
    var directionArray: [Double] = []
    /// This property is speedArray.
    var speedArray: [Double] = []
    /// This property is dateTimeArray.
    var dateTimeArray: [String] = []
    /// This property is latArray.
    var latArray: [Double] = []
    /// This property is lonArray.
    var lonArray: [Double] = []
    /// This property is vehicleNo.
    var vehicleNo: String = ""
    /// This property is currentlat.
    var currentlat: Double!
    /// This property is currentlng.
    var currentlng: Double!
    /// This property is ignitionStartDateTime.
    var ignitionStartDateTime: String = ""
    /// This property is ignitionOffDateTime.
    var ignitionOffDateTime: String = ""
    /// This property is is_playing.
    var is_playing: Bool = false
    /// This property is image.
    var image = UIImage()
    /// This property is time.
    var time : Float = 0.0
    /// This property is totalTime.
    var totalTime: Float = 0.0
    /// This property is polylineStatus.
    var polylineStatus: Bool = true
    /// This property is mapTypeValue.
    var mapTypeValue: Int = 0
    /// This property is trafficValue.
    var trafficValue: Int = 0
    /// This property is zoomlevel.
    var zoomlevel: Float = 0.0
    /// This property is datePicker.
    let datePicker = UIDatePicker()
    /// This property is toolbar.
    let toolbar = UIToolbar()
    /// This property is doneTitle.
    var doneTitle = "Done"
    /// This property is cancelTitle.
    var cancelTitle = "Cancel"
    /// This property is startDateTime.
    @IBOutlet var startDateTime: UIButton!
    /// This property is endDateTime.
    @IBOutlet var endDateTime: UIButton!
    /// This property is historyTrack.
    @IBOutlet var historyTrack: UIButton!
    /// This property is titleLabel.
    @IBOutlet var titleLabel: UILabel!
    /// This property is mapview.
    @IBOutlet var mapview: GMSMapView!
    /// This property is activity.
    @IBOutlet var activity: UIActivityIndicatorView!
    /// This property is infowindow.
    @IBOutlet var infowindow: UIView!
    /// This property is infoWindowHeight.
    @IBOutlet var infoWindowHeight: NSLayoutConstraint!
    /// This property is playpause.
    @IBOutlet var playpause: UIButton!
    /// This property is progressbar.
    @IBOutlet var progressbar: UIProgressView!
    /// This property is maptype.
    @IBOutlet var maptype: UIButton!
    /// This property is traffic.
    @IBOutlet var traffic: UIButton!
    /// This property is vehicleNoLabel.
    @IBOutlet var vehicleNoLabel: UILabel!
    /// This property is trackTimeLabel.
    @IBOutlet var trackTimeLabel: UILabel!
    /// This property is speedLabel.
    @IBOutlet var speedLabel: UILabel!
    /// This property is polylineButton.
    @IBOutlet var polylineButton: UIButton!
    
    func configureModule(vehicleNo : String, currentlat : Double, currentlng : Double) {
        self.vehicleNo = vehicleNo
        self.currentlat = currentlat
        self.currentlng = currentlng
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.infoWindowHeight.constant = 0
        self.infowindow.isHidden = true
        self.titleLabel.text = NSLocalizedString("Trip History", comment: "")
        polylineButton.setTitle(NSLocalizedString("HIDE", comment: ""), for: UIControl.State.normal)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            self.titleLabel.text = English.Trip_History
            startDateTime.setTitle(English.Select_start_date_and_time, for: .normal)
            endDateTime.setTitle(English.Select_end_date_and_time, for: .normal)
            historyTrack.setTitle(English.History_Track, for: .normal)
            polylineButton.setTitle(English.HIDE, for: UIControl.State.normal)
            doneTitle = English.Done
            cancelTitle = English.Cancel
        } else if type == Constants.japanese {
            self.titleLabel.text =  Japanese.Trip_History
            startDateTime.setTitle(Japanese.Select_start_date_and_time, for: .normal)
            endDateTime.setTitle(Japanese.Select_end_date_and_time, for: .normal)
            historyTrack.setTitle(Japanese.History_Track, for: .normal)
            polylineButton.setTitle(Japanese.HIDE, for: UIControl.State.normal)
            doneTitle = Japanese.Done
            cancelTitle = Japanese.Cancel
        }
        progressbar.setProgress(0.0, animated: true)
        activity.layer.cornerRadius = 10
        image = UIImage(named: "play.png")!
        playpause.setImage(nil, for: UIControl.State.normal)
        playpause.setImage(image, for: UIControl.State.normal)
        infowindow.layer.shadowColor = UIColor.black.cgColor
        infowindow.layer.shadowOffset = CGSize.zero
        infowindow.layer.shadowRadius = 3.0
        infowindow.layer.shadowOpacity = 0.7
        mapview.delegate = self
        mapview.mapType = GMSMapViewType.normal
        mapview.isTrafficEnabled = false
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: currentlat, longitude: currentlng, zoom: 15.0)
        mapview.camera = camera
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /// This method is used for startDateButtonAction.
    @IBAction func startDateButtonAction(_ sender: Any) {
        clear()
       //Format Date
        datePicker.frame = CGRect(x: 0, y: 80, width: self.view.bounds.size.width, height: self.view.bounds.size.height-60)
        datePicker.datePickerMode = .dateAndTime
        datePicker.maximumDate = Date()
        datePicker.backgroundColor = .white
        toolbar.sizeToFit()
        toolbar.backgroundColor = .white
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        let date = formatter.date(from: (startDateTime.titleLabel?.text)!)
        if date != nil{
            datePicker.date = date!
        }
        let doneButton = UIBarButtonItem(title: doneTitle, style: .plain, target: self, action: #selector(doneStartDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: cancelTitle, style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolbar.frame = CGRect(x: 0, y: 20, width: self.view.bounds.size.width, height: 60)
        self.view.addSubview(toolbar)
        self.view.addSubview(datePicker)
    }
    
    /// This method is used for endDateButtonAction.
    @IBAction func endDateButtonAction(_ sender: Any) {
        //Format Date
        clear()
        datePicker.frame = CGRect(x: 0, y: 80, width: self.view.bounds.size.width, height: self.view.bounds.size.height-60)
        datePicker.datePickerMode = .dateAndTime
        datePicker.maximumDate = Date()
        datePicker.backgroundColor = .white
        toolbar.sizeToFit()
        toolbar.backgroundColor = .white
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        let date = formatter.date(from: (endDateTime.titleLabel?.text)!)
        if date != nil{
            datePicker.date = date!
        }
        let doneButton = UIBarButtonItem(title: doneTitle, style: .plain, target: self, action: #selector(doneEndDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: cancelTitle, style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolbar.frame = CGRect(x: 0, y: 20, width: self.view.bounds.size.width, height: 60)
        self.view.addSubview(toolbar)
        self.view.addSubview(datePicker)
    }
       
    /// This method is used for doneStartDatePicker.
    @objc func doneStartDatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        startDateTime.setTitle(formatter.string(from: datePicker.date), for: .normal)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        ignitionStartDateTime = formatter.string(from: datePicker.date)
        datePicker.removeFromSuperview()
        toolbar.removeFromSuperview()
    }
    
    /// This method is used for doneEndDatePicker.
    @objc func doneEndDatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        endDateTime.setTitle(formatter.string(from: datePicker.date), for: .normal)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        ignitionOffDateTime = formatter.string(from: datePicker.date)
        datePicker.removeFromSuperview()
        toolbar.removeFromSuperview()
    }
    
    /// This method is used for cancelDatePicker.
    @objc func cancelDatePicker(){
        datePicker.removeFromSuperview()
        toolbar.removeFromSuperview()
    }
    
    /// This method is used for goButtonAction.
    @IBAction func goButtonAction(_ sender: Any) {
        clear()
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if ignitionStartDateTime == "" && ignitionOffDateTime == "" {
            if type == Constants.english {
                message = English.Please_select_start_end_date_and_time
            } else if type == Constants.japanese {
                message = Japanese.Please_select_start_end_date_and_time
            }
            showAlert("", message: message)
        } else if ignitionStartDateTime == "" {
            if type == Constants.english {
                message = English.Please_select_start_date_and_time
            } else if type == Constants.japanese {
                message = Japanese.Please_select_start_date_and_time
            }
            showAlert("", message: message)
        } else if ignitionOffDateTime == "" {
            if type == Constants.english {
                message = English.Please_select_end_date_and_time
            } else if type == Constants.japanese {
                message = Japanese.Please_select_end_date_and_time
            }
            showAlert("", message: message)
        } else {
            dateValidation()
        }
       
    }
    
    /// This method is used for dateValidation.
    func dateValidation() {
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startDate = formatter.date(from: ignitionStartDateTime)!
        let endDate = formatter.date(from: ignitionOffDateTime)!
    
        let order = Calendar.current.compare(startDate, to: endDate, toGranularity: .day)
        switch order {
        case .orderedDescending:
            if type == Constants.english {
                message = English.Please_select_start_date_before_end_date
            } else if type == Constants.japanese {
                message = Japanese.Please_select_start_date_before_end_date
            }
            showAlert("", message: message)
        case .orderedAscending:
            let components =  Calendar.current.dateComponents([.day], from: startDate, to: endDate)
            let count = components.day!
            print(count)
            if count < 3 {
                GetIgnitionHistoryDetails()
                activity.startAnimating()
            } else {
                if type == Constants.english {
                    message = English.Difference_between_start_date_and_end_date_should_be_maximum_of_3_days
                } else if type == Constants.japanese {
                    message = Japanese.Difference_between_start_date_and_end_date_should_be_maximum_of_3_days
                }
                showAlert("", message: message)
            }
        case .orderedSame:
            let components =  Calendar.current.dateComponents([.day], from: startDate, to: endDate)
            let count = components.day!
            print(count)
            if Reachability.connectedToNetwork() == false
            {
                var title = NSLocalizedString("No Internet Connection", comment: "")
                var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    title = English.No_Internet_Connection
                    message = English.Make_sure_your_device_is_connected_to_the_internet
                } else if type == Constants.japanese {
                    title = Japanese.No_Internet_Connection
                    message = Japanese.Make_sure_your_device_is_connected_to_the_internet
                }
                let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                GetIgnitionHistoryDetails()
                activity.startAnimating()
            }
        }
    }
    
    /// This method is used to clear the data.
    func clear() {
        if self.timer1 != nil {
            self.timer1.invalidate()
        }
        if self.timer2 != nil {
            self.timer2.invalidate()
        }
        if self.timer3 != nil {
            self.timer3.invalidate()
        }
        self.i = 0
        self.time = 0.0
        self.totalTime = 0.0
        self.polyline.map = nil
        self.progressbar.setProgress(0.0, animated: true)
        self.mapview.clear()
        self.image = UIImage(named: "play.png")!
        self.playpause.setImage(nil, for: UIControl.State.normal)
        self.playpause.setImage(self.image, for: UIControl.State.normal)
        self.is_playing = false
    }
    
    /// This method is used for back button action.
    @IBAction func back(_ sender: Any) {
        if timer1 != nil {
            timer1.invalidate()
        }
        if timer2 != nil {
            timer2.invalidate()
        }
        if timer3 != nil {
            timer3.invalidate()
        }
        self.output.back()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is used for polyline button action.
    @IBAction func polylineButton(_ sender: Any) {
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if polylineStatus == true {
            polyline.map = nil
            polylineStatus = false
            polylineButton.setTitle(NSLocalizedString("SHOW", comment: ""), for: UIControl.State.normal)
            if type == Constants.english {
                polylineButton.setTitle(English.SHOW, for: UIControl.State.normal)
            } else if type == Constants.japanese {
                polylineButton.setTitle(Japanese.SHOW, for: UIControl.State.normal)
            }
        } else if polylineStatus == false {
            drawRoute()
            polylineStatus = true
            polylineButton.setTitle(NSLocalizedString("HIDE", comment: ""), for: UIControl.State.normal)
            if type == Constants.english {
                polylineButton.setTitle(English.HIDE, for: UIControl.State.normal)
            } else if type == Constants.japanese {
                polylineButton.setTitle(Japanese.HIDE, for: UIControl.State.normal)
            }
        }
    }
    
    /// This method is used for maptype buttonaction.
    @IBAction func maptype(_ sender: Any) {
        var image1 = UIImage()
        if mapTypeValue < 2 {
            mapTypeValue += 1
        } else {
            mapTypeValue = 0
        }
        switch mapTypeValue {
        case 0 :
            image1 = UIImage(named: "maptypeyellow")!
            mapview.mapType = GMSMapViewType.normal
            break
        case 1 :
            image1 = UIImage(named: "maptypegrey")!
            mapview.mapType = GMSMapViewType.hybrid
            break
        case 2 :
            image1 = UIImage(named: "maptypewhite")!
            mapview.mapType = GMSMapViewType.terrain
            break
        default:
            image1 = UIImage(named: "maptypeyellow")!
            mapview.mapType = GMSMapViewType.normal
            break
        }
        maptype.setBackgroundImage(nil, for: UIControl.State.normal)
        maptype.setBackgroundImage(image1, for: UIControl.State.normal)
    }
    
    /// This method is used for traffic button action.
    @IBAction func traffic(_ sender: Any) {
        var image2 = UIImage()
        if trafficValue == 0 {
            mapview.isTrafficEnabled = true
            image2 = UIImage(named: "trafficcolor")!
            traffic.setBackgroundImage(nil, for: UIControl.State.normal)
            traffic.setBackgroundImage(image2, for: UIControl.State.normal)
            trafficValue = 1
        } else if trafficValue == 1 {
            mapview.isTrafficEnabled = false
            image2 = UIImage(named: "trafficgrey")!
            traffic.setBackgroundImage(nil, for: UIControl.State.normal)
            traffic.setBackgroundImage(image2, for: UIControl.State.normal)
            trafficValue = 0
        }
        
    }
    
    /// This method is used for zoomin button action.
    @IBAction func zoomin(_ sender: Any) {
        zoomlevel = mapview.camera.zoom
        zoomlevel = zoomlevel + 2.0
        ZoomMap(zoomTo: Float(zoomlevel))
    }
    
    /// This method is used for zoomout button action.
    @IBAction func zoomout(_ sender: Any) {
        zoomlevel = mapview.camera.zoom
        zoomlevel = zoomlevel - 2.0
        ZoomMap(zoomTo: Float(zoomlevel))
    }
    
    /// This method is used to zoom on map..
    func ZoomMap(zoomTo: Float){
        mapview.animate(toZoom: zoomTo)
    }

    /// This method is used for playpause button action.
    @IBAction func playpause(_ sender: Any)
    {
        play()
    }
    
    /// This method is used to play animation on map and progress bar.
    func play() {
        if latArray.count != 0 && lonArray.count != 0
        {
            totalTime = (duration * Float(latArray.count - 1))
            if is_playing == false
            {
                image = UIImage(named: "pause")!
                playpause.setImage(nil, for: UIControl.State.normal)
                playpause.setImage(image, for: UIControl.State.normal)
                if self.timer1 == nil {
                    timer1 = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: false)
                }
                timer2 = Timer.scheduledTimer(timeInterval: TimeInterval(duration), target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
                timer3 = Timer.scheduledTimer(timeInterval: TimeInterval(duration), target: self, selector: #selector(updateProgress), userInfo: nil, repeats: true)
                is_playing = true
            }
            else if is_playing == true
            {
                image = UIImage(named: "play")!
                playpause.setImage(nil, for: UIControl.State.normal)
                playpause.setImage(image, for: UIControl.State.normal)
                timer2.invalidate()
                timer3.invalidate()
                is_playing = false
            }
        } else {
            var message = NSLocalizedString("No records found.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                message = English.No_records_found
            } else if type == Constants.japanese {
                message = Japanese.No_records_found
            }
            self.showAlert("", message: message)
        }
    }
    
    /// This method is used to update progress bar.
    @objc func updateProgress () {
        let a: Float = (1 / totalTime) * duration
        time += a
        print(time)
        progressbar.setProgress(time, animated: true)
        if time >= 1.0 {
            timer3!.invalidate()
        }
    }
    
    /// This method is used to replay animation on map and progress bar.
    @IBAction func replay(_ sender: Any) {
        if latArray.count != 0 && lonArray.count != 0
        {
            DispatchQueue.main.async(execute: {
                if self.timer1 != nil {
                    self.timer1.invalidate()
                }
                if self.timer2 != nil {
                    self.timer2.invalidate()
                }
                if self.timer3 != nil {
                    self.timer3.invalidate()
                }
                self.i = 0
                self.time = 0.0
                self.totalTime = 0.0
                self.polyline.map = nil
                self.progressbar.setProgress(0.0, animated: true)
                self.mapview.clear()
                self.image = UIImage(named: "play.png")!
                self.playpause.setImage(nil, for: UIControl.State.normal)
                self.playpause.setImage(self.image, for: UIControl.State.normal)
                self.is_playing = false
                self.sourcemarker.position = CLLocationCoordinate2DMake(self.latArray[0], self.lonArray[0])
                self.sourcemarker.icon = UIImage(named: "startmarker")
                self.sourcemarker.map = self.mapview
                self.destinationmarker.position = CLLocationCoordinate2DMake(self.latArray.last!, self.lonArray.last!)
                self.destinationmarker.icon = UIImage(named: "endmarker")
                self.destinationmarker.map = self.mapview
                self.marker.position = CLLocationCoordinate2DMake(self.latArray[0], self.lonArray[0])
                self.marker.icon = UIImage(named: "moving")
                self.marker.map = self.mapview
                self.drawRoute()
                let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: self.latArray[0], longitude: self.lonArray[0], zoom: 15.0)
                self.mapview.camera = camera
                let when = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.play()
                }
            })
        } else {
            var message = NSLocalizedString("No records found.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                message = English.No_records_found
            } else if type == Constants.japanese {
                message = Japanese.No_records_found
            }
            self.showAlert("", message: message)
        }
    }
    
    /// This method is called to updatemarker on mapview .
    func updateMarker(coordinates: CLLocationCoordinate2D, duration: Double) {
        // Movement
        CATransaction.begin()
        CATransaction.setAnimationDuration(duration)
        marker.position = coordinates
        // Center Map View
        let camera = GMSCameraUpdate.setTarget(coordinates)
        mapview.animate(with: camera)
        CATransaction.commit()
    }
    
    //    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
    //        var destinationLocation = CLLocation()
    //        destinationLocation = CLLocation(latitude: position.target.latitude,  longitude: position.target.longitude)
    //        let destinationCoordinate = destinationLocation.coordinate
    //        print(destinationCoordinate)
    //    }
    
    /// This method is used to draw route on map.
    func drawRoute()
    {
        //        var color1 = UIColor()
        //        color1 = UIColor(hexString: "#ff9800")!
        polyline.path = path
        polyline.strokeColor = UIColor.red
        polyline.strokeWidth = 3.0
        polyline.map = mapview
    }
    
    /// This method is used to animatePolylinePath on map.
    @objc func animatePolylinePath()
    {
        if i == 0
        {
            self.i += 1
            updateMarker(coordinates: path.coordinate(at: UInt(self.i)), duration: Double(duration))
        }
        else if i > 0
        {
            self.i += 1
            if (UInt(self.i) < self.path.count())
            {
                updateMarker(coordinates: path.coordinate(at: UInt(self.i)), duration: Double(duration))
            }
            else
            {
                timer2.invalidate()
                image = UIImage(named: "play.png")!
                playpause.setImage(nil, for: UIControl.State.normal)
                playpause.setImage(image, for: UIControl.State.normal)
                //                trackTimeLabel.text = "Track Time"
                //                speedLabel.text = "Speed"
            }
        }
        
        if dateTimeArray.count != 0 && i < dateTimeArray.count
        {
            trackTimeLabel.text = dateTimeArray[i-1]
            speedLabel.text = "\(speedArray[i-1]) Kms"
        }
    }
    
    /// This method is used to GetIgnitionHistoryDetails.
    func GetIgnitionHistoryDetails() {
        var lat: Double = 0.0
        var lon: Double = 0.0
        directionArray = []
        speedArray = []
        dateTimeArray = []
        latArray = []
        lonArray = []
        var baseUrl:String = ""
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        let parameters = [
            "VehicleNo" : vehicleNo.encrypt(),
            "ignitionStartDateTime" : ignitionStartDateTime.encrypt(),
            "ignitionOffDateTime" : ignitionOffDateTime.encrypt()
        ]
        print("\(baseUrl)" + "GetIgnitionHistoryDetails")
        print(parameters)
        AF.request("\(baseUrl)" + "GetIgnitionHistoryDetails", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                return
            }
            print(response)
            switch response.result {
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let sts = JSON.object(forKey: "status") as? NSString {
                        self.status = (sts as String).decrypt()
                    }
                    if self.status == "success" {
                        self.dataArray = JSON.object(forKey: "historyData")as! NSArray
                        for i in 0 ..< self.dataArray.count {
                            if let value = ((self.dataArray[i] as AnyObject).object(forKey: "lat") as? String)?.decrypt() {
                                lat = NSString(string: value).doubleValue
                            }
                            if let value = ((self.dataArray[i] as AnyObject).object(forKey: "lon") as? String)?.decrypt() {
                                lon = NSString(string: value).doubleValue
                            }
                            if lat != 0.0 && lon != 0.0
                            {
                                self.latArray.append(lat)
                                self.lonArray.append(lon)
                                if let value = ((self.dataArray[i] as AnyObject).object(forKey: "direction") as? String)?.decrypt() {
                                    self.directionArray.append(NSString(string: value).doubleValue)
                                }
                                if let value = ((self.dataArray[i] as AnyObject).object(forKey: "speed") as? String)?.decrypt() {
                                    self.speedArray.append(NSString(string: value).doubleValue)
                                }
                                if let value: String = ((self.dataArray[i] as AnyObject).object(forKey: "time") as? String)?.decrypt() {
                                    self.dateTimeArray.append(value)
                                }
                            }
                        }
                        self.path.removeAllCoordinates()
                        for index in 0 ..< self.latArray.count {
                            self.path.addLatitude(self.latArray[index], longitude: self.lonArray[index])
                        }
                        DispatchQueue.main.async(execute: {
                            self.vehicleNoLabel.text = self.vehicleNo
                            self.sourcemarker.position = CLLocationCoordinate2DMake(self.latArray[0], self.lonArray[0])
                            self.sourcemarker.icon = UIImage(named: "startmarker")
                            self.sourcemarker.map = self.mapview
                            self.destinationmarker.position = CLLocationCoordinate2DMake(self.latArray.last!, self.lonArray.last!)
                            self.destinationmarker.icon = UIImage(named: "endmarker")
                            self.destinationmarker.map = self.mapview
                            self.marker.position = CLLocationCoordinate2DMake(self.latArray[0], self.lonArray[0])
                            self.marker.icon = UIImage(named: "moving")
                            self.marker.map = self.mapview
                            self.trackTimeLabel.text = self.dateTimeArray[0]
                            self.speedLabel.text = "\(self.speedArray[0]) Kms"
                            self.infoWindowHeight.constant = 150
                            self.infowindow.isHidden = false
                            self.drawRoute()
                            let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: self.latArray[0], longitude: self.lonArray[0], zoom: 15.0)
                            self.mapview.camera = camera
                            let bounds = GMSCoordinateBounds(path: self.path)
                            self.mapview.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 40))
                            self.zoomlevel = self.mapview.camera.zoom
                            self.activity.stopAnimating()
                        })
                    } else if self.status == "failure" {
                        if let msg = JSON.object(forKey: "message") as? NSString {
                            self.message = (msg as String).decrypt()
                        }
                        if self.message == "No records found" {
                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                            if type == Constants.english {
                                self.message = English.No_records_found
                            } else if type == Constants.japanese {
                                self.message = Japanese.No_records_found
                            }
                        }
                        self.showAlert("", message: self.message)
                        DispatchQueue.main.async(execute: {
                            self.activity.stopAnimating()
                        })
                    }
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
}
