import Foundation

protocol HistoryDetailsViewInput: class {
       func configureModule(vehicleNo : String, currentlat : Double, currentlng : Double)
}
