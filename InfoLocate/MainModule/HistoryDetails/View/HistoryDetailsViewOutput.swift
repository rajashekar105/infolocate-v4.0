import Foundation

protocol HistoryDetailsViewOutput {
    func back()
}
