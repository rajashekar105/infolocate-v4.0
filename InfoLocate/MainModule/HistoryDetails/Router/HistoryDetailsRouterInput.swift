import Foundation

protocol HistoryDetailsRouterInput {
    func back()
}
