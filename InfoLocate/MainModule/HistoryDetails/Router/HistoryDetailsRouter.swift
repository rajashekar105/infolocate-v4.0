import UIKit

class HistoryDetailsRouter:HistoryDetailsRouterInput {
    
    weak var viewController: HistoryDetailsViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
