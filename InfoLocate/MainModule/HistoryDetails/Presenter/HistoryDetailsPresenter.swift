import Foundation

class HistoryDetailsPresenter: HistoryDetailsModuleInput, HistoryDetailsViewOutput, HistoryDetailsInteractorOutput {
    
    weak var view: HistoryDetailsViewInput!
    var interactor: HistoryDetailsInteractorInput!
    var router: HistoryDetailsRouterInput!
    
    // MARK: - HistoryDetailsViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - HistoryDetailsModuleInput
    
    func configureModule(vehicleNo : String, currentlat : Double, currentlng : Double) {
        view.configureModule(vehicleNo : vehicleNo, currentlat : currentlat, currentlng : currentlng)
    }
    
    // MARK: - HistoryDetailsInteractorOutput
    
}
