//
//  AppDelegate.swift
//  InfoLocate
//
//  Created by Infotrack on 19/03/20.
//  Copyright © 2020 Hemalatha T. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Firebase
import UserNotifications
//import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var locationManager = CLLocationManager()

    struct Push: Decodable {
        let aps: APS
        
        struct APS: Decodable {
            let alert: Alert
            
            struct Alert: Decodable {
                let title: String
                let body: String
            }
        }
        
        init(decoding userInfo: [AnyHashable : Any]) throws {
            let data = try JSONSerialization.data(withJSONObject: userInfo, options: .prettyPrinted)
            self = try JSONDecoder().decode(Push.self, from: data)
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.registerForPushNotifications()
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        /// Register for push notifications
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        

        
        GMSServices.provideAPIKey("AIzaSyAL6y7JuEoZ3_eQ7ssYPBCN1K0rQPeau3k")
        
        
        let remoteNotif = launchOptions?[.remoteNotification] as? [AnyHashable : Any]
        if remoteNotif != nil {
            let rootRouter = RootRouter.getRouter()
            rootRouter.window = self.window
            let launchRouter = SplashRouter()
            launchRouter.rootRouter = rootRouter
            launchRouter.presentDashboardFromWindow(window: self.window!)
        } else {
            let rootRouter = RootRouter.getRouter()
            rootRouter.window = self.window
            let launchRouter = SplashRouter()
            launchRouter.rootRouter = rootRouter
            launchRouter.presentSplashFromWindow(window: self.window!)
        }
        
        
        return true
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(fcmToken)")
      UserDefaults.standard.set(fcmToken, forKey: "FCMToken")

      let dataDict:[String: String] = ["token": fcmToken]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
        
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
            })
        }
    }
        
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            let tokenParts = deviceToken.map { data -> String in
                return String(format: "%02.2hhx", data)
            }
            
            let token = tokenParts.joined()
            print("Device Token: \(token)")
            Messaging.messaging().apnsToken = deviceToken
    }
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        self.window?.endEditing(true)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
             let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                       
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                       }
            
            alertController.addAction(okAction)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //      if let messageID = userInfo[gcmMessageIDKey] {
        //        print("Message ID: \(messageID)")
        //      }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //      if let messageID = userInfo[gcmMessageIDKey] {
        //        print("Message ID: \(messageID)")
        //      }
        
        // Print full message.
        print(userInfo)
        
//        handleNotification(userInfo:userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    private func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Device Token : \(deviceToken)")
    }


    func handleNotification(userInfo: Any) {
        guard let push = try? Push(decoding: userInfo as! [AnyHashable : Any]) else { return }
        let message : String = push.aps.alert.body
        print(userInfo)
//        let str = userInfo as? String ?? ""
        let value = message.split(separator: "|")
        let alertId = value[1]
//        alertId.removeFirst()
        print(alertId)
        UserDefaults.standard.set("", forKey: "AlertNavigation")
        UserDefaults.standard.set(alertId, forKey: "AlertID")
        UserDefaults.standard.set("1", forKey: "AlertNavigation")
        
        let rootRouter = RootRouter.getRouter()
        rootRouter.window = self.window
        let launchRouter = SplashRouter()
        launchRouter.rootRouter = rootRouter
        launchRouter.presentDashboardFromWindow(window: self.window!)
    }
    
    func handleBodyNotification(message: String) {

            let value = message.split(separator: "|")
            let alertId = value[1]
            print(alertId)
            UserDefaults.standard.set("", forKey: "AlertNavigation")
            UserDefaults.standard.set(alertId, forKey: "AlertID")
            UserDefaults.standard.set("1", forKey: "AlertNavigation")
            
            let rootRouter = RootRouter.getRouter()
            rootRouter.window = self.window
            let launchRouter = SplashRouter()
            launchRouter.rootRouter = rootRouter
            launchRouter.presentDashboardFromWindow(window: self.window!)

        }
    
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)

    // Print message ID.
    //    if let messageID = userInfo[gcmMessageIDKey] {
    //      print("Message ID: \(messageID)")
    //    }

    // Print full message.
    print(userInfo)

    // Change this to your preferred presentation option
    completionHandler([[.alert, .sound]])
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    let message = response.notification.request.content.body
    // Print message ID.
    //    if let messageID = userInfo[gcmMessageIDKey] {
    //      print("Message ID: \(messageID)")
    //    }

    // Print full message.
    print(message)
    handleBodyNotification(message: message)

    completionHandler()
  }
}


extension UIApplication {
    class var topViewController: UIViewController? { return getTopViewController() }
    private class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController { return getTopViewController(base: nav.visibleViewController) }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController { return getTopViewController(base: selected) }
        }
        if let presented = base?.presentedViewController { return getTopViewController(base: presented) }
        return base
    }

    private class func _share(_ data: [Any],
                              applicationActivities: [UIActivity]?,
                              setupViewControllerCompletion: ((UIActivityViewController) -> Void)?) {
        let activityViewController = UIActivityViewController(activityItems: data, applicationActivities: nil)
        setupViewControllerCompletion?(activityViewController)
        UIApplication.topViewController?.present(activityViewController, animated: true, completion: nil)
    }

    class func share(_ data: Any...,
                     applicationActivities: [UIActivity]? = nil,
                     setupViewControllerCompletion: ((UIActivityViewController) -> Void)? = nil) {
        _share(data, applicationActivities: applicationActivities, setupViewControllerCompletion: setupViewControllerCompletion)
    }
    class func share(_ data: [Any],
                     applicationActivities: [UIActivity]? = nil,
                     setupViewControllerCompletion: ((UIActivityViewController) -> Void)? = nil) {
        _share(data, applicationActivities: applicationActivities, setupViewControllerCompletion: setupViewControllerCompletion)
    }
}


