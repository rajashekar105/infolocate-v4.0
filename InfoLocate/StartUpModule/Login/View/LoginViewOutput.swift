import Foundation

protocol LoginViewOutput {
    func back()
    func navigateToClientLoginViewComntroller()
    func navigateToDashboardViewController()
}
