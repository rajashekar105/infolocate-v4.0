import UIKit
import Alamofire

class LoginViewController: BaseViewController, LoginViewInput {
    
    /// This property knows LoginView output.
    var output: LoginViewOutput!
    /// This property is defaults.
//    let defaults = UserDefaults.standard
    /// This property is groupDefaults.
    let groupDefaults = UserDefaults.standard
    /// This property is Validate.
    let Validate = Validation()
    /// This property is loginStatus.
    var loginStatus: String = ""
    /// This property is loginMessage.
    var loginMessage: String = ""
    /// This property is loginID.
    var loginID: String = ""
    /// This property is groupEnable.
    var groupEnable : Int = 0
    /// This property is userName textfield.
    @IBOutlet weak var userName: UITextField!
    /// This property is password textfield.
    @IBOutlet weak var password: UITextField!
    /// This property is loginButton.
    @IBOutlet weak var loginButton: UIButton!
    /// This property is activity.
    @IBOutlet weak var activity: UIActivityIndicatorView!
    /// This property is switchClientButton.
    @IBOutlet weak var switchClientButton: UIButton!
    /// This property is poweredByLabel.
    @IBOutlet weak var poweredByLabel: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activity.layer.cornerRadius = 10
        loginButton.layer.cornerRadius = 5
        userName.placeholder = NSLocalizedString("User Name", comment: "")
        password.placeholder = NSLocalizedString("Password", comment: "")
        loginButton.setTitle(NSLocalizedString("Login", comment: ""), for: .normal)
        switchClientButton.setTitle(NSLocalizedString("Switch Client?", comment: ""), for: .normal)
//        switchClientButton.setTitleColor(.white, for: .normal)
        poweredByLabel.text = NSLocalizedString("Powered by Infotrack", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            userName.placeholder = English.User_Name
            password.placeholder = English.Password
            loginButton.setTitle(English.Login, for: .normal)
            switchClientButton.setTitle(English.Switch_Client, for: .normal)
            poweredByLabel.text = English.Powered_by_Infotrack_Telematics_Pvt_Ltd_Copyright
        } else if type == Constants.japanese {
            userName.placeholder = Japanese.User_Name
            password.placeholder = Japanese.Password
            loginButton.setTitle(Japanese.Login, for: .normal)
            switchClientButton.setTitle(Japanese.Switch_Client, for: .normal)
            poweredByLabel.text = Japanese.Powered_by_Infotrack_Telematics_Pvt_Ltd_Copyright
        }
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }

        self.userName.delegate = self
        self.password.delegate = self
        
        userName.leftViewMode = UITextField.ViewMode.always
        let userNameImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let userNameImage = UIImage(named: "NameTextField")
        userNameImageView.image = userNameImage
        userName.leftView = userNameImageView
        
        password.leftViewMode = UITextField.ViewMode.always
        let passwordImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let passwordImage = UIImage(named: "PasswordTextField")
        passwordImageView.image = passwordImage
        password.leftView = passwordImageView
        
//        let tapforkeyboard: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
//        view.addGestureRecognizer(tapforkeyboard)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is switchClient button action to navigate to client screen.
    @IBAction func switchClient(_ sender: Any) {
           
        var title = NSLocalizedString("Are you sure want to switch client?", comment: "")
        var cancelTitle = NSLocalizedString("CANCEL", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            title = English.Are_you_sure_want_to_switch_client
            cancelTitle = English.CANCEL
        } else if type == Constants.japanese {
            title = Japanese.Are_you_sure_want_to_switch_client
            cancelTitle = Japanese.CANCEL
        }
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.defaults.set("false", forKey: "clientLogin")
            self.defaults.set("", forKey: "BaseURL")
            self.defaults.set("", forKey: "clientId")
            self.defaults.set("", forKey: "password")
            self.output.navigateToClientLoginViewComntroller()
        }
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /// This method is login button action.
    @IBAction func login(_ sender: AnyObject) {
        
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            
            let name = Validate.checkEmpty(userName)
            let pass = Validate.checkEmpty(password)
            
            if name == false && pass == false {
                var message = NSLocalizedString("Enter User Name and Password", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Enter_User_Name_and_Password
                } else if type == Constants.japanese {
                    message = Japanese.Enter_User_Name_and_Password
                }
                showAlert("", message: message)
            }
            else if name == false {
                var message = NSLocalizedString("Enter User Name", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Enter_User_Name
                } else if type == Constants.japanese {
                    message = Japanese.Enter_User_Name
                }
                showAlert("", message: message)
            }
            else if pass == false {
                var message = NSLocalizedString("Enter Password", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Enter_Password
                } else if type == Constants.japanese {
                    message = Japanese.Enter_Password
                }
                showAlert("", message: message)
            }
            else
            {
                print(self.defaults.object(forKey: "fcmenabled") as Any)
                print(self.defaults.object(forKey: "passwordvalidation") as Any)
                let fcmenabled = self.defaults.object(forKey: "fcmenabled") as! Int
                let passwordvalidation = self.defaults.object(forKey: "passwordvalidation") as! Int
                print(fcmenabled)
                print(passwordvalidation)
                if passwordvalidation == 0 {
                    login()
                } else if passwordvalidation == 1 {
                    passwordValidate()
                }
            }
        }
    }
    
    /// This method is used for passwordValidate.
    func passwordValidate() {
        let pass_min = Validate.checkMinLength(password, iLength: 10)
        let pass_max = Validate.checkMaxLength(password, iLength: 15)
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        
        if pass_min == true && pass_max == true {
            let boolValue = Validate.validpassword(mypassword: password.text!)
            print(boolValue)
            if boolValue == true {
                print("Validation Successfull")
                login()
            } else {
                var message = "Password must contain atleast One Upper Case character, One Lower Case character and One Special Character"
                if type == Constants.english {
                    message = English.Password_must_contain_atleast_One_Upper_Case_character_One_Lower_Case_character_and_One_Special_Character
                } else if type == Constants.japanese {
                    message = Japanese.Password_must_contain_atleast_One_Upper_Case_character_One_Lower_Case_character_and_One_Special_Character
                }
               showAlert("", message: message)
            }
        } else {
            var message = "Password must be between 10-15 characters"
            if type == Constants.english {
                message = English.Password_must_be_between_10_15_characters
            } else if type == Constants.japanese {
                message = Japanese.Password_must_be_between_10_15_characters
            }
            showAlert("", message: message)
        }
    }

    /// This method called for isValidPassword.
    public func isValidPassword() -> Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,10}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
     /// This method is used to showAlert.
//    func showAlert(_ title: String, message:String) {
//
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
//            UIAlertAction in
//
//            self.view.isUserInteractionEnabled = true
//            self.view.endEditing(true)
//        }
//        alertController.addAction(okAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == userName {
            if self.view.frame.origin.y == 0 {
               animateViewMoving(true, moveValue: 170)
            }
        }
        if textField == password {
            if self.view.frame.origin.y == 0 {
               animateViewMoving(true, moveValue: 170)
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == userName {
            if self.view.frame.origin.y < 0 {
                animateViewMoving(false, moveValue: 170)
            }
        }
        if textField == password {
            if self.view.frame.origin.y < 0 {
                animateViewMoving(false, moveValue: 170)
            }
        }
    }
    
    var limitLength = 0
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == userName {
            limitLength = 50
        } else if textField == password {
            limitLength = 50
        }
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= limitLength
    }
   
    
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
    
    /// This method is used for userlogin api.
    func login() {
        activity.startAnimating()
        view.isUserInteractionEnabled = false
        var ctype:String = ""
        var baseUrl:String = ""
        
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        
        let user: String = userName.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        let parameters = [
            "username" : user.encrypt(),
            "password" : password.text!.encrypt()
        ]
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        
        print("\(baseUrl)"+"Login")
        print(parameters)
        AF.request("\(baseUrl)"+"Login", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                return
            }
            
            print(response.request ?? "")
            print(response)
            
            switch response.result {
                
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let status = JSON.object(forKey: "status") as? NSString {
                        self.loginStatus = (status as String).decrypt()
                    }
                    if self.loginStatus == "success" {
                        if let ID = JSON.object(forKey: "userid") as? NSString {
                            self.loginID = (ID as String).decrypt()
                        }
                        if let type = JSON.object(forKey: "ctype") as? NSString {
                            ctype = (type as String).decrypt()
                        }
                        if let groupEnabled = JSON.object(forKey: "isgroupenable") as? Int {
                            self.groupEnable = groupEnabled as Int
                        }
                        print(self.loginID)
                        print(ctype)
                        self.defaults.set(ctype, forKey: "ctype")
                        self.defaults.set("true", forKey: "userLogin")
                        self.defaults.setValue(self.loginID, forKey: "LoginID")
                        self.defaults.setValue(user, forKey: "username")
                        self.defaults.setValue(self.password.text!, forKey: "userpassword")
                        self.groupDefaults.setValue(self.groupEnable, forKey: "groupEnable")
                        
                        print(self.defaults.object(forKey: "fcmenabled") as Any)
                        print(self.defaults.object(forKey: "passwordvalidation") as Any)
                        
                        let fcmenabled = self.defaults.object(forKey: "fcmenabled") as Any as! Int
                        if fcmenabled == 0 {
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            self.output.navigateToDashboardViewController()
                        } else if fcmenabled == 1 {
                            self.updateTokenToServer()
                        }
                    } else
                        if self.loginStatus == "failure" {
                            if let message = JSON.object(forKey: "message") as? NSString {
                                self.loginMessage = (message as String).decrypt()
                            }
                            if type == Constants.english {
                                self.loginMessage = English.Please_enter_Valid_username_and_password
                            } else if type == Constants.japanese {
                                self.loginMessage = Japanese.Please_enter_Valid_username_and_password
                            }
                            self.showAlert("" , message: self.loginMessage)
                            DispatchQueue.main.async(execute: {
                                self.activity.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                            })
                    }
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    /// This method is used to updateTokenToServer with api.
    func updateTokenToServer() {
        
        var fcmToken: String = ""
        var baseUrl:String = ""
        
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        
        let user: String = userName.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
//        UserDefaults.standard.set(fcmToken, forKey: "FCMToken")
        if let token = defaults.object(forKey: "FCMToken") {
            fcmToken = token as! String
        }
        
        if fcmToken.isEmpty {
//            self.activity.stopAnimating()
//            self.showAlert("", message: "FCM Token is Empty")
        } else {
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            let parameters = [
                "userid" : self.loginID.encrypt(),
                "deviceToken" : fcmToken.encrypt(),
                "deviceType" : "18".encrypt(),
                "appVersion" : "3.0".encrypt(),
                "appSubVersion" : appVersion.encrypt()
            ]
            
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            
            print("\(baseUrl)"+"UpdateDeviceTokenV2")
//            print("http://5.195.195.181/FCMAPI/ItlService.svc/UpdateDeviceTokenV2")
            print(parameters)
            AF.request("\(baseUrl)"+"UpdateDeviceTokenV2", method: .post, parameters: parameters).responseJSON { response in
                guard response.error == nil else {
                    // got an error in getting the data, need to handle it
                    print(response.error!)
                    
                    var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                    
                    if type == Constants.english {
                        message = English.Something_went_wrong_Please_try_again_later
                    } else if type == Constants.japanese {
                        message = Japanese.Something_went_wrong_Please_try_again_later
                    }
                    self.showAlert("", message: message)
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    return
                }
                
                print(response.request ?? "")
                print(response)
                
                switch response.result {
                    
                case .success:
                    if let result = response.value {
                        let JSON = result as! NSDictionary
                        if let status = JSON.object(forKey: "status") as? NSString {
                            self.loginStatus = (status as String).decrypt()
                        }
                        if self.loginStatus == "success" {
                            
                            self.defaults.set("true", forKey: "userLogin")
                            self.defaults.setValue(self.loginID, forKey: "LoginID")
                            self.defaults.setValue(user, forKey: "username")
                            self.groupDefaults.setValue(self.groupEnable, forKey: "groupEnable")
                            
                            self.activity.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            self.output.navigateToDashboardViewController()
                        } else
                            if self.loginStatus == "failure" {
                                if let message = JSON.object(forKey: "message") as? NSString {
                                    self.loginMessage = (message as String).decrypt()
                                }
                                self.loginMessage = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                                 if type == Constants.english {
                                     self.loginMessage = English.Something_went_wrong_Please_try_again_later
                                 } else if type == Constants.japanese {
                                     self.loginMessage = Japanese.Something_went_wrong_Please_try_again_later
                                 }
                                 self.showAlert("", message: self.loginMessage)
                                //self.showAlert("" , message: self.loginMessage)
                                DispatchQueue.main.async(execute: {
                                    self.activity.stopAnimating()
                                    self.view.isUserInteractionEnabled = true
                                })
                        }
                    }
                    break
                    
                case .failure(let error):
                    print(error)
                    break
                }
            }
        }
    }
}
