import UIKit

@objc class LoginModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = LoginControllerFromStoryboard()
        
        let router = LoginRouter()
        router.viewController = viewController
        
        let presenter = LoginPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = LoginInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
                
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func LoginControllerFromStoryboard() -> LoginViewController {
        let storyboard = StoryBoard.startUpStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.LOGIN_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! LoginViewController
    }
    
}
