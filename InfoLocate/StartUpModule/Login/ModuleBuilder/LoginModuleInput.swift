import Foundation

protocol LoginModuleInput: class {
    
    func configureModule()
}
