import Foundation

class LoginPresenter: LoginModuleInput, LoginViewOutput, LoginInteractorOutput {
    
    weak var view: LoginViewInput!
    var interactor: LoginInteractorInput!
    var router: LoginRouterInput!
    
    // MARK: - LoginViewOutput
    
    func back() {
        router.back()
    }
    func navigateToClientLoginViewComntroller() {
        router.navigateToClientLoginViewComntroller()
    }
    func navigateToDashboardViewController() {
        router.navigateToDashboardViewController()
    }
    
    // MARK: - LoginModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - LoginInteractorOutput
    
}
