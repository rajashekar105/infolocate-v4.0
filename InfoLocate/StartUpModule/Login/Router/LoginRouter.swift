import UIKit

class LoginRouter:LoginRouterInput {
    
    weak var viewController: LoginViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToClientLoginViewComntroller() {
        Navigation.navigateToClientLoginScreen(viewController: viewController)
    }
    
    func navigateToDashboardViewController() {
        Navigation.navigateToDashboardScreen(viewController: viewController)
    }
    
}
