import Foundation

protocol LoginRouterInput {
    func back()
    func navigateToClientLoginViewComntroller()
    func navigateToDashboardViewController()
}
