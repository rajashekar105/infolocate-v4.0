import Foundation

class ClientLoginPresenter: ClientLoginModuleInput, ClientLoginViewOutput, ClientLoginInteractorOutput {
    
    weak var view: ClientLoginViewInput!
    var interactor: ClientLoginInteractorInput!
    var router: ClientLoginRouterInput!
    
    // MARK: - ClientLoginViewOutput
    
    func back() {
        router.back()
    }
    func navigateToLoginViewController() {
        router.navigateToLoginViewController()
    }
    func navigateToDashboardViewController() {
        router.navigateToDashboardViewController()
    }
    
    // MARK: - ClientLoginModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - ClientLoginInteractorOutput
    
}
