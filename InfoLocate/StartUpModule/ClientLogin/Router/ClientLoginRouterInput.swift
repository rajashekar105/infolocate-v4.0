import Foundation

protocol ClientLoginRouterInput {
    func back()
    func navigateToLoginViewController()
    func navigateToDashboardViewController()
}
