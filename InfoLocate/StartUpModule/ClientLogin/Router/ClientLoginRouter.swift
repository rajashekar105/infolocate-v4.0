import UIKit

class ClientLoginRouter:ClientLoginRouterInput {
    
    weak var viewController: ClientLoginViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigateToLoginViewController() {
        Navigation.navigateToLoginScreen(viewController: viewController)
    }
    
    func navigateToDashboardViewController() {
        
    }
}
