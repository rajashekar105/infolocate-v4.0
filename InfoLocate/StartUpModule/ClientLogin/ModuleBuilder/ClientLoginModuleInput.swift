import Foundation

protocol ClientLoginModuleInput: class {
    
    func configureModule()
}
