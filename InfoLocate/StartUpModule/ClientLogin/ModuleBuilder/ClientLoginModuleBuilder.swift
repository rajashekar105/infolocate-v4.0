import UIKit

@objc class ClientLoginModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = ClientLoginControllerFromStoryboard()
        
        let router = ClientLoginRouter()
        router.viewController = viewController
        
        let presenter = ClientLoginPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = ClientLoginInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func ClientLoginControllerFromStoryboard() -> ClientLoginViewController {
        let storyboard = StoryBoard.startUpStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.CLIENT_LOGIN_VIEW_CONTROLLER_IDENTIFIER)
        return viewController as! ClientLoginViewController
    }
    
}
