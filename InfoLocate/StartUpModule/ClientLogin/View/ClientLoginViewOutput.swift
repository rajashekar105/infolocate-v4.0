import Foundation

protocol ClientLoginViewOutput {
    func back()
    func navigateToLoginViewController()
    func navigateToDashboardViewController()
}
