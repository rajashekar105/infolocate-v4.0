import UIKit
import Alamofire
class ClientLoginViewController: BaseViewController, ClientLoginViewInput {
    
    var output: ClientLoginViewOutput!
    
//    let defaults = UserDefaults.standard
    /// This property is Validate.
    let Validate = Validation()
    /// This property is loginStatus.
    var loginStatus: String = ""
    /// This property is loginMessage.
    var loginMessage: String = ""
    /// This property is activityIndicator.
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    /// This property is clientId.
    @IBOutlet var clientId: UITextField!
    /// This property is password.
    @IBOutlet var password: UITextField!
    /// This property is loginButton.
    @IBOutlet var loginButton: UIButton!
    /// This property is poweredByLabel.
    @IBOutlet weak var poweredByLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var clientLoginStatus = "false"
        clientId.placeholder = NSLocalizedString("Client Name", comment: "")
        password.placeholder = NSLocalizedString("Password", comment: "")
        loginButton.setTitle(NSLocalizedString("Login", comment: ""), for: .normal)
        poweredByLabel.text = NSLocalizedString("Powered by Infotrack", comment: "")
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            clientId.placeholder = English.Client_Name
            password.placeholder = English.Password
            loginButton.setTitle(English.Login, for: .normal)
            poweredByLabel.text = English.Powered_by_Infotrack_Telematics_Pvt_Ltd_Copyright
        } else if type == Constants.japanese {
             clientId.placeholder = Japanese.Client_Name
            password.placeholder = Japanese.Password
            loginButton.setTitle(Japanese.Login, for: .normal)
            poweredByLabel.text = Japanese.Powered_by_Infotrack_Telematics_Pvt_Ltd_Copyright
        }
        if let status = defaults.object(forKey: "clientLogin") {
            clientLoginStatus = status as! String
        }
        
//        if clientLoginStatus == "true" {
//            if Reachability.connectedToNetwork() == false
//            {
//                var title = NSLocalizedString("No Internet Connection", comment: "")
//                var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
//                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//                if type == Constants.english {
//                    title = English.No_Internet_Connection
//                    message = English.Make_sure_your_device_is_connected_to_the_internet
//                } else if type == Constants.japanese {
//                    title = Japanese.No_Internet_Connection
//                    message = Japanese.Make_sure_your_device_is_connected_to_the_internet
//                }
//                let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
//
//                let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
//                }
//
//                alertController.addAction(okAction)
//
//                self.present(alertController, animated: true, completion: nil)
//            } else {
//                getBaseURLAutomatically()
//            }
//
//
//        }
        
        activityIndicator.layer.cornerRadius = 10
        loginButton.layer.cornerRadius = 5
        clientId.delegate = self
        password.delegate = self
        
        clientId.leftViewMode = UITextField.ViewMode.always
        let clientIdImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let clientIdImage = UIImage(named: "NameTextField")
        clientIdImageView.image = clientIdImage
        clientId.leftView = clientIdImageView
        
        password.leftViewMode = UITextField.ViewMode.always
        let passwordImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let passwordImage = UIImage(named: "PasswordTextField")
        passwordImageView.image = passwordImage
        password.leftView = passwordImageView
        
        //        let tapforkeyboard: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        //        view.addGestureRecognizer(tapforkeyboard)
    }
    
    /// This method is used to dismiss the keyboard.
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This method is loginButton action.
    @IBAction func loginButton(_ sender: Any) {
        
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            
            let name = Validate.checkEmpty(clientId)
            let pass = Validate.checkEmpty(password)
            
            if name == false && pass == false {
                var message = NSLocalizedString("Enter Client Name and Password", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Enter_Client_Name_and_Password
                } else if type == Constants.japanese {
                    message = Japanese.Enter_Client_Name_and_Password
                }
                showAlert("", message: message)
            }
            else if name == false {
                var message = NSLocalizedString("Enter Client Name", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Enter_Client_Name
                } else if type == Constants.japanese {
                    message = Japanese.Enter_Client_Name
                }
                showAlert("", message: message)
            }
            else if pass == false {
                var message = NSLocalizedString("Enter Password", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Enter_Password
                } else if type == Constants.japanese {
                    message = Japanese.Enter_Password
                }
                showAlert("", message: message)
            }
            else
            {
                getBaseURL()
                activityIndicator.startAnimating()
                self.view.isUserInteractionEnabled = false
            }
        }
    }
    
    /// This method is used to showAlert.
//    func showAlert(_ title: String, message:String) {
//
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) {
//            UIAlertAction in
//
//            self.view.isUserInteractionEnabled = true
//            self.view.endEditing(true)
//        }
//        alertController.addAction(okAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == clientId {
            if self.view.frame.origin.y == 0 {
               animateViewMoving(true, moveValue: 170)
            }
        }
        if textField == password {
            if self.view.frame.origin.y == 0 {
               animateViewMoving(true, moveValue: 170)
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == clientId {
            if self.view.frame.origin.y < 0 {
                animateViewMoving(false, moveValue: 170)
            }
        }
        if textField == password {
            if self.view.frame.origin.y < 0 {
                animateViewMoving(false, moveValue: 170)
            }
        }
    }
    
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.1
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
    
    var limitLength = 0
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//                if string.count > 1 {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { [weak self] in
//                   self?.animateViewMoving(true, moveValue: 170)
//                    }
//                }
            
        if textField == clientId {
            limitLength = 50
        } else if textField == password {
            limitLength = 50
        }
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= limitLength
        
    }

    /// This method is used to getBaseURL from client login api.
    func getBaseURL() {
        
        let client: String = clientId.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        let parameters = [
            "ClientName" : client.encrypt(),
            "Password": password.text!.encrypt()
        ]
        print(parameters)
        
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        
        AF.request(ServerConfiguration.CLIENT_LOGIN_API, method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
                return
            }
            
            print(response.request ?? "")
            print(response)
            
            switch response.result {
                
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    let status: String = JSON.object(forKey: "status") as! String
                    self.loginStatus = (status as String).decrypt()
                    
                    if self.loginStatus == "success" {
                        let mdvrIpAddress: String = (JSON.object(forKey: "mdvripaddress") as? String)!.decrypt()
                        if mdvrIpAddress != "NA" {
                            self.defaults.set(mdvrIpAddress, forKey: "mdvripaddress")
                        } else {
                            self.defaults.set("", forKey: "mdvripaddress")
                        }
                        print(self.defaults.value(forKey: "mdvripaddress") as Any)
                        let mdvrPort: String = (JSON.object(forKey: "mdvrport") as? String)!.decrypt()
                        if mdvrPort != "0" {
                            self.defaults.set(mdvrPort, forKey: "mdvrport")
                        } else {
                            self.defaults.set("", forKey: "mdvrport")
                        }
                        print(self.defaults.value(forKey: "mdvrport") as Any)
                        let languageid: String = (JSON.object(forKey: "languageid") as? String)!.decrypt()
                        Defaults.setValueforKey(value: languageid, for: Constants.language_Key)
                        let fcmenabled = Int((JSON.object(forKey: "fcmenabled") as? String)!.decrypt())
                        self.defaults.set(fcmenabled, forKey: "fcmenabled")
                        let passwordvalidation = Int((JSON.object(forKey: "passwordvalidation") as? String)!.decrypt())
                        self.defaults.set(passwordvalidation, forKey: "passwordvalidation")
                        let mdvrenabled = Int((JSON.object(forKey: "mdvrenabled") as? String)!.decrypt())
                        self.defaults.set(mdvrenabled, forKey: "mdvrenabled")
//                        let encenabled = Int((JSON.object(forKey: "encenabled") as? String)!)
//                        self.defaults.set(encenabled, forKey: "encenabled")
                        
//                        print(self.defaults.value(forKey: "encenabled") as Any)
                        print(self.defaults.value(forKey: "mdvrenabled") as Any)
                        print(self.defaults.value(forKey: "fcmenabled") as Any)
                        print(self.defaults.value(forKey: "passwordvalidation") as Any)
                        if let url = JSON.object(forKey: "url") as? String {
                            let baseURL = (url as String).decrypt()
                            print("baseURL = \(baseURL)")
//                            if fcmenabled == 0 {
//                                self.defaults.set(baseURL, forKey: "BaseURL")
//                            } else if fcmenabled == 1 {
//                                self.defaults.set("http://5.195.195.181/FCMAPI/ItlService.svc/", forKey: "BaseURL")
//                                self.defaults.set("http://staging.infotracktelematics.com/InfoLocateAPIStaging/Itlservice.svc/", forKey: "BaseURL")
//                            }
//                            self.defaults.set("https://staging.infotracktelematics.com/InfoLocateWebApi/InfoLocateApi/", forKey: "BaseURL")
                            // "https://staging.infotracktelematics.com/InfoLocateWebApi/InfoLocateApi/"
                            
                            self.defaults.set(baseURL, forKey: "BaseURL")
                           
//                            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
//                            if type == Constants.english {
//                                self.defaults.set(baseURL, forKey: "BaseURL")
//                            } else if type == Constants.japanese {
//                                self.defaults.set("http://earthsupportpoc.infotracktelematics.com/ilouserapitest/ApiServices/", forKey: "BaseURL")
//                            }
//                            self.defaults.set("http://earthsupportpoc.infotracktelematics.com/ilouserapitest/ApiServices/", forKey: "BaseURL")
//                            print("http://earthsupportpoc.infotracktelematics.com/ilouserapitest/ApiServices/RequestAlertVideo")
//                            baseUrl = "http://earthsupportpoc.infotracktelematics.com/ilouserapitest/ApiServices/"
                            
                            self.defaults.set("true", forKey: "clientLogin")
                            self.defaults.set(client, forKey: "clientId")
                            self.defaults.set(self.password.text, forKey: "password")
                            self.activityIndicator.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            self.output.navigateToLoginViewController()
                        }
                    } else if self.loginStatus == "failure" {
                        if let message = JSON.object(forKey: "message") as? NSString {
                            self.loginMessage = (message as String).decrypt()
                        }
                        print(self.loginMessage)
                        if type == Constants.english {
                            self.loginMessage = English.Please_enter_valid_clientname_and_password
                        } else if type == Constants.japanese {
                            self.loginMessage = Japanese.Please_enter_valid_clientname_and_password
                        }
                        self.showAlert("" , message: self.loginMessage)
                        DispatchQueue.main.async(execute: {
                            self.activityIndicator.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                        })
                    }
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    /// This method is used to getBaseURLAutomatically from client login api.
    func getBaseURLAutomatically() {
        var clientid: String = ""
        var password: String  = ""
        
        if let status = defaults.object(forKey: "clientId") {
            clientid = status as! String
        }
        
        if let status = defaults.object(forKey: "password") {
            password = status as! String
        }
        
        let parameters = [
            "ClientName" : clientid.encrypt(),
            "Password": password.encrypt()
        ]
        AF.request(ServerConfiguration.CLIENT_LOGIN_API, method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                self.activityIndicator.stopAnimating()
                return
            }
            
            print(response)
            switch response.result {
                
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    let status: String = JSON.object(forKey: "status") as! String
                    self.loginStatus = (status as String).decrypt()
                    
                    if self.loginStatus == "success" {
                        
                        if let url = JSON.object(forKey: "url") as? NSString {
                            let baseURL = (url as String).decrypt()
                            self.defaults.set(baseURL, forKey: "BaseURL")
                            self.userlogin()
                        }
                        
                    } else if self.loginStatus == "failure" {
                        if let message = JSON.object(forKey: "message") as? NSString {
                            self.loginMessage = (message as String).decrypt()
                        }
                    }
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    /// This method is used to navigate to userlogin screen.
    func userlogin() {
        
        var userLoginStatus = "false"
        
        if let status = defaults.object(forKey: "userLogin") {
            userLoginStatus = status as! String
        }
        
        if userLoginStatus == "true" {
            self.output.navigateToDashboardViewController()
        } else {
            self.output.navigateToLoginViewController()
        }
    }
    
    
}

