import Foundation

protocol SplashInteractorOutput: class {
    func evaluateAuthenticationPolicyMessageForLA(message: String)
}
