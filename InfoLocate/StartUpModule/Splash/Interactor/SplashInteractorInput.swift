import Foundation

protocol SplashInteractorInput {
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int)
}
