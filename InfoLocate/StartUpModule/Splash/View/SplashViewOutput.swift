import Foundation

protocol SplashViewOutput {
    func back()
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int)
    func navigateToLoginScreen()
    func navigateToDashboardScreen()
    func navigateToAlertScreen()
}
