import Foundation

protocol SplashViewInput: class {
    func evaluateAuthenticationPolicyMessageForLA(message: String)
}
