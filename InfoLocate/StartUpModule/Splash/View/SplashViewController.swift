import UIKit
import LocalAuthentication
import Alamofire
import Photos

class SplashViewController: BaseViewController, SplashViewInput {

    var output: SplashViewOutput!
    /// This property is loginStatus.
    var loginStatus: String = ""
    /// This property is loginMessage.
    var loginMessage: String = ""
    /// This property is gameTimer.
    var gameTimer: Timer?
    
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let status = Defaults.getValueForLogin()
//        if status == true {
//            self.output.navigateToDashboardScreen()
//        } else {
//            gotoLoginScreen(time: 1.5)
//        }
//        Defaults.setValueforKey(value: Constants.english, for: Constants.language_Key)
        PHPhotoLibrary.requestAuthorization( { status in
            switch(status) {
            case .authorized :
                print("authorized")
                break
            case .denied :
                print("denied")
//                if let bundleIdentifier = Bundle.main.bundleIdentifier, let appSettings = URL(string: UIApplication.openSettingsURLString + bundleIdentifier) {
//                    if UIApplication.shared.canOpenURL(appSettings) {
//                        UIApplication.shared.open(appSettings)
//                    }
//                }
                break
            case .notDetermined:
                break
            case .restricted:
                break
            case .limited:
                break
            @unknown default:
                break
            }
        })

        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            versionLabel.text = "\(English.Version): \(appVersion)"
        } else if type == Constants.japanese {
            versionLabel.text = "\(Japanese.Version): \(appVersion)"
        } else {
             versionLabel.text = "\(English.Version): \(appVersion)"
        }
        
        if Reachability.connectedToNetwork() == false
        {
            var title = NSLocalizedString("No Internet Connection", comment: "")
            var message = NSLocalizedString("Make sure your device is connected to the internet.", comment: "")
            let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
            if type == Constants.english {
                title = English.No_Internet_Connection
                message = English.Make_sure_your_device_is_connected_to_the_internet
            } else if type == Constants.japanese {
                title = Japanese.No_Internet_Connection
                message = Japanese.Make_sure_your_device_is_connected_to_the_internet
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle:UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            //        http://staging.infotracktelematics.com/InfoLocateClientAuthWebApi/api/service/CheckAppUpdate
            
//            checkForUpdateAPI()
            processedFurther()
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /// This method is called if there is no app update version.
    func processedFurther() {
        var userLoginStatus = "false"
                    
        if let status = defaults.object(forKey: "userLogin") {
            userLoginStatus = status as! String
        }
        
        if userLoginStatus == "true" {
            checkForPasswordChangeLogin()
        } else {
            gotoLoginScreen(time: 1.5)
        }
    }
    /// This method is called to navigate to login screen.
    func gotoLoginScreen(time: Double){
        DispatchQueue.main.asyncAfter(deadline: .now() + Double(time)) {
            self.output.navigateToLoginScreen()
        }
        return
    }
    
    func checkForPasswordChangeLogin() {
//        activity.startAnimating()
        view.isUserInteractionEnabled = false
        var baseUrl:String = ""
        
        if let url = defaults.object(forKey: "BaseURL") {
            baseUrl = url as! String
        }
        
        let user: String = defaults.object(forKey: "username") as! String
        let password: String = defaults.object(forKey: "userpassword") as! String
        
        let parameters = [
            "username" : user.encrypt(),
            "password" : password.encrypt()
        ]
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        
        print("\(baseUrl)"+"Login")
        print(parameters)
        AF.request("\(baseUrl)"+"Login", method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
//                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                return
            }
            
            print(response.request ?? "")
            print(response)
            
            switch response.result {
                
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    if let status = JSON.object(forKey: "status") as? NSString {
                        self.loginStatus = (status as String).decrypt()
                    }
                    if self.loginStatus == "success" {
                        self.output.navigateToDashboardScreen()
                    } else if self.loginStatus == "failure" {
                        self.gotoLoginScreen(time: 1.5)
                    }
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
   
    /// Method is used to check for update with api.
    func checkForUpdateAPI() {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let appSubVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
    
        let parameters = [
            "appName" : "V3".encrypt(),
            "appVersion" : appVersion.encrypt(),
            "appVersionCode" : appSubVersion.encrypt(),
            "appCategory" : "IOS".encrypt()
        ]
        print(appVersion)
        print(appSubVersion)
        
        AF.request(ServerConfiguration.CHECK_APP_UPDATE, method: .post, parameters: parameters).responseJSON { response in
            guard response.error == nil else {
                // got an error in getting the data, need to handle it
                print(response.error!)
                var message = NSLocalizedString("Something went wrong. Please try again later", comment: "")
                let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
                if type == Constants.english {
                    message = English.Something_went_wrong_Please_try_again_later
                } else if type == Constants.japanese {
                    message = Japanese.Something_went_wrong_Please_try_again_later
                }
                self.showAlert("", message: message)
                return
            }
            
            print(response)
            switch response.result {
                
            case .success:
                if let result = response.value {
                    let JSON = result as! NSDictionary
                    let status: String = JSON.object(forKey: "status") as! String
                    self.loginStatus = (status as String).decrypt()
                    
                    if self.loginStatus == "success" {
                        
                        if let forceupdate = JSON.object(forKey: "forceupdate") as? NSString {
//                           self.loginMessage = (forceupdate as String).decrypt()
                            print((forceupdate as String).decrypt())
                            let appUpdate = (forceupdate as String).decrypt()
                            if appUpdate == "0" {
                                self.processedFurther()
                            } else if appUpdate == "1" {
                                self.clearData()
                                self.showUpdateAlert()
                            }
//                            print(self.loginMessage)
                        }
                        
                        if let message = JSON.object(forKey: "message") as? NSString {
                            self.loginMessage = (message as String).decrypt()
                            print((message as String).decrypt())
                            print(self.loginMessage)
                        }
                        
                    } else if self.loginStatus == "failure" {
                        if let message = JSON.object(forKey: "message") as? NSString {
                            self.loginMessage = (message as String).decrypt()
                            print((message as String).decrypt())
                            print(self.loginMessage)
                        }
                    }
                }
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func clearData() {
        // Clear user
        self.defaults.set("false", forKey: "userLogin")
        self.defaults.setValue("", forKey: "LoginID")
        //  Clear client
        self.defaults.set("false", forKey: "clientLogin")
        self.defaults.set("", forKey: "BaseURL")
        self.defaults.set("", forKey: "clientId")
        self.defaults.set("", forKey: "password")
    }
    /**
     Simple Alert
     - Show alert with title and alert message and basic two actions
     */
    @objc func showUpdateAlert() {
        var title = English.Update_Required
        var message = English.There_is_a_new_update_of_InfoLocate_app_Please_update_from_Apple_app_store
        var exitButton = English.EXIT
        var updateButton = English.UPDATE
        
        let type = Defaults.getValueForKey(key: Constants.language_Key) as! String
        if type == Constants.english {
            title = English.Update_Required
            message = English.There_is_a_new_update_of_InfoLocate_app_Please_update_from_Apple_app_store
            exitButton = English.EXIT
            updateButton = English.UPDATE
        } else if type == Constants.japanese {
            title = Japanese.Update_Required
            message = Japanese.There_is_a_new_update_of_InfoLocate_app_Please_update_from_Apple_app_store
            exitButton = Japanese.EXIT
            updateButton = Japanese.UPDATE
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: exitButton, style: UIAlertAction.Style.default, handler: { _ in
            // Cancel Action
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    exit(0)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: updateButton,
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        // update action
                                        //let appID = Bundle.main.infoDictionary?["CFBundleIdentifier"] as! String
                                        let appurl = ServerConfiguration.APP_UPDATE_URL_LINK
                                        let url: URL = URL(string: appurl as String)!
                                        if #available(iOS 10.0, *) {
                                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                        } else {
                                            UIApplication.shared.openURL(url)
                                        }
                                        self.showUpdateAlert()
                                       // self.gameTimer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.showUpdateAlert), userInfo: nil, repeats: true)
        }))
        self.present(alert, animated: false, completion: nil)
    }
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = ""
        
        var authError: NSError?
        let reasonString = "To access the secure data"
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                
                if success {
                    //TODO: User authenticated successfully, take appropriate action
//                    self.output.navigateToHomeScreen()
                } else {
                    //TODO: User did not authenticate successfully, look at error and take appropriate action
                    guard evaluateError != nil else {
                        return
                    }
//                    print(self.output.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                    
                    //TODO: If you have choosen the 'Fallback authentication mechanism selected' (LAError.userFallback). Handle gracefully
                }
            }
        } else {
            
            guard authError != nil else {
                return
            }
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
//            print(self.output.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
        }
    }
    
    func evaluateAuthenticationPolicyMessageForLA(message: String) {
//        print(message)
        if message == "The user did cancel" {
            self.output.navigateToLoginScreen()
        } else {
//            Alert.showAlertWith(message: message, viewController: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func back(_ sender: Any) {
        self.output.back()
    }
    
   
}
