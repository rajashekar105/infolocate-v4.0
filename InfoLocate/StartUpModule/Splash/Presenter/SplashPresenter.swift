import Foundation

class SplashPresenter: SplashModuleInput, SplashViewOutput, SplashInteractorOutput {
    
    weak var view: SplashViewInput!
    var interactor: SplashInteractorInput!
    var router: SplashRouterInput!
    
    // MARK: - SplashViewOutput
    
    func back() {
        router.back()
    }
    
    func navigateToLoginScreen() {
        router.navigateToLoginScreen()
    }
    
    func navigateToDashboardScreen() {
        router.navigateToDashboardScreen()
    }
    
    func navigateToAlertScreen() {
        router.navigateToAlertScreen()
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int){
        interactor.evaluateAuthenticationPolicyMessageForLA(errorCode: errorCode)
    }
    
    func evaluateAuthenticationPolicyMessageForLA(message: String) {
        view.evaluateAuthenticationPolicyMessageForLA(message: message)
    }
    
    // MARK: - SplashModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - SplashInteractorOutput
    
}
