import Foundation

protocol SplashRouterInput {
    func back()
    func navigateToDashboardScreen()
    func navigateToLoginScreen()
    func navigateToAlertScreen()
}
