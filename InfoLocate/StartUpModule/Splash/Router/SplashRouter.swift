import UIKit

class SplashRouter:SplashRouterInput {
    
    var rootRouter: RootRouter!
    weak var viewController: SplashViewController!
    
    func back() {
        Navigation.back(viewController: viewController)
    }
    
    func presentSplashFromWindow(window:UIWindow) {
        let builder = SplashModuleBuilder()
        let splashViewController = builder.build()
        rootRouter.showRootViewController(vc: splashViewController, window: window)
    }
    
    func navigateToLoginScreen() {
        Navigation.navigateToClientLoginScreen(viewController: viewController)
    }
    
    func navigateToDashboardScreen() {
        Navigation.navigateToDashboardScreen(viewController: viewController)
    }
    
//    func navigateToDashboardScreen() {
//        Navigation.navigateToHistoryDetailsScreen(vehicleNo: "", viewController: viewController)
//    }
    
    func navigateToAlertScreen() {
        Navigation.navigateToRecentAlertsScreen(viewController: viewController)
    }
    
    func presentDashboardFromWindow(window:UIWindow) {
        let builder = DashboardModuleBuilder()
        let controller = builder.build()
        rootRouter.showRootViewController(vc: controller, window: window)
    }
        
    
}
