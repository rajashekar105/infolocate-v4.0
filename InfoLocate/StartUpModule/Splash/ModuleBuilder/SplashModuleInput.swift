import Foundation

protocol SplashModuleInput: class {
    func configureModule()
}
