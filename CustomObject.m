//
//  CustomObject.m
//  InfoLocate
//
//  Created by Infotrack on 21/04/20.
//  Copyright © 2020 Hemalatha T. All rights reserved.
//

#import "CustomObject.h"

@implementation CustomObject

- (NSString *) someMethod {
    self.someProperty = @"printed";
//    NSLog(@"%@", self.someProperty);
    return self.someProperty;
    
}

@end
