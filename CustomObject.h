//
//  CustomObject.h
//  InfoLocate
//
//  Created by Infotrack on 21/04/20.
//  Copyright © 2020 Hemalatha T. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomObject : NSObject

@property (strong, nonatomic) id someProperty;

- (NSString *) someMethod;

@end
